module.exports = {
  base: process.env.VUEPRESS_BASE || "",
  dest: "public",
  title: "Notes",
  description: "My class notes from JCU.",
  head: [
    ["link", { rel: "icon", sizes: "16x16", href: `/favicon-16x16.png` }],
    ["link", { rel: "icon", sizes: "32x32", href: `/favicon-32x32.png` }],
    [
      "link",
      {
        rel: "apple-touch-icon",
        sizes: "76x76",
        href: `/apple-touch-icon.png`,
      },
    ],
    ["link", { rel: "manifest", href: `/site.webmanifest` }],
    [
      "link",
      { rel: "mask-icon", href: `/safari-pinned-tab.svg`, color: "#5bbad5" },
    ],
    ["meta", { name: "msapplication-TileColor", content: "#00a300" }],
    ["meta", { name: "theme-color", content: "#ffffff" }],
    ["script", { src: "/js/sortable.min.js" }],
  ],
  themeConfig: {
    logo: "/android-chrome-72x72.png",
    sidebar: "auto",
    lastUpdated: "Last Updated",
    docsDir: "notes",
    repo: "https://gitlab.com/hajnyon-jcu/notes",
    repoLabel: "GitLab",
    editLinks: true,
    editLinkText: "Make contribution.",
  },
  plugins: [
    [
      "@vuepress/last-updated",
      {
        transformer: (timestamp, lang) => {
          let date = new Date(timestamp);
          return `${date.getDate()}-${date.getMonth() +
            1}-${date.getFullYear()}`;
        },
      },
    ],
    [
      "vuepress-plugin-reading-time",
      {
        excludes: [],
      },
    ],
  ],
  markdown: {
    linkify: true,
  },
};
