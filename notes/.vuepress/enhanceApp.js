// async function is also supported, too
export default () => {
    try {
        // add sortable to tables on page https://github.com/HubSpot/sortable
        document.addEventListener('DOMContentLoaded', (event) => {
            const tables = document.getElementsByTagName('table');
            for (const table of tables) {
                table.setAttribute('data-sortable', '');
            }
        });
    } catch (error) {
        console.error('Error occurred while initializing sortable.', error);
    }
};
