# Lesson 6 tasks

## Task 1 - Questions

[Text - UK Rains Broke River Flow Record and Climate Change is to Blame](https://www.prf.jcu.cz/data/files/20/70/853/50205.uk-rains-broke-river-flow.pdf)

**What statistics does the article use to demonstrate the dangers of climate change? Which is the scariest? Do you believe all of them? Why/why not?**

Records in rainfall and temperature peaks causing floods and higher temperatures int winter. Probably the floods is the scariest as it could hit large areas. However the temperature is also not to miss as icebergs are slowly melting which is pretty alarming.

**What local factors helped to cause the floods?**

Previous heavy rains which caused landslides that blocked rivers.

**What climatic factors helped cause the floods?**

Higher temperatures increasing the chances of heavy rains by over 50%

**Were these floods caused by some extent by the El Nino phenomenon?**

Scientists says that El Nino mark on December floods is weak.

## Task 2 - Words Description

Words to use: Exceed, Discharge, Peak, Damage, Reduce, Cause, Human-induced, Increase, Exceptionally, Warn, Connection, Key factor, Release, As a result

### Landslide

Landslide usually happens when limit of humidity that land can hold is **exceeded** and part of it falls off. To **increase** the reinforcement of the land forestation can be used as well as special construction.

### Tsunami

Tsunami is often created **as a result** of earthquake. It is essentially a large wave in the ocean which hits coast areas **causing** huge **damage** as sometimes is difficult to **warn** inhabitants in time.

### Tornado

Tornado is a wind whirl that originates from storm. It is most common in the USA. It can cause a lot of **damage** and in its **peak** it can travel at the speed of hundreds of km/h.

## Task 3

Read about the following four methods of flood defenses and take notes on the following questions:

[Article](https://www.bbc.com/news/uk-25929644)

### What are the advantages and disadvantages of each?

1. Flood barriers
    - demountable :heavy_check_mark:
    - lightweight :heavy_check_mark:
    - relatively inexpensive :heavy_check_mark:
2. Natural flood management
    - sustainable :heavy_check_mark:
    - reducing power of main flow :heavy_check_mark:
3. Sustainable drainage
4. Dredging rivers
    - not clear cut :heavy_multiplication_x:
    - expensive :heavy_multiplication_x:
    - can weaken near constructions :heavy_multiplication_x:

### Which do you think are most effective and why?

In my opinion the combination of flood barriers, natural management and sustainable drainage seems to be most effective. If I had to pick just one I would probably took flood barriers as it can be built quickly, is relatively inexpensive and can target specific areas.

## Task 4 essay

### Assignment

Word count 500+

To be submitted on Moodle.

Deadline: 13th April

**Instructions**

Choose an animal which is new or interesting for you. (You can refer back to your notes from the beginning of the semester for this).

Write a description of the animal, using some of the following prompts:

-   Appearance
-   Behaviour
-   Habitat
-   Relationships to other animals
-   What makes it exceptional and why it evolved this way

Please try to use some of the vocabulary from the links below. It will extend your vocabulary and raise the linguistic level of your piece.

https://www.macmillandictionary.com/thesaurus-category/british/words-used-to-describe-animals

https://www.macmillandictionary.com/thesaurus-category/british/animal-behaviour

### Essay

[The Fossa](essay-6.md)
