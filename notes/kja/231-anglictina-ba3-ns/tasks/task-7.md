# Lesson 7 tasks

## Task 1 - Questions

[Text - Hell’s Grannies: Driving in Old Age](https://www.prf.jcu.cz/data/files/20/70/853/50216.hells-grannies-driving-in-old-age.pdf)

**Why does “mobility matter” to old people?**

It helps them get the groceries, go to the doctor and with other necessary tasks.

**What are the problems with normal license-renewal tests?**

It is too simple and doesn't consider a lot of factors.

**What is DriverLab and how does it work?**

It is essentially car simulator which can pretend different situation in which driver's reactions are tested.

**What special features does it include?**

A lamp as a sun, water dispenser as rain, LEDs as traffic lights

**What is the most common problem of standard driving simulators? How will DriverLab address this problem?**

Standard simulators often cause nausea. They hope they tackle this problem by moving the car as the scenery is moving around.

**What other uses may DriverLab have in the future?**

It could target new drivers as well, helping them learn how to drive in unusual situations, on motorways etc.

## Task

**Design another invention to improve the life of older people**
Think about:
-the most common problems of the elderly and what they need
-What skills you have
-What you would like to exist for you when you are old
Try and use these words (or any others) from the article: Features, Ensure, Safety, Purpose, In practice, Widespread, Worth

In my opinion the biggest problem of seniors is their inactivity. Elderly don't have strict plan for each day such as daily jobs, school or many hobbies at least in my experience. **In practice** that means they spend whole day in front of TV, thinking about unnecessary stuff such as repeating task too often (lawn moving, painting walls) or worse: start to think they have all illnesses and diseases in the world aka hypochondriacs. So my invention should tackle this unproductive and almost wasted part of seniors life. My plan **features** service that provides specifically tailored tasks or part time jobs that seniors can do to profit other organizations and clubs. For example a local soccer club is looking for a manager to move lawn, watering and small maintenance fixes. It is part time job and the club doesn't have much money to pay for a worker. So they input their offer to the system. For a senior who for example played soccer when younger, or has grandson who plays there, it could be interesting part time job or hobby even for smaller amount of money. Elderly could find new purpose in life and also help someone else. The service would **ensure** that all offers are suitable for older persons and also take care of all the paper work. It would take small fee from each completed pairing of offer with an elderly.

## Task - Listening

**What are the advantages and disadvantages of growing old?**

Dis: slower, lower energy
Adv: experience, knowledge

**What momentous events did Vivian experience?**

first plane, cure to polio, second world war

**Does age bring wisdom? What do you think?**

As stated, I think you don't become wiser but more knowledgeable.

## Task - vocabulary

Try to guess the meanings of the following common collocations and idioms relating to age.

### Common colations

-   Disaffected youth – young people who do not accept society’s values
-   Juvenile delinquent – a criminal who is still legally a minor.
-   Midlife crisis – period of dissatisfaction in the middle of one’s life.
-   Going through a phase – going through a period of difficult behaviour.
-   Feel their age – when someone feels as old as they are (e.g. when you go on a long, challenging hike in your seventies you may suddenly realize you are not as young as you used to be)

### Idioms

-   A ripe old age – A very old age. “She survived the war and lived to a ripe old age”.
-   Tender age of – The young age of … “I left home at the tender age of sixteen”
-   “Act your age!”– Command to behave more maturely (when someone is acting like a child).
-   Over the hill – Another way of saying that someone is old
-   15, going on 50 - Somebody young who acts much older than they are (you can change the numbers to any that suit, e.g. “He is 19, going on 90”

### Discussion

Young people and teenagers often go through many phases. Can you think of any you went through when you were growing up?

What are the signs of a midlife crisis?

When do you feel over the hill?

What makes you feel your age?

Is there a problem with juvenile delinquents or disaffected youth in the CR?

Do you know anybody who is \_**\_ going on \_\_\_**?
