# Lesson 10 tasks

## Task 2 - Listen to One Minute English: Does your age affect your political views?

Answer the following questions. Feel free to avoid any which are too personal to discuss with somebody else:

**What is your answer to this question before listening? How would you describe your own politics?**

I guess the age affects political views a lot. Younger people tend to not be interested so much in the politics and don't have enough knowledge to work with. But with people getting older their opinions and views are getting brighter and they start to favour politic view that is close to them. Also parents have big impact on the teenagers. At least this was my case.

**Do we get less idealistic as we get older? Why? Is this your experience of people you know? -Is it a problem?**

I believe thats because when you are older you always remember previous events

**Do you vote pragmatically?**

It is pretty hard to judge this based on politic situation in our country. I would like to say I vote pragmatically but I have to wait few more years to evaluate my previous choices.

**How do you feel about voting? Is it a joy/duty/boring chore?**

I feel it as choosing the smaller evil these days opposed to choosing the better candidate. But so far I attended all voting I could and I would say it is a duty for me as a citizen.

**Do you support politicians who are for or against the status quo?**

I am more for politicians who are against status quo.
