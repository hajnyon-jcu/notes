# Lesson 5 tasks

## Task 1

[Text - In praise of human guinea pigs](https://www.prf.jcu.cz/data/files/20/70/853/50194.in-praise-of-human-guinea-pigs.pdf)

### Questions

**Why was medical treatment more dangerous in the past?**

Treatments often lead to death.

**What is an RCT and why is it “brilliant?”**

Randomized Control Trial. Trying new treatment (or other things) on a smaller group first while the rest is getting the same as before.

**How can RCTs be used in the fight against poverty?**

Evidence to donors and philanthropists that they money are well spent.

**What are the practical and ethical complications of RCTs?**

Some cannot be targeted to group, eg: change in interests rates.
Some cannot be executed as they can be harmful, eg: how unhealthy smoking is.

**How are RCTs used in the UK and USA?**

UK - education related tests.
USA - housing programme and also education.

**Why do politicians dislike RCTs? What about doctors?**

The evaluation often takes years.
Doctors started to use them as some treatments were proved harmful.

**What is your opinion on RCTs? Have you ever heard of them or taken part in them?**

RTCs are clever way of testing different approaches but as stated, it can take long time to see the result. I've heard of them many times especially in software development, where newly added features are often tested on a small group of users before shipping it to all. It is called _feature flags_ there and there are a lot of providers of such a service. I probably also took part of them and I guess I don't know about most of them.

### Opposite words:

Safe - dangerous

Often - rarely

Unsure - convinced

Allowing - resisting

Defended - questioned

Useful - useless

Due to - despite

Unrecognized - established

Seldom - often

Infrequently - often

### Detailed language work

**1) Give an example of an idea which is “simple, yet brilliant” (line 6)**

From the basic ones I would give you a pulley. But from the more complex I would suggest _SlowLoris_ which in computer security is a denial of service attack. And it is simple yet brilliant.

**2) Line 9. Write a sentence which begins “Despite my best efforts,…..”**
_(e.g. “Despite my best efforts, I was unable to learn enough Spanish to communicate effectively in the three months I lived in Spain”)_

Despite my best efforts, I haven't been able to got up on the first alarm.

**3) Write a sentence beginning “In the past decade…..” (line 12)**

In the past decade the popularity of podcasts grew significantly.

**4) Give an example of reckless behaviour (line 27)**

littering

**5) Line 35. Complete the following sentence for yourself. “I see little point in…..(ing)”**
_(e.g. “I see little point in studying science when the work of the best scientists of my generation is being ignored” - to loosely paraphrase Greta Thunberg)_

I see little point in sharing your life on social medias.

### TED

Find some TED Talks which interest you at TED: Ideas worth spreading and use the interactive transcript option to read along to your chosen talk as you listen. Find out how to use interactive transcripts here. After you’ve listened once, go through and repeat different sections, trying to mimic exactly how the speaker said it. This is a popular and useful method of developing your language fluency and pronunciation. Please keep a very short written log of this experience under the headings below. Keep it safe, we will look at it when back in class.

**Name of your TED talk**

[The Infinite Hotel Paradox](https://www.ted.com/talks/jeff_dekofsky_the_infinite_hotel_paradox?language=en#t-346178)

**What was your experience of this exercise? (was it useful/fun/hard/challenging….)**

Useful for talks with more difficult to understand speaker (accent, speech speed)

**Are there any words or phrases you will try to use or pronounce differently in the future?**

Hotel, night manager.
