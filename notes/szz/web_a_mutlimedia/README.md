# Web a multimedia

## Znackovaci jazyky

### 1. Značkovací jazyky – historie, HTML (XHTML), validita, nadstavby nad HTML, standardy, nástroje pro tvorbu HTML.

-   slouzi pro popis dat
    -   binarni - rychle zpracovatelne, ale necitelne
    -   textove
        -   zapis a cteni vyzaduje konverzi
        -   citelne a snadno editovatelne
        -   idealni pro prenositelnost mezi programy
    -   HTML, XML, tradicnimi predstaviteli nastroje pro formatovani textu nroff, troff, TeX
-   historie
    -   znacky pouzivany uz v dobach knihtisku (znacka pro zmenu fontu, vynechani mista na obrazek atd)
    -   XML (Extensible Markup Language)
        -   IBM 80. leta
        -   puvodne GML (Generalized Markup Language)
        -   vznikl na zaklade potreby ukladat pravni spisy - problem nekompatibility v programech - potrebovali sjednotit format
        -   postupny vyvoj do SGML (Standard Generalized Markup Language)
            -   obecny, umoznoval definici vlastni ch znackovacich jazyku pomoci DTD
            -   prilis komplexni na implementaci - pouzivala se jen podmnozina -> vznik XML
            -   nejznamejsi aplikaci je HTML
        -   narozdil od SGML je predem dano mnoho parametru
        -   pocita se sirsim pouzitim (neni omezeno na anglictinu) - podpora ruzneho kodovani
        -   vyuziti: RSS feedy, konfiguracni soubory, prenos dat (import, export, REST API), open office
        -   vlastnosti
            -   kodovani - preferovani UTF8
            -   format je informacne bohatsi (i tagy nesou informaci `<user id="22">Ondra</user>`)
            -   snadna konverze do jinych formatu
            -   snadna implementace v jazycich
            -   validace a kontrola struktury dokumentu (provadi parser)
            -   podpora namespacu - v jednom dokumentu lze pouzit nekolik druhu znackovani
            -   odkazy - vize moznosti nez v HTML, standardy: XLink, XPath, XPointer
        -   vyhody: validace pred samotnym zpracovanim (proti JSONu, ten musim nejdriv parsnout)
        -   nevyhody: velikost (ale zalezi)
-   HTML (XHTML) (HyperText Markup Language)
    -   prvni definice v r. 1991 jako soucast projektu WWW - pro sdileni vysledku vyzkumu po celem svete
    -   pozadavky uzivatelu HTML vzrustaly - obohacovani HTML (formulare atd)
    -   1995 vznikl navrh HTML 3.0 - obsahoval napr matematicke vzorce, lepsi obtekani obrazku, styly
        -   na poc 1996 bylo jasne ze prohlizece nedokazi tolik naimplementovat a W3C rozhodlo, ze udelaji jen neco
    -   1997 HTML 4.0 - frames, tabulky, na dlouhou dobu beze zmeny
    -   **XHTML**
        -   puvodne mel byt nastupcem HTML
        -   odvozen od XML (ne od SGML), melo byt striktne standardizovane
        -   zakazany ruzne atributy, ktere se ale pouzivaly
        -   lower case, striktne rozdelena struktura dat
        -   IE neumel zpracovat, nepouzivalo se
    -   2007 W3C a WHATW se spojili a zacalo vznikat HTML 5
        -   pridava nove veci: semanticke tagy, podpora novych technologii (audio, video, canvas), odstranuje nepouzivane prvky
        -   podporuje syntaxi XML i SGML
-   validita
    -   DTD - Document Type Definition
        -   popisuje strukturu XML
        -   vyhody
            -   automaticka kontrola XML proti DTD
            -   pomoci parseru lze zjistit zda je v dokumentu chyba
            -   muzeme vyuzivat vice/cizi DTD
        -   nevyhody
            -   slaba typova kontrola
            -   nestandardni syntaxe
    -   schema XML
        -   cilem bylo nahradit DTD a rozsirit moznosti
            -   lepsi moznost definice datovyuch typu
            -   syntaxe schemat zalozena na XML
        -   obsahuje zakladni datove typy, odvozene (muzu definovat)
        -   koncovka `xsd`
        -   muzu pouzivat namespace
        -   ruzne pristupy k navrh schematu
-   nadstavby HTML
    -   CSS
    -   JS
    -   DOM - Document Object Model
        -   reprezentace HTML/XML dokumentu pomoci stromove struktury
    -   souhrne oznacovano jako DHTML - Dynamicke HTML
-   standardy
    -   XML
        -   linky: XPath, XPointer, XLink
        -   Atom - nastupce RSS
        -   DTD, EPUB, GraphML, SVG, RSS
    -   HTML
        -   standardy pro pristupnost (W3C) - zpracovani HTML cteckou, titulky a textove popisy videa
        -   mobilni pristupnost
        -   HTML 5 a dalsi verze
-   nastroje pro tvorbu HTML
    -   v podstate jakykoliv program pracujici s textem
    -   textove editory
        -   prinaseji: syntax highlighting, doplnovani, validaci
        -   Notepad++, PSPad, VSCode, Sublime, Atom
    -   WYSIWYG editory
        -   pracuje se s grafickou reprezentaci stranky (uzivatel nemusi znat HTML)
        -   Adobe Dreamweaver, MS Expression Web (novy FrontPage)

### 2. Styly pro WWW – principy a možnosti užití CSS, aktuální verze a její vlastnosti, úroveň podpory v prohlížečích, alternativy a nadstavby (SASS, LESS), nástroje pro tvorbu CSS.

-   principy a moznosti uziti
    -   oddeluje vzhled od obsahu, prehlednejsi a vetsi moznosti formatovani, snazsi sprava, rychlejsi nacitani
    -   obsahuje styly pro pismo, zarovnani, obarvovani, moznost spicifikace zarizeni (tisk, mobil atd), animace
    -   je to kaskada stylu ktera se pouziva
        1. defaultni nastaveni prohlizece
        2. externi definice `style.css`
        3. definice stylu `<head>`
        4. inline styly
    -   CSS specifity
    -   vyhody: oddeleni stylu od dat, cachovani, lze dynamicky menit pomoci CSS
    -   nevyhody: podpora prohlizecu, defaultni nastaveni stylu prohlizece (CSS reset)
-   verze
    -   CSS1 - malo veci, mala podpora prohlizecu
    -   CSS2 - nove veci (min/max width hight, cursor, position atd), ale prohlizece implementovali po svem
    -   CSS3 - rezdelene na moduly, vetsi podpora prohlizecu
        -   pridany nove vlastnosti:
            -   `border-radius`, `box-shadow`, `transform` (posun, rotace, skalovani)
            -   barevne modely RGBA, HSL, HSLA, pruhlednost, gradienty
            -   selektory
            -   pseudotridy
-   uroven podpory v prohlizecich
    -   vetsina podporovana, nektere veci nejsou doimplementovany (safari on iOS, IE 11)
    -   podpora mezi prohlizeci neni konzistentni
    -   nektere vlastnosti jsou treba specifikovat ruzne pro ruzne prohlizece
-   alternativy a nadstavby
    -   SASS - Syntactically Awesome Style Sheets
        -   preprocesor
        -   syntaxe podobna Haml - bloky pomoci odsazeni, pravidla oddelena novou radkou
        -   novejsi synaxe - SCSS - Sassy CSS
            -   blokove formatovani (jako CSS)
        -   rozsiruje CSS o
            -   promenne, nesting, loops, arguments, dedicnost
    -   LESS - Leaner StyleSheets
        -   dynamicky preprocesor stylu
        -   ovlivnen SASSem
        -   obdobna syntaxe jako CSS (validni CSS je validni LESS)
        -   rozsiruje: promenne, nesting, mixins, operators, functions
    -   LESS i SASS muzeme preprocesovat na serveru nebo az na clientovi
    -   Stylus
        -   navic interpolace. operatory
    -   PostCSS
        -   sklada se z pluginu, ktere postupne zpracovavaji kod
        -   muze zahrnovat i lint, optiomalizaci svg a dalsi
-   nastroje
    -   testovani na vice prohlizecich (BrowserStack)
    -   W3C - oficialni specifikace
    -   caniuse.com a MDN - podpora vlastnosti v prohlizecich
    -   nastroje, ktere dokazi podle vyspecifikovanych verzi prohlizecu upozornit na nepodporovane vlastnosti pouzite v CSS (nebo polyfills)
    -   knihovny a frameworky (bootstrap, foundation, css reset)
    -   metodologie BEM, OOCSS (Object Oriented CSS), ACSS (Atomic CSS)

### 3. eXtensible Markup Language – XML. Struktura XML dokumentů. Validita XML dokumentů (DTD, XSD). Dotazovací jazyk XPath.

-   slouzi pro popis dat
    -   binarni - rychle zpracovatelne, ale necitelne
    -   textove
        -   zapis a cteni vyzaduje konverzi
        -   citelne a snadno editovatelne
        -   idealni pro prenositelnost mezi programy
    -   HTML, XML, tradicnimi predstaviteli nastroje pro formatovani textu nroff, troff, TeX
-   historie
    -   znacky pouzivany uz v dobach knihtisku (znacka pro zmenu fontu, vynechani mista na obrazek atd)
    -   XML (Extensible Markup Language)
        -   IBM 80. leta
        -   puvodne GML (Generalized Markup Language)
        -   vznikl na zaklade potreby ukladat pravni spisy - problem nekompatibility v programech - potrebovali sjednotit format
        -   postupny vyvoj do SGML (Standard Generalized Markup Language)
            -   obecny, umoznoval definici vlastni ch znackovacich jazyku pomoci DTD
            -   prilis komplexni na implementaci - pouzivala se jen podmnozina -> vznik XML
            -   nejznamejsi aplikaci je HTML
        -   narozdil od SGML je predem dano mnoho parametru
        -   pocita se sirsim pouzitim (neni omezeno na anglictinu) - podpora ruzneho kodovani
        -   vyuziti: RSS feedy, konfiguracni soubory, prenos dat (import, export, REST API), open office
        -   vlastnosti
            -   kodovani - preferovani UTF8
            -   format je informacne bohatsi (i tagy nesou informaci `<user id="22">Ondra</user>`)
            -   snadna konverze do jinych formatu
            -   snadna implementace v jazycich
            -   validace a kontrola struktury dokumentu (provadi parser)
            -   podpora namespacu - v jednom dokumentu lze pouzit nekolik druhu znackovani
            -   odkazy - vize moznosti nez v HTML, standardy: XLink, XPath, XPointer
        -   vyhody: validace pred samotnym zpracovanim (proti JSONu, ten musim nejdriv parsnout)
        -   nevyhody: velikost (ale zalezi)
-   struktura
    -   tag
        -   ohranicen `<>` a koncicim tagem `</>`
    -   element - logicka komponenta dokumentu
    -   atribut - zpresnuje vyznam (enum, styl)
    -   dokument zacina XML deklaraci `<?xml version="1.0" encoding="utf-8" ?>`
    -   dokument musi mit 1 root element
    -   Characted DATA `<![CDATA[data ktera nejsou interpretovany jako XML]]>`
    -   specialni znaky escapovany `&` -> `&amp;`
    -   podpora komentaru
    -   instrukce - pouziti pro pripojeni stylu, prikazy pro preprocesory atd `<?xml-stylesheet href="styl.css" type="text/css"?>`
-   validita
    -   well-formed
        -   pokud je k dispozici pouze XML dokument
        -   kontrola, zdali je XML dokument správně strukturovaný (např. že se nekříží značky)
    -   DTD - Document Type Definition
        -   popisuje strukturu XML
        -   vyhody
            -   automaticka kontrola XML proti DTD
            -   pomoci parseru lze zjistit zda je v dokumentu chyba
            -   muzeme vyuzivat vice/cizi DTD
        -   nevyhody
            -   slaba typova kontrola
            -   nestandardni syntaxe
    -   schema XML - XSD (XML Schema Definition)
        -   cilem bylo nahradit DTD a rozsirit moznosti
            -   lepsi moznost definice datovyuch typu
            -   syntaxe schemat zalozena na XML
        -   obsahuje zakladni datove typy, odvozene (muzu definovat)
        -   koncovka `xsd`
        -   muzu pouzivat namespace
            -   odliseni ruznych schemat (pouzivani stejneho jmena tagu)
            -   rozsireni o preddefinovane typy (sequence, union, list, ...)
        -   ruzne pristupy k navrh schematu
            -   matrjoska - elementy zanoruju do sebe
            -   salamova kolecka - vsechny elementy jsou globalni a pak skladam pomoci odkazu
            -   slepy benatcan - pro vsechny elementy nadefinuji typy, pak zase skladam (pouziva se pri striktni definici typu)
    -   dalsi: Relax NG, Schematron
-   XPath
    -   dotazovani v ramci dokumentu pomoci cest ve stromove reprezentaci dokumentu
    -   vyber elementu
        -   pomoci jmena `a/b`
        -   vsechny potomky `a/*`
        -   aktualni element `.`
    -   vyber atributu pomoci `@attr` nebo `@*` pro vsechny atributy elementu
    -   filtrovani pomoci indexu `a[1]` nebo funkci `a[last()]` nebo atributut `a[@attr]`
    -   dalsi
        -   `if` parameterem je XPath a kdyz je true pouzije se obsah elementu
        -   `choose`, `when`, `otherwise`, `switch` + `case` + `default`
        -   `message` - console log
        -   atribut terminate - try/catch

### 4. Transformace XML dokumentů pomocí jazyka XSLT. Styly v XML (CSS, XSL).

-   XSL
    -   eXstensible Stylesheet Language
    -   umoznuje definovat vzhled elementu
    -   oddeluje styl od dat
    -   aut. gen. obsahu, cislovani, kapitol atd
-   XSL Transformations - umoznuje z XML vytvaret ruzne dalsi formaty na zaklade formatovacich formatu (XSL FO)
    -   HTML, PDF atd
-   XML + XSLT -> XSLT procesor -> XML/HTML/text
    -   ![](../../uai/687-znackovaci_jazyky/xslt.png)
-   XML + XSLT -> XSLT procesor -> formatovaci objekty (FO - take XML) -> PDF/PS/RTF
    -   vytvorime si styl, ten se pomoci XSLT aplikuje na XML - dostaneme nove XML, ktere se sklada z form. objektu - tento dokument pak umi interpretovat procesor FO, ktery z nej vytvori treba PDF
    -   ![](../../uai/687-znackovaci_jazyky/xslt-fo.png)
-   XSLT procesor
    -   mohou byt soucasti prohlizece
    -   saxon (java), xalan, xt, msxml (MS)
    -   libxslt/xsltproc - C
-   XLS
    -   muzeleme linknout v xml nebo prohnat nejakym programem (viz vys)
        -   `<?xml-stylesheet href="mujstyl.xsl" type="text/xsl"?>`
    -   `for-each`
        -   `sort` - select (podle ktere hodnoty), order, case-order, ..
    -   `value-of` - vrati hodnotu nejakeho elementu
    -   `apply-templates` - muzeme skladat vice
    -   pouziti pomoci xsltproc: `xsltproc faktury.xsl faktury.xml > faktury.html`
-   XPath
    -   cesty v ramci dokumentu
    -   dokument reprezentovan jako strom
    -   vyber elementu
        -   pomoci jmena
        -   `*` dcerine
        -   `.` aktualni element
        -   tyto zapisy lze kombinovat pomoci `/` a `//`
    -   vyber atributu pomoci `@atribut`
        -   `@*` - vsechny atributy prvku
    -   filtrovani uzlu
        -   `a[1]` `a[6]` index, `a[last()]` `a[@atribut]`
    -   dalsi
        -   `if` parameterem je XPath a kdyz je true pouzije se obsah elementu
        -   `choose`, `when`, `otherwise`, `switch` + `case` + `default`
        -   `message` - console log
        -   atribut terminate - try/catch

## Architektura aplikaci

### 5. Monolitická architektura (N-vrstvá), vrstvy a jejich funkce (prezentační, aplikační, business, datová). Clean (Onion) architektura

-   zakladni vrstvy
    -   datova
    -   business
    -   aplikacni
    -   prezentacni
    -   **vstupne vystupni operace** - sluzby ovladani souboru (I/O operace)
    -   **datove sluzby** - akce s db (definice, manipulace, transakcni zpracovani)
    -   **logika dat** - **zde konci sluzby db serveru**
        -   podpora operaci s daty (integritni omezeni)
        -   nekdy oznacovano jako business logika
        -   struktura dat (tabulky), dodrzovani klicu a jinych integritnich omezeni, spravnost dat
    -   **logika aplikace**
        -   vsechno co nepatri do ostatnich vrstev :)
        -   vypocty, akce v ramci aplikace (pridavani, odesilani
        -   pres API, logovani)
        -   pr.: kdyz vlozim v IS skoly novou tridu -> logika aplikace rika, ze se ma zalozit i tridni kniha, priradi tridniho uzivatele atd..
    -   **prezentacni logika** - rizeni interakce (pr.: pri prechodu na jinou stranku uzivatele upozornim, ze nema ulozene zmeny, redirect po uspesnym ulozeni, error stranka pri 404 apod.)
    -   **prezentacni sluzby** - graficky/textovy/.. rozhrani pro uzivatele
-   monoliticka
    -   aplikace jako celek, resi vsechny logicky
-   3 vrstva
    -   controller - rozhrani s okolnim svetem (API atd)
        -   vidi business vrstvu
    -   business - operace specificke pro pouziti
        -   vidi perzistentni
    -   perzistentni
-   ports & adapters / clean / onion architecture
    -   rozdil oproti 3 vrstvy je ze to je zapouzdreny jak cibule
    -   uprostred je business vrstva - nema pristup nikam jinam
        -   ma vydefinovany porty kteryma komunikuje s okolnim svetem
        -   port na ukladani dat (ale nevi kam to uklada)
        -   pouzity pouze moje veci (zadny importy externich sluzeb)
    -   aplikacni vrstva - service, byva pouzity nejaky framework
    -   adapter - ostatni sluzby (databaze, web service, API)

### 6. Architektura klient/server, mikroslužby, virtualizace a kontejnery, messaging

-   klient / server
    -   aktualne asi nejpouzivanejsi aplikace (drive tlusty/tenky klient, dneska spis pouzity v pripadech jako PWA)
    -   klient dava pokyny serveru, ktery je provadi - REST API
-   microservices
    -   casti, deployuji se samostatne, kazda resi nejakou cast
    -   v idealnim pripade na sobe nezavisle (vetsinou se ale navzajem volaji - reseno messagingem)
    -   orchestrace a choregrafie
        -   zpusob jak spolu mikroservicy interaguji
        -   orchestrace - prijde request na sluzbu a ta az to rozposila na dalsi, ktery zpracovavaji jednotlive kroky a vaci to zpatky, sluzba ma u sebe nadefinovane kroky
        -   choreografie - vic volnejsi, sluzby volaji alsi sluzby a nikdo nevi o tom flow, ale zase to neni zavisly na jedny sluzbe
-   messaging
    -   obecne se pouziva pro loosely coupled propojeni sluzeb (volnejsi), rozkladani zateze, bufferovani zprav, mix
    -   jeden usecase je rozdeleni mikrosluzeb tak, aby na sobe nebyli primo zavisle - decoupling
    -   delivery mody - at most once, at least once, exactly once
    -   retence zprav
    -   typy
        -   klasicky - drzi zpravu dokud ji nekomu nedoruci, pak smaze
        -   streaming - perzistence zprav, opakovani zpracovani zprav, jsou definovany pravidla (vidim kudy a kam to tece systemem)
    -   broker
        -   na nej se pripojuji klienti - consumer, produceri (lisi se typem pripojeni, rozdelovanim na brokery)
        -   jeden/vice, High Availability, skalovani
        -   nektery request reply, nektery asynchronni, nektery deadletter queue (zpravy ktere se nedaji dorucit jdou do jednoho topicu)
        -   bufferuje zpravy - odstini mikrosluzby od vypadku (dokaze pockat nez sluzba nastartuje pri vypadku apod)
    -   muze zvysovat latenci v systemu
    -   Kafka, RabbitMQ, gRPC
    -   nevyhoda muze byt single point of failure (ale melo by se resit skalovanim)
    -   CQRS - Command Query Request Separation - jina cesta pro zapis a jina pro cteni (model pouzivany v messagingu)
-   virtualizace a kontejnery
    -   docker
        -   nastroj pro virtualizaci sluzeb
        -   docker hub - repozitar existujicich imagu
        -   cli nebo GUI nadstavby (portainer)
        -   **container** (kontejner)
            -   odlehceny virtualni stroj (vlastni OS, SW, zdroje - RAM, disk atd)
            -   moznost kompletniho oddeleni sluzeb bezicich na jednom stroji
            -   vlastne bali aplikace a pousti je na sdilenem jadre (muze balit i cele OS)
            -   rozdil oproti virtualu - ten ma cele jadro a OS u sebe - nic nesdili
            -   orchestracni nastroje: kubernetes
        -   **image** (obrazy)
            -   charakteruzuje sluzbu a veskery SW nutny pro beh kontejneru
            -   data ukladana mimo kontejner
        -   **volume** (svazky)
            -   zakladni kontext pro kontejner
            -   samostatny disk s vlastni adr. strukturou
            -   kompletne oddeleny od hostitelskeho pocitace
        -   **site**
            -   moznost vytvoreni vlastni site pro skupinu kontejneru
            -   default: samostatna sit pro kazdy kontejner
            -   mapovani portu na hostitelsky system
        -   **stack**
            -   komplet z vice kontejneru
            -   zalozeno na `docker-compose`
        -   **swarm**
            -   vicenasobne spusteni daneho kontejneru
            -   moznost skalovani dle zatizeni
            -   master and worker nodes
    -   kubernetes
        -   system pro spravu sitovych sluzeb na distribuovanem prostredi
        -   orchestracni nastroj (management kontejnerizovanych aplikaci - sluzeb)
        -   obsahuje nastroje pro nasazeni, spravu, skalovani
        -   zakladem jsou kontejnerova reseni (docker, rocket)
        -   vyviji google - opensource
        -   **pod**
            -   nejmensi nasaditelna jednotka
            -   vytvoreni, planovani, sprava
            -   logicka kolekce kontejneru umoznujici provozovat danou aplikaci (sluzbu)
        -   **replica set** - zajistuje beh podu na vice strojich soucasne (opetovne spusteni a vytvoreni podu pro padu stroje)
        -   **service** - logicky set podu s jednotnou pristupovou politikou zajistujici chod aplikace
        -   **volume** - slozka na disku nebo v jinem kontejneru (ukladani dat)
        -   **node** - fyzicky nebo virtualni stroj spravovany k8s
        -   **cluster** - mnozina fyzickych a virtualnich stroju a dalsich komponent, zajistujici beh aplikace

### 7. Vysvětlete architektonické vzory MVC, MVP (SC/PV), MVVM (binding). Uveďte příklady oblastí použití.

-   MVC - Model View Controller
    -   uzivatel vykona akci v GUI
    -   tu zachyti `Controller`
    -   `Controller` rozhodne, jak na akci zareagovat a typicky zmeni hodnoty v `Modelu` nebo primo ovlivni `View`
    -   `View` zobrazi zmeny uzivateli
    -   ruby on rails
    -   ![](./mvc.png)
-   MVP - Model View Presenter
    -   evoluce MVC
    -   zpracovani uzivatelsky vstupu se deje pomoci GUI
    -   **model** - data, business logika
    -   **view**
        -   zpracovava uzivatelsky vstup, neobsahuje zadnou aplikacni logiku
    -   **presenter**
        -   obsahuje aplikacni a prezentacni logiku, manipuluje s `modelem`, `view` se aktualizuje na zaklade udalost v `modelu` nebo primou aktualizaci `presenterem`
    -   rozdil od MVC
        -   uzivatelsky vstup i vystup plne kontroluje `View`
        -   primarni motivace pro oddeleni View a Presenteru neni nutnost osetreni vstupu, ale architektonicke duvody (presentacni vrstva se lepe udrzuje)
        -   `View` ma typicky primou vazbu na `Presenter`
        -   `Presenter` v mnoha pripadech pracuje primo s `View`, takze i tato vazba je silnejsi nev v pripad MVC
    -   Windows Forms, WPF, Java Swing, nektere android aplikace nebo JS FE
    -   ![](./mvp.png)
    -   **SC** - Supervising Controller
        -   slozitejsi prezentacni logika, manipuluje libovolne s `View`
        -   zobrazuje data z modelu (observer, data binding)
    -   **PV** - Passive View
        -   `View` je co nejprimitivnejsi, mizi vazba `View` a `Model`
        -   `Presenter` - aplikacni logika, synchronizace mezi `View` a `Model`
        -   primarni motivace je odizolovani view a presenteru pro lepsi pokryti testy
        -   ![](./pv.png)
-   MVVM - model view view-model
    -   chybi spojeni view a modelu
    -   viewmodel
        -   podobna funkce jako controller
        -   komunikuje s modelem
        -   prezentacni logika
        -   veskera data, ktery view bude potrebovat + prezentacni logika - zpracovani akci (click, input atd - commands)
    -   view - interakce s uzivatelem (pomerne hloupe)
    -   propojeni view <-> view-model
        -   1:1
        -   commands (typicky jen smerem do view-modelu)
        -   data binding - provazani promenny s input/view elementem
            -   muze byt vice smerny
        -   notifikac - typicky view-model nevi o view (jen pomoci bindingu), ale muzou se pouzit
    -   pouzit: desktopova/mobilni aplikace
    -   ![](./mvvm.png)

### 8. Vysvětlete princip a použití alespoň 1 návrhového vzoru z každé skupiny:

#### 1. Vzory týkající se tvorby objektů (např. Singleton, Lazy loading, Object Factory)

-   singleton
    -   jedina instance objektu v cele aplikaci
    -   bez DI muzou byt problemy (mockovani, hodne svazany s kodem)
    -   pr: cache, connector do db, thread pool
-   object factory
    -   na zaklade parametru vyrabi instance objektu
    -   vraci bud interface nebo abstraktni tridu
    -   pr: definice hry (postavy, mistnosti), json -> instance objektu

#### 2. Vzory týkající se struktury programu (např. Adapter, Interface, Facade)

-   adapter
    -   transformace dat do urciteho rozhrani
    -   konverze jedne struktury dat do jine (ruzne interface)
-   interface
-   facade
    -   vic slozitejsich rozhrani, chci s nima jednoduse pracovat - udelam facade a tim to zabalim
    -   teoreticky muze omezit usecase rozhranich pod, ale tezime

#### 3. Vzory týkající se chování (např. Observer, Dependency Injection, Publisher-Subscriber)

-   observer
    -   vzor, ktery definuje subcribovaci mechanismus tak, aby vice dalsich klientu mohlo dostavat notifikaci o zmene hodnoty
    -   vi presne, komu ma data posilat (oproti pub-sub)
    -   subscribnu se na nejakou zmenu a tu sleduju
-   dependency injection
-   publisher-subscriber
    -   publishers predavaji data, ktera pub-sub rozdistribuuje subscriberum
    -   publishers nevi o subscribers a naopak (loosely coupled)

## Webove technologie

### 9. Programování webových aplikací na straně klienta – JavaScript, základní rysy jazyka, možnosti užití, knihovny, bezpečnostní model JS, testování a ladění JS (nástroje, postupy).

-   JS
    -   puvodne odlehcena verze Javy - podobna syntaxe
    -   snaha o rozsireni statickych webovych stranek - proto v Netscapu vyvinuli scriptovaci jazyk pro dynamicke chovani stranek
    -   high-level, dynamicky typovany, objektove orientovany jazky
    -   v prohlizeci JavaScript pracuje s DOM (Document Object Model)
        -   pristup k metodam, vlastnostem a udalostem
        -   zavislost na implementaci v prohlizeci
    -   postupne se zacal standardizovat - ECMAScript
    -   JSON - JavaScript Object Notation
        -   standard od r 2009
        -   vymenny datovy format (jednodusi nex XML)
        -   objekty, pole, zakladni datove typy
        -   `JSON.parse()`, `JSON.stringify()`
-   vyuziti
    -   prohlizec - dynamicke webove stranky, SPA
    -   server - nodejs, deno
    -   desktop/mobil aplikace - electron, cordova, react native
    -   scripting
-   knihovny
    -   jQuery - knihovna funkci pro vyuziti v JS
        -   ucely
            -   seskupeni casto pouzivanych ukonu (selekce a manipulace s HTML elementy, css, DOM udalosti, animace, AJAX)
            -   odstraneni nekompatibility mezi prohlizeci
        -   dalsi rozsireni jako jQueryUI, jQueryMobile
        -   typicke callback funkcemi
-   bezbecnostni model
    -   potencionalni problem stazeni nebezpecneho kodu (at uz spatnou validaci XSS nebo pouzitim nebezpecne knihovny)
    -   autori prohlizecu se snazi riziko minimalizovat
        -   script bezi v sandboxu, kde muze vykonavat pouze povolene operace (nesmi vytvaret soubory, spouste jine programy apod)
        -   scripty maji pristup pouze k datum dane stranky (Same-Origin policy) - jinak CSRF
-   ladeni
    -   prohlizece - debug nastroje (konzole, DOM, network, breakpoints, performance, storage, lighthouse)
    -   editory - debug mod (pripojeni na prohlizec), breakpointy primo v kodu
    -   nastroje typu Sentry
-   testovani
    -   unit testing - jest, ava
    -   integration, e2e - cypress, puppeteer, playwright

### 10. Programování webových aplikací na straně serveru – technologie, Node.js (pozice, klady a zápory), integrace s dalšími službami (DB), možnosti užití.

-   technologie
    -   moznost vyuziti prostredku serveru (vykon, disk ...)
    -   vazba na dalsi sluzby - db, mail ..
    -   nastroje
        -   CGI - Common Gateway Interface
        -   ASP, PHP, JSP, Node.js
        -   Java
            -   drive applety
            -   objektovy pristup
            -   Servlets, Java Server Pages, Tomcat GlassFish
-   nodejs
    -   open source, cross platform runtime for JavaScript
    -   jednovlaknovy beh, asynchronni komunikacni model, udalostmi rizene programovani (callback funkce)
    -   zakladem interpret V8 z chromu a C++ nadstavba
    -   npm - balickovaci system
        -   package.json, package-lock.json
    -   standardni knihovny (fs, events, http, stream)
        -   streamy - zpracovavani sekvence datovych udalosti be ukladani do pameti (napr. cteni souboru)
    -   globalni objekty (`__dirname`, `__filename`, funkce jako `setTimeout`, `console`, `Date`, `Math`)
    -   promisy - reprezentace vysledku asynchronich operaci
    -   **pozice**
        -   vyuzivano pro webove servery, realtime web applications
        -   pouzivaji velci hraci na trhu
    -   **klady**
        -   miri na optimalizaci skalovatelnosti a propustnosti komunikace
        -   cross platgorm
        -   jeden jazyk pro frontend/backend
        -
    -   **zapory**
        -   nestabilni API
        -   nestandardni knihovny a velke mnozstvi knihoven
        -   narocne vypocetni ulohy
        -   asynchroni model
-   integrace s dalsimi sluzbami (db)
    -   knihovny pro zjednoduseni psani aplikaci (express, nest, koa, hapi...)
    -   integrace s db, mail, API pomoci knihoven
        -   typicky primo sluzba ma knihovny pro komunikaci (redis, mysql, mongo atd)
    -   integrace se realizuje ruznymi protokoly
        -   TCP, Socket, sdilena pamet
-   moznosti uziti
    -   server - nodejs, deno
    -   desktop/mobil aplikace - electron, cordova, react native
    -   scripting

### 11. Komunikace mezi síťovým klientem a serverem – sockets a jejich použití, protokol http a jeho verze, WebRTC, využití REST API. JSON/XML.

-   ucelem je zajistit vymenu dat mezi klientem a serverem
-   rada technickych variant zalozene nad TCP a UDP
-   sockets
    -   socket je softwarova struktura, ktera slouzi jako koncovy bod pro prijem a odesilani dat v siti na protokolech TCP a UDP
    -   identifikovan IP adresou a portem
-   http
    -   protokol na aplikacni vrstve, pouziva TCP, ale muze byt adaptovano i na UDP
    -   zdroje jsou definovany pomoci URL (Uniform Resource Locators) pouzivajici URI (Uniform Resource Identifiers) - HTTP, HTTPS
    -   metody
        -   GET - data primo soucasti URL, volani nema dalsi efekt, da se cachovat, data viditelna v url, omezeni delky
        -   POST - data soucasti requestu (body), volan s postrannimi efekty (modifikace dat na serveru), neni cachovan, neni omezen velikosti
        -   PUT - vytvoreni neb oupdate zdroje na serveru
        -   HEAD - obdoba GET bez navratove hodnoty
        -   PATCH - uprava zdroje
        -   OPTIONS - doplnkove informace ke komunikaci tykajici se vybraneho zdroje
    -   verze
        -   v1 - standardizace pouzivanych metod a technik, pro kazdy request je vytvorena samostatna connectiona, v1.1 connectiona je znovu pouzivana na vice dotazu (latence), pridava dalsi metody (OPTIONS, PUT ..)
        -   v2 - revize v1.1
            -   binarne kompresovane hlavicky
            -   pouziva se jedine TCP spojeni (vetsinou enkryptone) per server namisto 2-8
        -   v3 - nove vyvijena verze protokolu, pouziva QUIC + UDP
    -   priklad

```txt
GET /index.html HTTP/1.1
Host: www.example.com
User-Agent: Mozilla/5.0
Accept: text/html,application/xhtml+xml
Connection: keep-alive
```

-   WebRTC - Web Real-Time Communication
    -   online prenosy mezi 2 prohlizeci (media, data)
    -   peer-to-peer komunikace na bazi streamu (standardne pouzivana enkryptovana komunikace)
    -   ICE - Interactive Connectivity Establishment
    -   SDP - Session Description Protocol
    -   k navazani spojeni je treba server (napr nodejs) - pouze management spojeni
    -   STUN server - Session Traversal Utilities for NAT - udrzovani IP adres klientu
    -   TURN server - Traversal Using Relays around NAT - zprostredkovani komunikace klientu pokud ji nelze uskutecnit primo
    -   funkcionalita
        -   `getUserMedia()` - pristup ke kamere, mikrofonu
        -   `RTCPeerConnection` - rozhrani pro nastaveni video hovoru
        -   `RTCDataChannel` - poskytuje metody na nastaveni peer-to-peer cesty mezi prohlizeci
    -   muze pracovat pres socket.io
-   API - Aplication Programming Interface
    -   nastroje pro zajisteni komunikace mezi systemy
    -   OpenAPI - standard pro popis a dokumentaci API (JSON, YAML - swagger)
    -   nezavisle na platforme (loosely coupled) - pouze definice zpusobu komunikace
    -   REST - REpresentational State Transfer
        -   nekolik principu, aby se API mohlo oznacit jako RESTful
            -   oddeleni klienta od serveru - staci aby kazda strana dodrzela domluveny interface (API) a implementace se muze menit
            -   bezestavove - server nevi, v jakem stavu je klient, kazda zprava tedy musi byt pochopitelna, ikdyz klient nezna historii
        -   omezeny pocet metod (GET, PUT, DELETE, POST) - CRUD

### 12. Komunikace mezi webovým klientem a serverem – AJAX, Long Pooling, WebSockets.

-   AJAX - Asynchronous JavaScript and XML
    -   asynchronni i synchronni prenost dat mezi klientem a serverem
    -   postaven nad `XMLHttpRequest`
    -   spojeni vyvolavano vzdy uzivatelem
    -   volani
        -   vytvoreni objektu `XMLHttpRequest`
        -   nastaveni komunikace se serverem (POST nebo GET, hlavicky, async/sync)
        -   odeslani pozadavku na srver
        -   zpracovani odpovedi
    -   obaleni pomoci jQuery - zjednoduseni
    -   CORS - Cross Origin Request Sharing
        -   zda a jak jsou povoleny dotazy na jinou domenu
        -   preflight request - dotaz zda bude CORS pozadavek kladne vyrizen
-   Long Polling
    -   chytrejsi strategie nez short polling (periodicke dotazovani serveru)
    -   standardne pri pollingu se klient periodicky dotazuje serveru na data (eg kazdych 10s)
    -   to znamena, ze nova data dostane ale az za 10s a ne hned
    -   long polling je dotaz na server, na ktery neni odpovezeno dokud nejsou nova data
    -   hned po doruceni dat se opet posle dalsi dotaz, ktery zase ceka
-   WebSockets
    -   plne duplexni obousmerna komunikace pres jediny TCP socket
    -   moznost vyvolat komunikace i stranou serveru
    -   komunikace
        -   nejdriv zadost klienta `GET ws://aa.bb.com HTTP/1.1 Connection: Upgrade`
        -   odpoved serveru `HTTP/1.1 101 WebSocket Protocol Handshake Upgrade: WebSocket`
    -   nutna podpora na strane serveru

### 13. Role based access control – principy, možnosti realizace, souvislosti s dalšími webovými technologiemi, identity management.

-   RBAC - Role Base Access Control
    -   drive prima vazba mezi uzivateli a objekty (nesystemove)
    -   udeluje prava uzivatelu k objektum na zaklade role
        -   role jsou mezi uzivateli a objekty
        -   uzivatele se stejnymi rolemi maji stejna prava k danemu objektu
        -   muze byt i hierarchie
        -   uzivatele se meni cast, role ne
    -   role pridelovany podle ukolu uzvatele (vazba na organizacni strukturu)
    -   ruzne typy modelu
    -   komponenty
        -   uzivatel (subject) - clovek, agent, pocitac
        -   role - definice pozice a odpovednosti
        -   pravo (permission) - povoleni konkretniho zpusobu operace k jednomu nebo vice objektum
        -   objekt - komponenta systemu, ke ktere je definovan omezeny pristup (soubor, tiskarna, terminal, databazovy zaznam atd)
        -   operace - zpusob prace s danym objektem
        -   sezeni (session) - casove a vecne omezene pouzivani systemu, nutnost ziskani aktualnich roli uzivatele
        -   hierarchie rolo (role hierarchy) - M:N (1:n) relace mezi rolemi, nekolik moznosti navrhu
        -   uzivatelske nastaveni (subject assignement) - M:N relace mezi pravy a objekty
        -   nastaveni prav (permission assignement) - M:N relace mezi pravy a operacemi
        -   nastaveni objektu - relace mezi pravy a objekty
-   realizace
    -   RBAC0 - core level
        -   koncept role jako soucasti uzivatelovo session v pocitacovem systemu
        -   potreba v kteremkoliv RBAC systemu
        -   ![](./rbac-core.png)
    -   RBAC1 - hierarchical level
        -   podpora hierarchii - dedicnosti roli
        -   ![](./rbac-hierarchical.png)
    -   RBAC2 - constrained level
        -   resi konflikty plynouci z kombinaci roli pro jediny subjekt
        -   Static Separation of Duty - oddeleni uzivatelu a roli
        -   Dynamic Separation of Duty - podminky na aktivni role uzivatele v ramci session
        -   ![](./rbac-constrained.png)
    -   dalsi varianty - kombinace predchozich
-   souvislosti s dalsimi webovymi technologiemi
    -   databazove systemy
    -   CMS (wordpress, drupal)
    -   cloud providers (Azure RBAC) - omezeni pristupu k resourcum
-   identity management
    -   sada sluzeb a pravidel pro spravu uzivatelu v organizaci
        -   informace o osobach, zarizenich (pocitace), skupinach, rolich
        -   autentifikacni a autorizacni pravidla
        -   zajisteni jednotneho pristupu
    -   bezpecnostni komponenta
    -   identita
        -   zaklad IdM
        -   jednoznacna identifikace daneho objektu, ke kteremu lze pridavat informace
        -   obvykle myslena osoba, uzivatel, ale v IdM muze byt i zarizeni, sluzby nebo jednotlive programy
    -   shlukovani do skupin (viceurovnove usporadani)
    -   ridi se
        -   uzivatelsky zivotni cyklus (CUD - Create, Update, Delete, vazby - skupiny, role a politiky - hesla, prava)
        -   zivotni cyklus zarizeni a dalsich entit - CUD, klice (ssh, X509), politiky (kontrola pristupu, autorizacni pravidla)
        -   dalsi aspeokty tykajici se bezpecnosti
        -   vazby - casto casove omezene
        -   politiky - obnova hesel, definice typu prav (read, write, listing atd)
        -   technologie - zpusob prenosu zabezpecenych udaju
    -   typy reseni
        -   centralizovane
            -   pro: sprava na jednom miste, reporting, vynucovani, vyvoj
            -   proti: nachylnost na vypadky, skalovani
        -   distribuovane
        -   obvykle kombinace
            -   rozdeleni do mensich centralizovanych celku podle kriterii (lokace, organizace ..)
    -   odpovednosti serveru IdM
        -   autentizace pro uzivatele a sluzby (hesla, SSO, 2FA, certifikaty, klice)
        -   autorizacni pravidla pro vsechny sluzby
        -   sprava souvisejici se sitovanim (DNS, DHCP)
        -   audit a reporting
    -   odpovednosti klienta IdM
        -   nacitani informace (uzivatele, skupiny, sitove skupiny, zarizeni, role, certifikaty)
        -   autentizace - hesla, tikety
        -   autorizace - HBAC (Host Base Access Control), sudo pravidla, klice ssh
        -   zjistovani a aktualizace DNS
        -   synchronizace casu
    -   nastroje
        -   stejne dulezite jako pouzite technologie
        -   bezpecnost vs slozitost
        -   diangosticke nastroje jsou nutne
    -   realizace
        -   AD - Active Directory - Microsoft
        -   LDAP - Lightweight Directory Access Protocol a Kerberos (Key distribution center), FreeIPA - linux
    -   GUI, API rozhrani pro napojeni third party

### 14. HTML 5 – popis, přínosy a rozdíly oproti předchozím verzím html (multimedia, sémantika).

-   HTML5
    -   zakladni znackovaci jazyk pro web
    -   vychazi z SGML
    -   soucasna verze 5 (verze dany sablonou DTD)
    -   struktura
        -   rozlisena forma a obsah
        -   `head`, `body`, dalsi elementy
    -   uzka vazba na DOM
-   prinosy
    -   reakce na aktualni potreby: RIA (pristupnost), standardizace webovych API
    -   nove prvky (semantika, multimedia)
-   rozdily oproti drivejsim verzim
    -   mutlimedia (`<video>`, `<audio>`, `<track>`)
        -   podpora vice zdroju napr u obrazku `<source>`
        -   canvas
        -   geolokace - podpora GPS v klientovi, moznost zjiste polohy z prohlizece
        -   komunikace + websockets
        -   audio
    -   web API - workers, storage, drag n drop
    -   service worker - offline aplikace
        -   soubor manifest - vyuziti v pripade offline stavu prohlizece
    -   server-sent events - zasilani zprav ze serveru na klienta bez zadosti
        -   na FE funguje obdobne jako websockets
        -   FE nemuze posilat zpravy na BE, pouze obracene
    -   semantika
        -   nove tagy: `<article>`,`<canvas>`,`<aside>`,`<footer>`, atd
        -   formulare - nove typy poli (validace)

### 15. Responzivní web (princip, návrh), html frameworky (bootstrap, zurb foundation).

-   responzivni web
    -   historie
        -   puvodne byl web pouze pro desktop zobrazeni - nebylo potreba resit
        -   s prichodem dalsich rozliseni a hlavne dalsich zarizeni (mobily, tablety, pda, laptopy, ctecky)
        -   HTML je "responzivni" samo o sobe (prohlizec uzpusobi zobrazeni rozliseni)
        -   reseni: jina aplikace pro mobily (m.facebook.com), fixed layout, JS resizing
    -   princip
        -   okolo 2009-2010 se objevil term responsive design
        -   fluidni design rozlozeni - pouzivani procent, aby se stranka prizpusobila rozmerum
        -   fluidni obrazky - puvodne pouze skalovani pomoci % v css, dnes ruzne formaty a velikosti pomoci `<picture>` a `<img>` `srcset` a `sizes` - prohlizec pak vybere spravny obrazek
        -   media query - definovani ruznych css stylu pomoci breakpointu a stylu obrazovky (screen, print)
        -   moderni techniky: multicol, flexbox, grid
    -   navrh
        -   jaka zarizeni budou pouzivana
        -   jake prohlizece
        -   vyber frameworku/knihovny
        -   testovani
-   HTML frameworky
    -   bootstrap
        -   puvodne z Twitteru
        -   rapidni prototypovani - snadnejsi a rychlejsi vyvoj
        -   framework obsahujici sablony pro ruzne komponenty a hotove snippety pro vyuziti v aplikacich
        -   responzivni by default
        -   ikony, formulare, tlacitka, typografie, js snippety
        -   5 verze odstranuje zavislost na jQuery a podporu IE
    -   zurb foundation
        -   open source responzivni frontend framework
        -   HTML a CSS komponenty, sablony a snippety
        -   typografie, formulare, tlacitka apod
        -   sablony pro stylovani emailu
        -   css, sass, rails gem

### 16. MVC a front-end frameworky – React, Angular, jQuery.

-   MVC - Model View Controller
    -   uzivatel vykona akci v GUI
    -   tu zachyti `Controller`
    -   `Controller` rozhodne, jak na akci zareagovat a typicky zmeni hodnoty v `Modelu` nebo primo ovlivni `View`
    -   `View` zobrazi zmeny uzivateli
    -   ruby on rails
    -   ![](./mvc.png)
    -   u FE frameworku bud pouzit samostatne nebo jako `View` cast
-   React
    -   js knihovna pro vytvareni rychleho a interaktivniho uzivatelskeho rozhrani
    -   vznikl ve FB v r 2011
    -   popisuje, jak ma vypadat vysledek, ne kod ktery neco meni
    -   DOM - casta aktualizace elementu - narocne
        -   Virtualni DOM - react si drzi virtualni kopie DOMu
        -   pri zmene nejake hodnoty porovna novy virtualni DOM se starym a pokud se neco zmenilo, prekresli pouze tu cast DOMu, ktera se zmenila
    -   komponenty
        -   funkce/trida ktera reprezentuje cast aplikace
        -   komponenty se skladaji a tim vznika aplikace a jeji casti
    -   JSX - JavaScriptXML - syntaxe v podstate HTML rozsirena o JS
    -   State - udaje o lokalnim stavu aplikace
    -   props - vlastnosti - podobne atributum v HTML, posilani dat dolu
    -   hooks - podpurne funkce. ktere umoznuji stavet komponenty pouze za vyuziti funkci
        -   rozliseni funkci podle logiky
        -   `useState` - pridani mistniho stavu v komponente funkce
    -   zivotni cyklus komponenty
        -   pripojeni - vytvoreni elementu v DOMu a zacina poslouchani zmen
        -   akutalizace - pri aktualizaci dat (state, props) dojde k prekresleni
        -   odpojeni - komponenta odebrana z DOM
-   Angular
    -   novejsi verze zalozene na TS, prepis z AngularJS
    -   google, part of MEAN stack
    -   modularni - core funkce presunuty do modulu
    -   framework zalozeny na komponentach
    -   routing, form management, client-server communication, DI, PWA
-   jQuery
    -   JS knihovna pro usnadneni prace s DOMem, eventy, css animacemi a ajaxem
    -   2 - dropped IE 6-7
    -   3 - promises, defferends, html5 compatible

## Multimedia

### 17. Multimédia a jejich členění – popis jednotlivých forem, možné kombinace.

-   sdeleni - informace pro prijemce
-   medium
    -   vlastni forma sdeleni
    -   typicky cleneno podle lidskych smyslu
-   komunikacni kanal - zpusob prenostu sdeleni (kodovani, protokol...)
-   druhy
    -   text (linearni medium)
    -   hypertext (nelinearni medium - texty propojene odkazy)
    -   grafika (fotografie, obrazky)
    -   zvuk (audio, rec, hudba ..)
    -   video (animace, video)
-   dalsi media: hmat (Brailovo?), cich?, chut?
-   multimedia
    -   kombinace ruznych medii ve sdeleni
    -   pro nase ucely hlavne elektronicky prenositelna
    -   obecny pojem pro sdeleni zejmena zvukem a videem
-   hypermedia
    -   rozsireni pojmu hypertext pro medialni oblast
    -   nelinearita
    -   interaktivita (Aspen Movie Map)
-   atributy medii
    -   forma: text, hypertext, video, audio, 3d, VR ...
    -   tok: jednosmerny, obousmerny
    -   ucastnici: asynchronni (offline), synchronni (online)
    -   kardinalita: 1:1, 1:N, N:N
-   vnimani medii
    -   zpusob komunikace s okolim
    -   fyziologicka faze - receptory, transformace
    -   psychicke zpracovani - uvedomeni, interpretace
-   psychologie
    -   clovek vnima to co chce
        -   hist. zkusenost, podprahove vnimani, kontext
    -   genderove odlisnosti, vek, kultura ...
-   text
    -   umele vytvorene medium
    -   zalozen na (obvykle) konecne abecede
    -   je nutne se ho naucit
    -   lehce elektronicky prenositelne (kodovani)
    -   mnohoznacnost
        -   vice moznosti pro vyjadreni tehoz, pochopeni tehoz
    -   i graficke atributy (font)
    -   obsah - stylistika, emocni zabarveni, terminologie
    -   forma - velikost, barva ..
-   hypertext
    -   nelinearni medium zalozene na blocich textu a jejich propojeni odkazy
    -   priblizeni chovani cloveka
        -   asociace
        -   pokud me neco zajima, chci vedet vice
    -   rozvej hlavne s WWW (html)
    -   odkaz muze byt i graficky (obrazek)

### 18. Rastrová grafika – princip, digitalizace, transformace, barevná paleta.

-   bez doplnkove informace
-   udaje o kazdem bodu samostatne
-   hlavni pouziti u fotografii (vytvarena mimo digitalni prostredi)
-   typicke transformace
    -   barevne - jas, kontrast, efekty
    -   prostorove - spirala, morphing
-   soubory
    -   konstantni rozliseni (problem pri zoomu)
    -   velikost souboru (rozmery, barevna hloubka, komprimace)
    -   komprese
        -   bezeztratova
            -   BPM, TIFF, TGA, PCD
        -   ztratova
            -   GIF, PNG, JPG
        -   kompresni pomer = vystup/vstup
        -   ucinnost, casova a pametova narocnost, kvalita
        -   metody - RLE (ucinne pro specificke obr.), slovnikove (), rozdilove kodovani, prediktivni kodovani
    -   WebP - google, ztratova i bezeztratova
-   import
    -   fotoaparat - clona, expozice, citlivost, snimac, svetelnost..
    -   scanner - rozliseni, barevna hloubka
    -   screenshot
-   transformace
    -   barevne
        -   snizeni poctu barev
        -   barevny odstni
        -   jas, kontrast
        -   histogram
    -   geometricke
        -   zmena rozliseni, manipulace s casti obr. nebo celkem, simulace nastroju (stetec, pero)
-   programy
    -   jednoduche editory - malovani, picasa3 ..
    -   retuze a montaz - photoshop, gimp ..

#### Terie barev

-   propustene a odrazene svetlo
-   barevne hranice technologii - podle poctu odstinu zakl. barev
-   zakladni model
    -   ruzny pocet zakladnich barev
    -   kazde zarizeni svuj gamut
-   RGB - red, green, blue
    -   pro zarizeni vyzarujici svetlo
    -   odpovida fyziologii oka
    -   je aditivni (pridani barev)
-   CMYK - cyan, magenta, yellow, black
    -   tiskarny
    -   pohlcovani ruznych delek svetla pigmenty
    -   je subtraktivni (ubirani barev)
-   HSB - hue, saturation, brightness
    -   malirske michani barev
    -   lepe odpovida lidskemu vnimani
-   primo pojmenovane bravy
    -   standardy
-   reprezentace barev
    -   pro kazdy bod nutny udaj o barve
    -   kriteria
        -   pocet zobrazitelnych barev - napr 16.7 mil
        -   pocet soucasne zobrazitelnych barev
    -   dva hlavni pristupy
        -   indexove
            -   barvy nastaveny predem z sirsi palety (RGB paleta - pro kazdou barvy 1B)
            -   na konkretni barvu odkazovano indexem
            -   od poctu barev je odvozen index (16 barev => 4bit index, 256 barev => 1B index)
        -   obecne
            -   RGB jednotlive pro kazdy bod
            -   podle poctu bitu na jeden bod je dan pocet moznych barev (16b => HiColor - RGB 5,6,5b, 24b => TrueColor - RGB po 1B)
-   vnimani barev
    -   receptory - oko
        -   tycinky - 120-130mil
        -   cipky - 5-6mil
        -   rozliseni cca 70Mpix
    -   procesor - mozek
-   psychologie barev
    -   ruzne barvy ruzne pusobi na lidi

### 19. Ukládání grafiky – ztrátová a bezeztrátová komprese, formáty, vektorová grafika.

-   komprese
    -   bezeztratova
        -   BPM, TIFF, TGA, PCD
    -   ztratova
        -   GIF, PNG, JPG
    -   kompresni pomer = vystup/vstup
    -   ucinnost, casova a pametova narocnost, kvalita
    -   metody
        -   RLE - ucinne na omezene abecede
        -   slovnikove - postupne buduju slovnik, ktery koduju
            -   LZW - (gif, tiff, pdf)
        -   rozdilove kodovani
        -   prediktivni kodovani - odvozeni z predchozich hodnot + chyba (koeficient pro min. chybu)
        -   unarni kodovani - cisla na kody
        -   entropicke kodovani - podle pravdepodobnosti vyskytu znaku
            -   Huffmanovo kodovani - symboly jsou listy v bin. strome s hranami udavajici kod (bzip2, jpeg, mp3)
            -   aritmeticke kodovani
        -   kontextova komprese - prediktivni model
    -   WebP - google, ztratova i bezeztratova
-   vektorova grafika
    -   objektovy pristup
        -   strukturalni popis objektu (primka, souradnice, barva ..)
    -   doplnkova informace
    -   jiny sw - prevod popisu na zobrazeni
    -   z principu bezeztratove
    -   uklada se pouze popis
    -   formaty: WMF (Win), CDR (Corel), SVG (web)
    -   programy CorelDraw, Illustrator, Inkscape

### 20. Zpracování zvuku – digitalizace, způsoby syntézy, transformace v časové i frekvenční rovině.

-   mechanicke vlneni
-   analogove vs digitalni
    -   kontinualni vs diskretni
    -   vzdy ztratovy prevod
-   praze se zvukem
    -   zaznam - digitalizace A/D
    -   ulozeni - formaty
    -   zpracovani - upravy
    -   prezentace - D/A
-   vnimani zvuku
    -   clovek: 20Hz - 20kHz
    -   mensi rozsah intenzit nez zrak
    -   logaritmicka citlivost
        -   prah slysitelnosti, bolesti
    -   frekvencni spektrum - zastoupeni frekvenci
    -   prostorovy vjem - stereo
    -   kvalita sluchu - maskovani frekvenci, casove maskovani
-   dB korekce A,B,C,D
    -   prizpusobeni lidskemu vnimani hlasitosti
-   intenzita zvuku
    -   objektivni vs subjektivni metriky
    -   merici kmitocet - 1000Hz, sinusovy prubeh
    -   hodnoceni hlasitosti - Fon [Ph] - logaritmicka
    -   subjektivni hodnoceni hlasitosti - Son [son] - linearni
-   dB v PC - 0 je maximum - technocka mez zarizeni
-   druhy
    -   mira podilu ruznych frekvenci jako u svetla
    -   cisty ton - jedina frekvence
    -   ton s harmonickymi slozkami a dominantni frekvenci - jiny tvar vlny
    -   bily sum - nahodny signal s rovnomernou spektralni hustotou
    -   ruzovy sum - vykonova frekvencni hustota je primo umerna prevracene hodnote frekvence
-   sireni zvuku
    -   rychlost sireni
    -   pohlcovani zvuku - selektivni na zaklade kmitoctu
    -   odraz, rezonance, dozvuk
-   snimani
    -   mikrofony
        -   dynamicky - magnet v civce
        -   kondenzatorovy - zmena vlastnosti dielektrika
    -   typy
        -   frekvencni charakteristika
        -   smerova charakteristika
        -   provedeni
-   vystup
    -   piezoelektricke menice - zejmena vyssi kmitocty
    -   reproduktory - civka v magnetickem poli
    -   sluchatka - eliminace ruchu
-   zesilovani
    -   elektronicke, analogove, digitalni

#### Obecny zvuk

-   nestrukturovany - lze zaznamena jakykoliv zvuk
-   ruzne formaty zaznamu
    -   PCM - pulsne kodova modulace
        -   snima okamzite hodnoty napeti v danych intervalech
        -   vzorkovaci frekvence
            -   pocet sejmnutych vzorku za vterinu (Hz)
            -   musi byt 2x vetsi nez nejvyssi zaznamenavana frekvence zvuku (pouzivani 44.1KHz)
        -   pocet rozlisitelnych urovni signalu - kvantovani
            -   pocet bitu pro ulozeni tohoto udaje - 16b => 65k urovni
        -   mono vs stereo vs 4.1 vs 5.1 vs 7.1
            -   nasobne pozadavky
        -   pr.: 1s zvuku ve stereo v CD -> `44100 Hz x 2B x 2(stereo) = 176 400 B (150 kB/s)`
    -   ADPCM, Delta, CCITT
-   prace se zvukem
    -   na urovni prubehu signalu - casova osa, transformace
    -   visestopy zaznam a jeho zpracovani
    -   scenar
    -   audacity, razor, cakewalk
-   transformace
    -   amplitudove - uprava obalky, ztlumeni, zesileni, normalizace, fadein/out
    -   casove - dozvuk, echo, zrychleni
    -   frekvencni - fourierova transformace, propuste, zadrze
    -   vzorkovaci - uprava vzorkovaci frekvence
-   ukladani dat
    -   kontejner - struktura pro ukladani dat o zvuku
        -   obecne pro jakakoliv data
        -   Ogg, AVI, MP4, WAV
    -   kodek - algoritmus pro transformaci dat pro ulozeni a zpet
    -   nelze libovolne kombinovat
-   souborove formaty
    -   ruzny datovy tok, komprese, ztrata informace
    -   WAV, RAW, CD - velikost, ale mozna prima konverze
    -   MP3 (vyuziva nedokonalosti lidskeho sluchu), AC3 - ztrata informace
-   generovani
    -   nove zvuky nebo napodobeni originalu
    -   generatory a syntezatory

#### Strukturovany zvuk

-   prace s doplnkovou informaci
-   omezena abeceda
-   hudba - MOD, MIDI
-   analyza a synteza reci
-   MOD - Amiga
    -   uklada se samotny prubeh skladby
    -   sejmute vzorky nastroju
    -   stopa, kde bude vzorek prehravan
    -   cislo vzorku
    -   cas pocatku prehravani
    -   doba trvani prezentace vzorku
    -   frekvence vzorku, hlasitost
    -   4 samotne kanaly
    -   zpracovani - na urovni not, moznost vymeny nastroju
    -   tvorba - samplery, syntezatory
-   MIDI
    -   strukturovana zvukova data
    -   v souboru pouze prubeh skladby (uspora mista za cenu omezeni moznosti)
    -   16 kanalu
    -   zpracovani - komfortni, notovy zapis, zmeny nastroju
    -   trackery - editace strukturalniho popisu
    -   sequencery - editace hudebnich souboru vcetne not, vicestopy, smycky
    -   syntezatory - prace s oscilatory
-   standardy
    -   VST - virtual studio technology
        -   pluginy pro ruzne ucely
    -   LADSPA - Linux Audio Developers Simple Plugin API

### 21. Digitální video – princip, kvalitativní parametry, formáty, pořízení, způsoby šíření.

-   sekvence rychle se stridajicich obrazku
-   frekvence 25-30fps, dnes vice
    -   vychazi z normy (h.264, MPEG2)
-   formaty
    -   lisi se zpusobem komprese (kvalita, velikost)
    -   podle informace
        -   bez doplnkove informace - AVI, MPEG, QuickTime
        -   s doplnkovou informaci - nektere animace
    -   podle datoveho toku
        -   CBR, VBR
-   animace
    -   priklad videa s doplnkovou informaci (flash) - neni to podminka (GIF)
    -   digitalni vs klasicka
    -   ruzny postup
    -   prace se scenou - casto 3D
        -   meni se parametry v zav. na case
        -   snimek se vytvori a zaradi do video sekvence
-   transformace
    -   casova osa
    -   strih, hudba, vkladani ruznych medii
    -   titulky
    -   zmena kodovani
-   porizeni
    -   kamery - ruzny ucel, snimace, pametove medium, rozliseni, format
    -   screencast
    -   konvertory
-   strih - postprodukce
    -   kombinace nekolika zdroju do vysledneho produktu
    -   strih, titulky, dabing

#### AVI

-   Audio Video Interleave
-   kontejnerovy format
-   vice datovych stop
-   ruzne kodeky
-   streaming
-   starsi format - nestandardni info o pomeru stra, casove znacky

#### QT - Apple Quick Time Movei

-   soucast sirsiho rozhrani
-   kontejnerovy format
-   zaklad MPEG4
-   `.mov`
-   rada kodeku

#### MPEG

-   Motion Pictures Experts Group
-   komprese
    -   I-frame - plny snimek
    -   P-frame - rozdilovy (odlisnost mezi aktualnim a predchozim snimkem)
    -   B-frame - rozdilovy (rozdil mezi dvema nejblizsimi I nebo P snimky)
    -   nutne mit vzdy potrebne snimky
    -   problem pri editaci
-   MPEG1 - CD, 1.5Mb/s
-   MPEG2 - DVD, 15Mb/s
-   MPEG4
    -   kontejnerovy format MP4
    -   H.264/AVC - 50% uspora objemu dat
    -   HEVC/H.265 - dvojnasobna ucinnost proti H.264
    -   zvuk v mp3, aac
-   MPEG7 - nastupce MPEG4
    -   archivace multimedii + vyhledavani
-   MPEG21 - pro 21. st.
    -   obecny kontejner
-   MJPEG - samotne obrazku v jpeg

#### Dalsi formaty a kodeky

-   DivX - firemni kode, vysoka komprese
-   Xvid - odnoz DivX 4
-   RealMedia - streaming
-   WebM - pro HTML5
-   archivace
    -   VCD
    -   XVCD
    -   SVCD
    -   DVD
    -   Blue-Ray

## Databaze

### 22. Relační databáze – programování na straně serveru (uložené pohledy, procedury, triggery, inline a tabulkové funkce, agregační funkce).

-   v nekterych pripadech je vhodne presunout vypocty a operace s daty primo na db server
-   vyhodou je moznost opetovneho vyuziti
-   nevyhodou je zvyseni zateze db serveru
-   ulozene pohledy - views
    -   sql prikazy ktere jsou pojmenovane a ulozene v systemu
    -   poskytuje objekt ve stejne podobe jako tabulka, akorat v nem nejsou data ulozena, jen vi jak je poskladat
-   funkce - vraci hodnotu
-   procedury
    -   jako funkce, ale nevraci hodnotu
    -   definice sekvence prikazu nad databazi
-   triggery
    -   automaticky spoustene akce pri INSERTu, UPDATu nebo DELETu (kontrola integrity dat, doplneni hodnot)
-   inline a tabulkove funkce
    -   tabulkova funkce vraci tabulku
-   agregacni funkce

### 23. Relační databáze – struktura dat, indexace a vyhledávání (ukládání stromových struktur, princip indexace a běžné typy: uspořádaný / hešovaný / B+ strom, optimalizace SQL dotazů).

-   uloziste (hardisk, sitove...)
    -   data v souborech -> data organizovana do bloku -> nekolik radku
    -   presun dat do RAMky - PCIE (zabira cas i prostredky)
    -   pravdepodobnost ze budeme chtit nasledujici radek -> presuneme cely blok
    -   **blok** - razene - pridava se na konec
        -   organizace pomoci Clustered index - udava poradi
        -   pri mazani by se nevyplatilo to setrasat -> radek se pouze oznaci jako smazany -> shrink - jednou za cas se data preskladaji
    -   pri zakladani se alokuje vetsi prostor
    -   hledani v souborech - narocne (musel bych tahat bloky do pameti) -> index
        -   kazdemu radku dam row id a do indexu si ulozim hodnotu -> pak prohledavam jen index a pomoci nej se dostanu na spravny radek -> slozitost linearni (protoze se implementuje jako spojak)
        -   stromy - logaritmus, musi byt vyvazeny -> B(balanced) stromy -> B+ stromy
        -   pri mazani musim aktualizovat index
        -   zpomalime manipulaci s daty (musim indexy aktualizovat) -> plati pravidlo - kdyz chci podle neceho hledat mel bych to mit indexovane
        -   vyuziti **hash** - otisk dat - hash tabulky -nemusim pak porovnavat vsechno
-   pamet - pri praci s daty je potreba je nacist bloky
    -   jednou za cas se zase data ulozi zpatky do souboru
-   ukladani stromovych struktur
    -   standardne tabulka s odkazy na predka
    -   zasobnik - neusetri se SQL dotazy ale zpracovani pouze whilem
    -   pridani dalsich atributu (o hloubce a poradi v ramci stromu)
        -   muzeme lepe vypisovat, ale hure vkladat
    -   pouziti
-   princip indexace
    -   pomoci vhodnych struktur urychlujeme pristup k radkum tabulky podle urcitych parametru
    -   usporadany
    -   hash
        -   vyuziti hashovacich tabulek
        -   nepodporuje jine operace nez rovna se
    -   B+ strom
        -   slozeny se serazenych hodnot odkazujici na radku v tabulce - rychle hledani
-   optimalizace SQL dotazu
    -   indexy
    -   partitioning
    -   dotazy na konkretni sloupce
    -   minimalizace WHERE
    -   odstraneni kartezskeho soucinu
    -   nejdrive filtrovat pak az radit apod.

### 24. Big Data – škálování databází (mirroring, partitioning, sharding), CAP teorém, ACID/BASE.

-   nosql - historicky ruzne vyznamy, ale vsechny teoreticky spravne
    -   not only sql
    -   no sql
    -   non relational
-   na relacnich db neni vylozene nic spatne, ale maji nevyhody pro konkretni pouziti
    -   pevne dane schema - nemuzu ukladat dynamicka data
    -   nevhodne pro velka data (stovky milionu zaznamu)
    -   rozdeleni na vic serveru (distribuovane systemy)
    -   vysoka dostupnost pri zapisu
    -   oddeleni logicke a fyzicke vrstvy u dotazu s velkym poctem JOIN operaci - ty ktery se pouzivaj spolu, jsou ulozeny u sebe
-   skalovatelnost
    -   horizontalni - vice nodu
    -   vertikalni - lepsi hw

#### Horizontalni skalovatelnost

-   rucne na aplikacni vrstve - db switching
    -   sam zajistuju v aplikaci kam data ukladam/nacitam
    -   pr. rozhazuju data podle roku (na server1 rok 2019, server2 rok 2020 atd)
    -   _nevyhody:_ hodne dat (musim casto pridavat server), overhead
-   db mirroring
    -   ![DB Mirroring](../../uai/683-pokrocile-databazove-systemy/db_mirroring.png)
    -   data zapisuju do masteru (CUD), ale ctu je z vice slavu (na ktere se mi to replikuje)
    -   pred slavama - load balancer
    -   vyhoda: rychlost a skalovatelnost cteni
    -   _nevyhody:_ overhead na masterovi (neskaluje), hlidani replikace - neaktualnost dat (napr. jeden slave ma nespravna data - problem), selhani masteru, pomala distribuce CUD operaci, porad omezena velikost
    -   varianta - basic cluster database
        -   porad stejne rozdeleny, ale je to na shared storage (virtualizovany)
        -   nedochazi k zasilani aktualizacnich zprav, ale k rizenymu kopirovani bloku pameti (na sdilenym ulozisti) - pomerne rychla distribuce
        -   do jisty miry muzu pridat 2 mastery (failover)
        -   _nevyhody:_ stale muze dojit k neaktualnich hodnot, narocne na sitovy provoz, stale omezena velikost db
-   partitioning - u relacnich db
    -   deleni tabulky bud na sloupce (vertikalne) nebo na radky (horizontalne)
    -   _vertical_
        -   rozdeleni na sloupce
        -   tabulky na ruznych discich/serverech => rozlozeni zateze
        -   malo pouzivane - obvykle potrebujeme cele radky
    -   _horizontal_ (=sharding) - **neni to scaling!**
        -   horizontal partitioning se vetsinou pouziva u relacnich, sharding vetsinou u nosql
        -   rozdeleni na radky
        -   tabulky na ruznych serverech => velmi dobre rozlozeni zateze
        -   failover lze resit zdvojenim serveru
        -   _nevyhody:_
            -   udrzovani primarnich klicu a autoinkrement (pouzivame GUID nebo UID - neco jako hash, nemel by byt stejny)
            -   vazby mezi daty na ruznych serverech
                -   mame globalni cast - shard table a delime pouze tabulky u kterych je to vykonove vyhodne
                -   vazby resime ruzne: napr. mirrorovanim nekterych tabulek, kopirovanim nekterych dat => abych se vyhnul joinum napric servery
        -   pro aplikace se cluster tvari jako jedina databaze (je tam nakej server - management/router server, kterej dotazy rozhazuje na urcitou db)
        -   rozlozeni dat na shardy
            -   proste logiky
                -   pr. prijmeni uzivatel
                -   nemuzeme jednoduse pridat shard (musime upravit logiku => prenest data)
            -   nahodne
                -   data celkem rovnomerne rozproztrena
                -   dotazy na hledani musim posilat na vsechny shardy, protoze nevim kde jsou -> muzeme pridat indexy na management server
                -   muzeme v klidu pridat dalsi shard
            -   matematicky (fuknce)
                -   pr. hash funkce podle ktery urcim na kterej shard
                -   je potreba rychla fce, aby nezatezovalo server
                -   nemuzeme jednoduse pridat shard (musim zmenit hash funkci + prehashovat data v db)
            -   komplexni strategie
                -   kombinace nebo varianty predchozich + dalsi veci, napr geograficka prislusnost
-   kombinace

![](../../uai/683-pokrocile-databazove-systemy/scalling.png)

#### ACID

-   **atomic** - transakce se provede cela nebo vubec
-   **consistency** - transakce prevadi db z jednoho konzistentniho stavu do dalsiho konz. stavu
-   **isolation**
    -   transakce by se nemely navzajem ovlivnovat - napr. ctu data ktery nemusi mit validni hodnotu
    -   provedeni pomoci zamku, casovych znamek atd (viz [databaze 1](../697-databaze/README.md#acid))
-   **durability** - po potrvzeni transakce jsou data trvale zapsana a muzeme je povazovat za platna
-   NoSQL porusujou hlavne consistency - nemame cizi klice a napr. kdyz mame nakopirovanyho uzivatele u komentaru, po zmene username se mi to tam nepromitne - musim to aktualizovat sam
    -   ale neplati, ze nemohou ACID splnovat, ale spise ohledne dat ne indexace apod. (ty pak odpovidaji spise BASE)

#### BASE model

-   vetsinou implementuji nosql, snazi se tim resit hlavne poruseni consistency
-   **Basic Availability** - uprednostnujeme rychlou odpoved pred jeji spravnosti (vetsinou ruzne levely nastaveni, napr: ravendb - normal = kdyz db nema co delat, zacne aktualizovat indexy)
-   **Soft state** - opak konzistentniho stavu - prechodny stav - vetsinou se db vyskytuje v nekonzistentnim stavu - nosql db vetsinou neceka, nez se aktualizuje vsechno spojeny s nakym dotazem (treba update indexu) a rovnou vraci data, ktery ale nejsou "aktualni"
-   **Eventual Consistency** - dotaz ze chci opravdu konzistentni odpoved - tj. pockam na update db (indexy) a az pak vracim data - za cenu delsiho casu

#### CAP theorem

-   Brewer's theorem
-   tvrdi, ze vzdy je mozne docilit 2 ze 3 moznosti:
    -   **Consistency** - kazdej read mi vrati nejnovejsi write nebo error
    -   (High) **Availability** - kazdej read mi vrati data a ne error (ale nemusi byt nejnovejsi)
    -   **Partition Tolerance** - moznost udelat distribuovany system (napr. vice node)

::: warning
**zastaraly model**
:::

![](../../uai/683-pokrocile-databazove-systemy/cap.png)

-   CP - ACID - vetsina relacnich, resime deleni dat (P) - mirroring etc - distribuce
-   AP - BASE - vetsina NoSQL
-   CA - ACID - relacni, ale neresime deleni dat, neni distribuovana

::: tip
**novejsi model**
:::

-   databaze se vyskytuji v celem spektru CAP
-   nemusi splnovat vsechno nebo muze vsechno trochu atd.
-   z tohoto modelu vyplyva, ze pro konkretni problem je nejlepsi konkretni databaze
    -   vybirame podle usecasu, ne podle "nejlepsi" db

![](../../uai/683-pokrocile-databazove-systemy/cap-new.png)

-   relaci - okolo C
-   column oriented - okolo A
-   key-value - okolo A skoro az k P
-   dokumentove - zhruba uprostred, ale neumi to vsechno nejlip
-   grafove - mezi CA, moc nejdou rozdelit na vice serveru
-   data lake - vhodne pro P, ale protoze to je nad file systemem -> spatny hledani (pomaly), spatna konzistence (hodne nodu)
-   data warehouse - mezi CP - rychle cteni, distribuce
    -   pouziva data z relacnich databazi
    -   ma data v "nenormalnich" formach -> rychlejsi cteni dat
    -   typicky se tam jednou data nahrajou a uz se nemeni - jeden insert, opakovany cteni
    -   lepsi vykon cteni - vetsi dostupnost

![](../../uai/683-pokrocile-databazove-systemy/cap-new-types.png)

### 25. NoSQL databáze – typy a základní principy (Key-Value, Column-Oriented, DocumentOriented, Graph Database), volba vhodné databáze, indexace a vyhledávání v NoSQL.

-   **key-value**
    -   klic -> hodnota
    -   pristup pouze pomoci klice (indexace)
    -   klic vetsinou hash, nebo nese jeste nejakou informaci (datum)
    -   hodnota obvykle serializovana a komprimovana
    -   problem kdyz chci dalsi index
    -   redis
    -   pouziti: cache
-   **column-oriented**
    -   v podstate dvourozmernych poli (na prvni pohled podobny jako RDB)
    -   ![](../../uai/683-pokrocile-databazove-systemy/column-oriented.png)
    -   data uchovava po sloupcich (na disku jsou u sebe data z jednoho sloupce) - casto i serazeny
    -   casto u hodnoty ve sloupci ulozena i reference na predchozi a dalsi sloupec toho radku (aby bylo rychly i dostani celyho radku)
    -   hodnoty muzou mit i vice hodnot (napr. produkty objednavky)
    -   sloupce lze vnorovat -> SuperColumn
    -   k datum pristupuju radek + sloupec
    -   radky mohou mit vice sloupcu (nejsou tam null jako v RDB)
    -   google big table, apache cassandra
-   **document-oriented**
    -   rozsireni key-value
    -   ukladani strukturovanych dokumentu - json, xml ...
    -   velka volnost s obsahem - hodnoty
    -   muzeme cokoliv ze struktury dokumentu indexovat
    -   oproti RDB porad chybi vazby
    -   mongodb (bson - binarni json), couchdb, ravendb, dynamodb (amazon)
-   **graph-database**
    -   zalozeny na teorii grafu - vrcholy a hrany (nody a vazby)
    -   existuji vazby - nekdy se i hlidaji (napr pri smazani), maji atributy
    -   neni zadny schema - pridavam nody a vazby
    -   hledani - potrebujeme klic a pak uz jdeme po hranach dal
    -   vrcholy - jsony, key-value - jednoduchy objekty
    -   hrany - reference meze vrcholy + atributy navic
    -   ![](../../uai/683-pokrocile-databazove-systemy/graph-db.png)

![](../../uai/683-pokrocile-databazove-systemy/nosql-types.png)

## Data mining

### 26. Proces dobývání znalostí (zdroje dat, statistika dat, normalizace, zpracování textových a nestrukturovaných dat). Popište dílčí kroky, ze kterých se proces dobývání znalostí skládá a jejich účel. V blokovém schématu vyznačte návaznosti mezi jednotlivými kroky.

-   jde o proces objevování implicitních (dopředu neznámých) a potenciálně použitelných znalostí v datech. Zahrnuje: databáze, statistiku a umělou inteligenci.
-   cíl: automatické vyhledávání zákonitostí v rozsáhlých souborech dat.
-   zdroje dat - soubory, web, databaze
-   statistika dat - nahrazeni velkych objemu dat mene zaznamy - redukce pomoci statistickych metod
-   normalizace - transformace dat do pozadovaneho rozsahu
-   zpracovani dat
    -   textove
    -   nestrukturovane
-   proces
    -   selekce dat - vyber relevantnich dat
    -   predzpracovani dat - cisteni odlehlych hodnot, doplnovani chybejicich, agregace, detekce zavislych atributu
    -   transformace dat - podle potreb analytickych metod (selekce atributu, vazeni atributu, normalizace)
    -   dolovani znalosti - vyuziva metody umele inteligence a strojoveho uceni
        -   modelovani zavislosti, klasifikace dat do trid, shlukova analyza
    -   vizualizace - intepretace vysledku - grafy, tabulky atd
    -   interpretace - vyjadreni vysledku ve forme reportu a zprav srozumitelnych

### 27. Klasifikace dat. Popište princip a použití klasifikátoru. Vysvětlete princip algoritmů k-NN (k-nearest neighbors), rozhodovací stromy a asociační pravidla.

-   klasifikator
    -   model ktery zarazuje vzory do disktretnich kategorii
    -   vstup: atributy
    -   vystup: kategorie
-   k-NN
    -   nejjednodusi klasifikator
    -   k - male prirozene cislo, urcuje kolika sousedu se ptam
    -   pro neznamy vzor z trenovaci mnoziny vybere k neblizsich sousedu
    -   vybrani sousede hlasuji o kategorii - vetsi zastoupeni vitezi
-   rozhodovaci stromy
    -   znalosti reprezentovany v podobe stromu
    -   uzly reprezentuji tridu dat
    -   vetveni reprezentuje strukturu dane tridy dat
    -   zvoli se jeden atribut (koren)
    -   data se rozdeli podle hodnot zvoleneho atributu na podmnoziny a kazde podmnozine se priradi novy uzel stromu
-   asociacni pravidla
    -   pravidlo typu `if ... then ...`
    -   z hodnot atributu se vytvareji konjukce a pocitaji se cetnosti vyskytu vyse uvedenych kombinaci pro konkretni hodnoty atributu
    -   algoritmus apriori

### 28. Shluková analýza. Popište účel a použití shlukové analýzy. Vysvětlete princip hierarchického shlukování a algoritmu k-means.

-   shlukova analyza
    -   metoda analyzy dat
    -   resi problem nalezeni podobnosti v neznamych datech
    -   podobna data typicky lezi blizko sebe - tvori shluky
    -   zajimaji nas parametry shluku - stred (teziste) a velikost (rozptyl)
-   hierarchicke shlukovani
    -   metoda zdola nahoru
    -   na zacatku kazdy vzor je umisten do samostatneho shluku
    -   pak opakovane shluky spojujeme az ziskame jeden shluk
    -   shluky si poznamenavame
-   k-means
    -   odhadneme pocet shluku
    -   nahodne vygenerujeme stredy shluku
    -   urcime k jakemu shluku vzor patri (k jehoz stredu je nejblize)
    -   z dat shluku vypocteme novy stred
    -   opakujeme do ustaleni

## Bezpecnost

### 29. Certifikát, veřejný klíč, řetězce certifikátů, ověřování certifikátů, kvalifikovaný elektronický podpis, časová razítka

-   certifikat
    -   digitalne podepsany verejny sifrovaci klic
    -   ruzne typy: x.509
    -   pouzivany pro identifikaci protistrany
    -   vydava certifikacni autorita (duveryhodna treti strana)
-   verejny klic
-   rezce certifikatu
-   overovani certifikatu
-   kvalifikovany eletronicky podpis
-   casova razitka

### 30. Využití certifikátů na webu – SSL/TLS, HTTPS, RLP, autentizace z využitím certifikátu, bezpečný email

-   SSL/TLS
-   HTTPS
-   RLP
-   autentizace s certifikatem
-   bezpecny email
