# Informacni a komunikacni technologie

## Architektura pocitacu

### 1. Číselné soustavy, převody číselných soustav, základní operace ve dvojkové soustavě. Kódování znaků v počítači, reprezentace čísel se znaménkem a čísla v plovoucí řádové čárce.

-   pozicni
    -   hodnotu urcuje pozice v dane sekvenci cisel
    -   z-adicka soustava
        -   ai - z-adicka cislice
        -   z - zaklad soustavy
        -   n - nejvyssi rad s nenulovou cislici
        -   m - nejnizsi rad s nenulovou cislice
    -   typy
        -   dvojkova
        -   osmickova - napr. zapis prav na unixu
        -   desitkova
        -   sestnactkova - 0-9, A, B, C, D, E, F
-   nepozicni
    -   rimske cislice (I, II, III, IV, V ...)
    -   egyptske cislice (pro kazdou mocninu 10 jiny znak, ktery se opakoval)
    -   recke cislice (zapis pomoci pismen alfabety)
-   prevody soustav
    -   dvojkova -> desitkova
        -   nasobime hodnotu h na pozici x zakladem z na mocninu `A = h*z^x`
        -   `1001101` -> `1*2^6 + 0*2^5 + 0*2^4 + 1*2^3 + 1*2^2 + 0*2^1 + 1*2^0 = 77`
    -   dvojkova -> desitkova se zlomkovou casti
    -   desitkova -> dvojkova
    -   desitkova -> dvojkova se zlomkovou casti
    -   desitkova -> sestnactkova
    -   sestnactkova -> desitkova
    -   prevod mezi pribuznymi soustavami
        -   zaklad jedne je mocninou zakladu druhe
        -   napr: z=10, z=100, z=1000
        -   napr: z=2, z=4, z=8 apod.
        -   pr. z=2 -> z=16 (rozdelime po ctvericich)
            -   `1011 0001 0010 1010` -> `B 1 2 A`
-   zakladni operace ve dvojkove soustave
    -   scitani
    -   odcitani
    -   nasobeni
    -   deleni
    -   viz [698 - Architektura pocitacu](../../uai/698-architektura_pocitacu-1/README.md)
-   kodovani znaku
    -   ASCII - American Standard Code for Information Interchange
        -   znaky zakodovane do 8bitu (jen 256 moznosti)
    -   ISO normy - 8859-1, 8859-2
    -   CP1250 - kodova stranka pro jazyky stredni a vychodni evropy (latinka) ve starsich windows
    -   unicode - UTF-7, UTF-8, UTF-16, UTF-32
        -   promenlivy pocet bitu, napr. UTF-8 pouziva 8bitu pro prvnich 128 znaku ASCII a az 4byty pro dalsi znaky
-   reprezentace znaku se znamenkem
    -   doplnkovy kod
        -   kladne cislo - pouze prevedene do binarni
        -   zaporne - binarni zapis znegovan a pote prictu 1
        -   rozsah -128 az 127
    -   primy kod
        -   prvni bit udava znamenko (1 pro zaporne cislo, 0 pro kladne)
        -   existuji 2 nuly - rozsah pro 8bitu je tedy <-127,127>
    -   aditivni kod
        -   zvolim cast kterou pridavam
        -   kladne - `127 + x` => prevedu do bin
        -   zaporne - `127 - x` => prevedu do bin
        -   rozsah pro 8bitu je tedy <-128,127>
-   plovouci radova carka
    -   `A = m*z^e` - m = mantisa, z = zaklad, e = exponent
    -   norma IEEE 754
        -   **float 32** - 32bitu, znamenko 1bit, exponent 8bitu (aditivni kod), mantisa 23bitu
        -   **float 64** - 64bitu, znamenko 1bit, exponent 11bitu (aditivni kod), mantisa 52bitu

### 2. Kombinační a sekvenční logické obvody, formální popis logických obvodů booleovskými funkcemi a konečnými automaty.

-   kombinacni a sekvencni logicke obvody
    -   stavebni prvky
        -   AND
        -   OR
        -   NAND
        -   NOR
        -   XOR
        -   inventor
    -   kombinacni
        -   vystup zavisi pouze na aktualnim vstupu
        -   spojovanim prvku se tvori slozitejsi funkce
        -   zpusob zapisu
            -   pravdivostni tabulka
            -   schema pomoci logickych symbolu
            -   algebraicky tvar
                -   UNDF - soucet disjukci mintermu
                -   UNKF - soucet konjukci maxtermu
        -   chovani kombinacniho obvodu v case
            -   obvod ma zpozdeni v case `Td`, to je doba mezi zmenou vstupu a tomu odpovidajici zmene vystupu (kazde hradlo ma zpozdeni)
            -   celkove zpozdeni je zavisle na tom, ktere bity se na vstupu meni (meli bychom pocitat s nejvetsim moznym zpozdenim)
        -   zakladni obvody
            -   pulscitacka
            -   uplna scitacka
            -   vicebitova scitacka
            -   multiplexor - elektronicky clen fungujici na principu prepinace (pomoci vstupu S vybere jeden ze vstupu A...N na vystup Y)
            -   komparator - porovnava 2 vstupy a na vystup dava 1 pokud se rovnaji, 0 jinak
            -   nasobicka - obsahuje n^2 scitacek, scitacky tvori n seriove razenych stupnu - znacne zpozdeni
    -   sekvencni
        -   vystup zavisi na vstupu a na posloupnosti minulych vstupu (vstupni historii) - tim se lisi
        -   hodinovy signal
            -   signal ktery se periodicky meni mezi 0-1 s urcitou frekvenci
            -   urcuje okamzik provedeni a take rychlost provadeni navaznych operaci
            -   zakladnim udajem je frekvence `fclik` - casto se uvadi hodinova a urcuje take vykon procesoru
                -   32768Hz nizkoprikonove mikrokontrolery
                -   500kHz - 200 MHz mikrokontrolery
                -   200MHz - 1GHz embedded procesory (ARM, X86)
                -   > 1GHz serverove a desktopove procesory
        -   zakladni obvody
            -   synchronni klopny obvod R-S (hladinovy)
                -   z nej se odvozuji dalsi obvody
                -   synchronni = je synchronizovan hodinovym signalem
                -   hladinovy = vystup Q se muze menit po celou dobu kdy je `clk` v 1 -> musi se zajistit stabilni vstupy po celou tutot dobu (aby doslo nanejvys k jedne zmene)
                -   resi se hranovymi klopnymi obvody, ktere meni vystup Q pouze pri zmene hodinoveho signalu -> takove obvody jsou ale konstrukcne narocnejsi a obsahuji vice RS klopnych obvodu
            -   hranovy klopny obvod typu D
                -   je to jednobitova pamet, obvod si pamatuje posledni hodnotu zapsanou na vstup D
                -   `D` - pri vzestupne hrane na CLK prepise vstup D na vystup Q
                -   `CLK` - hodinovy signal
                -   `RESET` - pri 0 na tomto vstupu nastavi vystup Q na 0 bez ohledu na dalsi
        -   popis sekvencnich obvodu - konecne automaty
            -   konecne automat KA = (mnozina stavu, vstupni abeceda, vystupni abeceda, prechodova funkce, vystupni funkce, pocatecni stav)
            -   dostava symboly ze vstupni abecedy na vstup a produkuje symboly z vystupni abecedy na vystup
            -   typy
                -   Moore - vystupni hodnoty ulozene ve stavech v tzv. tabulce vystupu
                -   Mealy - vystupy zalezi na stavu a na vstupnim symbolu
                -   hw realizace
                    -   ![](../../uai/698-architektura_pocitacu-1/hw-ka.png)

### 3. Základní architektury počítačů, funkční bloky, funkce řadiče, aritmetickologické jednotky, registrů a vstupně/výstupních jednotek.

-   zakladni architektury pocitacu
    -   Von Neumannova
        -   instrukce a data jsou ulozena v operacni pameti
        -   nelze odlisit data a instrukce
        -   ![](../../uai/698-architektura_pocitacu-1/neumann.png)
    -   Harvardska
        -   intrukce a data fyzicky oddelena - 2 pameti
        -   ![](../../uai/698-architektura_pocitacu-1/harward.png)
-   PC procesorem 8096
    -   ![](../../uai/698-architektura_pocitacu-1/pc-schema.png)
-   soucasne PC + schema blokove centralni procesorove jednotky
    -   ![](../../uai/698-architektura_pocitacu-1/pc.png)
    -   ![](../../uai/698-architektura_pocitacu-1/cpu.png)
-   funkcni bloky
    -   radic
        -   ridi cinnost jednotlivych casti pocitace pomoci ridicich signalu, ktere jednotlivym modulum zasila
        -   realizuje instrukcni cyklus
        -   **obvodovy** - konecne automat, D-klopne obvody + kombinacni logika
        -   **mikroprogramovatelny** - realizuje slozitejsi intrukce, ma pamet pro ulozeni mikroinstrukci -> instrukce procesoru realizovana sadou mikroinstrukci
    -   registry procesoru
        -   udrzuji stav procesoru
        -   slouzi pro odkladani mezivysledku
        -   tvori operandy aritmetickych a logickych operaci
        -   typy
            -   PC - program counter
                -   adresa instrukce, ktera je nactena do instrukcniho registru
                -   je menen instrukcemi pro rizeni instrukcniho toku (skoky, volani podprogramu)
            -   IR - instrukcni registr
                -   do IR se nacita instrukce, ktera bude provedena v ramci instrukcniho cyklu
                -   programatorsky nepristupny
            -   PSW - program status word
                -   stavovy registr (stavova informace a priznak procesoru)
            -   SP - stack pointer
                -   ukazatel zasobniku
    -   ALU - Aritmeticko-logicka jednotka
        -   provadi aritmeticke (scitani, nasobeni, posuv) a logicke (logicky soucin, negace ..) operace
    -   jednotka spravy pameti
        -   umoznuje pristup do virtualni pameti
        -   preklada virtualni adresy na fyzicke
        -   kontrola koherence cache
        -   prepinani mezi pametovimi bankami
    -   pameti RAM a ROM
        -   RAM - Random Access Memory
            -   elektronicka polovodicova pamet
            -   pouziva se hlavne jako operacni pamet pocitacu (ulozene bezici programy, vcetne systemu a jejich data)
            -   obsah v soucastnosti pouzivanych polovodicovych RAM se po odpojeni napajeni vymaze (volatilita), proto se data ukladaji na disky nebo do flash pameti (neni volatilni)
            -   temer okamzite cteni a zapis jakekoliv pametove bunky
            -   pocet zapisu a cteni neni omezeny
        -   ROM - Read Only Memory
            -   elektronicka polovodicova pamet
            -   trvale ulozena data, energicky nezavisle, pouze ke cteni, data jsou ulozena bud vyrobcem nebo u pameti typu PROM je mozne data jednou zapsat
            -   data je mozne menit u elektricky reprogramovatelnych EEPROM, FLASH

### 4. Instrukční cyklus počítače. Jednotlivé fáze zpracování instrukce. Pojem proudové zpracování (pipelinig) instrukcí.

-   instrukcni cyklus
    -   IF - Instruction Fetch
        -   nacteni binarne zakodovane instrukce z pameti do instrukcniho registru
    -   ID - Intruction Decode
        -   dekodovani instrukce
        -   dekoder instrukci - kombinacni obvod, ktery je schopen na zaklade operacniho znaku urcit o jakou instrukci se jedna, prip. o jakou skupinu instrukci
    -   OF - Operand Fetch
        -   nacteni operandu z registru nebo pameti na vstup ALU
    -   EX - Execute
        -   vykonani instrukce, ALU provede vsechny instrukce
        -   vystupem je vysledek operaci, napr u `ADD` je to soucet operandu
    -   WB - Write Back
        -   zapsani vysledku ALU do pameti nebo registru
    -   Interuption detection
        -   test zadosti o preruseni
        -   preruseni je rezim, ve kterem procesor vykonava specifickou, predem danou cast programu, tzv. obsluhu preruseni (interrupt handler)
        -   vykonani obsluhy se provadi na zaklade vnejsiho signalu (typicky ozn. INT), ktery nazyvame zadosti o preruseni
        -   pokud zadosti muze byt vyhoveno, provede se specialni instrukcni cyklus odpovidajici instrukci volani podprogramu
-   typy instrukci
    -   aritmeticke: ADD (soucet), SUB (rozdil), MUL (soucin), DIV (podil), CMP (compare - porovnani)
    -   logicke: AND, OR, COM (complement - negace), XOR
    -   posuvy: SHL/SHR/ASR (posun vlevo, vpravo, aritmeticky v pravo), ROL/ROR (rotace vlevo, vpravo), RLC/RRC (rotace vlevo, vpravo pres carry)
    -   skokove instrukce: JMP (nepodmineny skok), JZ/JC (podminene skoky), CALL (skok do podprogramu), RET/RETI (navrat z podprogramu/preruseni)
    -   presuny: MOV, XCH (exchange - vymena)
-   paralelni zpracovani instrukci (pipelining)
    -   mame 4 jednotky: IF, ID, EX, WB
    -   ty pracuji paralelne a kazda jednotka v danem taktu zpracovava sobe odpovidajici fazi instrukcniho cyklu, ale jine instrukce nez ostatni jednotky
    -   cinnost pripomina vyrobni linku, kde kazdy pracovnik provadi stale stejnou operaci a polotovar preda dal, na konci je hotovy vyrobek
    -   zpusob zvyseni vykonu procesoru soucasnym provadenim nekolika casti strojovych instrukci
    -   zakladni myslenkou je rozdeleni zpracovani jedne instrukce mezi ruzne casti procesoru a tim i umozneni zpracovani vice instrukci najednou
    -   ![](./instrukce-pipelining.png)
-   faze zpracovani instrukci je rozdelena min. na 2:
    -   IF - nacteni a dekodovani instrukce
    -   EX - provedeni instrukce a pripadne ulozeni vysledku

### 5. Soubor instrukcí: typy a kódování instrukcí, adresní módy. Zásobník procesoru. Rozdíly v souborech instrukcí v architekturách CISC a RISC.

-   typy instrukci
    -   aritmeticke: ADD (soucet), SUB (rozdil), MUL (soucin), DIV (podil), CMP (compare - porovnani), INC/DEC (inkrement, dekrement o 1)
        -   typicky nastavuji priznaky
            -   C - Carry (prenost do vyssiho radu)
            -   Z - Zero (nulovy vysledek)
            -   OV - Overflow (preteceni doplnkoveho kodu)
            -   N - Negative (zaporny vysledek)
    -   logicke: AND, OR, COM/NOT (complement - negace), XOR
    -   posuvy: SHL/SHR/ASR (posun vlevo, vpravo, aritmeticky v pravo), ROL/ROR (rotace vlevo, vpravo), RLC/RRC (rotace vlevo, vpravo pres carry)
        -   SHR - Shift Right - posune vsechny bity vpravo, zleva doplni 0 a vypadly bit dava do Carry
        -   ASR - Arithmetic Shift Right - posune vsechny bity vpravo, zleva doplni znamenkovy bit (nejvyssi rad), vypadly bit dava do Carry (chova se jako deleni 2)
        -   SHL - Shift Left - posune vsechny bity vlevo, zprava se doplni 0 a vypadly bit se dava do Carry
    -   skokove instrukce
        -   nepodminene - skok na adresu proveden vzdy
            -   JMP (nepodmineny skok)
    -   podminene - skod se provede pri splneni podminky
        -   EQ (equal), NEQ (not equal), JZ, JC, GE (greater or equal), LT (less than), CALL (skok do podprogramu), RET/RETI (navrat z podprogramu/preruseni)
    -   asolutni - instrukce nebo registr udava primo adresu kam se skoci, PC (program counter) se primo nastavi na adresu
    -   relativni
        -   instrukce udava hodnotu, ktera se pricte k aktualnimu PC
        -   skok je nezavisly na presunu programu v pameti
        -   realizovano pomoci PC = PC + offset
        -   posunuti provadeji prekladace (programator se o to nemusi starat)
    -   presuny: MOV, XCH (exchange - vymena)
        -   MOV - move
            -   typicky presun z registru do registru, registru do pameti, pameti do registru nebo pameti do pameti - ne vsechny moznosti mohou byt u daneho procesoru podporovany nebo vazany na instrukci MOV
            -   funkce odpovida prirazeni v programovani `r1 = r2`, tedy zdrojovy operand `r2` se nemeni a cilovy `r1` se prepise na novou hodnotu (neodpovida unixovemu `mv`!)
-   kodovani instrukci
    -   instrukce s pevnou delkou - 32bit, minoritne i delsi 64bit instrukce
    -   instrukce s promennou delkou - delka se meni od 1 bytu az po nekolik bytu
    -   slozeni
        -   operacni znak - jednoznacne urceni instrukce
        -   operandy - zrojovy registr, cilovy registr, prima adresa, prima konstanta, adresni mod (primy/neprimy, inc/dec atd), relativni nebo absolutni adresu
-   adresni mody
    -   registrovy - `MOV R1, R2`
    -   prima konstanta - `MOV R1, 100`
        -   naplneni registru (nebo pameti) konkretni hodnotou
    -   prima adresace - `LD R1, 100` (Load)
        -   adresa do pameti, ktera je soucasti instrukce
    -   neprima adresace - `LD R1, [R2]`
        -   adresa se typicky ziskava z registru (u nekterych procesoru i varianta z pameti)
        -   s postinkr - `LD R1, [R2+]`
        -   s preinkr - `LD R1, [+R2]`
        -   s postdekr - `LD R1, [R2-]`
        -   s predekr - `LD R1, [-R2]`
    -   neprima adresace s bazovym registrem a posunutim - `LD R1, [R2+100]`
    -   neprima adresace s bazovym registrem, index registrem a posunutim - `LD R1, [R2+R3+100]`
    -   neprima adresace s bazovym registrem, index registrem, meritkem a posunutim - `LD R1, [100+R2+R3*2]`
    -   PC relativni
-   zasobnik procesoru
    -   procesory implementuji zasobnik, ktery je ulozen v pameti
    -   vyclenen registr SP - Stack Pointer - ukazaten na vrchol
    -   zasobnik roste shora dolu (x86) nebo zdola nahoru (nektere mikrokontrolery)
    -   ukladaji se na nej navratove adresy pro skoky do podprogramu, ale take parametry a lokalni promenne funkci nebo obsahy registru
    -   priklad:

```
0000: call 0002
0001: jmp  0001
0002: call 0005
0003: add  r1, #5
0004: ret
0005: mov  r1, #10
0006: ret
```

-   program zacina po resetu na 0x0000, skoci na 0x0002 (na zasobnik dava 0x0001, kde bude potom pokracovat), skoci na 0x0005 (na zasobnik dava 0x0003, kde bude pak pokracovat), presune 10 do r1, ret -> vraci se na adresu ze zasobniku, tj. 0x0003, pricte 5 k r1, ret -> vraci se na adresu ze zasobnik 0x0001, nekonecna smycka

-   architektury
    -   ISA - Instruction Set Architecture
        -   soubor instrukci tvorici rozhrani mezi SW a HW
        -   architektura souboru instrukci ma zasadni vliv na architekturu procesoru
        -   zahrnuje
            -   registry procesoru
            -   instrukcni sadu: datove typy, operace, specifikace operandu, adresni rezimy
            -   kodovani instrukci do bin. podoby
            -   adresni prostory
            -   vyjimky
        -   typy
            -   stradacova (mikrokontrolery)
                -   ma jeden registr (stradac), ktery je implicitne vyuzivan aritmetickymi a logickymi operacemi
                -   ty maji jako jeden operand stradac a druhy je cten z pameti
                -   jednoduchy HW
                -   prilis mnoho MOV operaci (vkladani operandu do stradace, uklizeni vysledku ze stradace do pameti)
            -   zasobnikova (Java VM)
                -   operandy jsou na vrcholu zasobniku, operandy se odstrani a vysledek vlozi na zasobni
                -   instrukce maji kratke kodovani (u aritmetickych operaci staci jen operacni znak)
                -   skokove instrukce stejne jako u jinych ISA
                -   nevyhodou je prilis mnoho presunu pamet <-> zasobnik
                -   zasobnik lze hw realizovat jen s omezenou hloubkou - je treba odkladat (spilling) a naplnovat (filling) z pameti
            -   registrova
                -   obecna - aritmeticke operace mohou mit jeden operand z pameti
                -   load-store (RISC procesory) - aritmeticke operace jsou provadeny pouze mezi registry, pro pristup do pameti se pouzivaji instrukce `ld` nebo `st`
                -   register-memory (x86)
                -   memory-memory (vyjimecna)
    -   CISC - Complex Instruction Set Computer
        -   pocitac s rozsahlym souborem instrukci
        -   casto obsahuje specializovane instrukce (slozite) a je jich hodne
        -   vyhody
            -   snizena cetnost nacitani instrukci
            -   moznost vicenasobneho vyuziti funkcnich jednotek v ruznych fazi vykonani instrukce
            -   diky mikroprogramovemu radici muzeme zmeni instrukcni repertoar
        -   nevyhody
            -   potreba slozitejsiho dekoderu instrukci - dekodovani delsi i pro jednoduche instrukce
            -   tezko se zavadi proudove zpracovani instrukci - jsou ruzne dlouhe
            -   vykon zavisi na podilu slozitejsich instrukci
    -   RISC - Reduced Instruction Set Computer
        -   pocitac s redukovanym souborem instrukci
        -   jen jednoduche instrukce, typicky kodovany stejnym poctem bitu
        -   vyhody:
            -   jednoduchost - male mnozstvi instrukci
            -   jednoduchy dekoder + rychly
            -   rychly obvodovy radic
            -   vyssiho vyssiho vykonu se dosahuje proudovy zpracovanim - pipelining

### 6. Vstupně/výstupní operace, vyvolání a obsluha přerušení, přímý přístup do paměti. Registry periferií, mapování periferií do paměti a do vstupně výstupního prostoru.

-   vstupne/vystupni operace
    -   periferie se ovladaji pres registry (napr IDE disk ma 8+2 registry)
    -   registry se mapuji bud do vyhrazeneho vstupne/vystupniho prostoru (PC) nebo se mapuji do pametoveho prostoru (AVR, ARM)
    -   registry periferii nesme podlehat cachovani
    -   u I/O registru muze mit vyznam
        -   zapsani hodnoty
        -   zapis bez ohledu na hodnotu
        -   cteni hodnoty
        -   cteni hodnoty bez ohledu na prectenou hodnotu
    -   neplati co zapisi, to take prectu
    -   hodnota zapsana do registru ovlivnuje dale periferii
    -   vstupne vystupni instrukce In a OUT, napr: `IN DX, EAX; OUT EAX, DX`
-   vyvolani a obsluha preruseni
    -   preruseni
        -   vyvolani podprogramu vnejsi nebo vnitrni udalosti
        -   vnejsi preruseni jsou asynchroni (mohou prijit kdykoliv a v kterekoliv cast provadeneho programu)
        -   kazdy procesor ma typicky jeden vstup pro preruseni, pokud vic tak se pouziva radic preruseni, ktery vybira na zaklade priority
    -   obsluhy preruseni
        -   radic preruseni, ktery vybral na zaklade priority mezi zadostmi o preruseni, informuje procesor o tom, ktery zdroj preruseni vybral
        -   procesor podle toho vybee adresu podprogramu:
            -   bud obsluzne podprogramy maji pevne dane adresy v pameti (mikrokontrolery: AVR, ARM, ..)
            -   nebo v pameti existuje tabulka, kde je kazdemu zdroji preruseni urcena adresa a dalsi informace
    -   typicke vlastnosti obsluhy preruseni
        -   musi byt transparentni (po navratu nesmi zmenit hodnotu zadneho registru procesoru, ani priznakoveho)
        -   zacatek obsahuje radu instrukci `push`, ktere ukladaji registry pouzite v obsluze preruseni
        -   konec obsahuje radu instrukci `pop`, ktere obnovi stav registru ze zasobniku
        -   casto se opakujici preruseni musi obsahovat minimum kodu, jinak se dramaticky snizi vykon celeho systemu
        -   konci instrukci RETI, ktera je podobna jako RET, ale muze integrovat dalsi funkce (napr automaticke obnoveni priznaku, informovani radice o dokonceni obsluhy)
-   primy pristup do pameti
    -   DMA - Direct Memory Access
    -   prenost dat mezi periferii a operacni pameti bez ucasti procesoru, tzn. ze v procesoru nebezi program, ktery by prenos zajistoval, je zajistovan na HW urovni radicem DMA
    -   vyuzivani na radici disku a radici USB
    -   radic DMA musi umet prevzit systemovou sbernici (tj generovat vsechny adresove a ridici signaly pro pamet a periferii), cimz docasne blokuje pristup procesoru k operacni pameti, procesor v radici (zapisem do registru) nastavi typicky pocatecni adresu v operacni pameti, kam se data prenesou, delku prenostu v bytech a odstartuje prenos, radic DMA prerusenim informuje procesor o dokonceni prenosu
    -   u prvnich PC (do zavedeni PCI sbernice) byly DMA prenosy zajistovany radici DMA na motherboardu, dnes jsou soucasti periferie a periferie prebira sbernici (Busmastering)
-   registry periferii
-   mapovani periferii do pameti

### 7. Paměťový podsystém počítače. Typy pamětí a jejich hierarchie. Skrytá paměť (cache), princip činnosti, konstrukce a vliv na rychlost výpočtu. Virtuální adresní prostor, překlad virtuální adresy na fyzickou metodou stránkování. TLB (Translation lookaside buffer).

-   pamet pocitace je zarizeni, ktere slouzi k ukladani programu a dat s nimiz pocitac pracuje
-   zakladni deleni
    -   registry
        -   pametova mista na procesoru, ktera jsou pouzivana pro kratkodobe uchovani prave zpracovavanych informaci
        -   mezi registry a operacni pameti muze byt jeste nekolik vrstev cache (L1, L2, ..)
            -   slouzi ke zkraceni pristupove doby, rychlejsi nez operacni pamet, ale daleko mensi
            -   L1 ~16Kb, L2 ~1MB
            -   prvni pristup na data je dat rychlosti pameti, ale dalsi rychlejsi, doku data nejsou vyrazena z pameti
            -   cache je transparentni - procesor z funkcniho hlediska nepozna jestli je cache pouzita
        -   velmi rychla (pristup < 1ns)
        -   8, 16, 32 nebo 64 bitu
        -   mala velikost (typicky stovky bytu, max jednotky kB)
    -   vnitrni (operacni pamet)
        -   vetsinou pameti osazene na zakladni desce
        -   vetsinou realizovany pomoci polovodicovych soucastek
        -   do teto pameti jsou zavadeny spoustene programy a jejich data, se kterymi se pracuje
        -   rychla (pristup ~ 25ns)
        -   adresovatelna po slabikach (bytech)
        -   velikost desitky az stovky GB
    -   vnejsi pamet
        -   vetsinou externi zarizeni pouzivajici vymenna media jako disky nebo magneticke pasky
        -   dlouhodobe uchovani informaci a zalohovani dat
        -   pomala (pristup ~15ms)
        -   adresovatelna po sektorech (512B)
        -   velikost GB az TB
-   dalsi deleni
    -   material a fyzikalni princip
        -   magneticke - zalozene na magnetickych vlastnostech (informaci uchovava smer magnetizace)
        -   polovodice
        -   magneto-opticke - pomoci svetla se meni magneticke vlastnosti
        -   feritove
    -   cinnosti polovodicovych pameti
        -   dynamicke - periodicky se obnovuji ulozene informace (refresh), informace ulozena v kondenzatoru DRAM
        -   staticke - informace zustava ulozena i bez obnovovani (klopne obvody), SRAM
    -   zavislost na napeti
        -   napetove zavisle
        -   napetove nezavisle
    -   podle schopnost zapisu
        -   RWM - nahodny pristup do pameti
        -   ROM - pouze cteni, data zapsana pri vyrobe
        -   PROM - pouze cteni, data mozne jednou zapsat
        -   EPROM - data mozne smazat a opet nahrat
        -   WMM - jen pro zapis (cerna skrinka)
        -   EEPROM - programovatelna uzivatelem, mazani formou elektronickych impulsu
        -   FLASH - mazani probiha po blocich, omezeny pocet zapisu
    -   podle pristupu
        -   RAM - Random Access Memory - libovolny pristup
        -   sekvencni - doba pristupu je zavisla na umisteni
        -   seriovy - FIFO
-   cache - skryta pamet
    -   plne asociativni
        -   kazde radek fyzicke pameti muze byt ulozen v jakemkoliv radku cache
    -   primo adresovatelna (stupen asociativity = 1)
        -   jeden radek fyzicke pameti muze byt ulozen jen v jednom radku cache
    -   s omezenou asociativitou (stupen asociativity > 1)
        -   jeden radek fyzicke pameti muze byt ulozen jen X radcich cache (?)
        -   polozky se z cache odstranuji
            -   nahodne
            -   FIFO - vzdy vyrazena nejstarsi polozka
            -   LRU (Least Recently Used) - vyrazena nejmene pouzivana polozka, realizovano citacem pro kazdou polozku
    -   optimalizace programu
        -   casova lokalita - program pouziva opakovane data ze stejne adresy
        -   prostorova lokalita - program pouziva opakovane data z blizkych adres
-   virtualni adresni prostor
    -   logicky adresni prostor prirazeny napr. jednomu procesu
    -   do tohoto prostoru se pomoci soustavy tabulek mapuje fyzicka pamet
    -   soucet velikosti virtualnich adresnich prostoru vsech soucasne bezicich procesu mnohonasobne prevysuje velikost fyzickeho prostoru (a tudiz i pameti)
    -   soucet fyzicke pameti namapovane v danem okamziku ve vsech virtualnich adresnich prostorech musi byt mensi nebo roven celkove velikosti fyzicke pameti a velikosti odkladaciho prostoru na disku (swap partition/file)
    -   **zvysuje stabilitu systemu**
        -   adresove prostory procesu jsou oddeleny - chyba v programu nezpusobi prepis dat jineho procesu
        -   podobne lze omezit pristup zapisu nebo cteni do nekterych casti pameti
    -   **odstranuje nutnost relokace programu**
        -   pokud se zavadeji 2 programy do fyzicke pameti, kazdy musi zacinat na jine adrese
        -   prekladac ale tuto adresu potrebuje predem (vypocet absolutnich skoku) a pokud by byl program presunut v pameti, je treba ho relokoval (prepocitat adresy)
        -   ve virtualnim adresnim prostoru mohou byt 2 programy na stejnych adresach, ale na ruznych adresach ve fyzicke pameti
    -   **transparentni mechanismus mapovani diskove pameti do adresnich prostoru**
        -   mechanismy virtualnich adresnich prostoru dovoluji pristupovat k disku stejnym zpusobem jako k hlavni pameti
        -   bezne uzivane sekvencni operace cteni a zapisu do souboru jsou nahrazeny operacemi cteni a zapisu do pameti
        -   pametovymi operacemi muzeme zapisovat a cist z libovolneho mista v souboru a v jakemkoliv poradi
    -   prostor je rozdelen do stranek (bloky pameti o velikosti 4KB)
        -   operacni system modifikuje zaznamy v tabulkach a tim mapuje stranky z fyzicke pameti do stranek ve virtualnim adresovem prostoru
        -   kazdy proces ma sve tabulky stranek, ktere mapuji jinou cast fyzicke pameti - tim se zajisti ze jeden proces nemuze prepsat data jinemu
        -   pokud polozka v tabulce stranek indikuje, ze je stranka odlozena na disku (swap) pak je vygenerovana vyjimka, kterou OS zpracuje tak, ze nacte stranku z disku do pameti - pokud neni v pameti misto, nejdrive odlozi jinou stranku (dlouho nepouzivana)
        -   TLB - Translation Lookaside Buffer
            -   asociativni pamet (castecna nebo plna asociativita), ktera urychluje preklad adres _virtualni_ -> _fyzicka_
            -   klicem je virtualni adresa a hodnotou je fyzicka adresa (napr pri strankovani potrebujeme 2-3 cteci operace nez najdeme fyzickou adresu - podle hloubky hierarchie stranke)
            -   TLB urychluje proces tim, ze v sobe uchovava pary virtualni - fyzicka adresa

### 8. Flynnova klasifikace paralelních systémů (SIMD, MIMD, SISD, MISD), Amdahlův zákon. Měření výkonnosti počítačů, benchmarky.

-   Flynnova klasifikace
    -   umoznuje paralelni systemy tridit z hlediska poctu toku instrukci a dat
    -   systemy s 1 tokem instrukci SI (Single Instruction stream), systemy s nekolika toky instrukci MI (Multiple Instruction stream) a analogicky SD (Single Data) a MD (Multiple Data)
    -   **SISD**
        -   pocitac zpracovavajici data seriove podle 1 programu, typicky von Neumannuv typ
        -   na jednu instrukci pripada jeden jednoduchy datovy typ
        -   typicky jednoprocesorove architektury
    -   **SIMD**
        -   pocitac pouzivajici vetsi mnozstvi stejnych procesoru rizenych spolecnym programem
        -   pritom data zpracovavana v jednotlivych procesorech jsou ruzna, takze kazdy procesor pracuje s jinou hodnotou ale vsechny procesory soucasne provadeji stejnou operaci
    -   **MIMD**
        -   multiprocesorovy system, v nemz kazdy procesor je rizen samostatnym programem a pracuje s jinymi daty nez ostatni procesory
        -   lze si predstavit jako paralelne propojene samostatne pocitace
        -   teoreticky muze kazdy provadet jiny program
        -   vypocet muze byt asynchronni nebo synchronni (pouziti barier)
    -   **MISD**
        -   jedny data jsou postupne zpracovavany vice instrukcemi
        -   typickym zastupcem jsou systolicka pole - specialni architektury (tridici programy, hornerovo schema apod)
-   Amdahluv zakon
    -   zformulovan v r. 1967, kdy si pocitacovy architekt Gen Amhdal pri studiu vykonnosti paralelnich aplikaci vsiml, ze paralelizaci problemu muzeme dosahnout jen relativne velmi omezeneho zrychleni
    -   pokud rozdelime dobu behu na sekvencnim systemu na dve casti `T1 = ts + tp`, kde
        -   `ts` oznacuje cas straveny vypoctem sekvencni casti programu
        -   `tp` oznacuje cas straveny vypoctem casti, kterou je mozne paralelizovat
    -   pokud tedy problem paralelizujeme, muzeme ovlivnit pouze `tp`
    -   v idealnim pripade dosahneme s `N` procesory casu `Tp = ts + tp/N`
    -   zrychleni potom podle definice bude `S = T1 / Tp = (ts + tp) / (ts + tp/N)`
    -   oznacime pomer casu straveneho vypoctem sekvencni casti ku celkovemu casu jako `f = ts / (ts + tp)`
    -   a dostavame rovnici `S = 1 / (f + (1-f) / N) < 1/f`
    -   je dulezite si uvedomit, ze Amdahl uvazuje problem o konstantni velikosti - tzn. s roustoucim poctem procesoru se nemeni rozsah ulohy - neboli jde o vyreseni daneho problemu co nejrychleji
-   mereni vykonnosti pocitacu
    -   merime
        -   dobu behu/odezvy (execution/response time)
            -   cas vykonavani konkretni ulohy
        -   propustnost (throughput)
            -   mnozstvi prace vykonane za jednotku casu
    -   porovnavani vykonnosti dvou pocitacu
        -   pokud X je n-krat rychlejsi nez Y pak: `Px/Py = n`, kde `P` je Performance
        -   pokud X je n-krat rychlejsi nez Y pak doba behu Y je n-krat delsi nez X: `Py/Px = n`, kde `P` je Performance
    -   srovnavaci test je test nebo rada testu, jejichz cilem je zjistit vykon systemu
        -   GPU - typicky narocne vykreslovani 3d scen (video, hry)
        -   CPU - pracovni zatez, rychlost vykonavani pokynu
            -   ruzne CPU muzou mit jine vysledky pro ruzne operace
            -   PCMark10 - sw pro testovani vykonu, spousti radu testu (prace s tabulkami, uprava fotografii, videohovory, fyzikalni vypocty, web browsing apod)
            -   CineBench - zkousi na procesoru vykreslovani videa

### 9. Vícejádrové a vícevláknové procesory, architektura VLIW, procesory pro grafické karty (GPU).

-   vicejadrovy procesor
    -   deve nebo vice nezavislych jader
    -   pro plne vyuziti musi aplikace podporovat multithreading
    -   kazde jadro vlastni ALU -> paralelizace
    -   ![](./multi-core.png)
-   vicevlaknovy procesor
    -   duplikace casti CPU, ktera obsahuje registry
    -   pro aplikace to vyvolava dojem, ze jader je vice a zasilaji pro zpracovani vice instrukci najednou
    -   mimoradne efektivni zpusob zvyseni vykonu hlavne pri pocetnich operacich
    -   aplikace musi umet pracovat s vice vlakny (multithreading) nebo zpracovavat vice uloh naraz (multitasking) aby to vyuzila
    -   kazde vlakno ma svoji sadu registru, ale sdileji vykonne jednotky ALU
    -   ![](./multi-thread.png)
-   VLIW architektura (Very Long Instruction Word)
    -   vice paralelne pracujicich jednotek
    -   explicitne vyjadreny paralelismus
    -   siroke instrukce rozdelene do nekolika sub-instrukci
    -   pouze jedn skokova sub-instrukce na instrukci
    -   tato architektura umoznuje instrukcni paralelismus (vykonavani nekolika nezavislych instrukci soubezne) aniz by CPU musel vyhodnocovat casovou navaznost jednotlivych operaci a mozne kolize
    -   paralelismu je dosazeno sloucenim vzajemne nezavislych operaci v jeden celek (instrukcni paket)
    -   ![](./vliw.png)
-   procesory pro graficky karty - GPU
    -   Graphic Processing Unit
    -   specializovany procesor umisteny na graficke karte nebo integrovan na zakl. desce nebo primo v procesoru (intel HD graphics)
    -   zajistuje vykreslovani dat ulozenych v operacni pameti na zobrazovacim zarizeni (monitor, projektor atd)
    -   chova se plne ja pralelni procesor pri vypoctech
    -   diky tomu mnohdy vyssi vykon nez CPU v nekterych vypoctech (operace s pohyblivou radkou)
    -   obsahuje radu obvodu pro co nejrychlejsi pocitani vypoctu (graficke sceny - vektorove vypocty)
        -   souhrne jsou urychlovaci technologie oznacovany jako graficka akcelerace
        -   unifikovane shadery
            -   pokrocilejsi technika akcelerace
            -   moderni nahrada za pixelove a vertexove jednotky
            -   kazda firma vlastni architekturu shaderu - jsou programovatelne a diky tomu nemusi pocitat pouze data ale i dalsi vypocty (veda, kryptoanalyza - password cracking, bitcoin apod)

## Site

### 10. Linková vrstva (MAC, ETHERNET (MAC adresa, rámec); Přepínač (popis a fungování, přepínání, přepínací tabulka, VLAN).

-   linkova vrstva
    -   druha vrstva ISO/OSI modelu, v TCP/IP modelu je spojena s fyzickou
    -   zajistuje komunikaci mezi dvema nebo vice uzly propojenymi datovym spojem -> proto linkova vrstva neresi hledani cesty -> prenasi data mezi dvema uzly, ktere maji mezi sebou prime spojeni
    -   zabaluje pakety ze sitove vrstvy do tzv. ramcu
    -   komunikace mezi zarizenimi probiha pomoci MAC adres
    -   obsahuje dve podvrstvy MAC a LLC
    -   odlisuje kde zacina a konci ramec prenasenych dat (fyzicka vrstva nerozlisuje jednotlive bity!)
    -   zabezpecuje, aby datovy ramec v poradku dorazil -> potvrzovani (acknowldgement)
    -   flow control - prevence zahlceni prijemce
    -   majoritnim protokolem je Ethernet (II)
        -   konkurencni protokoly vyuzivaji jeho nevyhod pri vysokych rychlostech (infiniband, fibre channel)
    -   **LLC** - Logical Link Control
        -   zabezpecuje zapouzdreni do ramce vyssich vrstev (sitova)
        -   zajistuje multiplexing
        -   detekuje protokol sitove vrstvy (IP atd)
        -   defacto zajistuje, aby jakykoliv protokol z L3 mohl byt transportovan po libovolnem sitovem mediu
        -   zajistuje flow control
    -   podvrstva **MAC** - Media Access Control
        -   pridava adresy do hlavicky ramce, identifikuje medium na lokalni siti
        -   oznacuje zacatek a konec ramce
-   Ethernet
    -   ruzne typy: Ethernet, Fast Ethernet, GigabitEthernet
    -   lisi se rychlostmi ale take narokem na typ linky, ale hlavne zpusobem kodovani signalu (tim padem i frekvenci)
    -   typ ramce je pro Ethernet stejny (jedina vyjimka IEEE 802.3ac)
    -   Gigabit Ethernet - zavadi tzv. Jumbo ramce (Jumbo Frames)
        -   standardni maximalni velikost dat v Ethernet ramci je 1500B
        -   Gigabit zvysuje na 9000B (mensi zatez CPU pri vyssich rychlostech)
    -   ramec
        -   hlavicka (header)
            -   preambule - 7B, slouzi ke snadnemu rozpoznani zacatku prichazejiciho ramce
            -   SFD - Start of Frame Delimiter - 1B oddelovac
            -   cilova MAC adresa - 6B
            -   zdrojova MAC adresa - 6B
            -   typ/delka - 2B
                -   hodnoty <= 1500 oznacuji delku dat (body)
                -   hodnoty >= 1536 oznacuji protokol zapouzdreny v datech (body)
        -   data (body)
            -   46-1500B, pokud jsou data < 46B, tak se zbytek doplni
        -   paticka (tail)
            -   FCS - Frame Check Sequence - 4B, kontrolni soucet
        -   mezera mezi ramci
            -   nutny cas ke zpracovani ramce a pripraveni se na prijeti nasledujiciho
            -   se zvysujici rychlosti ethernetu se snizuje
    -   MAC adresa
        -   jednoznacne identifikuje medium na lokalni siti (oznaceni sitove karty pocitace)
        -   sklada se ze sesti dvojic hexadecimalnich znaku - 6B
            -   napr. `00:1E:4C:5A:FF:58`, `FF:FF:FF:FF:FF:FF` – MAC adresa broadcastu
            -   prvni 3B identifikuji vyrobce, posledni 3B identifikuji dane zarizeni
        -   jednoznacny identifikator na urovni HW nelze meni, je nahran v ROM daneho media - na SW urovni menit lze
-   Switch - prepinac
    -   typicke zarizeni linkove vrstvy
    -   oproti HUBu neposila data na vsechny aktivni porty, ale jen prepina (posila jen na porty, kam je zprava cilena - podle MAC adresy)
    -   umoznuje konfigurovat VLAN (resp. umi cist extra velikost ramce s touto informaci)
    -   ma ulozenou prepinaci tabulku
    -   zmensuje kolizni domenu
    -   **kolizni domena**
        -   cast pocitacove site, ktera je sdilena vice sitovymi zarizenimi
        -   pokud zacne v kolizni domene vice rozhrani zaroven vysilat, dojde ke kolizi a tim ke znehodnoceni signal ua zarizeni musi opakovat vysilani -> pouziti protokolu z rodiny CSMA
            -   CSMA/CD - sdilene => kolize a ruseni
                -   Carrier Sense Multiple Access - nez vysilam, zkontroluju ze nikdo nevysila
                -   Collision Detection - odposlouchavam jestli nevznikly kolize - kdyz jo, dovysilam zpravu a pak nahodne dlouho cekam
        -   pokud se v siti nahradi rozbocovac (hub) prepinacem, pak kazdy pocitac na siti je kolizni domenou jen sam se sebou
    -   **prepinaci tabulka**
        -   relace mezi MAC adresou a portem prepinace
        -   relace si do tabulky plni sam prepinac podle sitoveho provozu
        -   pokud prepinac dostane zpravu pro dosud neznamou adresu, chova se jako hub, z odpovedi pak zjisti komu patri a zapise do tabulky
        -   pokud prijde ramec z portu, kteremu prislusi jak zdrojova tak cilova MAC, pak je ramec zahozen
    -   **L2 switch** - pouziva MAC tabulku viz vyse
    -   **L3 switch**
        -   obsahuje specialni hw
        -   pouziva informace ze sitove vrstvy (cilovou IP adresu)
        -   v podstate se chova jako tradicni router, ale ma nizsi latenci
        -   prepinac nemusi vykonavat pouze fuknci zarizeni linkove vrstvy, ale muze zastavat i zakladni funkce treti vrstvy (sitova)
        -   MLS - Multi Layer Switching nebo smerovac je nutny pro komunikaci jednotlivych VLAN mezi sebou
        -   i levnejsi typy prepinacu maji zakladni management
            -   filtrace MAC adres
            -   porty prepinace maji vlastni MAC adresy
            -   lze sledovat v prikazovem radku i v GUI
-   VLAN - Virtual LAN
    -   rozdeleni fyzicke site (topologie) na logicke casti, at uz na jednom ci vice zarizenich (switch)
        -   usnadnuje spravu, zvysuje moznosti dalsiho zabezpeceni, zvysuje vykon a umoznuje rozdelit sit (napr dle organizaci ve firme apod)
    -   rozsireni standardniho ethernet ramce o dalsi 4B pro ID VLAN - VLAN tagging (= standard 802.1Q)
        -   2B - VLAN Protokol ID
        -   3bity - priorita
        -   1bit - muze byt ramec zahozen pri prehlceni
        -   12bitu - VLAN ID -> max 4096 - 2 ruznych VLAN (2 cisla rezervovana)

### 11. Síťová (Internetová) vrstva (IPv4 (adresa, hlavička, maska, unicast, multicast, broadcast, zápisy, distribuce IP); Základy směrování (popis, princip, TTL, gateway, Djikstra & Belman-Ford)

-   sitova vrstva
    -   resi smerovani provozu mezi vice sitovymi segmenty
    -   prenasi IP datagramy mezi vzdalenymi PC, kazdy datagram ma IP adresu
    -   zakladni jednout je paket, ktery se vklada do linkoveho ramce
    -   zapouzdruje TCP(UDP) segment (ze 4. vrstvy ISO/OSI) do IP packetu (3. vrstva ISO/OSI)
    -   aktivnim prvkem je router (smerovac)
-   IPv4 - Internet Protocol v4
    -   smerovaci protokol v sitovem prostredi
    -   kazdemu sitovemu zarizeni je pridelena IP adresa
    -   32bitu -> 4x8bitu -> 4x<0,256>
    -   zapis v desitkove nebo dvojkove soustave: `192.168.0.1`
    -   specialni adresy
    -   `127.0.0.1` - loopback
    -   `169.254.0.0/16` - lokalni linkove (pouzivaji se pro autokonfiguraci bez DHCP, neroutuji se mimo lokalni sit)
    -   `192.168.0.0/26`, `172.16.0.0/12`, `10.0.0.0/8` - lokalni adresy
-   IPv4 datagram
    -   **hlavicka**
        -   4bity -verze
        -   4bity -delka hlavicky
        -   1B - typ sluzby - TOS - Type Of Service - pouzivano pro VoIP, real-time streaming
        -   2B - celkova delka
        -   2B - identifikace -
        -   3bity - priznaky -
        -   13bitu - offset fragmentu -
        -   1B - TTL -
        -   1B - protokol -
        -   2B - kontorlni soucet (checksum) -
        -   4B - zdrojova adresa -
        -   4B - cilova adresa -
        -   ?B - volitelne moznosti -
        -   ?B - padding - doplneni nul aby bylo s predchozim polem dosazeno 4B
    -   **body** - TCP (UDP) segment
-   maska (pod)site
    -   smerovac podle ni zjistuje, do jake (pod)site dana IP adresa patri
    -   32bitu - zleva same 1 (cast site) a zprava same 0 (rozsah site): `255.255.0.0`
        -   CIDR (Classless Inter-Domain Routing) zapis: IPv4/16 (udava pocet 1 v masce): `0.0.0.0/16`
-   podsite
    -   site jsou deleny do podsiti - subnetu
    -   slouzi k logickemu deleni site do mensich hierarchickych casti
    -   ke spojovani subnetu slouzi routery
    -   historicky se adresy delili podle trid
        -   A - maska `/8`
        -   B - maska `/16`
        -   C - maska `/24`
        -   atd
        -   dnes uz masky jakehokoliv tvaru (ne jen delitelne 8)
-   unicast - proces posilani paketu z jednoho pocitace (zdroj) na jiny pocitac (cil)
-   broadcast - proces posilani paketu z jednoho pocitace (zdroj) vsem pocitacum v dane siti
    -   typicky dotaz na DHCP o prideleni IP adresy
    -   cilova zarizeni paket zpracuji a bud na nej nereaguji nebo zpet komunikuji unicastem
-   multicast - proces posilani paketu z jednoho pocitace (zdroj) vybrane skupine pocitacu
    -   napr vysilani audia a videa (RTP protokol)
    -   hlavnim ucelem je snizeni zateze na linku
-   distribuce IP adres
    -   vyuzivan hierarchicky system
    -   IANA - Internet Assigned Number Authority
        -   prideluje bloky RIR (Regional Internet Registry) -> pro Evropu RIPE NCC
    -   koncovy uzivatel obdrzi adresu od ISP (Internet Service Provider) -> ISP ma rozsahy IP adres od LIR (Local Internet Registry), NIR (National Internet Registry) nebo primo od RIR
-   neverejne/privatni IP adresy
    -   rozsahy adres, ktere se vyuzivaji pro domaci, kancelarske a podnikove LAN
    -   nejsou prideleny zadne konkretni organizaci
    -   `192.168.0.0/26`, `172.16.0.0/12`, `10.0.0.0/8`
-   NAT - Network Address Translator
    -   modifikace zdrojove nebo cilove adresy pri komunikaci privatni sit <-> internet
    -   vytvari de facto relace vnitrnich IP adres a verejnych IP adres
    -   pouzivan pri pristupu vice pocitacu z lokalni site do internetu pres jednu verejnou adresu
    -   muze modifikovat i hlavicky protokolu (cislo portu u TCP, UDP apod)
-   zaklady smerovani
    -   hlavni cil je nalezt optimalni cestu mezi zdrojem a cilem
    -   tedy cestu z jedne site do druhe
    -   optimalni - prutukova kapacita X spolehlivost X vzdalenost
    -   ke smerovani je nutna jak cilova IP adresa tak maska
    -   **TTL** - Time To Live
        -   pocet routeru (hopu), pres ktere muze paket projit, nez je zahozen
        -   pokud TTL klesne na 0 dojde k zahozeni paketu -> zdrojovy PC je informovan ICMP zpravou `Time Exceeded`
        -   ochrana pred zahlcenim site zacyklenymi pakety
    -   **Gateway**
        -   z/do site se dostavame pres branu (gateway)
        -   je to jedna IP adresa z rozsahu daneho segmentu, vetsinou prvni nebo posledni (ale neni pravidlem)
        -   aktivni zarizeni, pres ktere je smerovam provoz z jedne site do jine
    -   **Smerovaci tabulka**
        -   zaznamy se smerovacimi udaji
        -   razeny podle delky masky (prvni jsou konkretni IP adresy s maskou `/32`)
        -   nejkratsi maska `/0` je default gateway - brana do internetu (pouziva se, pokud router nema danou IP adresu v tabulce)
    -   **OSPF** - Open Shortes Path First
        -   link-state algoritmus - zjistuje stav linek
        -   na zaklade Dijkstrova algoritmu sestavi strom nejkratsich cest a pote naplni smerovaci tabulku vyslednymi optimalnimi cestami
        -   lze vytvaret oblasti - smerovani se resi pro kazdou oblast zvlast
        -   topologii site zjistime progledavanim grafu do sirky (prioritni fronta) -> ze zjistene topologie vytvorime pomoci Dijkstrova algoritmu strom nejkratsich cest
    -   **RIP** - Routing Information Protocol
        -   protokol umoznujici routerum komunikovat mezi sebou a reagovat na zmeny topologie site
        -   pouziva Bellman-Forduv algoritmus pro urceni nejkrats cesty v siti
        -   metrikou je vzdalenost - pocet hopu
        -   periodicky zasila informace dalsim routerum - route update
        -   pomala vymena informaci - kazdy uzel ma vlastni smerovaci tabulku
        -   na zacatku vi smerovac jen o sobe
            -   kazdych 30s odesle svoji smerovaci tabulku ostatnim
            -   ostatni si z tabulky aktualizuji svoji tabulku a odeslou atd
        -   maximalni vzdalenost je 16 hopu - eliminace smycek (TTL 16)
        -   nevhodny pro velke site - kvuli vzajemnemu zasilani tabulek hrozi zahlceni
-   DHCP - Dynamic Host Configuration Protocol
    -   automaticka konfigurace adres pocitacu v siti
    -   server prideluje pocitacum hlavne IP adresu, masku site, default gateway a adresu DNS server
    -   na pocitaci bezi klient, ktery prodluzuje platnost udaju
    -   postup
        -   DHCP DISCOVER paket - klient pozada o IP adresu
        -   DHCP OFFER paket - server klienta eviduje a nabidne mu IP adresu
        -   DHCP REQUEST - klient si si vybere nabidnutou IP adresu
        -   DHCP PACK - server potvrdi -> klient ji muze pouzivat
        -   klient musi obnovit pred skoncenim platnosti

### 12. Síťová (Internetová) vrstva (IPv6 (adresa, pravidla zápisu, rozdíly oproti IPv4, anycast, stavová/bezstavová konfigurace, metody tvorby IPv6 adresy, specifické IPv6 adresy, objevování sousedů, IPv6-IPv4/VLAN mapping, privacy extensions, přechodové mechanismy, bezpečnost)

-   sitova vrstva
    -   resi smerovani provozu mezi vice sitovymi segmenty
    -   prenasi IP datagramy mezi vzdalenymi PC, kazdy datagram ma IP adresu
    -   zakladni jednout je paket, ktery se vklada do linkoveho ramce
    -   zapouzdruje TCP(UDP) segment (ze 4. vrstvy ISO/OSI) do IP packetu (3. vrstva ISO/OSI)
    -   aktivnim prvkem je router (smerovac)
-   IPv6 - Internet Protocol v6
    -   128bitu, zapisuje se haxadecimalne 8\*16bitu: `A524:72D3:2C80:DD02:0029:EC7A:002B:EA73`
    -   zapis se muze zkracovat: prvni nula se nemusi psat, kazda dalsi nula lze zapsat jako `::`, ale pouze jednou v adrese (nejdelsi pocet nul)
    -   jako maska jen CIDR, napr `/128`
    -   prvnich 64 bitu
        -   je globalne unikatnich, tj smerovanych bez modifikaci
        -   TLA prideluje IANA
            -   zatim je vyclenen prostor `2000::/3` (tj 1/8 IPv6 prostoru)
            -   pridelovan RIR po `2001::/16` castech
        -   48bitu je globalne routovatelna cast
        -   16bitu prefixe podsite
    -   poslednich 64bitu
        -   je cast pro hosty
        -   musi byt unikatni na dane lince (segmentu)
        -   muze byt odvozeno z 2. vrstvy (MAC)
        -   typy konfigurace
            -   staticka
                -   manualne
                -   EUI-64 (ovozeni z MAC)
            -   automaticka
                -   stavova (DHCPv6)
                -   bezstavova (bez DHCPv6 - SLAAC)
    -   specialni adresy
        -   `0:0:0:0:0:0:0:0` nebo `::/128` - IPv6 neni definovana
        -   `0:0:0:0:0:0:0:0` nebo `::/0` - default route
        -   `0:0:0:0:0:0:0:1` nebo `::/1/128` - loopback
        -   `FE80::/10` - lokalni linkove
            -   pouzivaji se pro autokonfiguraci bez DHCP
            -   neroutuji se mimo lokalni sit
            -   objevovani sousedu (ND) na stejnem sitovem segmentu, objevovani routeru (NA)
        -   `FC00::/7` - lokalni adresa
            -   prefix se vytvari algoritmem
                -   ziska se aktualni cas (64bit NTP format) a EUI-64 adresa (rozhrani aktualne pripojene do site s DHCPv6)
                -   hodnoty se slouci
                -   vpocte se SHA-1
                -   vezme se poslednich 40bitu a pridaji se k prefixu znacici rozsah pro vnitrni sit
-   IPv6 datagram
    -   `4bity` - verze
    -   `1B` - trida
        -   co s paketem pri zahlceni
        -   priorita
    -   `20bitu` - identifikace toku dat
        -   spolu s IP adresou identifikuji smer toku dat
    -   `2B` - delka dat
    -   `1B` - dalsi hlavicka
        -   odkaz na dalsi hlavicku, ktera muze nasledovat za adresou
        -   dalsi hlavicka vzdy obsahuje delku a odkaz na dalsi hlavicku
        -   napr. informace pro smerovace, smerovaci informace, zahlavi fragmentu (smerovace po ceste nemuze fragmentovat), autentizacni hlavicka (IPSec), 58 - ICMPv6
    -   `1B` - pocet hopu - obdoba TTL
    -   `16B` - adresa odesilatele
    -   `16B` - adresa prijemce
-   multicast
    -   rozsireni IPv4
    -   `FF00::/8`
    -   zahrnuje i broadcast (jinak v IPv6 neexistuje)
-   anycast
    -   novinka v IPv6
    -   slouzi k identifikaci skupiny uzlu, ktere maji stejny ucel a data mohou byt vymenovana s kterymkoli z nich
    -   hodi se napr k rozkladani zateze mezi nekolik serveru nebo automaticky vyber nejvhodnejsiho serveru na zaklade spojeni (geograficka lokace)
    -   data jsou odeslana skupine prijemcu, ale doruzena jedinemu (na toho, na ktereho se narazi nejrychleji)
-   unicast
    -   komunikace 1:1
-   DHCPv6 - stavova konfigurace
    -   stavove automaticke nastaveni
    -   poskytuje vsechny udaje kroume default gateway (vychozi brany) - tu poskytuje router
    -   vyzaduje specialni server (narozdil od bezestavove)
    -   postup
        -   velkou cast informaci klient ziska jiz z RA (Router Advertisiment), zbytek od DHCPv6
        -   vysle multicast se zadosti o prideleni sitoveho nastaveni
        -   dale stejne jako DHCPv4
            -   DHCP DISCOVER paket - klient pozada o IP adresu na multicast
            -   DHCP OFFER paket - server/y klienta eviduje a nabidne mu IP adresu/y
            -   DHCP REQUEST - klient si si vybere nabidnutou IP adresu
            -   DHCP PACK - server potvrdi -> klient ji muze pouzivat
            -   klient musi obnovit pred skoncenim platnosti
    -   klient je identifikovan pomoci DUID - DHCP Unique Identifier -> generovan na zakalde HW typu, casu v s a MAC adresy
-   Bezestavova konfigurace
    -   nepotrebuje zadny server
    -   zakladnim pilirem je objevovani sousedu - ND (Neighbour Discovery)
        -   kazdy router rozesila v intervalech do siti, k nimz je pripojen, ohlaseni smerovace - RA (Router Advertisiment)
        -   v nem jsou zakladni informace, hlavne prefixy adres dane site a zda sam router muze slouzit pro predavani paketu ven (default gateway)
        -   z RA se klient dozvi jake adresy pouziva sit a doplni si identifikator rozhrani (64bit) vygenerovany z MAC adresy
        -   pomoci vyzvy sousedum overi klient unikatnost IPv6 (detekce duplicit - DAD)
    -   nektere informace (napr o DNS serverech) mohou byt doplnene z bezestavovych DHCP serveru
    -   ND - Neighbour Discovery
        -   nahrazuje ARP u IPv4
        -   funkce
            -   zjistovani linkovych adres sousednich uzlu ze stejne podsite a jejich aktualizace
            -   hledani routeru
            -   presmerovani
            -   zjistovani sitovych parametru pro automatickou konfiguraci
            -   overovani dosazitelnosti sousedu
            -   detekce duplicitnich adres
        -   ICMP zpravy
            -   RA - Router Advertisiment - zprava routeru o jeho sitovych parametrech (prefix adres)
            -   RS - Router Solicitation - pokud neprichazi RA muze o nej klient pozadat
            -   NA - Neighbour Advertisiment - posila soused aby poskytl informace
            -   NS - Neighbour Solicitation - zadost souseda o ohlaseni
            -   Redirect - doporucuje zdroji aby zpravy posilal pres jineho souseda (umoznuje opravovat nedokonalosti ve smerovaci tabulce)
-   prechodove mechanismy
    -   preference IPv6 nad IPv4
    -   pristupy
        -   dual stack
            -   IPv4 i IPv6 na jednom sitovem rozhrani
            -   tzn obe konektivity
            -   routery jsou nastaveny na podporu obou protokolu
            -   mechanismus se rozhoduje jaky stack pouzije podle cilove IP adresy (z hlavicky paketu)
        -   tunneling - 6in4
            -   IPv6 zapouzdren protokolem IPv4 -> snizeni kvality sluzby
            -   routery musi byt nakonfigurovany na dual stack
    -   preklad IPv6 -> IPv6
        -   vyuziti napr u automatickych tunelu
        -   IPv4 se "prilepi" jako poslednich 32 bitu
-   bezpecnostni mechanismy
    -   AH - Authentication Header - overeni totoznosti uzivatele
    -   ESP - Encapsulation Security Payload - sifrovani datagramu

### 13. Transportní vrstva (TCP (popis, TCP segment, hlavička, navazování spojení, služby a porty, velikost okna); UDP (popis, UDP datagram, pseudo IPv4, kontrolní součet, služby a porty, výhody, nevýhody)

### 14. Aplikační vrstva (Protokoly (DHCP, HTTP, FTP, POP3, IMAP, SMTP, SSH, SSH, TELNET); DNS (popis a pojmy, systém a hierarchie DNS, iterace, TLD, doména, DNSSEC, útoky)

## Operacni systemy

### 15. Jaké jsou základní funkce operačního systému? Popište stručně zavedení operačního sytému u PC. Jak se dělí operační systémy? Uveďte příklady. Pojmy multitasking, monotasking, multiuser, monouser, real time.

### 16. Metody správy paměti pomocí jedné souvislé oblasti a pomocí sekcí. Pojmy vnitřní a vnější fragmentace paměti.

### 17. Metoda správy paměti pomocí stránkování a stránkování na žádost. Virtuální paměť. Segmentace paměti. Správa paměti v reálných OS.

### 18. Stavový diagram procesů, jednotlivé stavy. Pojem multitaskingu a jeho druhy. Správa procesů v reálných OS.

### 19. Vysvětlete pojmy synchronizace procesů a kritická sekce. Popište algoritmy pro přístup do kritické sekce. Synchronizační problémy a jejich řešení.

### 20. Deadlock. Vysvětlete příčiny deadlocku. Jak lze deadlocku zamezit. Jak je řešen deadlock v reálných OS.

### 21. Pojmy adresářová struktura, soubor, souborové systémy. Metody plánování disku. Ochrana souborů a adresářů v reálných OS.

## OOP

### 22. Syntaxe a sémantika programovacího jazyka. Pojem proměnná, její deklarace a inicializace. Rozsah platnosti proměnných (lokální, statické). Syntaktická kategorie příkaz a výraz v programovacím jazyce.

### 23. Datové typy. Primitivní a objektové datové typy. Statické a dynamické typy. Substituční princip.

### 24. Řídící struktury jazyka. Složený příkaz resp. blok. Příkazy pro větvení programu (podmíněný příkaz - varianty, vícenásobné větvení). Příkazy cyklu a možnosti jejich ukončení. Strukturování programu pomocí metod, rekurzivita.

### 25. Základní pojmy objektově orientovaného paradigmatu. Objekt, třída, metoda a její parametrizace, zapouzdření. Vznik a zánik objektu. Přetěžování metod a operátorů.

### 26. Dědičnost a polymorfismus. Hierarchie objektů. Vztah nadtřída a podtřída. Překrývání metod a dynamické vyhledávání metody při běhu programu. Polymorfní proměnné a způsoby polymorfního zpracování objektů.

### 27. Abstraktní třídy, interfejsy a jejich implementace. Návrh tříd s využitím principu abstrakce a specializace, programování vůči interfejsu.

## Databaze

### 28. Databázový systém, data, databáze, systém řízení báze dat (SŘBD/DBMS), typy DBMS, relační, objektové, postrelační DBMS.

### 29. SQL, historie jazyka, DDL, DML, DCL, TCL.

### 30. Postup návrhu databáze, analytik, designér, kodér, analytický model, konceptuální model, fyzický model.

### 31. Entitně relační model, entita, instance entity, relace, atribut, kardinalita relací.

### 32. Relační datový model, relace (tabulka), primární a cizí klíč (vztahy mezi tabulkami), atribut.

### 33. Normalizace relačního modelu, 1. až 3. NF, problematika funkčních závislostí.

### 34. Transakční zpracování dat, transakce, ACID – pravidla, transakční žurnál.

### 35. Řízení konkurenčního přístupu k datům, binární zámky, víceúrovňové zámky, časové známky, multiverze.
