# Otazky ke SZZ

## Okruhy

-   [Informacni a komunikacni technologie](./informacni_a_komunikacni_technologie/README.md)
    -   [prf doc](https://www.prf.jcu.cz/images/PRF/fakulta/katedry/uai/pro-studenty/szz/bakalarske-studium/6020ikt.pdf)
-   [Teoreticke zaklady informatiky](./teoreticke_zaklady_informatiky/README.md)
    -   [prf doc](https://www.prf.jcu.cz/images/PRF/fakulta/katedry/uai/pro-studenty/szz/bakalarske-studium/6068tzi.pdf)
-   [Web a multimedia](./web_a_mutlimedia/README.md)
    -   [prf doc](https://www.prf.jcu.cz/images/PRF/fakulta/katedry/uai/pro-studenty/szz/bakalarske-studium/6030web-a-multimedia.pdf)

## Dalsi info

-   [SZZ organizace a otazky](https://www.prf.jcu.cz/index.php/cz/fakulta/katedry/katedra-informatiky/pro-studenty/statni-zaverecne-zkousky)
