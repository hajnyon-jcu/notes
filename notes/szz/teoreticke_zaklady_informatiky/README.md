# Teoreticke zaklady informatiky

## Algoritmizace a datove struktury

### 1. Pojem algoritmus a jeho složitost – definice algoritmu, časová a paměťová složitost, časová složitost a třídy P a NP, příklady časových složitostí. Klasifikace algoritmů podle použitého paradigmatu (přístupu), principy těchto paradigmat.

-   algoritmus - posloupnost kroku vedouci k reseni dane ulohy
    -   vlastnosti
        -   spravnost
        -   rezultativnost
        -   konecnost
        -   hromadnost (ruzna data)
        -   determinismus
        -   efektivita
            -   casova
            -   pametova
    -   deleni
        -   rekurzivni x iterativni
        -   seriovy x paralelni
        -   deterministicky x nedeterministicky
-   slozitost
    -   vztah velikosti vstupni mnoziny dat k pridelenym prostredkum
    -   nejlepsi, nejhorsi a prumerna slozitos
    -   cas - pocet kroku potrebnych k dokonceni algoritmu
    -   pamet
    -   asymptoticka
        -   odhad slozitosti funkce pro dostatecne velky objem dat
        -   zanedbavaji se konstanty
        -   O(1) konstantni, O(log2N) logaritmicka, O(N) linearni, O(N log N) linearni logaritmicka, O(N^2) kvadraticka, O(k^N) exponencialni, O(N!) faktorialova
        -   ruzne notace O() - horni odhad, dolni odhad, odhad shora i zdola
-   tridy problemu
    -   P - problem muze byt vyresen v polynomialnim case na deterministickem pocitaci
    -   NP - problem muze byt vyresen v polynomialnim case na nedeterministickem pocitaci
    -   NP uplny - nejtezsi problemy z NP
-   paradigma - pristupy
    -   rozdel a panuj - opakovana redukce problemu na mensi problemy dokud nejde vyresit (merge sort)
    -   zrave metody - greedy methods - v kazdem kroku se vybira moznost, ktera se jevi nejlepsi (Kruskaluv algoritmus)
    -   redukce - prevod problemu na jiny problem, jehoz reseni je jednodusi
    -   dynamicke programovani - rozdeleni problemu a ukladani a znovupouzivani vysledku (Pascaluv trojuhelnik)
    -   heuristicke algoritmy - pro problemy kde nezname reseni - aproximace
    -   geneticke algoritmy - napodobovani biologickych procesu

### 2. Abstraktní datové struktury – definice, užití a implementace. Zásobník, fronta, prioritní fronta, lineární spojové seznamy.

-   pole
    -   nejrozsirenejsi dat. str.
    -   seznam zaznamu, indexy, vetsinou v jednom bloku pameti
-   zasobnik
    -   LIFO - last in first out
    -   operace zpet v text editoru nebo prohlizeci, kompilatory, procesor, kontrola gramatiky (syntaxe)
    -   impl. pomoci pole nebo spojovy seznam
-   fronta
    -   FIFO - first in first out
    -   tiskova fronta, obsluha udalosti, commandu
    -   impl. pomoci pole nebo spojovy seznam
-   prioritni fronta
    -   jako fronta ale prvky maji navic prioritu - prvky s vyssi se dostanou na radu rychleji
    -   systemove ulohy, priorita procesu
-   linearni spojove seznamy
    -   zretezeny seznam, kazdy prvek hodnota + ukazatel na dalsi
    -   lze pomoci nej implementovat zasobnik, frontu
    -   vyhody oproti poli: snadna realokace, neni omezena velikost, snadne vkladani
    -   modifikace: cyklicky, obousmerne zretezeni

### 3. Základní řadící algoritmy – Selection Sort, Insertion Sort, Bubble Sort, Heap Sort, Quick Sort – popis, složitost.

-   selection sort
    -   rozdeli pole na 2 casti - setridenou a nesetridenou
    -   z nesetridene vezme nejmensi prvek a da ho na konec setridene
    -   `O(n^2)`
-   insertion sort
    -   rozdeli pole na 2 casti - setridenou a nesetridenou
    -   z nesetridene vezme prvni prvek a hleda pro nej misto v setridene
    -   `O(n^2)`
-   bubble sort
    -   porovnava sousedni hodnoty, v kazdem pruchodu se nejvyssi prvek dostane na konec
    -   `O(n^2)`
-   heap sort
    -   zakladem je binarni halda - binarni strom pro pole
    -   postavime haldu nad zadanym polem, pak iterujeme
        -   vymenime vrchol s poslednim prvkem, utrhneme ho - dostavame nejvyssi prvek
        -   opravime haldu a opakujeme
    -   `O(n log n)`
-   quick sort
    -   rychle rekurzivni razeni
    -   vybere se pivot, podle nej se rozdeli na 2 casti
    -   ty se rekurzivne setridi zase quicksortem
    -   `O(n^2)` ale v prumeru `O(n log n)`

### 4. Binární vyhledávací stromy – definice, užití a implementace, vyvažování, AVL stromy – princip algoritmu.

-   prirozena rekurentni datova struktura
-   sklada se z uzlu
    -   koren - nema rodice
    -   list - nema potomky
-   vyuziti
    -   souborovy system, dedicnost v OOP, diagramy, rodokmeny, hierarchicke struktury obecne
-   binarni strom
    -   uzel muze mit max 2 potomky
    -   ukladani a vyhledavani dat (rychle)
    -   impl. pomoci referenci (podobne jako spojovy seznam)
    -   BST - Binary Search Tree
        -   uzly maji vlastnosti (lze u nich urcit `<`, `>`), uzel nalevo je mensi a uzel napravo je vetsi
        -   potreba vyvazovani - predchazeni shromazdeni dat na jedne strane
            -   jinak ztracime vyhody
        -   u vyvazeneho hledani `O(log n)`
    -   AVL - samo vyvazovatelny binarni strom
        -   aby byl vyvazeny, musi platit ze hloubka jeho deti se lisi max o 1
        -   slozitost vyvazeni `O(log n)`
        -   vkladani prvku - vlozi se na sve misto a cestou zpet se kontroluje vyvazost
        -   rotace doprava/doleva, doleva-doprava
        -   mazani podobne

### 5. Vícecestné stromy, 2-3-4 stromy, 2-3 stromy, B stromy, B+ stromy, popis, použití.

-   definice
    -   kazdy uzel ma max N synu
    -   uzel nesmi mit 1 syna
    -   list nesmi byt prazdny
-   2-3-4 stromy
    -   2-3-4 je pocet deti uzlu
        -   uzel s 1 polozkou musi mit 2 deti
        -   uzel s 2 polozkami musi mit 3 deti
        -   uzel s 3 polozkami musi mit 4 deti
    -   uzel nesmi mit 1 syna
    -   list nesmi byt prazdny
    -   hodnoty zase razeni podle velikosti
    -   vyhledavani - obdoba binarniho
    -   vkladani - nova polozka se vklada vzdy do listu
        -   pokud se po ceste nenarazi na plny uzel vlozi se do listu
        -   pokud se po ceste narazi na plny uzel musi se rozdeli - vyvazeni
-   2-3 stromy
    -   obdoba akorat 2-3 potomci
    -   problem pri deleni, kdy mi chybi 3. prvek - pouziju vkladany
-   B stromy
    -   kazdy uzel max N polozek
    -   kazdy uzel krome korene je alespon zpoloviny zaplnen N/2
    -   vkladani - pokud je list zaplnenm prostredni polozka se posune vys (prip. se pokracuje az ke korenu)
-   B+ stromy
    -   vsechny hodnoty jsou v listech, vnitrni uzly obsahuje pouze klice
    -   listy jsou navzajem propojene
    -   pouziti: NTFS, XFS, velke databaze

### 6. Hašovací tabulky – princip hašování, hašovací funkce, kolize, řešení kolizí, popis složitost.

-   asociuje hasovaci klice s hodnotami
-   vyuziti napriklad pro namapovani na pole - rychlejsi vyhledavani
-   hashovani - mapovani klice na cela cisla (napr. modulo)
    -   genrovani hashkodu - klic -> cele cislo
        -   integer cast, soucet komponent, vypocet polynomu
    -   kompresni funkce - cele cislo -> rozsah [0, N-1]
        -   modulo, MAD - multiply add divide
    -   cilem je rozprostreni klicu na cely interval
    -   dvojite hashovani - zlepseni kolizi
-   kolize
    -   po kompresi nemusi platit ze jednomu kodu odpovida prave jeden klic - kolize
    -   muzeme vyresit spojakem, vkladanim o index nahoru
-   slozitosti
    -   vkladani a hledani prumerne `O(1)`
    -   vsechny klice koliduji `O(n)`
    -   faktor naplneni - cim vice se hodnoty plni, tim vetsi sance kolize

### 7. Základní pojmy z teorie grafů – graf, rozdělení a typy grafů, vážený graf, podgraf, cesta, kružnice, strom, kostra grafu, různé reprezentace grafu a jejich výhody a nevýhody.

-   graf
    -   usporadana dvojice vrcholu V (objekty) a hran E (vztahy)
-   typy
    -   orientovany x neorientovany
    -   jednoduchy x multigraf (vice hran mezi vrcholy)
    -   ohodnoceny x neohodnoceny
    -   souvisle x nesouvisle (neexistuje cesta mezi kazdou dvojici vrcholu)
-   vazeny graf - ohodnocene hrany
-   podgraf - podmnozina grafu (vznikne vyjmutim nekterych vrcholu a hran z grafu)
-   cesta - posloupnost vrcholu a hran, kde hrany navazuji a vrcholy se neopakuji
-   kruznice - vrcholy jsou spojene do kruhu (cyklus)
-   strom - souvisly graf, ktery neobsahuje kruznici jako podgraf
    -   vrcholy stupne 1 = listy
-   kostra grafu - podgraf, ktery je stromem (spojuje vsechny vrcholy grafu, ale neobsahuje kruznici)
-   reprezentace
    -   matice sousednosti - vhodna pro huste grafy, lze pouzit pro orientovane i ohodnocene grafy
    -   matice incidence - vztah mezi hranou a vrcholy, lze pouzit pro orientovane i ohodnocene grafy
    -   seznam sousedu - pro kazdy vrchol mame seznam sousedu - snadne zjisteni sousedu, pridani hrany
    -   podle definic - V = mnozina vrcholu, E = mnozina hran = dvojice vrcholu
-   vyuziti: mapy, struktury, elektronicke obvody, site

### 8. Procházení grafů – procházení grafem do hloubky a do šířky, příklady - složitost.

-   DFS - Depth First Search - prochazeni do hloubky
    -   nejdrive se zanorujeme co nejhloubeji az pak dalsi soused
    -   realizace rekurzi nebo zasobnikem
    -   `O(|V| + |E|)` kde V vrcholy a E hrany
    -   pr: detekce cyklu v grafu, bludiste
-   BFS - Breadth First Search - prochazeni do sirky
    -   nejdrive prohledavame sousedy az pak se vydame cestou
    -   vhodne kdyz je reseni blizko, nevhodne kdyz hledame vsechna reseni (pametove narocne)
    -   realizace pomoci fronty
    -   pr: nejkratsi cesta konem na sachovnici, crawlers, socialni site - pratele
    -   `O(|V| + |E|)` kde V vrcholy a E hrany

### 9. Grafové algoritmy – hledání minimální kostry – Primův algoritmus, Kruskalův algoritmus – principy algoritmů, složitost.

-   minimalni kostra
    -   kostra grafu s nejmensim souctem hran u vazeneho grafu
    -   pokud pridame hranu tak vznikne cyklus
    -   pokud odebereme hranu tak vzniknou 2 grafy
-   primuv algoritmus
    -   vybereme nejkratsi hranu
    -   oznacime jeji vrcholy
    -   dale volime nejkratsi hranu, vedouci do jeste nenavstiveneho vrcholu
    -   slozitost: pole `O(2*|V| + |E|)`
-   kruskaluv algoritmus
    -   nejprve seradi hrany podle velikosti
    -   kostru zacina od prazdne mnoziny hran E0
    -   postupne prochazi serazene hrany
        -   pokud nove konstruovany graf po pridani hrany neobsahuje kruznici, nechame ji tam
        -   jinak hranu nepridame
    -   pokud najdeme |V| -1 hran pak mame kostru jinak graf neni spojity
    -   `O( |E| log|V|)`

### 10. Grafové algoritmy – hledání minimální cesty – Dijkstrův algoritmus, Floyd–Warshallův algoritmus – principy algoritmů, složitost

-   minimalni cesta
    -   cesta mezi dvema vrcholy v ohodnocenem grafu, ktera ma nejmensi vahu
    -   vyuziti pro planovani cest, smerovani paketu atd
-   dijkstruv algoritmus
    -   pamatujeme si navstvene vrcholy a nejkratsi cestu, kterou se k nim da dostat
    -   v kazde pruchodu cyklu vybereme vrchol, jehoz vzdalenost od startu je nejmensi z dosud navstivenych vrcholu
        -   tento vrchol navstivime a aktualizujeme vzdalenosti do jeho sousedu
        -   opakujeme dokud existuje nenavstiveny vrchol
    -   casova slozitost `O(|V|^2 + |E|)`
-   floyd-warshalluv
    -   hleda vzdalenost mezi kazdymi dvema vrcholy (i pokud mezi nimi neni hrana)
    -   postupne projde vsechny moznosti cest mezi kazdymi dvema vrcholy
    -   pokud zjisti kratsi vzdalenost, aktualizuje ji
    -   casova slozitost `O(|V|^3)`

### 11. Vyhledávání v textu – princip vyhledávání hrubou silou, metody Rabin–Karp, Knuth–Morris–Pratt, Boyer–Moore, principy algoritmů - složitosti.

-   hruba sila
    -   pro kazdou pozici v textu kontrolujeme zda nezacina hledanym vzorem
    -   pokud ano, tak hledame nasledujici znak atd
    -   `O(m*n)` - nejhorsi pripad
    -   ma ale dobre vysledky nad velkymi abecedami, ne nad binarnimi soubory
-   rabin-karp
    -   vypocita
        -   kontrolni soucet pro vzor delky _m_
        -   kontrolni soucet pro kazdy podretezec delky _m_
    -   prochazi a porovnava soucty - kdyz shoda -> zkontroluje znak po znaku
    -   kontrolni soucet
        -   zvolime prvocislo _q_
        -   zvolime pocet znaku v abeced _d_
        -   ![crc](../../uai/689-algoritmy-2/rabin-karp-crc.png)
    -   cas - nejhorsi `O(m(n-m+1)`
-   knuth-morris-pratt
    -   vyhledava zleva do prava
    -   posouva se o nejvetsi mozny skok
    -   chybova funkce
        -   nejdelsi prefix P[0..k] ktery je suffixem P[1..k]
    -   cas - optimalni `O(m+n)`
    -   nikdy se neposouva zpet => vyhodny u velkych souboru
-   boyer-moore
    -   zrcadlovy pristup - zaciname na konci
    -   preskakuje skupiny znaku ktere se neshoduji
    -   funkce Last() - zobrazuje znaky abecedy do mnoziny celych cisel
        -   vraci posledni vyskyt znaku
        -   uchovava se jako pole
    -   cas `O(mn+A)` - rychlejsi na vetsi abecede

## Vypocetni inteligence

### 12. Neuronové sítě, základní pojmy – neuron, synapse, axon, dendrit, váha, práh, aktivační funkce.

-   neuronove site
    -   schopnost modelovat libovnou zavislost vstup-vystup
    -   1 vstupni vrstva, 1 vystupni, 1-n skrytych
    -   inpirace cinnosti lidskeho mozku
    -   uci se na zaklade prikladu (pary vstup-vystup) nebo samostent (bez ucitele)
-   neuron
    -   zakladni stavebni prvek (jednotka) neuronove site
    -   imituje biologicky neuron
    -   jedna nebo vice vstupnich - transformovany na jednu vystupni hodnotu
    -   kazdy vstup ma vahu - tou je nasoben, vynasobene vstupy jsou secteny a pricte se k nim hodnota prahu -> slouzi pro vypocet aktivacni funkce - to je vystup neuronu
-   synapse
    -   spojeni dvou neuronu
    -   axon -> dendrit (plasticita - meni se v case - uceni)
        -   inhibicni - snizuje napet - staci aby jedna byla neaktivni a neuron nevypali
        -   excitacni - zvysuje napeti - kdyz je jeich aktivovano vice, neuron vypali
-   axon "vysilac"
    -   vybezek neuronu (jeden), vystup prenosu informace mezi neurony - vede vzruch
-   dendrit "prijimac"
    -   vybezky neuronu na ktere jsou privedeny axony jineho neuronu
-   vaha
    -   priorita vstupu - citlivost na vstup z tohoto dendritu
    -   urcuje do jake miry bude danym vstupem ovlivne vystup neuronu
    -   zmena vah behem procesu uceni
-   prah
    -   prahova hodnota aktivace neuronu (jeli vetsi prah, neuron je aktivovan - excitace)
-   aktivacni funkce
    -   urcuje vystup umeleho neuronu, ma za ukol zavest nelinearitu do neur. siti (sigmoidni, ReLU, softmax)

### 13. Základní modely neuronových sítí - Perceptron a RBF. Typické aktivační funkce neuronů a topologie neuronových sítí.

-   perceptron
    -   nejjednodusi model dopredne neuronove site
    -   jednovrstva neuronova site
    -   reseni linarne separabilnich ukolu
    -   uzivan pri uceni s uciteleme pro uceni binarnich klasifikatoru
    -   **oddeluje data primkami**
-   RBF neurony
    -   v nejjednodusi forme je to trivrstva dopredna neuronova sit
        -   prvni vrstva - vstup do site
        -   druha vrstva - skryta, skladajici se z rady nelinearnich aktivacnich jednotek RBF
        -   treti vrstva - vystup site
    -   aktivacni funkce - Gaussova funkce
        -   reprezentuji shluky dat
        -   vyhoda proti perceptronu - rychlejsi uceni
    -   **oddeluje data kolecky**
-   aktivacni funkce
    -   sigmoidni funkce - vyuzivana pro binarni klasifikaci, vystupni hodnota 0-1
    -   ReLU - Rectified Linear Activation
        -   pokud je vstupni hodnota <= 0 vystup 0
        -   pokud je vstupni hodnota > 0 vystup se rovna vstupu
    -   softmax
        -   vhodna pro multikategorialni klasifikatory
        -   transformuje vstupni vektor hodnot na vystupni vektor hodnot, ktere se pohybuji mezi 0 a 1
        -   soucet hodnot je rovny 1
    -   tanh - hyperbolic tangent
        -   vyuzivana v doprednych neuronovych sitich
        -   casto pouzivany pro binarni klasifikaci
-   topologie
    -   hustota propojeni
        -   uplne - pro kazdy neuron existuje spojeni se vsemi neurony v predchozi a nasledujici vrstve
        -   castecne
    -   pocet vrstev
    -   pocet neuronu ve skrytych vrstvach
    -   dopredna
        -   signal se siri pouze od vstupu k vystupu
        -   jednokrokovy vypocet (kazdy neuron pocitan jen jednou)
    -   rekurentni
        -   ma vnitrni stav
        -   vhodne pro casove rady
        -   krome doprednych spojeni existuji i zpetne vazby
        -   iterativni (opakovany) vypocet

### 14. Učení neuronové sítě – cíl, pojmy: zevšeobecnění, přeučení, problém uváznutí v lokálním minimu, trénovací, testovací a validační množina.

-   cil
    -   optimalizacni uloha
    -   cilem je najit takovou konfiguraci vah, kdy je prumerna chyba site mensi nez pozadovana
    -   casto pouzivane gradientni metody nebo prirodou inspirovane algoritmy
    -   pozadavek na prilis nizkou chybu muze vest k preuceni
-   zevseobecneni - generalizace
    -   schopnost neuronove site spravne reagovat na data, ktera nebyla pouzita behem procesu uceni
-   preuceni
    -   nadmerna doba uceni (prilis mnoho epoch) - muze zpusobit rust chyby pro netrenovana data
    -   nizke chyby v trenovaci mnozine, vysoke pro jina data
-   problem uvaznuti v lokalnim minimu
    -   cenova funkce nema konvexni prubeh - vice minim (lokalnich)
    -   muze se stat ze uvizne v lokalnim minimu
    -   reseni: setrvacnost, nahodne poradi vyberu vzoru z trenovaci mnoziny
-   mnozina
    -   trenovaci - mnozina vzoru urcena k uceni site
    -   testovaci - mnozina vzoru urcena pro testovani
    -   validacni mnozina - testovani site behem uceni - pouziva se pro zabraneni preuceni

### 15. Učení neuronové sítě metodou s učitelem, princip algoritmu zpětného šíření (Backpropagation), parametry algoritmu, delta pravidlo.

-   s ucitelem
    -   zalozeno na minimalizaci chyby mezi momentalni a pozadovanou odezvou neuronove site
    -   roli ucitele hraje pozadovana odezva site, kterou se snazime naucit
    -   site je ucena pomoci paru dat vstup-vystup
    -   v prubehu uceni pomoci zmen vah minimalizujeme chybu
-   backpropagation - algoritmus zpetneho sireni
    -   algoritmus pro upravu vah ve stejny moment
    -   nahodne inicializujeme vahy v siti
    -   vezmeme par vstup-vystup z trenovaci mnoziny
    -   prozeneme siti
    -   spocteme odchylku podle vystupu a pozadovaneho vystupu
    -   upravime vahy
    -   provedeme pro vsechna data
    -   muzeme menit i po davce (nekolika epochach atd)
-   delta pravidlo
    -   gradientni metoda uceni algoritme back propagation
-   parametry algoritmu
    -   rychlost uceni - eta
        -   interval <0-1>
        -   koeficient urcuje o kolik se ma upravit vaha neuronu
        -   vysoka rychlost - oscilace
    -   setrvacnost - alfa
        -   interval <0-1)
        -   koeficient, ktery udava jakou merou se ma do zmeny vahy zapocitat predchozi zmena
        -   muze pomoci se vymanit z lokalniho minima

### 16. Učení bez učitele. Samoorganizující se neuronové sítě (SOM).

-   bez ucitele
    -   algoritmy nemaji na vstupu data provazana s cilovou promenou
    -   cilem je vytvorit vhodnou a zpravidla jednodusi reprezentaci vstupnich dat
    -   site samy v datech objevuji vzory, zavislosti a informace
    -   uziti: trideni dat (clustery), detekce anomalii, asociacnich pravidel (zajimave vztahy, ktere nejsou na prvni pohled videt)
-   SOM - Self Organizing Map
    -   ucene bez ucitele
    -   provadi shlukovou analyzu
    -   z vice rozmernych vstupu vytvori dvourozmernou mapu clusteru (shluku)
    -   podobna data namapovana blizko u sebe
    -   oproti K-means neni treba predem znat pocet shluku
    -   algoritmus
        -   inicializujeme vahy nahodnymi hodnotami
        -   vybereme vektor z ucici mnoziny a dame na vstup
        -   vypocteme vystup
        -   zjistime neuron s nejnizsi odezvou - to je reprezentant
        -   upravime vahy neuronu
        -   udelame pro vsechny vstupy
    -   ruzne usporadani neuronu v SOM (linearni, ctvrcove, hexagonalni, toroid mrizka)

### 17. Rekurentní neuronové sítě

-   site obsahujici zpetne vazby - v grafu jsou cykly (oproti doprednym sitim)
-   uchovavaji vnitrni stav
-   vyuziti - predpoved casovych rad, rozpoznani hlasu, strojovy preklad, generovani textu
-   vypocet rekurentni site probiha tak, ze postupne ji predkladame vzory a ona na jejich zaklade pocita vystupy
    -   do vypoctu se vzdy promitne i hodnota z predchazejiciho kroku
-   pro uceni roztrhneme zpetne vazby a ucime algoritmem backpropagation
-   elmanova sit
    -   rekurentni sit
    -   dvouvrstva s algoritmem backpropagation
-   Hopfieldova sit
    -   jedna plne propojena vrstva - tzn vystup kazdeho neuronu je napojen na vstupy vsech ostatni neuronu krome sebe
-   site s casovym zpozdenim - Time Delay Neural Network
    -   modeluji urcity druh pameti
    -   princip je zalozen na tom, ze vstupni hodnoty jsou do skrytych vrstev predavany s urcitym zpozdenim

### 18. Fuzzy množina a fuzzy logika – funkce příslušnosti, fuzzy pravidla, fuzzyfikace, inference a defuzzyfikace.

-   fuzzy mnozina = neurcita/rozmazana mnozina
    -   nelze rict ze prvek patri do mnoziny, ale muzeme rict ze tam patri s nejakou mirou prislusnosti
    -   prvek muze patrit k vice fuzzy mnozinam (rozsirime mnoziny o spojitou hodnotu prislusnosti)
-   fuzzy logika
    -   odvozena z teorie fuzzy mnozin
    -   logicke vyroky se zde ohodnocuji mirou pravdepodobnosti
    -   logika operuje s hodnotami v intervalu <0,1>
-   fuknce prislusnosti
    -   zobrazeni mnoziny prvku do mnoziny mer prislusnosti
    -   umoznuje matematicky vyjadrit pojmy jako trochu, dost, hodne atd
    -   priklad: velka zima, zima, teplo, horko
-   fuzzy pravidla
    -   pravidla tvori lingvisticky model, vyznam jednotlivych terminu (zima, horko) vyjadruje funkce prislusnosti
-   fuzzyfikace
    -   prevod hodnot promennych na miry prislusnosti
-   inference
    -   vyhodnoceni fuzzy pravidel a ziskani vystupnich fuzzy mnozin
-   defuzzyfikace
    -   prevod vystupnich fuzzy mnozin na vystupni promenne
    -   provadi se nejcasteji vypoctem teziste dane fuzzy mnoziny (graf rozdelime na casti)
-   cely proces
    -   vstupni promenne
    -   fuzzyfikace
    -   pravidla -> inference
    -   defuzzyfikace
    -   vystupni promenne
-   vyuziti
    -   ridici systemy, domacnost, auta, letadla, obecne rozhodovani

### 19. Genetický algoritmus, princip, základní pojmy.

-   geneticky algoritmus
    -   evolucni algoritmus - inspirovany procesy v prirode
    -   darwinova teorie o vyvoji druhu - simulace boje organizmu o preziti
    -   kazdy jedinec je kandidatnim resenim daneho problemu a jeho kvalita je vyjadrena pomoci **fitness funkce**
    -   ukolem algoritmu je vyslechtit takoveho jedince (reseni), pro ktereho bude tato ohodnocovaci funkce vychazet nejlepe
-   princip
    -   nahodne vygenerujeme pocatecni populaci
    -   vyhodnotime zdatnost jedincu
    -   cast jedincu odstranime (nejslabsi)
    -   populaci doplnime o dalsi jedince (krizeni, mutace, reprodukce)
    -   pokracujeme, dokud neni splnena podminka ukonceni
    -   vezmeme jednince s nejlepsi zdatnosti jako reseni
-   pojmy
    -   genom
        -   kodovany predpis jak sestavit jedince
    -   jedinec - zakodovan posloupnosti 0 a 1, jeden zastupce populace
    -   populace - vice jedincu
    -   selekce - vyber jedincu do dalsi populace
        -   casto pouzivana metoda je ruletova selekce (nahodne s pravdepodobnosti danou jejich zdatnosti)
        -   nebo muzeme vzit jen procento nejzdatnejsich
    -   genotyp - soubor vsech genetickych informaci, pri uceni robota jezdit je genotypem informace popisujici jak vytvorit neuronovou sit
    -   fenotyp - soubor znaku chovani jedince (vlohy), schopnost neuronove site ridit robota tak aby projel bludistem
    -   operace
        -   mutace - nahodna zmena genomu (nahodna zmena bitu nebo bitu, prodlouzeni o bit apod)
        -   krizeni - kombinace genomu dvou jedincu (nekde genomy rozpulime a slozime)
    -   diverzita populace
        -   nekdy se muze stat, ze se populace ubira do lokalniho minima
        -   zabraneni: nechat prezit i slabsi jedince, zvyseni mutace na genotypove podobnych jedincich
-   vyhody: proveruje nekolik reseni najednou, na pocatku je cela populace, mezi genotypem a fenotypem muze byt slozity vztah
-   nevyhody: vyskoa pametova a operacni narocnost, zdatnost se musi vyhodnotit vzdy pro vsechny jedince
-   geneticke programovani - vytvareni programu genetickym algoritmem
-   muze se pouzivat pro uceni neuronovych siti - slechteni topologie ale i vah site

### 20. Přírodou inspirované optimalizační algoritmy a jejich principy – Particle Swarm Optimization (PSO), Ant Colony Optimization (ACO).

-   PSO - Particle Swarm Optimization
    -   prirodou inspirovany optimalizacni algoritmus
    -   modeluje chovani hejn ptaku
    -   vhodnu pro spojitou optimalizaci
    -   rada podobnosti s genetickym algoritmem
    -   algoritmus
        -   zaklad je hejno ptaku - castic
        -   kazda castice se pohybuje po spojitem prostoru, kde hledame reseni, ma svoji rychlost a smer
        -   kazda castice vyhodnocuje fitness sveho reseni v kazdem bode prostoru (pamatuje si nejlepsi reseni)
        -   pro vsechny castice se udrzuje globalne nejlepsi reseni - to bude vysledkem
        -   v kazdem kroku se vypocita novy pohyb jedince v zavislosti na poloze, smeru jedincovo reseni, smeru globalniho reseni, nahodne slozky, rychlosti
        -   rychlost je ovlivnena vzdalenosti castice od jejiho nejlepsiho reseni a vzdalenosti od globalniho reseni
-   ACO - Ant Colony Optiomization
    -   prirodou inspirovany optimalizacni algoritmus
    -   inpirace chovanim mravencu pri hledani potravy
    -   algoritmus
        -   mravenec vytvari reseni tim, ze se v urcitych krocich (uzlech grafu) nahodne rozhoduje, kam se vyda
        -   pravdepodobnost je ovlivnea feromonem (tekava latka - vyprchava)
        -   feromona mravenec klade na hrany pokud nasel cil (pri zpatecni ceste)
        -   feromon je urcitou formou pameti
    -   pouziti: logistika, dopravni systemy, smerovani v sitich, reseni obchodniho cestujiciho

### 21. Hluboké neuronové sítě, typické vrstvy (konvoluční, pool, plně propojené), typické aktivační funkce, frameworky pro učení a inferenci (Tensorflow), aplikace.

-   hluboka neuronova sit
    -   architektury modelu
    -   pocitacovy model se uci provadet klasifikacni ulohy primo z obrazku, textu nebo zvuku
    -   mohou dosahovat velmi dobre presnosti
    -   trenovani pomoci velkeho souboru oznacenych dat
    -   obsahuji mnoho vrstev
    -   pouzivaji konvolucni a pool vrstvy - diky tomu jsou schopny zpracovavat velke vstupy
-   konvolucni
    -   probiha zde operace konvoluce - na vstupni snimek je aplikovan zvoleny pocet filtru -> filtr (detektor priznaku)
    -   filtr je ctvercova matice posouvana po pixelech, jednotlive pixely filtru a vstupniho obraku jsou nasobeny a vznikaji priznakove mapy
    -   navrzene tak, ze ctou male casti obrazku a aplikuji na ne porad stejnou operaci
    -   princip
        -   na prvnich vrstvach se chovaji jako detektory hran
        -   v hlubsich vrstvach detekuji slozitejsi objekty
-   RELU
-   Pool
    -   maji za ukol snizit citlivost vstupu na ruzne deformace obrazu (natoceni apod)
    -   slouzi k redukci vrstev (co do rozmeru)
    -   ruzne typy: pocitani prumeru, vybirani maxim - nad priznakovou mapou
-   plne propojena vrstva
    -   napojeni na vystup
-   aktivacni funkce
    -   sigmoidni funkce - vyuzivana pro binarni klasifikaci, vystupni hodnota 0-1
    -   ReLU - Rectified Linear Activation
        -   pokud je vstupni hodnota <= 0 vystup 0
        -   pokud je vstupni hodnota > 0 vystup se rovna vstupu
    -   softmax
        -   vhodna pro multikategorialni klasifikatory
        -   transformuje vstupni vektor hodnot na vystupni vektor hodnot, ktere se pohybuji mezi 0 a 1
        -   soucet hodnot je rovny 1
    -   tanh - hyperbolic tangent
        -   vyuzivana v doprednych neuronovych sitich
        -   casto pouzivany pro binarni klasifikaci
-   frameworky pro uceni
    -   tensorflow
    -   keras, pytorch

## Teoreticka informatika

### 22. Konečný automat – deterministický a nedeterministický automat, regulární jazyk, konstrukce ekvivalentního deterministického automatu.

-   konecny automat
    -   stroj ktery cte sekvencne znaky vstupniho retezce
    -   nema pamet
    -   pri kazdem taktu precte jeden vstupni znak a podle symbolu a vnitrniho stavu prejde do noveho stavu
    -   pokud je po precteni posledniho znaku v koncovem stavu - slovo prijima jinak ne
    -   jazyk prijimany konecnym autmatem = mnozina vsechn slov prijimana konecnym automatem
-   DFA - Deterministicky konecny automat
    -   pro kazdy vstupni symbol ma prave jednu hranu
    -   kazdy krok je presne definovan
    -   vzdy lze urcite v jakem stavu se automat nachazi
-   NFA - Nedeterministicky konecny automat
    -   muze obsahovat vice hran s prechodem pres stejny znak a prazdne hrany (lambda)
    -   nelze vzyd presne urcit jeden stav, ve kterem se automat nachazi
    -   po nacteni stejneho symbolu se muze dostat do vice mist
    -   muze preskocit do jineho stavu bez nacteni symbolu (lambda)
    -   duvod nedeterminismu
        -   pro nektere jazyky se najde snadneji nedeterministicky automat
        -   v praxi se alternativy vyskytuji casto (gramatiky)
-   regularni jazyk
    -   jazyk lze zapsat regularnim vyrazem
    -   regularni vyraz:
        -   sjednoceni `+`
        -   zretezeni `.`
        -   uzaver `*`
-   konstrukce ekvivalentniho deterministickeho automatu
    -   ekvivalence - dva automaty jsou ekvivalentni pokud prijimaji stejny jazyk

### 23. Regulární jazyky – regulární výrazy, vlastnosti regulárních jazyků.

-   regularni jazyk
    -   jazyk ktery jde zapsat regularnim vyrazem
-   regularni vyraz
    -   pro kazdy regularni vyraz existuje nedeterministicky konecny automat
    -   sjednoceni `+`
    -   zretezeni `.`
    -   uzaver `*`
-   vlastnosti
    -   uzavrenost vuci zakladnim operacim
    -   pro kazde dva regularni jazyky existuje algoritmus, ktery rekne zda se rovnaji
-   vyuziti: vyhledavani v textu, manipulace s textem

### 24. Bezkontextové gramatiky a jazyky – vlastnosti bezkontextových jazyků.

-   prepisovaci pravidla jsou pouze ve tvaru `X -> y` kde X je promenna a y retez promennych a terminalnich symbolu
-

### 25. Syntaktická analýza a LL gramatiky.

-   syntakticka analyza
    -   proces ktery pro zadanou bezkontextovou gramatiku a retezec urci, zda retezec patri do jazyka gramatiky
    -   pokud ano, ziskame syntaktickou strukturu retezce v podobe leveho rozkladnu
-   metody
    -   shora dolu - nalezne pripadny levy rozklad dane vety v gramatice
    -   zdola nahoru - nalezne pripadny pravy rozklad dane vety v gramatice

### 26. Turingův stroj – struktura a výpočty.

-   konecny automat doplneny o pamet v podobe pasky
-   usporadana sedmice
    -   Q konecna mnozina stavu ridici jednotky
    -   vstupni abeceda
    -   konecna mnozina symbolu - abeceda pasky
    -   prechodova funkce
    -   pocatecni stav
    -   specialni prazdny symbol
    -   mnozina koncovych stavu
-   nema zadny vstupni soubor, veskera data musi byt zapsana na pasku
-   prechodova funkce na zaklade aktualniho stavu a precteneho symbolu zmeni stav, prepise symbol a posune hlavu na pasce o policko vlevo nebo vpravo

### 27. Algoritmická řešitelnost – Turingova hypotéza, pojem algoritmu, algoritmicky neřešitelné problémy.

-   turingova hypoteza
    -   otazka: "Jsou vypocetni moznosti turingova stroje stejne jako moznost standardniho pocitace?"
    -   hypoteza: kazdy vypocet provedeny jakymkoliv mechanickym prostredkem muze byt take proveden turingovym strojem
    -   neda se formalne dokazat, zatim nikdo nevyvratil
-   algoritmus
    -   Všeobecná pravidla určující transformaci vstupních dat na výstupní
    -   Návod, který určuje postup vedoucí k řešení dané úlohy
    -   Posloupnost kroku vedoucích k řešení dané úlohy
    -   Např. recept – transformuje mouku, vodu, vajíčka na palačinky
-   algoritmicky neresitelne problemy
    -   existuji ulohy pro ktere nemuze existovat algoritmus
