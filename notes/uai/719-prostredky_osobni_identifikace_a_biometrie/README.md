# 719 - Prostredky osobni identifikace a biometrie

- prezentace https://drive.google.com/drive/folders/0B7mEkmpCbAPZbzBrcUh6Rnl4a3c

## Uvod

### Historie

- identifikace/autentizace v osobnim kontaktu
  - spoleha na biometrii - poznani tvare
- identifikace/autentizace bez pritomnosti osoby
  - pecet, podpis, mechanicke klice
- duvodi pro identifikaci
  - pristup k aktivu (truhla)
  - overeni puvodu (dopis)

### Zakladni pojmy

- identifikace - urceni identity (username)
- autentizace - overeni identity (heslo, 2fa //)
- autorizace - odvozeni opravneni podle identity
- zakladni atributy informacnich aktiv
  - duvernost - vyzaduje autentizaci - informace lze vydat pouze opravnenym
  - integrita - vyzaduje autentizaci - lze provadet pouze povolene zmeny
  - zodpovednost -vyzaduje autentizaci - za kazdy pristup musi byt zodpovedny konkretni subjekt
  - dostupnost

### Identifikace a identita

- identita - bez pameti neni
  - na zaklade historie ovlivnuje soucasnost
  - ruzna dle kontextu
  - v IT:
    - vetsinou jednoznacny identifikator
    - sada vlastnosti vazanych k urcitemu subjektu, na zaklade kterych subjekt jedna nebo s nim jednaji ostatni
- identifikace
  - proces urceni identity osoby nebo systemu
  - vysledkem je jednoznacny identifikator
- volba identifikatoru
  - jednoznacny
    - prirozeny - jmeno, prezdivka
    - umely - id
  - vypovidajici hodnota
    - vyznamovy - RC
    - bezvyznamovy - nenese jinou informaci nez unikatnost
- identita ruzna v ramci osoby
  - klient banky - cislo, heslo
  - hrac fotbalu - modry dres + cislo
  - nahodny kolemjdouci - muz, 50 let, modre dziny, zelene triko
  - majitel emailu - adresa
- propojovani atributu identity
  - zadouci - vysetrovani vrazdy
  - nezadouci - poskozeni soukromi, aktualni v dobe soc. siti, problem pro statni IT
- vedoma identifikace - prihlasuju se heslem
- nevedoma - automaticka (cookies, fingerprinting - otisk browseru)
- duvody pro zavedeni identifikace - sprava pristupu
  - k aktivu (cokoli s hodnotou - fyzickou, informacni, jinou)
  - pristup - moznost aktivum pouzivat/menit/nicit/vytvaret

### Autentizace

- potvrzeni identity na zaklade dukazu
  - neco vim - znalost (heslo, postup, RC)
    - subjekt prokazuje znalost hesla
    - riziko zcizeni/kopie - nemuzu overit ze je heslo ukradene
    - zpravidla nutno odhalit pri provadeni autentizace
  - neco mam - vlastnictvi (cipova karta, otp generator)
    - subjekt prokazuje vlastnictvi predmetu
    - typy
      - PKI - public key infrastructure
        - SW - privatni klic na disku
        - HW - privatni klic na cipove karte
      - samotne zarizeni ze ktereho se pristupuje (FIDO, Windows Hello)
      - staticke OTP - GRID tabulka
      - dynamicke OTP - tokeny (cas, citac), mobilni (SMS)
      - autentizacni kalkulator
      - kombinace
    - fyzicky predmet - snadne zcizeni
    - neni mozne vytvorit kopii (krome GRID, SW PKI)
    - bez kombinace s "neco vim" snadne zneuziti
    - nekdy nutnost dalsi hw/dalsi predmet
  - necim jsem - rysy osoby (otisk prstu, duhovka, hlas)
    - porovnavani vzoru s sablonou
    - neni 100% uspesnost - statistika s tresholdem
    - vyhody: mam porad u sebe, male riziko zcizeni, obtizne kopirovatelne (ale otisk prstu?)
    - nevyohdy: nutnost specialniho hw, nekompatibilita ruznych implementaci, neni 100%
- vice faktorova autentizace
  - nizsi komfort, vyssi bezpecnost
  - typicke kombinace: vim + mam (karta + pin), mam + mam (cipova karta s match on-card biometrii)

### Autorizace

- urceni zda ma subjekt po autentizaci opravneni k pristoupeni k aktivu
- modely
  - DAC - Discretionary Access Control
    - k objektu je pripojen seznam opravneni pro jednotlive subjekty (uzivatele, skupiny) s popisem opravneni pro operaci
    - opravnene subjekty mohou poskytovat opravneni dale
    - UNIX opravneni, windows ACL
  - MAC - Mandatory Access Control
    - pristupova opravneni jsou urcovana globalni politikou na zaklade atributu objektu a subjektu
    - SeLinux, ruzna rozsireni UNIXu
    - kazdy objekt ma prirazen bezp. kontext `jméno:role:typ:úroveň`
    - kazdy proces/uzivatel ma bezp. k. `uživatel:role:doména:úroveň`
    - v politice systemu existuje
      - definice uzivatelu, domen ..
      - definice pro oznacovani objektu (labelling)
      - definice povolenych prechodu mezi domenami
      - definice povolenych operaci
  - RBAC - Role Based Access Control
    - kombinace DAC a MAC

### Arch. IS s ohledem na autentizaci a autorizaci

- duveryhodne komponenty
  - ty pod kontrolou provozovatele
  - lze duverovat predavanym informacim a indentifikace je dostatecna
- neduveryhodne komponenty
  - mimo kontrolu provozovatele
  - vzdy je treba pozdovat explicitni autentizaci uzivatele
- pr: prohlizec (neduveryh.) -> aplikacni server (overeni - duveryh.) -> db (duveryh.)
- v ramci systemu lze predavat identifikator bez overeni, ale mimo nej je uz treba autentizace
- princip oddeleni zodpovednosti
  - separation of dutties - SoD
  - vyznamny prvek bezpecnosti systemu
  - jeden uzivatel nemuze sam provadet vsechny citlive oprace
  - pro dokonceni operace je nutna ucast vice uzivatelu
  - napr. rozdeleni vytvareni uzivatelu a pridelovani roli -> jsou treba 2 lidi pro pridani uctu
- autorizace operaci
  - pouziva se pro provadeni citlivych operaci na internetu
  - autentizovana relace je vyuzita pro zadani citlive operace (platba)
  - je vyzadovana dodatecna autorizace (SMS kod, el. podpis ..)

### Clovek nebo stroj

- inverzni Turinguv test
  - casto nutne odlisit uzivatele od robota
  - vetsinou z duvodu zabraneni zneuziti zdroju (spam, pretizeni systemu ..)
  - Turinguv test - uloha na rozpoznani clovek/stroj
  - inverzni - pocitac hada jestli jsem clovek/stroj
    - captcha - rozpoznani pismen z obrazku
      - pocitace lepsi v reseni, popr lidska sila
    - kognitivni testy - zalozene na semantice (psany text, obrazky)
      - napr. sada obrazku a uhodnou odpoved na "Jaky je dnes den"

### Autentizace systemu

- duveryhodnost systemu
  - system ke kteremu se prihlasuji musi byt duveryhodny
  - fyzicka kontrola - muj pocitac, bankomat (jestli nema fake ctecku)
  - internet - zname stranky, https, overena adresa
- autentizace systemu vuci uzivateli
  - https - certifikat serveru
    - vydany duveryhodnou CA
  - kognitivni techniky - personalizovana pozadi, barevna schemata

## Statni identita

[prednaska](https://youtu.be/herjG5qcb5g)

### Narozeni

- rodne cislo (diskutuje se o zmene na bezvyznomovy identifikator)
  - matrika
  - zapis do knihy narozenych (datum, jmeno, pohlavi, rodice)
  - do 3 dnu zapis do AIS (agendovy IS) Evidence osob
    - pouzivaji se agendove ID (ZIFO)
    - propisuje se do ROB - Registru obyvatel
  - vznika ZIFO - zakladni identifikator osoby
    - snaha o ochranu osobnich udaju
  - AIFO - agendovy identifikator fyzicke osoby
    - id pro nejaky okruh - napr. Agenda obcanskych prukazu, Registr vozidel atd. - v kazdem mam jine
- kdyz chci pridat do systemu (Registr vozidel)
  - dam OP -> AIFO
  - podle nej ziskaj ZIFO (UOOU - Urad pro ochranu os. udaju)
  - a vygeneruj dalsi AIFO podle kteryho budu v tom systemu
  - takhle to v praxi uplne nefunguje, ikdyz by mohlo

### Autentizace v beznem zivote

- ID
  - slouzi v zemi vydani
  - u nas obcanka
  - zakladni udaje o osobe pro prokazani identity vuci statni moci
- Cestovni pas
  - totoznost pri cestach do zahranici
  - standardizovany format (zastituje ICAO - mezinarodni organizace civilniho letectvi)
- probiha elektronizace
  - eID, ePassport
  - prevod udaju do strojove citelne podoby
  - musi fungovat ikdyz prestane fungovat elektronicka cast => vizualni inspekce
  - eID - v evrope se zacina standardizovat
  - pridavaji se biometricke udaje
  - poskytovani urcitych typu informaci pri zachovani soukromi (napr. Nemecko - dovrseni 18 v automatech na alkohol)
  - eID v CZ
    - do roku 2017
      - jedina elektronizace byl provazanost na ostatni systemy (el. podpis)
      - BOK - bezpecnostni osobni kod
    - od roku 2017
      - op ma cip (nemusi byt aktivovan)
      - aktivni cip - prihlaseni eidentita.cz
        - muze uchovavat certifikaty, klice nejen pro el. podpis
    - od roku 2022
      - bude mit bezkontaktni cip jako v ePass - ukladani biometrickych udaji
      - nepujde nacist NFC
  - ePas
    - vybaven bezkontaktni cipem (lze cist NFC) - ma stejne udaje jako vytistene v pase + fotku (barevna) + otisky prstu (mohou cist pouze spec. zarizeni s opravnenim a overovat pomoci nich - domluva v ramci EU - postupne se staty domlouvaji)
    - funkcni i bez elektr. casti

### eIDAS

- narizeni o identifikaci a duveryhodnych sluzbach pro el. transakce
- zavadi pojem elektronicke identity do legislativy
- system, kde stat urci identitu a tu prideli osobam (pravnicke i fyzicke)
  - prideluje prostredky elektronicke identifikace (v eIDASu to znamena spis autentizaci)
- standard, ktery by mel zajistit, ze se statem dokazu komunikovat i v jinych zemich
- posuzuje se mira spolehlivosti
  - nizka - jmeno heslo
  - znacka - 2FA
  - vysoka - hw overeni (napr. obcanka)
- praxe v CR - eidentita.cz
  - prihlasovani
    - eObcanka
    - NIA ID (jmeno heslo sms)
    - IIG - cizi obcanka
    - Prvni certifikacni autorita, a.s. - treti strana (ma svou kartu)
    - mojeID - FIDO token
    - CSOB - pribyde, dostala akreditaci
  - moznosti budou pribyvat (banky a soukromnici)

## 2FA

prednasky:

- [prvni](https://youtu.be/kwolXf0yorU)
- [druha](https://youtu.be/qltwo5TGv84)
- [treti](https://youtu.be/3wQb9z_6cpA)

### Proc pouzivame

- posileni bezpecnosti uctu, prihlasovani atd.
- hrozba kradeze udaju, identity atd (platebni karty, bankovni ucty, ucet na soc. siti atd)
  - financni zisk, zisk informaci, neopravnena modifikace informaci
  - napr. ransomware - zasifrovani disku a vydirani
- ruzne druhy
  - phishing - vymameni udaju pres podobny odkaz (prihlasovaci formular ktery vypada jako nejaka stranka)
  - malware -

### Hesla

- vyvoj
  - nejdrive se zvolilo heslo
    - to utocnik dokazal ukrast (ze souboru - plain text)
  - hashovany heslo
    - utocnik pouzil keylogger
  - memory protection - deleni pristupu a rizeni, aby se v pocitaci nedalo delat cokoliv (kdokoliv)
  - zacalo se utocit na siti - napr. nesifrovane spojeni
    - dnes se internet zacina sifrovat skoro vsude (https, letsencrypt)
  - one time password
- moznosti kradeze identity
  - bruteforce
  - slovnikovy utok - zkousim ruzna hesla (primo sady pro prolomeni hesel)
  - odposlech hesel
    - man in the middle - odposlech na siti
      - utocnik na siti vas primeje aby sel provoz pres nej - pak dokaze odposlouchavat
    - man in the browser - odchyceni primo v pocitaci
      - manipulace certifikatu (proxy), podsouvani js kodu (napr rozsireni v chromu) ..
  - socialni inzenyrstvi
  - credential stuffing - zkouseni prozrazenych hesel (uniky hesel)
  - vyuziti chyb v aplikaci
    - cross site scripting (XSS)
    - cross site request forgery (CSRF)
- prohlizece a password managery zahrnuji a varuji pred pouzitim unikleho hesla
  - [haveibeenpwned.com](haveibeenpwned.com)
- obrana
  - fyzicka - ziskani databaze/soubor udaju uzivatelu
  - sifrovani
    - sifrovani celeho uloziste
    - sifrovani dat v db
  - hash funkce
    - HASH - jednosmerna funkce
    - SALT
      - eliminuje detekci stejneho hesla (nahodna hodnota ktera se prida k heslu)
      - ukladam taky do db k heslu
- UNIX
  - `/etc/passwd` - drive ulozene hashe hesel
  - `/etc/shadow` - nyni ulozene hashe hesel pod sudo (vetsinou salt + bcrypt)
- Windows
  - SAM databaze - obsahuje hash
  - dnes pouziva PIN ktery zpristupni TPM modul (zakladni deska)
- utoky na heslo
  - zkouseni hesel - omezeno poctem pouzitim
    - brute force
    - slovnik
    - rainbow attack - vyuziti predpocitanych tabulek pro zlomeni hesla
- silne heslo
  - dlouhe, nejlepe random (password manager), nahodna slova, eliminovat hodne pouzivana slova
  - entropie - mira neusporadanosti/nahodnosti
  - bitcoin deterministic wallet passphrase mnemonic - 12 random slov
- obrana pred utoky
  - sifrovani
  - challenge/response -
  - password authenticated key exchange

### Metody 2FA

- metody
  - heslo - komfortni, lehce napadnutelne
  - SMS OTP - opis, bezpecnejsi
  - PKI SW -
  - PKI - HW
  - online Token
  - Token podpis offline - muze byt placene za transakci
  - kod v Authentifikatoru
- 2FA (Two Factor aut.), MFA (Multi factory aut.)
- zvysuji bezpecnost, ale do urcite miry se snizuje komfort
- vice kanalova autentizace - oddelene kanaly (PC-telefon, PC-dopis atd.)
  - fyzicky - sms, postovni zasilka
  - kryptograficky - autentizacni/autorizacni kalkulator, neni fyzicky oddelen, ale mam dalsi zarizeni
- legislativa
  - eIDAS - pro urovne znacna (sw) a vysoka (hw)
  - PSD2 - regulace poskytovani online fin. sluzbe
    - zavadi _strong customer authentication_ (SCA)
- pri zavadeni bezpecnostnich opatreni se zohlednuje cena-naklady/komfort uzivatele/stupen pozadovane bezpecnosti
- adaptivni autentizace
  - prvky autentizace se mohou menit v zavislosti na podminkach (napr. posilam penize na znamy ucet, nechce po me smsku)
  - vetsinou zavisi na rizikovosti transakce
  - podle PSD2 musi banky toto implementovat
  - ruzne typy
    - komplexni FDS
    - whitelis/blacklist uctu
    - klasifikace uctu
  - je vystavena na centralnich validacnich sluzbach

#### Cipove karty

- karta vybavena cipem, komunikacni rozhrani
- samostatne zarizeni (neni stale pripojeno) - nezavisly prvek mimo IS
- omezene moznosti interakce -> mensi plocha k utoku
- vysoka mira standardizace - setreni penez (vyrobci)
  - ruzne casti standardizace
    1. fyzicke charakteristiky (rozmery, pevnost..)
    2. poloha a rozmery kontaktu
    3. elektricke charakteristiky (napeti, proudy) a zakladni protokoly
    4. struktura souboru a prikazy
    5. ... a mnoho dalsich
- SIM, nova obcanka s cipem, ISIC, platebni karta
- **pametovy cip** - pouze ma ulozenou hodnotu kterou muzu cist/psat
  - telefonni karty, jednoduche RFID tagy (napr obleceni)
- **procesorovy cip**
  - jednocipove procesory (8/16bit), nejaka pamet + vetsinou kryptograficky koprocesor (RSA a alg elipticke krivek), muze tam byt i procesor pro biometrii (otisk prstu)
  - pokrocila logika, lepsi ochrana
  - napr SIM, EMV (Europay, MasterCard, Visa) platebni karty, eID, ePas, OpenCard
- komunikacni rozhrani - pliskove kontakty
  - kontaktni - seriove rozhrani - napajeni (VCC, GRD), CLK - hodiny, I/O
  - bezkontaktni - cip + antena
    - napajeni pomoci ele. magn. indukce
    - ne zcela spravne shrnovana pod RFID
    - ruzne standardy - NFC (14MHz) a dalsi (vstupni karty - 115kHz)
  - kombinovane
    - dualni - jeden cip s dvojim rozhranim (platebni karta)
    - hybridni - 2 cipy na jedne karte

##### Ulozeni dat na karte

- hierarchicky system ukladani
  - nepouzivaji se nazvy, ale 2-bytove identifikatory (tj ~64k hodnot)
  - MF - root slozka (master file)
    - DF - vlastne slozky (dedicated files)
      - EF - soubory (elementary files)
        - transparent - soubor bajtu (dej mi 8.-10. byte)
        - cyclic - cyklicky buffer - typicky karta si pamatuje X poslednich operaci
        - record - zaznamy ktere maji definovanou delku a ptam se na ne (dej mi 4. zaznam)
        - nektere soubory jsou aplikacne zavisle (napr tajne klice - nejdou vycist)

##### Komunikace s kartou

- komunikacni protokol
  - analogie HTTP
  - 7 bytove dotazy
    - CLA - trida instrukce
    - INS - instrukce
    - P1, P2 - parametry
    - Lc field - pocet bytu ktere zapisuju
    - Data filed - data
    - Le field - kolik chci cist dat
  - odpoved
    - data field - data
    - SW1, SW2 - stavove byty o uspechu operace
  - karta a ctecka umi navazat bezp. komunikaci (analogie https)
  - ruzne prikazi: read, write atd
- typicka komunikace
  - reset karty
    - pri vlozeni karty do ctecky - zapnuti
    - ATR - answer to reset - karta rekne co je zac
  - nastaveni bezp. kontextu - napr verify pin (overi se a prejde do stavu overeni a napr. zpristupni nejake operace)
    - navazani bezpecneho kanalu (karta vi ze s nami muze mluvit pres tenhle kanal, protoze jsme se overili)
  - vymena zprav (zapis/cteni)

##### Middleware

- sw ktery umoznuje komunikaci na vyssi urovni
- je treba
  - ctecka - externi, externi s klavesnici (bankomat, terminal), interni
  - usb pripojeni, drive seriova linka
  - ovladac pro OS - usb (CCID profil - standard pro komunikaci - jako HID)
    - PC/SC - low level pristup ke karte
- ruzne zamereny middleware (bezpecnost - kryptografie, eID)
- napr
  - PKCS#11 - kryptograficke operace (generovani klicu atd)
  - PKCS#15 - ukladani rozsahlejsich datovych struktur
  - MS Crypto API - propojeni windows s kartou (program pak dokaze stejn pracovat s klicem v OS nebo v karte)

##### Prakticke pouziti

- karta je mala cast cele infrastruktury
  - karta
  - CMS - Card Management System (vydani karty, predani, stazeni)
  - cteci zarizeni - terminal
  - sw
- karta musi byt zaclenena do celeho systemu
  - nezvladnuto u OpenCard (pouze jeden terminal na nabijeni)

#### OTP generatory

- one time password generator
- hw zarizeni, sw v mobilu
- druhy faktor zavisly na vlastnictvi
- sam o sobe nepodporuje identifikaci uzivatele => pred pouzitim se musim identifikovat
- muze podporovat PIN
- varianty
  - SMS heslo - jednorazove heslo - prokazani vlastnenim SIM karty
    - ochrana GMS siti
    - ochrana bankovnim klicem - rozsifrovani pomoci klice na SIMkarte
- OATH
  - openauthentication
  - standard (potreba vyuzivat napr overovaci server jedne firmy s klicemi jinymi)
    - pro jednotnou implementaci vicefaktorove autentizace
    - sjednocen procesu v zajmu kompatibility - prenositelnost
  - neni to OAuth
  - **HOTP** - HMAC-Based OneTime Password
    - na zaklade sdileneho tajemstvi generuje pomoci HMAC kod
    - server a aplikace si musi vymenit sdilene tajemstvi (QR kod, kod)
    - musi se predavat citac (obe strany podle nej pocitaci kod) - nesmi se dekrementovat
  - **TOTP** - Time-Based OTP
    - v podstate stejny jako HOTP, ale misto citace je aktualni cas - musi byt synchronizovan
    - zaukrohlouvan na 30s, tj kazdych 30s dostavam kod ktery plati dobu
  - **OCRA** - OATH Challange Response Auth
    - zalozen na HOTP, ale misto citace pouziva kombinace nekolika hodnot: citac, detail transakce, heslo, timestamp ..
    - server vypocita kod - posle ho do autentikatoru, kde ho musim potvrdit - pak dostanu kod
    - s hw zarizenim jedna z nejbezpecnejsich metod
  - modely synchronizace
    - counter based - pokud omylem vygeneruju - server je rozsynchronizovany (server nastaveny toleranci a zkusi posunout counter++)
      - pokud je ale rozynchronizovany hodne - error - out of sync -> server se vrati na posledni hodnotu (kvuli ochrane proti bruteforce) - lze sesynchronizovat (zadaji se 3 po sobe jdouci kody - server najde prvni a zkusi ty dalsi 2)
    - time based - muze byt rozsynchronizovan cas
      - server zkousi par dopredu i par dozadu (jeste nepouzitych) - tolerance casu - ale rozdil si poznamena a pri pristim pouziti se to zohledni
- autorizacni kalkulator
  - nejbezpecnejsi, ale nejmene pohodlna
  - uzivatel zadava detaily pozadavku - data jsou symetricky podepsana
  - vytvari nezavisly kryptograficky kanal
  - podobny mechanizmus u hw bitcoin penezenek

##### FIDO

- aliance sdruzujici vsechny stakeholdery autentizace
- hledal se zpusob jak zvysit bezpecnost, rychlost a pohodlnost overeni
- funguje bez hesel - funguje na zakl. asymetricke krypt.
- FIDO1
  - U2F - Universal 2nd Factor
    - autentikator (usb,nfc,bt)
  - UAF - Universal Authentication Factor
    - spojeni password less experience
    - doplneni vlastnictvi zarizeni s biometrii
- FIDO2
  - W3C - standardizace
  - sjednoceni U2F a UAF
  - zalozena na privatnim klici ulozeny na zarizeni (pc, mobil nebo autentikator -usb..)
  - registrace na strance -> v autentikatoru vznikne klic -> zaregistruje se na serveru
  - login na stranku -> muze ne/chtit heslo -> potvrzeni podpisu v autentikatoru (otisk prstu) -> server zkontroluje podpis
  - soucast vsech prohlizecu
  - muzu vyuzivat hw (yubico), OS (Windows Hello, Android a iOS zamek obrazovky)

## Biometrie

prednasky:

1. [prvni](https://youtu.be/-_OIjSYIr_c)
2. [druha](https://youtu.be/Ais4o9FFFyM)
3. [treti](https://youtu.be/WJXSE7c4O-o)

- necim jsem
- 19. rozvoj formalnich postupu
  - bertillonaz - mereni charakteristik cloveka a pomoci metrik rozdeleni do 243 skupin
    - po pridani barvy oci a vlasu - 1701 skupin
  - daktyloskopie
- nelze zapomenout/ztratit
- tezke falzifikovat
- neprenositelne na osobu
- mereni fyzickych charakteristik
  - staticke - ocni duhovka, sitnice, otisk, tvar, DNA
  - dynamicke - hlas, pohyb, pismo, podpis, dynamika psani na klavesnici
- procesy
  - identifikace
  - verifikace - porovnani 2 vzorku jestli jsou stejne
- predpoklady pouziti
  - meritelne
  - jedinecne (vysoka mira duveryhodnosti)
  - nemenne (alespon v nejake mire - oblicej => starnuti)
  - technicky realizovatelne
  - automatizovatelne
- pouziti
  - bezpecnostne komercni - v ramci bezneho zivota - prihlasovani do mobilu atd
    - nizsi rozlisovaci schopnost
    - verifikace - dalsi zpusob overeni identity
    - doba zpracovani - vteriny
  - forenzni - odhalovani zlocinu: napr otisky prstu, chodidel, DNA, hlas ..
    - vyssi rozlisovaci schopnost
    - identifikace - hledam o koho jde
    - doba zpracovani - nezalezi
- kriteria biometricke technologie
  - operacni - viz predpoklady pouziti
  - technologie - vlastnosti implementace - cas zpracovani, chybovost..
  - vyroba - kvalit, podpora ..
  - finance
  - matematicka, algoritmicka, bezpecnostni - spravnost teorie, algoritmu, bezpecnost
- klasifikace vzhledem k prostredi a uzivatelum
  - ne/spolupracujici - pruchod na letisti (necham se vyfotit), zakaz vstupu na stadion (vyfoceni netusene - GDPR)
  - ne/zjevne - podobny jako predchozi
  - aktivni/pasivni

### Zakladni principy

- biometricky vzorek (sample) - nasnimany otisk prstu, obraz obliceje
- biometricka charakteristika (characteristic) - urceni informaci ze vzorku
- biometricke markanty (identificators) - vybrani charakteristik, ktere budu pouzivat pro vypocet -> markanty
- biometricka sablona (template) - ulozeni vysledku k pozdejsimu porovnani z budoucimi vzorky
- **identifikace** - vyhledani shody v N sablonach, vypocetne narocne (1:1 porovnavani)
- **verifikace** - porovnani sablony se vzorkem, relativne rychle
- neni 100% zalozene na statistice - pri dane miry shody povazujeme za overene
- mereni vykonu:
  - FAR - False Accept Rate - pravdepodobnost chybneho prijeti
    - vypocet: `FAR = pocet prijeti/celkovy pocet pokusu` - lide kteri nemaji pristup k zarizeni
    - Apple touch id: 0,002%, face id: 0,0001%
  - FRR - False Reject rate - pravdepodobnost chybneho odmitnuti
    - vypocet: `FRR = pocet odmitnuti/celkovy pocet pokusu` - lide kteri maji pristup
  - obe metriky provazane - snizuju FAR ale zvysuje se FRR
  - ERR - Equal Error Rate - shodne hodnoty FAR a FRR
- porovnava se vzorek `P'` a sablona `P` - mira ztotozneni `Sim`
  - treshold `Th` => `s=Sim(P.P')` if `s>Th`

### Otisky prstu

- relativne spolehliva
- cca stoleti vyuzivana - soud, policie
- poslednich 30 let automatizace - pronikani do spotrebni oblasti
- prevazne vyuzivajici jako verifikace (ve spotrebni)
- markanty
  - crossover - krizeni
  - core - stred papylarni linie
  - island - ostrov
  - a dalsi
- verifikace: porovnavam markanty vzorku a sablony
  - musim resit orientaci vzdalenost, vzajemna poloha ..
- klasifikace: snazim se otisk zaradit do kategorie a podle toho snizuju mnozstvi ktere potrebuju prohledat
- senzory
  - kontaktni - opticke, elektronicke, tlakove ..
  - bezkontaktni - opticke, ultrazvukove
- standardy
  - napr. elektronicky pas (aby ho mohli kontrolovat vsichni)
  - BioAPI - posli vzorek, dostanes vyhodnoceni
  - Biomentric Data Interchange Formats - ukladani a prenost dat
    - ruzne formaty: ukladani markantu, ulozeni obrazku atd
  - Fingerprint Image Quality - kvalita obrazku otisku (kolik markantu dostanu)
  - format souboru v kterem jsou ulozena data CBEFF
  - format obrazu - WSQ - specificka potreba (JPEG - nachylnost k tvorbe artefaktu - horsi zpracovani)
- algoritmy zpracovani - NIST - sada sw modulu
  - lze vyuzivat - public domain
  - zahrnuje: klasifikace otisku, detekce markantu, posouzeni kvality, cteni std formatu, mira shody (BOZORTH3)
- prakticke vyuziti
  - odemykani pocitacu, mobilu, zamku
  - vstup do budov
  - ePas - nyni nejrozsahlejsi nasazeni

#### ePas

- od 2009 snimany otisky
- jeden prst z kazde ruky
- problematicke body
  - clovek nema prsty
  - nekvalitni otisky
- proces snimani
  - vybere se prst
  - nasnimani otisku (3x)
  - vyber nejlepsiho snimku
  - znovu nasnimani prstu
  - porovnani - verifikace
    - hotovo OR
    - zpet na zacatek
- zajimavosti
  - 0.7% - se nedokazalo nasnimat v pozadovane kvalite
  - 98.5% - podarilo se nasnimat 2 otisky v pozadovane kvality

### Obraz obliceje

- zakladni prostredek identifikace
- ne zcela vhodne pro biometrii - zmeny s vekem, slozite zpracovani, citlive na porizeni sablony (kvalita fotky - stin, odlesky atd)
- vetsinou pouziti k identifikace (ale treba face id - verifikace)
- zakladni body - 12 markantu (usi, oci, nos, usta)
  - zakladni rozpoznani
- tridy
  - 2D - zpracovani dvojrozmerneho obrazu
  - 3D - 3D sken obliceje (infracervena dioda + snimac - dokaze zrekonstruovat 3D obraz) - napr. face id
- komplikace - spatny svetlo, uhel, zakryti casti obliceje, bryle
- format - JPEG nebo JPEG 2000
- format ulozeni
  - frontal image - rovny pohled do kamery
  - full frontal image - definovane pomery stran a min. velikost
  - token image - presne definovana velikost (napr vzdalenost oci = 60/90px) - algoritmus nemusi hledat oci
- pravidla snimani - bez pokryvky hlavy, osvetleni, uhel atd
- prakticke vyuziti
  - detekce tvare - vybava mobilu, fotaku ..
  - letiste kontroluje automaticky tvar s pasem
  - vyhoda: verifikace mozna i clovekem (lehce podle fotky)

### Biometricky elektronicky podpis

- tvar podpisu, dynamika (cas), sila pritlaku
- dulezita je kvalita vzorkovani (500Hz, 1024 urovni tlaku, 600dpi)
- legislativa
  - podepisovani dokumentu - snaha legitimizace jako alternativy el. podpisu
  - podminky - duveryhodne porizeni, ochrana itegrity dokumentu
- nepouziva se vetsinou ani k identifikaci ani k verifikaci
- prakticke vyuziti
  - "ukazatel ze se zakaznik necemu zavazuje"
  - vyuziti spise pri pozdejsim dokazovani
- kryptografie
  - nutno chranit ziskany vzorek pred kopirovanim

### Dalsi poznamky

- system biometrie je tak silny jako nejslabsi clanek
  - eID dostanu na zaklade papiru - rodny list
  - pro biometricky doklad se prokazuju nebiometricky
- psychocialni aspekty
  - ruzna telesna postizeni
  - nabozenske duvody
  - atd
- podvrzeni dat pri overeni
  - oblicej - fotografie
  - otisk - gumovy otisk
  - duhovka - fotografie
  - reseni - detekce teploty, chveni duhovky, pulsu
- ochrana osobnich udaju
  - dle GDPR jsou to specialni osobni udaje a by default je zakazano je sbirat
  - ale je mozne pouzivat se souhlasem

## Pouzivani identit v informacnich systemech

### Indentity & access managemenet - IAM

- zajistuje ze se s identitami pracuje rizenym zpusobem pomoci procesu
- tyka se to vetsich organizacich -> mnoho ruznorodych systemu
- je treba
  - efektivita spravy (nove ucty, zmeny udaju..)
  - bezpecnost
  - uzivatelsky komfort
- jak toho dosahneme
  - definice procesu a zivotniho cyklu identity
  - technologie
- standard NIST - An Identity Management Syste
  - system ktery vytvari, vydava a pouziva elektronicke identity
- produkty
  - komplexni systemy
  - licencovani vetsinou na pocet uzivatelu
  - vyhody: pripravene konektory na systemy, prizpusobitelne (procesy, ..)
  - nevyhody: nakladne, malo ohebne

#### Vytvoreni digitalni identity

- urceni primarniho zdroje
- v pripdate vice zdroju - zpusob spojeni
- jednoznacny identifikator - definice, mapovani na identity
- proces zrizeni vetsinou podleha kontrole vice osob/casti
- ulozeni (napr. Windows AD)

_technologie_

- technicke propojeni systemum s primarnim zdrojem identit
  - personalni system
  - zakladni IS - napr. AD
  - meta directory - nad ruznymi zdroji identit vytvori jednotne rozhrani
- konsolidace - hromadeni dat z mnoha systemu a jejich propojeni

#### Vydani digitalni identity

- predani pristupovych udaju
  - username, heslo, cipove karty, usb tokeny, otp tokeny, biometrie
- nekolik levelu overeni bezpecnosti predani - duveryhodnosti
  1. overeni emailu
  2. predlozeni prukazu
  3. fyzicka kontrola biometrickeho prvku

_technologie_

- podpurne aplikace pro predani usernamu, hesla (overeni emailu)
- personalizace cipove karty
  - elektronicka - zapis data na kartu (klice, data)
  - fyzicka - potisk karty
- distribuce udaju do systemu
  - zadna - system je integrovan primo s hlavnim ulozistem uz. udaju - LDAP, Kerberos, SAML
  - tvorba uzivatelu v cilovych aplikacich
    - agentska - konkretni system si to resi sam
    - bezagentska - konkretni system se pta centralniho systemu
  - online/davkova

#### Pouzivani digitalni identity

- prihlasovani/pridelovani pristupu
- udrzba
  - zmena, reset hesla
  - aktualizace roli a skupin
  - reseni ztraty (docasna karta), zapomenuteho hesla
- auditing - sledovani prihlasovani, odhlasovani, vstupy atd
- reporting - sledovani nepouzivanych identit

_technologie_

- centralni nastroj pro spravu identit (spravce)
- samoobsluzna sprava (reset hesla, odemceni uctu)
- SSO - single-sign on
- auditing, reporting nastroje

#### Ukonceni digitalni identity

- casto zanedbavan - ale zasadni pro zachovani bezpecnosti
- identita se zpravidla nemaze z duvodu vazby na jine zaznamy
  - jen se zablokuje (moznost vraceni)
  - nutno promitnout do vsech systemu

_technologie_

- odstranovani/blokace identit
- obdoba vydani identity

#### Prostredi

- intranet
  - uzavrene duveryhodne prostredi
  - striktne spravovane stanice se znamym SW
  - technologie LDAP, Kerberos, SAML
- internet
  - heterogenni neduveryhodne prostredi
  - nezname vybaveni na pocitacich
  - technologie OpenID Connect, OAuth, SAML

#### Technologie

- centralizovana sprava identit a jejich udrzovani
- prevlada Windows + Kerberos a AD (LDAP)
- SSO
  - uzivatel se prihlasi k centralni identite a ta se pak prenasi na dalsi systemy
  - na internetu prihlasovani pomoci facebooku, googlu atd
  - single sign off - odhlasi zdrojovy system i ostatni aplikace

##### LDAP

- lightweight directory access protocol
- vychazi z protokolu X.500 - sada protokolu pro korporatni svet - prihlasovani, klice atd
  - nebyla potreba cele teto sady - vznikali mensi casti
- adresarove ukladani udaju o osobach, pocitacich, mistnostech atd
- std stromova struktura
  - operace na stromem - vytvareni, zmena, hledani, prihlasovani
  - DN - distinguished name - jednoznacny identifikator
  - Entry - polozka - soubor i slozka
  - Atribut - key/value, typovane hodnoty, i vice hodnot (vice tel. cisel)
- rychly pristup k velkemu poctu udaju (optimalizace, indexace)
- umoznuje dobre vyhledavani - podbny jako SQL
  - `atributy kontext filtr`
  - `SELECT FROM WHERE`
  - zapisuje se v prefixove notaci (krome porovnani) `(&(objectCategory=preson)(objectCategory=user)(!cn=andy))`, wild cards
  - musime zohlednit SQL injection
- pouziti pro autentizaci
  - moznost ukladani hashovanych hesel
  - moznost overit heslo pomoci fce bind
  - zjisteni i dalsich informaci
  - podporovany v rade produktu (bez programovani - deklarativne)

##### Kerberos

- sitova autentizacni sluzba - SSO
- predpoklady: online, synchronizovane casy
- centralni autorita - KDC
  - sdilene tajemstvi na bazi hesla
- identita - **principal name**
  - uzivatel
  - sluzba
  - **realm** - username@realm.xx
- vice instanci spolu po spojeni umi komunikovat
- **ticket** - datova struktura potvrzujici identitu (prostrednictvim treti duverne strany - KDC)
- **service principal name** - identita sluzby
- zalozeno na kryptografii
  - autentizacni server - neautentizujeme se ticketem, vyda ticket granting ticket zasifrovany klicem z hesla - client ho pomoci zadaneho hesla rozsifruje a pak ho pouziva
  - ticket granting server - vystavuje tickety k dalsim sluzbam (kdyz uz mam ticket granting ticket)
- pouziti
  - GSS API - rozhrani pro zabaleni kerberos komunikace a autentizace serveru

##### Federalizace identity

- service provider
  - poskytuje sluzbu (eshop, knihovna ..)
  - nechce sam resit identitu - vyuziva jiz existujici
  - nezna data uzivatele
- identity provider
  - poskytuje overeni identity a dalsi informace uzivatele (nutny souhlas)
  - napr. Google, MS, mojeid, NIA
- cilem je implementace SSO nebo centralni sprava identit bez sdileni credentials
- moznosti retezeni (Gitlab -> MS -> Google) (portal obcana -> eidentita -> mojeid)
- technologie
  - umi to i kerberos

##### OAuth2

- framework, nikoliv protokol -> nekompatibilita
- zpristupnovani udaju aplikacim - pristup k casti dat/funkcionalite po nejakou dobu
- faze
  - registrace - client_id, client_secret, redirect URI
  - pouzivani - access_token, refresh_token
- pojmy
  - resource owner - vlastnik chranenych informaci (ja)
  - resource server - uloziste informaci (google disk)
  - authorization server - autentizuje, vyzaduje souhlas (vydava tokeny), overuje tokeny (google)
  - client - aplikace ktera se chce dostat k datum (drawio)
  - scope - urcuje ucel pristupu (read)
- endpoints - API - komunikace
  - authorization - prihlasovani
  - token - ziskani a overeni tokenu
  - redirection - presmerovani
- nekolik druhu pristupu - flow
  - code grant - napr prihlaseni v TV (opis nejakeho kodu)
  - implicit grant - webove aplikace bez serveru (neni nejbezpecnejsi)
  - resource owner credentials grant - zadavame heslo primo v aplikaci, ale ta ho pak uz nepouziva (snad)
  - client grant - aplikace naprimo komunikuje s resource serverem
- client musi byt registrovan
  - staticky - aplikace
  - dynamicka - vice aplikaci (vice zarizeni)
  - public - neni schopny udrzet tajemstvi (html5 aplikace)
  - confidental - udrzi tajemstvi (serverova aplikace)
- access token flow
  - ![](./oauth2.png)

##### OpenID Connect

- stavi na OAuth2
- sada standardu
- doplnuje
  - format predavanych informaci - JWT token
  - format a zpusob predavani metadat
- ![](./openid.png)
- JSON Web Token
  - json nebo base64 (kompaktni)
  - vysledkem OpenID procesu
  - properties
    - payload
    - protected
    - header
    - signature

##### SAML

- pouziva se podobne jako OpenID
- pouziva se XML
