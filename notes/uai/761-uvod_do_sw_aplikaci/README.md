# 761 - Uvod do SW aplikaci

## Algoritmus

-   postup reseni urcite tridy problemu
-   seznam presne definovanych kroku
-   po prevedeni konecneho poctu kroku program skonci

**Vlastnosti**

-   hromadnost - ruzna vstupni data
-   determinovanost - kazdy krok jednoznacny
-   konecnost a resultativnost - pro prislusna vstupni data se po konecnem mnozstvi kroku dojde k vysledku

Zapis

-   prirozeny jazyk
-   [vyvojove diagramy](https://cs.wikipedia.org/wiki/V%C3%BDvojov%C3%BD_diagram)
-   struktogramy (reprezentace grafického návrhu strukturovaného algoritmu či programu)
-   [pseudojazyk](https://cs.wikipedia.org/wiki/Pseudok%C3%B3d)
-   programovaci jazyk

## Program

-   předpis (zápis algoritmu) pro provedení určitých akcí počítačem zapsaný v programovacím jazyku

## Programovaci jazyky

### Strojove orientovane

-   **strojovy jazyk** - jazyk fyzickeho procesoru (finalni kod spustitelneho programu)
-   **assembler** - jazyk symbolickych adres

### Vyssi jazyky

-   **imperativni**
    -   prikazove, proceduralni
    -   C, C++, Java, Pascal, Basic...
    -   zpracovavane udaje maji formu datovych objektu ruznych typu (promenne, konstanty)
    -   program obsahuje
        -   deklarace - vyznam jmen (identifikatoru)
        -   prikazy - akce s datovymi objekty nebo zpusob rizeni vypoctu
-   **neimperativni** - napr. funkcionalni (LISP)
-   **virtualni stroj** - hypotetický procesor, který provádí příkazy vyššího programovacího jazyka

### Vlastnosti

-   syntaxe
    -   souhrn pravidel
    -   gramatika jazyka
    -   prostredky
        -   syntakticke diagramy (pr: _identifikátor je posloupnost písmen a číslic začínající písmenem_)
        -   Backus-Naurovy formy (bezkontextove gramatiky)
-   semantika
    -   vyznam jednotlivych konstrukci
    -   obvykle popsana slovne

### Implementace

**interpretacni metoda**

-   prekladac - zpracuje zdrojovy kod do jineho tvaru -> vnitrni forma
-   interpret - provadi instrukce programu nad vstupnimi daty
-   Java: .java -> prekladac -> .class -> JRE (multiplatformni interpret Javy) -> vystup

**kompilacni metoda**

-   prekladac - zpracuje zdrojovy kod do spustitelneho programu (instrukce proveditelne pocitacem)
-   spusti se jiz samotny program s daty
-   C: .c -> kompilace na cilovou platformu(y) -> spusteni na platforme -> vystpu
-   pro kazdou platformu/cilove prostredi je treba kompilovat jinou verzi

## Uvod do Javy

-   vyssi jazyk
-   objektovy, umoznuje i proceduralni
-   portabilni programy
-   vychazi z C
-   JRE - Java Runtime Environment
-   JDK - Java Develpment Kit
-   JVM - Java Virtual Machine
-   zdrojove soubory - `.java`
-   zpracovani
    -   .java pomoci prekladace (kompilatoru) - **javac** prelozeni do vnitrni formy (bytecode) `.class`
    -   interpretaci provede **java** v JVM a tim se provede vypocet

## Vypocetni proces a pamet pocitace

-   vypocetni proces - posloupnost akci na daty ulozenymi v pameti
-   data - jsou repr. posl. bitu
-   pamet - 8-mi bitove jednoty - bajty
    -   vnitrni (RAM), venjsi (disk, CD)
    -   kazde misto svou adresu
    -   kapacita v KB (1KB = 2^10B = 1024B, 1MB = 2^20B, 1GB = 2^30B)
-   instrukce stroj. jazyka - predepisuji aritmeticke, logicke a jine operace s posloupnostmi bitu

**datove typy**

-   hodnoty ruznych datovych typu v datovych objektech
-   specifikuje
    -   mnozinu hodnot (pr. INT <-2miliardy, +2miliardy)
    -   mnozinu operaci (pr. INT +,-,/,\*,<,>,<=,>=, ==, != apod)
-   primitivni typy
    -   byte - 8bitu v dopl. kodu ( –128 .. 127), int - 32bitu v dk ( -2147483648 .. 2147483647), short, long
    -   double - 64bitu v pohyblive des. c. (aproximace reálných čísel v absolutní hodnotě od 4.9406545841246544E-324 do 1.79769313486231579E+308), float
    -   bool - **8bitu - rychlejsi (+nektere platformy neumi pristupovat po bitech)** (true, false)
    -   char - 16bitu v unicode (znaky)

**promenna**

-   je datovy objekt, ktery je oznacem jmenem, je v nem ulozena hodnota typu
-   primy zapis = literal
-   pojmenovane konstanty - _final_
-   prirazeni
    -   [proměnná] = [proměnná] OP [výraz]
    -   [proměnná] OP= [výraz]
    -   je vyraz
-   typova konverze
    -   implicitni (automaticky)
    -   explicitni (pretypovani)
-   vypis
    -   `System.out.println` kazdy primitivni ma implicitni konverzi na string
    -   `System.out.printf` - formatovany vystup (jako C)

**vyraz**

-   muze obsahovat: proměnné, konstanty, volání funkcí, binární operátory, unární operátory, závorky
-   ternar je vyraz - lol
-   poradi je dano
    -   prioritou operátorů
    -   asociativitou operátorů

**operatory**

-   aritmeticke
    -   unarni: `-`(zmena znamenka)
    -   binarni: `+ - / * %`
-   inkrement, dekrement `++x x++ --x x--`
-   relacni: `> < >= <= == !=` (mensi priorita nez aritmeticke)
-   logicke: `! && || & |`

## Java

### Ridici struktury

-   posloupnost (navazujici prikazy)
-   vetveni
    -   `if` - neulpny
    -   `if else` - uplny
    -   `switch` - char, byte, short, int
-   cyklus (`for`, `while`, `do while`)
    -   `continue`, `break`

### Funkce

-   v Jave metody
-   `static <TYP> <NAZEV>(<TYP> <NAZEV_PARAMETRU>){ <TELO> }`
-   `static int foo(int parameter){return parameter;}`
-   daji se pretezovat

**procedura** - funkce vracejici `void`

**staticka promenna**

-   tridni promenne pouzitelne v cele tride, tzv nelokalni (inicializuji se na nulu)
-   pamet (na stacku) se prideli v okamziku, kdy se do pameti zavadi kod, uvolni po skonceni programu

**lokalni promenna**

-   v bloku, zastini statickou
-   pamet (na stacku) se prideli pri volani funkce a uvolni pri navratu

### Rozklad problemu na podproblemy

Postupný návrh programu rozkladem problému na podproblémy

-   zadaný problém rozložíme na podproblémy
-   pro řešení podproblémů zavedeme abstraktní příkazy
-   s pomocí abstraktních příkazů sestavíme hrubé řešení
-   abstraktní příkazy realizujeme pomocí metod

### Rekurze

-   rekurzivní algoritmus v některém kroku volá sám sebe
-   rekurzivní metoda v některém příkazu volá sama sebe ( i nepřímo )

-   rekurzivní metody jsou přímou realizací rekurzivních algoritmů
-   radu rekurzívních algoritmů lze nahradit iteračními, ty počítají výsledek "zdola nahoru", tj, od menších (jednodušších) dat k větším (složitějším)

### Pole

-   strukturovany datovy typ
-   pevny pocet prvky (nemenitelny), indexovan od 0
-   na stacku je ulozena adresa na hromade (kde jsou samotna data)
-   **referencni pole**
    -   `n` prvku typu `T` pouze dynamicky `new T[n]`
    -   adresu lze ulozit do `T[]` - _referencni promenna_ pole typu T
    -   ref. promennou lze i samostatne `int[] a;`
        -   kdyz lokalni promenna - undefined hodnota
        -   kdyz static - null hodnota
-   prirazeni neni definovano pro pole, ale pro ref. prom. pole

### Trida

-   programova jednotka tvorena mnozinou identifikatoru, ktere maji tridou definovany vyznam
-   zdrojem metod
-   zakladem aplikace je trida ve ktere
    -   je deklarovana spousteci metoda `main`
    -   mohou byt i dalsi metody
    -   mohou byt staticke prom.
-   popisuje strukturovany datovy typ
-   specifikuje
    -   množinu hodnot datových objektů
    -   množinu operací s datovými objekty
-   datové objekty typu třída - zkracene objekty - se nazyvaji instance tridy
-   v Jave jde instance vytvaret jen dynamicky `new` a pristupovat pomoci _referencnich promennych_

#### Metody

**staticka**

-   `static`
-   dostupna pomoci jmena tridy `Convertor.convert()`

**instancni**

-   operace nad instanci tridy
-   dostupna pres referenci na objekt `convertorInstance.convert()`

#### Objekt

-   strukturovany
-   abstrakce pametoveho mista ve kterem jsou ulozeny dilci hodnoty - polozky (public, private ..)
-   muze mit na sebe vic referenci (smaze se az kdyz nema referenci - garbage collector)

#### String

-   retezec znaku
-   vlastni hodnota nelze zmenit
-   konkatenace `str + str2` `concat()`
-   `==` a `!=` porovnavaji reference, porovnani hodnot metoda `equals()`
-   lexikograficke porovnani `compareTo`

### Kolekce

-   datova struktura obsahujici promenny pocet prvku
-   operace: vytvoreni, pridani/odebrani/vlozeni/pristup prvku, velikost ...
-   `ArrayList` - indexovany od 0
-   pouzivaji pouze reference na objekty, pokud chceme pouzit primitivni typ je treba ho dat do objektu `new Integer(int)` - wrapper class

**hierarchie**:

```txt
java.lang.Object
 |
 +--java.util.AbstractCollection
    |
    +--java.util.AbstractList
        |
        +--java.util.ArrayList
```

### Dedeni a polymorfismus

-   podedena trida dedi vlastnosti rodicovske a popr je rozsiruje
-   instancni metody
    -   v podedene muze mit jinou implementaci (jinak stejna)
    -   v podedene muzou pribyt nove metody
-   struktura objektu - podedena ma stejnou popr rozsirenou
-   referencni promenne
    -   do ref. prom. rodicovskeho typu muze byt prirazena instance podedeneho
    -   na rodicovskem typu lze volat jen jeho metody - pokud jsou ale jinak implementovane v podedenem provedou se podle toho (**polymorfismus**)
    -   do ref. prom. rodicovskeho typu lze priradit podedeny pouze s pretypovanim
-   vztah **rodicovska** -> **podedena** je tranzitivni

### Soubory

-   operace: otevreni, cteni, zapis, uzavreni
-   cteni/zapis:
    -   proudovy (sekvencni) - postupne cteni/zapis
    -   nahodily (primy) - ctu/zapisuju kdekoli
-   existuje trida pro cteni posloupnosti primitivnich typu ze souboru (cte rovnou spravne typy)

#### Textove soubory

-   ukladani textu
-   posl. charu a radek
-   reprezentovany bytem (byty) podle kodovani (lze nastavit readeru i writeru)
-   new line: \r \n \r\n
-   `FileReader` `FileWriter`
-   Java char: 16bitovy UNICODE (nutny prevod napr. z 8bit textu)
-   cteni po znacich/skupine znaku (treba radkach `BufferReader`)
-   cteni po lexikalnich elementech `StreamTokenizer` - cisla, slova, oddelovac radku, konec souboru

### Vyjimky

-   osetrovani chyb a neocekavaneho chovani
-   vyjimka je objekt
-   `try-catch`
-   v hlavicce `throw Exception`

### Argumenty command line

-   `public static void main(String[] args)`
-   dostaneme pole prikazu

## Casova slozitost

-   zavislost casu potrebneho pro vypocet na velikosti dat
-   cas = pocet provedenych operaci
-   slozitost v nejlepsim vs nejhorsim pripade (ten nas zajima) => staci uvazovat slozky s nejvyssim radem rustu
-   asymptoticka slozitost **O(n)**
-   pr. [casy beznych slozitosti](https://cs.wikipedia.org/wiki/Asymptotick%C3%A1_slo%C5%BEitost)
-   horsi nez polynomialni - **NP uplne** - neproveditelne (problem obch. cestujiciho)
-   pr.: hledani v poli o `n` prvcich
    -   `for` -> O(n)
    -   `binarni puleni/hledani` musi byt serazeno -> O(log2 n)

### Razeni pole

**bubble sort**

-   postupne porovnavame sousdici prvky dokud nejsou serazene
-   O(n^2)

**select sort**

-   hleda se nejmensi prvek - interval se zkracuje
-   O(n^2)

**insert sort**

-   postupne prvky vkladam na spravne misto v jiz serazene casti
-   o(n^2)

**merge sort**

-   ze 2 serazenych posloupnosti bereme vzdy nejmensi prvek a davame na vysledek
-   O(nlog n)

## Programovaci styly

**naivni** - primocare reseni bez metod ...

**proceduralni**

-   rozlozeni na podproblemy
-   obstraktni prikazy zavedene pomoci metod

**OOP**

-   prevadime realitu do objektu a interakci mezi nimi

## Datove struktury

**zasobnik**

-   stack - LIFO - last in first out

**fronta**

-   queue - FIFO - first in first out
