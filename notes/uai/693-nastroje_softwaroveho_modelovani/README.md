# 693 - Nastroje softwaroveho modelovani

## Hierarchie

-   dlouhodoby plan - vetsinou je soucasti nejake vize (v r. 2050 bude jcu stabilni instutuce ...)
    -   cim dal je cil, tim vetsinou abstraktnejsi
-   strednedoby plan - strategie
    -   pr. strategie rozvoje IS jcu (digitalizace papirovych dokumentu)
    -   jaky stav mame ted a jaky stav chceme docilit
-   akcni plan - 3,4,5 let -> projekt

    -   pr. budeme realizovat projekt digitalizace zadanek o dovolenou

## Projekt

-   zasazen do vize, vime proc ho delame -> muzeme naplnovat cile
-   soustredene usili definovane skupiny osob, ktera za pouziti def. prostreedku a casu se snazi dosahnout definovanych cilu
-   jinak: mam umysl, co chceme udela, aby jsme to mohli udelat, tak mam k disp. zdroje (lidske, financni a dalsi, casove)
-   velmi casto projekt nestavim na zeleny louce -> z neceho vyplyvaji
-   metodiky projektove rizeni
    -   primo pro IT projekty nebo obecnejsi
    -   princ2 (Zorka Rihova), IPMA - nejznamnejsi

## Logicky ramec

-   zakladni horizontalni osy: cil, ucel, vystupy, cinnosti
-   vertikalni: popis, ukazatel, prostredek, podminky

|          | popis | ukazatel                                                         | prostredek          | podminky                                                              |
| -------- | ----- | ---------------------------------------------------------------- | ------------------- | --------------------------------------------------------------------- |
| cil      |       | dobre prijiman uzivateli, ma to trend (narust 10% za tyden atd.) |                     |                                                                       |
| ucel     |       | kolik uzivatelu/jakej zisk atd, kde vezmu ty data                |                     |                                                                       |
| vystupy  |       | metrika - ano tento vystup jsme splnili - vady ABC               | akceptacni protokol |                                                                       |
| cinnosti |       | datumy                                                           | milniky             | predpoklady k fungovani (pr. sber pozdavku -> zajisteni skupiny lidi) |

-   vady - typu A - brani v pouzitim - B - omezuji pouzivani ale ne kriticky - C - drobnosti gui atd)
-   procesni analyza - predavani procesu (fungovani aplikace, )

### Analyza rizik

-   doplnek projektoveho rizeni
-   faze -
    -   technicke zabezpeceni
    -   prijeti
    -   zasahy vyssi moci
-   2 sloupce
    -   pravdepodobnost nastani
    -   zavaznost rizika

#### Doplneni cviceni

-   identifikace - zaklad
    -   vnejsi - co neovlinim - covid apod
    -   vnitrni - spojena s projektem - meli bysme umet ovlivnit
-   kvantifikace
    -   pravdepodobnost vyskytu - procenta
    -   vaha rizika - musime urcit stupnici (1-5 atd)
    -   dopad - pronasobeni predchozich 2
    -   cinnosti eliminujici dopad rizika
        -   vnitrni - zahrnu do projektu kroky k eliminaci
        -   vnejsi - kroky k naprave pri vzniku rizika
-   cleneni
    -   organizacni - harmonogram, realizace
    -   technicka
    -   personalni
    -   financni
    -   systemova (navrhu) - eliminace spravnym projektem

## Faze projektu

### Pripravna faze

-   zasadni
-   podstata a idea projektu
-

### Planovani

-   realny plany projektu
-   zaklad projektoveho rizeni
-   cas x naklady x rozsah (vykon, kvalita)
-   casovy harmonogram
    -   GanntProject - software - navrh jednotlivych kroku, navaznost atd.

### Realizace

### Udrzba

### Ukonceni

## BPMN - Business Process Model and Notation

![BPMN example](./bpmn.png)

-   standard pro modelovani procesu
-   procesy se daji kreslit na cemkoliv (ne jen IT)
-   priklad
    -   pool (skola)
        -   lane
            -   1. ucitel navrhujici dutku
            -   2. pedagogicka rada - schvaluje dutku
            -   3. reditel - ktery to musi podepsat
    -   pool (rodina) - black box - nemuzu ovlivnit co se tam deje, jen muzu ocekavat nejaky vystup

![Pizza example](./bpmn-pizza.png)

-   proces nekde zacne, skonci a nekdo ho vlastni
-   je jedno jestli je podporenej IS nebo ne (proces probiha nezavisle na )

-   proces
    ![](./bpmn-proces.png)

## UML - Unified Modeling Language

-   vznika pod dozorem OMG - Object Managament Group
-   mnozina diagramu ruznych typu
-   slouzi k modelovani
    -   primarne pro sw vyvoj
-   typy diagramu
    -   **strukturalni** - struktura - jaky casti jsou jak spojeny s cim
        -   class diagram - da se jim popsat hodne veci
        -   object diagram
        -   deployment diagram
    -   **behavioralni** - resi chovani - jak neco na neco reaguje
        -   komunikacni - jak co spolu komunikuje
        -   usecase diagram - vhodny pro modelovani sw (hlavne IS)
        -   activity diagram - podobnej BPMN, vyvojovy diagram
        -   state machine diagram - pomocny pri slozitejsich prubehach stavu
        -   sequence diagram - popis komunikace mezi komponentama
-   vsechny diagramy maji podobne principy
    -   nazev instance podrzeny / obecny nazev neni podtrzeny
    -   instance typu se pise s `:`

### Usecase diagram

-   vychazi z BPMN - modeluje business proces do sveta IT, ale neni to 1:1
-   shapes

    -   actor
        -   role figurujici v use casu (subjekt muze mit v ramci usecasu ruzne role - anonymni/zaregistrovany)
        -   role: administrator, zakaznik (nekdo nejak vystupuje vuci systemu)
        -   uzivatel - neni role (rika ze mame nakej ucet a kdyz mu pridelime prava muze se chovat jako napr. zakaznik)
        -   muzou od sebe dedit (generalizace, specifikace)
    -   use case - co nekdo umi
        -   predstavuju si to jako tlacitko - macknu a zaregistruju se atd.
        -   casto ma scenar (konkretni popis co se stane, napr. po kliknuti na tlacitko odesle pozadavek, ulozi to, ukaze potvrzeni atd)
    -   vazby
        -   include - stane se po
        -   extend - na zaklade neceho muzu i neco dalsiho udelat

-   stereotypy (interface, abstract, table)
-   pokud actor neni v BPMN, pak nefiguruje ani v UC diagramu
-   CRUDL - Create Read Update Delete List
    -   muzu seskupit typicky usecasy pod jeden
-   veci vazany na system
    -   jsou takovy, ktery nejde udela jinde
-   materialy od p. Kravala
    -   objects.cz knizka zadarmo
    -   https://www.objects.cz/wp/publikace-node/kniha-analyticke-modelovani/

#### Poznamky z cviceni

-   http://www.agilemodeling.com/artifacts/useCaseDiagram.htm
-   http://tynerblain.com/blog/2007/04/09/sample-use-case-example/
-   Action http://tynerblain.com/blog/2007/04/10/what-are-use-case-scenarios/

-   vazby
    -   asociace - klasicka vazba
    -   dedicnost
    -   `<<include>>` - reuse, includovana cinnost je "podblokem" cinnosti
        -   vazba je povinna
        -   po staru `<<uses>>`
    -   `<<extend>>` - cinnost, uziti is, ktere rozsiruje zakladni zpusob pouziti
        -   vazba neni povinna
        -   pouzivat s mirou
-   typy usecasu
    -   primarni usecase - pouziva akter
    -   sekundarni usecase - nepouziva primo akter
-   vazba mezi UC a BPMN
    -   obvykle je tam vazba, ale neni jasna ktera
    -   muze to byt 1 task z BPMN je vyuzivan vice UC (a ruzne kombinace 1:N, M:N, M:1 ...)
-   hranice systemu muze a nemusi byt (ale mela by)

#### UC Scenare

-   nadpis UC_101:Zobrazit nabidky prekladu k pozadavku
-   Actors - ktery jsou zahrnuty
-   IN - podminky co musi byt splneny pro vykonani usecasu (pr. prihlaseni)
-   OUT - vystup po vykonani akce (pr. odhlaseni, trvale zmenena data)
-   Scenar - mel by byt obecny, nemel by obsahovat konkretni technologie - popis bez ohledu na technologii
    -   standartni - obsluha (kdyz to dela clovek), system (kdyz neco dela system)
        -   bodovy seznam s konkretnimi operacemi
        -   muze obsahovat rozvetveni - IF

##### Poznamky z cviceni

-   http://tynerblain.com/blog/2007/04/09/sample-use-case-example/
-   formalni - presne dany format - **meli bysme pouzit** - sablona na moodlu
-   neformalni - zjednoduseny (nema vsechny kategorie)

### Class Diagram

-   nejvic pouzvanej, nejsirsi pouziti
-   obcas se muze hodit zacit Object Diagramem
    -   objekt znacime tak, ze je nazev podtrzeny
    -   kreslime jednotlive objekty
-   rozdil mezi Class a Object Diagramem
    -   trida a objekty
        -   programovani - predpis podle ktery tvorime instance
            -   realita - obecny predpis vlastnosti skupiny objektu
    -   class diagram - predpis skupiny
    -   object diagram - instance objektu
-   exploze trid - musime pridavat tridu napr. pro kazdy novy produkt

#### Poznamky ze cviceni

-   klicove abstrakce
    -   vemu podstatna jmena, ktera se vyskytuji v predchozich diagramech -> potencialni kandidati na classy -> slovnik pojmu - z toho vychazim
    -   **CRC analyza** - Class Responsibility Collaborator
        -   identifikuju odpovednosti a spolupracovniky
        -   kdyz nemuzu vymyslet odpovednost/spolupracovniky - asi tridu nepotrebuji
        -   ![crc](./crc.png)
-   muzeme generovat kod (proto uvadime private/public, metody atd)
-   vazby
    -   asociace - M:N
        -   asociacni trida - vaze se na spojeni (asociacni vazby)u
    -   agregace - vztag celek - cast
    -   kompozice - vztah celek - pevna cast
    -   generalizace - znacka u obecnejsi tridy
    -   realizace - vztah implementace (interface) - znacka u rozhrani
    -   abstrakce - sipka u obecnejsi tridy (abstraktni trida)
-   [tutorial class diagram](https://www.visual-paradigm.com/guide/uml-unified-modeling-language/uml-class-diagram-tutorial/)
-   [uml class](https://www.lucidchart.com/pages/uml-class-diagram)

### Object diagram

-   objekt znacime tak, ze je nazev podtrzeny
-   vazby
    -   ![](./vazby.png)
    -   **dedicnost**
        -   seshora dolu jako specializace
        -   zespoda nahoru generalizace
    -   **asociace** (bezna)
        -   M:N, M:1 atd
        -   obycejna cara
        -   nejjednodusi a nejcastejsi typ vazby
        -   melka vazba - instance tridy A muzou existovat bez trid B (ikdyz je tam vazba)
        -   muzu odekorovat (0..\* 1..1 apod) - parcialita (musi mit vztah?) a kardinalita (kolik jich muze byt) z db
    -   **asociace**
        -   sipka od A smerem k B
        -   A vi o B, ale B nevi o A
    -   **agregace**
        -   sipka s kosoctvrcem (diamond) na jedny strane (u "dulezitejsi" tridy A)
        -   trida A obsahuje nejake instance tridy C
        -   kresli se kdyz trida A je seznamem tridy C (napr. Trida je seznamem Studentu -> pri zruseni Tridy Studenti porad existuji)
    -   **kompozice**
        -   sipka s kosoctvercem (diamond) vybarvenym
        -   D je soucast A a D nemuze existovat bez A
        -   pr. Osoba ma Adresu, Adresa bez Osoby nema vyznam

*   ztratim vazbu na vyrobky v setu
    ![](./od-1.png)
    ![](./od-2.png)

## Activity diagram

-   mel by pripominat vyvojovy diagram (popis algoritmu)
-   popisuje prubeh nejakeho procesu, podobne BPMN
-   nekdy spojeny s UC scenarem - napr. rozlehle (hodne podminek)
-   elementy - pocatek (jedno kolecko) a konec (dvou kolecko) - akce - vazby se sipkou - rozhodovani - kosoctverec - fork - paralelni procesy / vetve - join - slouceni paralelnich procesu / vetvi
-   dokumentace scenaru, vyvojovy diagramy, patri do skupiny behavioralnich
-   casto je potreba pridat poznamky, aby bylo jasny co se tam dela (omezeny mnozstvi elementu)

### Poznamky z cviceni

-   [tutorial](https://www.lucidchart.com/pages/uml-activity-diagram)
-   ![](./activity.png)
-   ![](./activity2.png)

## Sequence diagram

-   sipky jsou si podobny, ale jejich vyznam je dulezity
-   elementy - lifeliny - jednotlive subjekty/casti - activation - v ramci ni zije lifeline - sipky - message - normalni (tucna sipka) - synchronni zprava (napr. GET request) - return message - carkovana - vracim data (napr. html stranka) - asynchronous message - normalni (normalni sipka) - asynchronni zprava (napr. POST z JS) - self message - volani sam sebe (napr. JS Form validace)
-   problem kdyz se zacnou michat synchronni a asynchronni

### Poznamky z cviceni

- specialni pripad sirsi skupiny - interakcnich (komunikacni, prehledove diagramy interakci)
- interakci mez aktery, kteri pouzivaji IS
- klade duraz na casovy prubeh
- rozsah
  - mohou byt na urovni use casu
  - uvnitr jednoho scenare
  - ale cim vice scenaru zahrnuto, tim mene prehledne
- vstupy
  - use case,
  - scenare pro use case
  - diagram aktivit pro use case
  - domenovy model - class diagram (hlavni tridy se promitnou do akteru v seq. diag.)
- odkazy
  - [popis cesky](http://orca.xf.cz/ooms/010/010.htm)
  - [popis cesky 2](http://mpavus.wz.cz/uml/uml-b-sequence-3-2-4.php)
- zpravy
    - okamzita vs s trvanim
    - synchronni vs asynchronni (muze se zdrzet - musim na ni pockat)
    - ![](./sekv-mess.png)
- kombinovane fragmenty
    - oznacene bloky s podminkou pro vykonani
    - napr. alt (blok se vytvari za urcite podminky), loop, ref (reference na jinou cast diagramu)
- interakce a brany
    - hierarchi sekvencnich diagramu
    - da se odkazovat mezi podrizenymi a nadrizenymi diagramy
- zpravy muzou mit urcenou dobu trvani (napr.: &lt;3s)
    - cara se kresli naklonena, ne vodorovna

## Pocitani cenu projektu

- metodika pocitani ceny projektu (hlavne IS)
- kazdy prvek se da zaradit do kategorie
	- obrazovka, tisk souboru (external output)
	- ulozeni - db, file.. (external store)
	- atd.
- identifikuju veci
	- elementy na strance - spocitam jako jednotky
- excel tabulka
	- dam ruzny veci (co budu delat - use casy, co uz mam hotovy)
	- pridam body z predchoziho
	- spocita mi to cenu






























