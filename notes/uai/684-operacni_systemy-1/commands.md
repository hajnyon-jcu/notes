# Commands

## Prace se serverem

-   `ssh`
-   `scp`

## Zakladni prikazy

-   `man`
-   `cd`
-   `mkdir`
-   `rmdir`
-   `pwd`
-   `touch`
-   `ls`
-   `cp`
-   `mv`
-   `rm`
-   `which` - umisteni spustitelneho souboru
-   `cat`
-   `less`
-   `chmod`
-   `chown`
-   `grep <retezec> <soubor>`
-   `locate` - vyhleda vsechny soubory obsahujici dane slovo, pouziva databazi ktera se obnovuje v noci (nebo `updatedb`)
-   `find`

## Presmerovani vstupu a vystupu

-   `cmd > tmp.txt`
-   `cmd >> tmp.txt`
-   `cmd 2> error.txt` - presmerovani chyboveho vystupu
-   `cmd > tmp.txt 2> error.txt`
-   `cmd < tmp.txt` - vstup prikazu je obsah souboru
-   `cmd1 | cmd2` - roura

## Posloupnost prikazu

-   `cmd1 ; cmd2` - po sobe
-   `cmd1 && cmd2` - cmd2 pouze pri uspesnem dobehnuti cmd1
-   `cmd1 || cmd2` - cmd2 pouze pri NEuspesnem dobehnuti cmd1

## Uzivatele

-   `finge` - vypis prihlasenych uzivatelu
-   `id <uzivatel>` - informace o uzivateli
-   `w` - jiny vypis uzivatelu
-   `who`
-   `last` - posledni prihlaseni uzivatele

## Archivace

-   `tar`
-   `gzip`
-   `gunzip`
-   `bzip2`
-   `unzip`

## Textove soubory

-   `cat`
-   `less`
-   `file` - informace o souboru
-   `head`
-   `tail`
-   `tac` - opacny vypis (posledni radek prvni...)
-   `wc`
-   `tr`
-   `sort`
-   `cut`

### Prevod formatu

-   `dos2unix`
-   `unix2dos`
-   `cstocs` - prevod kodovani cestiny (`cstocs 1250 utf8 <soubor>`)

### Textove editory

-   **McEdit**
-   **Vim**
-   **nano**

### Textove WWW prohlizece

-   **Lynx** - starsi
-   **Links** - novejsi
-   `wget` - stahovani souboru

## Procesy

-   `ps`
-   `pgrep` - cisla procesu s hledanym nazvem
-   `pstree` - stromova struktura procesu
-   `top` - seznam procesu podle vyuziti procesoru
-   `htop`
-   `kill` - ukonceni procesu
-   `killall <nazev>` - ukonceni vsech procesu s nazvem
-   `nice` - spusti prikaz s prioritou
-   `renice` - zmeni prioritu prikazu

## Skripty

-   `echo`
-   `A=1`
-   `B=$A+1`
-   `read A` - vstup
-   `$0` - `$9` - parametry skriptu
-   `$#` - pocet parametru
-   `shift` - parametry nad 9
-   `#!/bin/bash` - program kterym se skript spousti, `.sh`
