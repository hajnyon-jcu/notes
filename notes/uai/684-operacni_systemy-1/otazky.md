# Otazky

## 1. Co je operační systém. Úkoly operačního systému. Zavedení operačního systému. Rozdělení operačních systémů. Vlastnosti operačních systémů. Real time OS.

### OS

moduly ve vypocetni systemu, jez ovladaji rizeni prostredku, kterymi je system vybaven (procesor, pamet, I/O zarizeni atd)

### Ukoly OS

-   sprava a pridelovani pameti - virtualni pamet
-   sprava procesu - synchronizace
-   pridelovani procesoru - multitasking
-   reseni zablokovani - deadlock
-   sprava systemu souboru - VFS (Virtual File System)
-   sprava uzivatelu

### Zavedeni OS

-   z BIOSu (Basic Input Output System) nebo UEFI (Unified Extensible Firmware Interface) se zjisti medium, na kterem je OS
-   na tomto medie se na MBR (Master Boot Record) nacte OS nebo jeho zavadec
-   priklad Linux
    -   zavadec GRUB (GRand Unified Bootloaded) nebo LILO (LInux LOader)
    -   jadro `/boot/vmlinuz` a RAM disk `/boot/initrd` (nemusi byt) se zavedou do pameti a prevezmou rizeni

### Rozdeleni OS

-   podle spravy uzivatelu
    -   jednouzivatelske
    -   viceuzivatelske - uzivatele pracuji zaroven => rozdeleni prostredku
-   podle poctu spustenych programu
    -   jednoprogramove
    -   viceprogramove
-   struktury
    -   monoliticka - jadro a rozhrani
    -   vrstvena - casti systemu usporadany do vrstev
    -   virtualni pocitace - system rozdelen do modulu, kazdy vybaven prostredky

### Realtime OS

-   prace v realnem case
-   rizeni letadel, nemocnic, eletraren

## 2. UNIX – historie a vlastnosti OS UNIX, Souborové systémy v UNIXu, prvky ochrany souborů a adresářů v UNIXu. Linux – historie a vlastnosti OS Linux. Distribuce a jádro Linuxu. MacOS.

### Historie

-   vznik 2. pol. 60. let
-   ze systemu Multics, puvodne jako system pro zpracovani textu
-   programy
    -   jedna vec, ale dobre
    -   vzajemne spolupracuji
    -   textove jako vstup
    -   vystup jako vstup do dalsiho

### Vlastnosti

-   multiprogramovy, multiuzivatelsky, viceprocesorovy, terminalovy pristup

### Souborove systemy

-   extended 2 a 4, XFS (linux), ufs (Solaris), HFS (OS X)
-   jednotna struktura adresaru
-   norma POSIX a POSIX-2

### Prvky ochrany souboru a adresaru

-   prideluji se prava
    -   soubor - rwx
    -   adresar - rwx (vypis obsahu, modifikace, vstup)
-   lze rozsirit ACL (Access Control List)
-   root pravo na cokoliv

### Historie Linuxu

-   r. 1991 z OS Minix
-   volne siritelny OS, GNU GPL

### Vlastnosti Linuxu

-   zakladem je jadro, ktere je vlastne samotny linux, na nej se nabaluji dalsi programy, mnoziny programu tvori distribuce

### Distribuce Linuxu

-   komercni
    -   RedHat - RHEL
-   nekomercni
    -   RedHat - Fedora, CentOS
    -   Debian
    -   Ubuntu + flavours
-   prekladane

### Jadro Linuxu

-   linux kernel
-   puvodne jako linux oznacoval jen jadro, ale pozdeji vztazen na cely OS (jadro, knihovny, pomocne nastroje)

### MacOS

-   verze 1-9, postupne pribyvaly funkce
-   X - kombinace NeXT OS a MacOS -> v podstate UNIX na jadru Darwin z BSD

## 3. OS firmy Microsoft, přehled, vlastnosti, Souborové systémy ve Windows, přehled, vlastnosti, prvky ochrany souborů a adresářů ve Windows.

### Prehled, vlastnosti

-   MS DOS - jednouzivatelsky, jednoprogramovy, lokalni, vrstvena struktura (BIOS, jadro, textove rozhrani, prikazy)
-   Windows - 1 - 3
    -   95 - masivni kampan, podpora TCP/IP, multitasking, plug and play
    -   98 - DVD, USB, AGP, FireWire, vice monitoru
        -   SE - posledni kvalitni 9x verze
    -   ME - zamaskovani MS DOS, movie maker, media player
    -   NT 3.1 - prepsane jadro (bez MS DOS), 32bit, vice procesu, site, NTFS
    -   2000 - moderni HW, infraport, usb, wifi, vpn
    -   XP, Vista, 7 (i 64bit), 8 (dotykova zarizeni)

### Souborove systemy ve Windows

-   FAT - MS DOS
-   VFAT - Windows 95
-   NTFS - Windows NT+

### Prehled, vlastnosti Windows

### Prvky ochrany souboru a adresaru Windows

-   NTFS - lze nastavovat práva pro uživatele nebo pro skupinu uživatelů, možnost indexování – rychlé vyhledávání

## 4. Správa paměti – Metody: Správa jedné souvislé oblasti, Správa paměti pomocí sekcí.

### Sprava jedne souvisle oblasti

-   vycleni se cast pro OS kam ostatni programy nemuzou
-   zbytek pouzit pro aplikace
-   vyhody - rychlost, jednoduchost
-   nevyhody - neni vyuzita cela pamet, ulohe nelze vykonat pozadujeli vetsi pamet

### Sprava pomoci sekci

-   jednotlivym uloham jsou prirazovany kusy pameti (ruzne/stejne velke)
-   narocnejsi, nelze pridelit vice mista za behu
-   fragmentace
    -   vnitrni - uloze se prideli vice mista nez je potreba
    -   vnejsi - po uvolneni casti pameti se do nej nova uloha nevejde -> setrasani pameti

## 5. Správa paměti – Metody: Stránkování a stránkování na žádost

### Strankovani

-   pamet rozdelime na stranky (10kB) a uloham pamet prirazujeme po strankach
-   nemusi byt za sebou a pamet uvolnujeme

### Strankovani na zadost

-   virtualni pamet
-   nastavime casti disku -> vyuzivame jako pamet, ale pomalejsi
-   pokud RAM plna -> vymenime stranky z disky -> SWAP
    -   vhodny algoritmus: FIFO, LRU, LFU, NUR, LRU(used bit)

## 6. Procesy, životní cyklus procesu.

### Procesy

-   zakladni jednotka prace moderniho OS
-   aplikace si vytvori proces/y, soucasne jich bezi vice
-   rodicovske a synovske
-   PCB - Process Control Block
-   vlakna - procesy se deli na vlakna

### Zivotni cyklus

-   vytvoreni
-   pripraven
-   probiha
-   ceka
-   ukoncen

## 7. Multitasking a jeho druhy.

-   nepreemptivni
    -   pseudomultitasking - zarizuje sam system
    -   kooperativni
        -   proces sam preda procesor
        -   prepina zvlastni program - MacOS 8 a 9 - Finder
        -   prepina uzivatel - Windows 3.x
-   preemptivni - pravidelne stridani po urcite dobe

## 8. Synchronizace procesů. Kritická sekce a její řešení.

### Synchronizace

-   nutne synchronizovat paralelni procesy
-   casove zavisla chyba - vznika podle poradi pristupu k procesu

### Kriticka sekce a jeji reseni

-   cast, ktera nelze sdilet
-   je treba zajistit
    -   vzajemne vylouceni - do kriticke sekce muze jen jeden proces
    -   dostupnost kriticke sekce - kazdy proces ktery do kriticke potrebuje, musi mit moznost se tam dostat
    -   proces je v kriticke sekci konecnou dobu
-   resene problemy
    -   uvaznuti - deadlock - procesy vzajemne cekaji na nejakou akci
    -   blokovani - proces do kriticke sekce nemuze ikdyz je volna
    -   starnuti - prilis dlouhe cekani na pristup do kriticke sekce
-   reseni
    -   Petersonovu (zdvorily) alg - nez chce do kriticke sekce, zepta se ostatnich a pocka, nez tam pristoupi ostatni
    -   Bakery (pekaruv) algoritmus - FIFO
    -   Hardwarova reseni - funkce TestAndSet - na vsech modernich procesorech, resi si sam procesor - nevyhoda - nadmerne zatizeni procesoru
    -   Semafor - celociselna promenna udavajici pristupnost kriticke sekce
        -   kladne cislo - ukazuje kolik procesu muze vstoupit do kriticke sekce
        -   nula - kriticka sekce je blokovana, ale pred nami nikdo neceka
        -   zaporne cislo - kolik pred nami ceka procesu
        -   na rizeni semaforu se pouziva TestAndSet
    -   Monitor - outsourcing - specialni program, ktery hlida pristup do kriticke sekce a programy pristupuji pres nej

## 9. Synchronizační problémy.

-   producent a konzument - pasova vyroba
    -   jeden proces data produkuje, druhy konzumuje
    -   mezi nimi pamet s urcitou kapacitou
    -   producent vyrabi prilis rychle a ceka na konzumenta
    -   konzument spotrebovava prilis rychle a ceka na producenta
    -   -> je treba synchronizace
-   model a obraz - letadlo
    -   napr. obnova displeje
        -   moc rychly (nelze precist)
        -   moc pomaly (neaktualni)
-   ctenari a pisari
    -   napr. internetove noviny - ctenari ctou a pisari kteri meni informace podle aktualnosti -> ctenari ctou neaktualni informace nebo 2 pisari meni stejny clanek
    -   kdyz k sekci pristoupi pisar - nesmi k ni nikdo jiny
    -   pristup vice ctenaru k jedne kriticke sekci nevadi
-   hladovi filozofove - deadlock
    -   napr. kolem kulateho stolu sedi filozofove - uprostred je miska ryze a kazdy filozof ma jednu hulku -> kdyz dostanou hlad, zadny se nechce vzdat hulky, vsichni umrou -> deadlock

## 10. Deadlock a jeho řešení.

-   podminky vzniku
    -   vzajemna jedinecnost - existuji nesdilitelny zdroj
    -   drzi a ceka - proces drzi zdroj a ceka na zdroj, ktery drzi jiny proces
    -   nepreemptivnost - zdroj muze byt uvolnen pouze po ukonceni ulohy
    -   kruhove cekani - procesy cekajici vzajemne na zdroje
-   metody reseni
    -   prevence deadlocku
        -   v praxi neni jednoduche
        -   nesdilitelne zdroje potrebujeme
    -   vyhybani se deadlocku
        -   deadlock povolime ale snazime se system udrzovat tak, aby deadlock nenastal
        -   bankeruv algoritmus - nemuze pujcovat, pokud nemuze zajistit na vyber vsech vkladu
            -   v praxi - proces deklaruje vsechny pozadavky a pokud nejsou dostupne - nespusti se
        -   pouziva se u realtime operacnich systemu
    -   detekce deadlocku a zotaveni po uvaznuti
        -   pravidelna kontrola deadlocku a automaticke zotaveni
        -   prilis narocna na sys. zdroje, nerozsirila se
    -   ponechani reseni deadlocku na lidske obsluze
        -   pouzivana u vsech soucasnych nerealtimovych procesu
        -   zkusime odhalit ktere procesy se nachazi v deadlocku a ukoncujeme je
        -   nejjednoduse je restart a opetovne spusteni aplikace -> pravdepodobne stejny prubeh

## 11. Struktura odkládacího zařízení, soubor, adresář, plánování disku.

### Struktura

-   jednourovnova - vse v korenovem adresari (CP/M)
-   dvourovnova - v rootu jsou adresare, ale ty uz maji pouze soubory (RSX)
-   stromova - klasicka (FAT)
-   acyklicka
    -   podobna stromove, ale navic muze odkazovat na adresare a soubory
    -   nemuzou vznikat cykly
    -   NTFS, Extended
-   cyklicka

### Planovani disku

-   planovani disku
    -   cinnost, ktera rika jakym zp. se budou vyrizovat pozadavky na disku (tak aby se nemusela hlava vracet)
    -   typy
        -   FCFS - First-Come First-Served - pozadavky zpracovany tak jak prijdou
        -   SSTF - Shortest-Seek-Time-First - nejdrive vyrizen pozadavek s nejkratsim presunem hlavy
        -   SCAN - planovani - hlava projizdi diskem tam a zpet a cestou vyrizuje pozadavky
        -   C-SCAN - hlava po dojeti nakonec se vrati na zacatek - jakoby navazoval na konec - nejlepsi prumerne vysledky (vylepseni - preskakuje se konec na ktery uz nejsou pozadavky)

## 12. Souborové systémy – FAT, NTFS, UNIXové souborové systémy

### FAT

-   starsi (pouzit u MS DOST)
-   nepouziva zurnalovani
-   pouziva se tabulka FAT - File Allocation Table - informace kde je co ulozne
-   stromova struktura
-   maximalne 255 polozek v rootu
-   FAT N (12, 16, 32) umoznuje adresovat 2^N clusteru
-   maximalni 4GB soubor
-   neumi prava

### NTFS

-   prvni u Windows NT
-   neumoznuje male disky < 4GB
-   umoznuje nastavit prava, indexovat soubory
-   pouziva zurnalovani
-   nazvy v Unicode
-   dynamicke premapovani vadnych sektoru
-   sifrovani a komprese
-   prevne odkazy - funkcni i po presunu odkazovaneho souboru
-   ridke soubory - lepsi ulozeni

### UNIX

-   Extended
    -   unixove systemy
    -   ext (jiz se nepouziva), ext2 (pouzivana u zavadejicich souborove sys.), ext3 a ext4
    -   velikost bloku: 512, 1024, 2048, 4096
-   Dalsi UNIXove
    -   XFS
        -   zurnalovaci system, 64bit, temer neomezena velikost adresovaneho souboru
        -   jsou zurnalovana metadata ale ne data -> rychlejsi ale nebezpecnejsi
    -   ZFS - Zettabyte File System, 182bit,
    -   APFS - Apple File System
