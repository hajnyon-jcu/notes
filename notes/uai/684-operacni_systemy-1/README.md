# 684 - Operační systémy

[Commands](./commands.md)

[Otazky](./otazky.md)

## Uvod

**Operacni system** - moduly ve vypocetni systemu, jez ovladaji rizeni prostredku, kterymi je system vybaven (procesor, pamet, I/O zarizeni atd)

### Zavedeni OS u PC

-   z **BIOSu** (Basic Input Output System) nebo **UEFI** (Unified Extensible Firmware Interface) se zjisti medium, na kterem je OS
-   na tomto medie se na MBR (Master Boot Record) nacte OS nebo jeho zavadec
-   priklad Linux
    -   zavadec GRUB (GRand Unified Bootloaded) nebo LILO (LInux LOader)
    -   jadro `/boot/vmlinuz` a RAM disk `/boot/initrd` (nemusi byt) se zavedou do pameti a prevezmou rizeni
-   priklad bootovani na Red Hat
    -   z BIOSu se zjisti bootvaci disk
    -   z MBR se zavede GRUB a nacte jeho config `/etc/grub/grub.conf`
    -   zavede se jadro `/boot/kernel`
    -   zavede se pocatecni ramdisk `/boot/initramfs` (obsahuje potrebne ovladace) a pripoji se korenovy disk systemu
    -   spusti se start skripty v `/etc/init.d` a zavedou potrebne moduly `/lib/modules`
    -   nastavi se sit a pripoji souborove systemy, pripravi se prihlaseni uzivatele

### Zakladni funkce OS

-   sprava a pridelovani pameti - virtualni pamet
-   sprava procesu - synchronizace
-   pridelovani procesoru - multitasking
-   reseni zablokovani - deadlock
-   sprava systemu souboru - VFS (Virtual File System)
-   sprava uzivatelu

**Vztah: Uzivatel <-> OS**

-   Uzivatel ->
-   Aplikace ->
-   OS ->
-   Hardware

![Spravne fungujici OS](./user-os.png)

### Typy OS

-   nektera kriteria lze aplikovat vzdy jina nejsou jednoznacna

#### Podle spravy uzivatelu

-   jednouzivatelske (monouser)
-   viceuzivatelske (multiuser)
    -   vice uzivatelu pracuje soucasne
    -   musi zajistit rozdeleni a oddeleni prostredku

#### Podle poctu spustenych programu

-   jednoprogramove (monotasking)
-   viceprogramove (multitasking)

#### Realtime OS

-   musi byt schopny prace v realnem case
-   pr.: rizeni letadla, elektrarny, zdravotnictvi ..
-   musi reagovat okamzite - horni hranice
-   vetsinou male jadro (mikro jadro), ktere ovlada jen zakladni funkce a na vse dalsi jsou moduly
-   pr.: QNX, RTLinux, RTX (RealTime eXtension - nadstavba pro Windows)

#### Podle struktury

-   monoliticka - jadro a rozhrani - MS DOS, zarizeni (tiskarny)
-   vrstvena - casti systemu usporadany do vrstev - vnitrni dulezitejsi
-   virtualni pocitace - system je rozdelen do modulu, kazdy vybaven prostredky (obvykle se neovlivnuji navzajem)
    -   host - hostitel - na nem bezi virtualni stroje
    -   guest - host - system bezici na virtualnim stroji

![Struktura Linux](./struktura-linux.png)

## Unix/Linux

### Vznik UNIXu

-   2. pol. 60. let
-   ze systemu Multics, puvodne jako system pro zpracovani textu
-   autori: Ken Thompson a Denis Ritchie, nazev Brian Kernighan
-   puvodne v jazyce B
-   pravidla pro vyvoji
    -   psat programy, ktere delaji jednu vec, ale dobre
    -   psat programy tak, aby mohly navzajem spolupracovat
    -   psat programy tak, aby povely prijimaly ze vstupu v textove podobe
    -   psat programy tak, aby vystupy produkovaly v textove podobe a mohly byt pouzity jako vstup do dalsich programu

### Vlastnosti UNIXu

-   multiprogramovy
-   multiuzivatelsky
-   viceprocesorovy
-   terminalovy pristup

![Struktura UNIX](./struktura-unix.png)

-   jadro bezi v privilegovanem rezimu
-   obvykle tvoreno jednim souborem - monoliticky system `/boot/vmlinuz`
-   vse ostatni bezi v uzivatelskem rezimu

#### Uzivatele

-   username, heslo
-   skupina a cislo uzivatele
-   identifikace uzivatele (jmeno, prijmeni)
-   domaci adresar `/home/user`
-   interpret prikazu shell (bash)
-   dalsi info (datum a misto posledni prihlasni)
-   prava
    -   majitel souboru (u - user)
    -   skupina uzivatelu (g - group)
    -   ostatni (o - other)
    -   vsechny (a - all)
-   prava na
    -   soubor - cteni/zapis/spusteni (r,w,x)
    -   adresar - vypis/modifikace/vstup (r,w,x)
-   **root** - pravo na cokoliv
-   lze rozsirit pomoci ACL (Access Control List)

### Jadro systemu

-   2 vrstvy
    -   HAL (Hardware Abstraction Layer) - cast zavisla na hardware - halvne ovladace
    -   Kernel - cast nezavisla na HW - bezi zde demoni - systemove procesy, ktere bezi na pozadi

#### Struktura jadra

-   souborovy system - rozhrani mezi ovladacem pametoveho media a vyssimi vrstvami
-   **VFS** (Virtual File System) - rozhrani pro podobny pristup k ruznym souborvovym systemum, uzivatel se nemusi starat o fyzicke umisteni konkretniho souboru
-   rohrani systemovych rozhrani - rozhrani mezi jadrem a uzivatelem (programy)
    -   s touto vrstvou se komunikuje pomoci knihoven obsahujicich definice API (Application Programming Interface)
    -   zajisteni bezpecnosti
    -   znemozneni zasahu uzivatele do jadra
    -   zjednoduseni prace programatoru

![Struktura jadra](./struktura_jadra.png)

### Adresarova struktura

-   `/` - root
    -   `/dev` - soubory zarizeni
    -   `/tmp` - odkladaci a pomocne soubory
    -   `/bin` - zakladni spustitelne soubory (nekdy symlink na `/usr/bin`)
    -   `/sbin` - systemove privilegovane soubory
    -   `/lib` - sdilene knihovny systemu
    -   `/usr` - knihovny, zdrojove kody, spustitelne soubory, konfiguracni soubory a dalsi
        -   `/usr/lib` - vseobecne knihovny
        -   `/usr/local` - bod pro instalace aplikaci
            -   `/usr/local/bin` -
        -   `/usr/bin` - spustitelne soubory
    -   `/etc` - globalni konfigurace celeho pocitace
    -   `/mnt` - pripojeni dalsich souborovych zarizeni
    -   `/var` - pracovni a administrativni soubory
    -   `/opt` - dalsi programy
    -   `/home` - domovkse adresare uzivatelu

![Struktura adresaru](./struktura_adresaru.png)

-   system extended FS (e2fs, e4fs), XFS (linux), ufs (solaris), HFS (OS X)
-   UNIX ma jednotnou strukturu adresaru

### Distribuce UNIX

-   **BSD** (Berkley Software Distributions)
    -   BSD, OpenBSD, FreeBSD, PC BSD
-   **MINIX**
    -   vychazel z nej Linus Torvalds
-   **Solaris**
    -   drive Sun, nyni Oracle
-   **HP UX**
    -   hp unix
-   **IBM**
    -   AIX

### Linux

-   volne siritelny OS typu UNIX
-   Linus Torvalds + people of internet
-   jadlo je volne siritelne podle GNU GPL
-   zakladem je jadro, ktere je vlastne samotny linux
-   k nemu se nabaluji programy
-   mnoziny programu tvori distribuce

-   instalace z CD, USB, pres sit (FTP, lokalni sit) - je potreba aby to zvladala sitova karta
-   seznam na http://distrowatch.org

#### Distribuce Linux

-   baliky programu, dane dohromady firmami/skupinami lidi
-   komercni - zpoplatnene/nabizejici support
-   nekomercni
-   prekladane - uzivatel si sam kompiluje
-   live - pracuji primo z CD/USB

-   **Red Hat**
    -   redhat package manage
    -   od 2003 komercni
        -   RHEL - RedHat Enterprise Linux
    -   nekomercni
        -   Fedora, CentOS
-   **Debian**
    -   druha nejstarsi aktivni distribuce, zcela nekomercni
    -   `apt` -> .deb
-   **Ubuntu**
    -   debian s pridanymi baliky
    -   userfriendly
    -   flavours: kubuntu, lubuntu, xubuntu

## Apple

-   zalozeno 1976, Steve Wozniak a Steve Jobs - soucasny CEO Tim Cook
-   produkty
    -   _Apple I_ - case ze dreva, bezel na nem BASIC
    -   _Apple II_ - plastovy case, barevna grafika
    -   _Apple III_ - vykonejsi HW
    -   _IBM_ - MS DOS
    -   _Apple IIe_
    -   _Macintosh_ - reklam finale Super Bowlu
    -   _Mac II_
    -   _Macintosh LC_
    -   _PowerPC_
    -   notebooky
        -   _PowerBook_
    -   **1998 - navrat Jobse**
        -   OS X, _Imac_, _PowerBook G3_, _PowerMac G4_
    -   nyni: MacbookPro/Air, iMac, Mac mini, Mac Proc, iPod, iPhone, iPad, Apple TV

### MacOS

-   neni portovan pro jine platformy
-   verze 1 - 9, MacOS X
-   _MacOS 7_ - multitasking, odpodkovy kos, sdileni souboru, aliasy (linky), ovladaci panely, fonty
-   _MacOS 8_ - vicevlaknovy, kontextova menu, prekryvajici se okna
-   _MacOS 9_ - viceuzivatelsky
-   _MacOS X_ - kombinace OS NeXT STEP a MacOS, v podstate UNIX na jadru Darwin z BSD

### iOS

-   pro prenosna zarizeni, puvodne chteli pouzit Android nebo upraveny Linux, ale upravili a zjednodusili MacOS X

### BeOS

-   alternativni operacni system
-   port i na PC
-   pokracovani jako OS Haiku

## Microsoft

-   zalozen 1975 Albuquerque - Bill Gates a Paul Allen
-   puvodne prodavala interpreter jazyka BASIC pro Altair 8800
-   od r. 1980 operacni system MS DOS

### MS DOS

-   pro pocitac IMB PC 5150
-   koupen za 50k dollaru od firmy Seattle Computer Products jako QDOST (Quick and Dirty Operating System)
-   upraven pro IBM PC
-   jednouzivatelsky, jednoprogramovy
-   lokalni
-   vstvena struktura
    -   BIOS - zakladni ovladani HW
    -   jadro - MSDOS.SYS - monoliticke
    -   textove rozhrani - COMMAND.COM
    -   posledni vrstva obsahuje vnejsi prikazy, uzivatelske programy a 2 konf soubory (CONFIG.SYS - nastaveni HW, ovladace a AUTOEXEC.BAT - nastaveni SW)
-   verze 1.0 - 8.0
    -   posledni cista 6.22
    -   7.0 = Windows 95
    -   7.1 = Windows 98
    -   8.0 = Windows Me

### Windows 1.0

-   1985
-   5x5.25 palcovych 360kB disketach
-   zabraly mene nez 1MB
-   podporovaly nekolik ukazovacich zarizeni, grafik a tiskaren
-   spravce souboru, kalkulacka, kalendar, kartoteka, hodiny, pozn. blok, terminal, malovani ..
-   Microsoft Write - zakladni formatovani, koncovka `.doc`

### Windows 2.0

-   1987
-   DDE - dynamicka vymena dat mezi aplikacemi
-   prekryvani oken aplikaci
-   **Windows 2.1** - verze pro ruzne procesory

### Windows 3.0

-   1990
-   prechod z prostredi MSDOS na Windows
-   umi adresovat vetsi pamet pro aplikace
-   16-ti barevne prostredi
-   prepracovany spravce programu
-   prvni GPU optimalizovane pro Windows
-   **Windows 3.1**
    -   prvni verze DTP programu preveene z Mac OS - Adobe Photoshop, Quark Express
    -   prvni lokalizovane do cs
-   **Windows 3.11**
    -   integrovana podpora peer-to-peer site
    -   Microsoft Mail - predchudce Exchange
    -   sdileni souboru a tiskaren

#### Struktura

-   pribyl WIN.COM - spousti Windows a ovladace
-   radice spousteny ve WIN.INI pomoc `DEVICE`
-   spravce virtualnich zarizeni (VMM) - spousti programy DOSu nebezici pod Windows
-   jadro Windows
    -   KRNL386.EXE - spravce pameti a procesu
    -   GDI.EXE - graficke rozhrani
    -   USER.EXE - uzivatelske ovladani
-   konfiguracni soubory INI
    -   WIN.INI - SW, uzivatele
    -   SYSTEM.INI - HW
-   rozhrani mezi uzivatelem, programy a systemem - PROGMAN.EXE, shell
-   API rozhrani
-   DOS programy nevi o Windows -> ve virtualnich pocitacich

### Windows 95

-   1995
-   masivni kampan
-   podpora TCP/IP, multitasking, plug&play
-   nemel internetovy prohlizec - placeny pack, az pozdeji zdarma
-   DirectX - herni platforma

### Windows 98

-   podpora DVD, USB zarizeni, AGP, FireWire, vice monitoru
-   IE 4.0
-   **SE** - vnimana jako posledni kvalitni verze na bazi 9x

### Windows ME

-   2000 Windows Millenium Edition
-   zamaskovani MS DOSu
-   System REstore pro obnovu poskozenych souboru
-   Movie Maker, Media Player 7
-   kritika pro nespolehlivost

### Struktura Win9X

-   nejspodnejsi vrstva - komunikace s HW
-   abstraktni vrstva - VMM - spravce virtualnich zarizeni
-   ISFM (Instalable File Systems) - spravce souborovych systemu
-   spravce konfigurace - ovladace, hlavne plug&play
-   jadro
    -   KERNEL - spravce procesu, pameti, multitasking
    -   GDI - Graphic Device Interface - sprava grafiky, tisku, grafickych zarizeni
    -   USER - vstup z klavesnice, vystup do uzivatelskeho rozhrani
-   registr - centralni informacni databaze (shrnuje INI soubory), ulozeny v USER.DAT a SYSTEM.DAT
-   aplikace pro Win32 a Win16 bezi v systemovem virtualnim pocitaci, kazda svuj adresovy prostor (win32), spolecny adresovy prostor (win16)
-   DOS aplikace maji kazda svuj virtualni pocitac

### Windows NT 3.1

-   1993
-   prvni plne 32bitovy system
-   kompletne prepsane jadro (bez MS DOS)
-   urcen jak pro workstations tak pro servery
-   podpora vice procesu, integrovana podpora site, souborovy system NTFS

#### Struktura NT

-   cast bezi v privilegovanem rezimu a cast v uzivatelskem
-   HAL - rozhrani mezi HW a zbytkem jadra
-   jadro - NTOSKRNL.EXE - preruseni a sprava procesu
-   exekutiva - rizeni cele privilegovane casti
-   podsystemy prostredi - spravny beh aplikaci
-   NTDLL.DLL - rozhrani mezi beznymi procesy a systemem
-   systemove procesy - spousti system
-   sluzby systemu - sluzby poskytovane systemem
-   sprava oken a grafiky - kvuli rychlosti primo v jadre
    -   neobvykle - bezpecnostni riziko + narocnost zmeny rozhrani
-   NT nejsou vrstveny ale modularni system
-   klient-server architektura
-   **NT 3.5**
    -   OpenGL
    -   podpora 32bit aplikacim souborovych a tiskovych NetWare serveru
    -   jmena souboru az 255 znaku

### Windows NT 4

-   1996
-   podnikove prostredi - neobsahavaly multimedialni vybavu, ale lepsi vlastnosti pro sitove prostredi

### Windows 2000

-   r. 2000
-   podpora pro moderni hw - firewire, infraport, usb, wifi, remote desktop, VPN

### Windows XP

-   r. 2001
-   spojeni stability NT systemnu s 9x multimedialni vyuzitosti

### Windows Vista

-   Aero - nove graficke rozhrani
-   XML, IPv6

### Windows 7

-   32 i 64 bit pro osobni, 64 bit pro servery
-   narocnejsi na HW
-   binarni zavadec

### Windows 8

-   optimalizace pro dotykova zarizeni

### Windows bez Windows

-   emulator wine
-   MSDOS - dosemu a dosbox
-   ReactOS - volne siritelny, nestabilni

## Pridelovani pameti

-   RAM, operacni pamet, po vypnuti pocitace se vymaze
-   ulohy
    -   sledovani stavu kazdeho mista pameti
    -   strategie pridelovani pameti
    -   realizace pridelovani + uvolnovani
    -   realizace virtualni pameti

### Metody

-   pridelovani jedne souvisly oblasti pameti
    -   nejdrive se vycleni cast pro OS kam zbytek programu nemuzou
    -   zbytek se pouzije pro aplikaci
    -   nelze provadet multitasking
    -   pouziva se napr v arduinu, chytrych spotrebicich atd.
    -   vyhoda: rychlost, jednoduchost, nevyhody: multitasking, nelze spustit ulohu pozadujici vice pameti
-   pridelovani po sekcich
    -   jednotlivym uloham pridelovany bloky pameti (stejne/ruzne velke)
    -   narocnejsi, nelze pridelit vice mista za behu
    -   fragmentace
        -   vnitrni - uloze se prideli vetsi prostor nez je potreba
        -   vnejsi - po uvolneni pameti od ulohy 1, uloha 2 je vetsi a nevejde se do okna -> nelze umistit (nedostatecne dlouhy souvisly blok), lze vyresit setrasanim pameti (ale musi se cekat neze se to udela)
    -   nepouziva se
-   dynamicke premistovani sekci
-   strankovani
    -   pamet rozdelime na stranky(~10kB), ulohy take a stranky pridelujeme na volna mista
    -   nemusi byt za sebou a pamet uplne zaplnime
    -   stranky, ktere nejsou potreba se uvolni a mohou byt znovu pouzity
-   strankovani na zadost - virtualni pamet
    -   pamet nastavime casti disku, zase rozdelime na stranky
    -   ty pak vyuzivame stejne jako v pameti -> pristup je x krat pomalejsi
    -   stranku kterou chceme pouzit z disku -> musime ji nahrat
    -   pokud je RAM zcela zaplnena -> vymenime stranku z RAM za stranku z disku (SWAP)
    -   potreba vhodny algoritmus (aby se porad neswapovalo)
        -   FIFO - First In First Out
        -   LRU - Least Recently Used
        -   LFU - Least Frequently Used
        -   NUR - Not User Recently
        -   LRU - upravena, dnes pouzivana
            -   kazda stranka ma **used bit**
            -   pri pouziti se nastavi na 1
            -   spravce pameti cyklicky nuluje stranky
            -   pokud je treba uvolnit pamet - vybere se stranka s 0
        -   Linux - LRU a pricita se 3
            -   stranka pro cteni se nemusi zavadet do pameti
-   segmentace
    -   logicke seskupeni dat
-   segmentace a strankovani na zadost
    -   logicke seskupeni dat spolu s virtualni pameti

### Sprava u realnych OS

-   MS DOS - sprava jedne souvisle oblasti
-   Windows 3.X - 9.X - segmentace se strankovanim na zadost
-   Windows NT - 10 - strankovani na zadost, soubor virtualni pameti (v korenovem adresari - pagefile.sys)
-   Linux - strankovani na zadost, Swap soubor nebo disk
-   MaCOS - swap soubory se tvori dle potreby

## Procesy

-   zakladni jednotka prace moderniho OS
-   aplikace si vytvori proces/y
-   soucasne bezi vice procesu
    -   OS
    -   aplikace
-   rodicovske a synovske procesy
-   kazdy proces je reprezentovan zaznamem - PCB (Procss Control Block)
    -   jmeno, id, pridelene prostredky atd.
    -   linux - PID
-   **vlakna**
    -   moderni OS
    -   procesy se deli na vlakna a pri pridelovani vlaken mluvime o jadrech
    -   pr. nova zalozka v prohlizeci -> nove vlakno
    -   pr. nove okno prohlizece -> novy proces

![Proces lifecycle](./proces.png)

-   proces vytvoren
-   proces pripraven
-   vybran ke zpracovani - probihajici
    -   bud ukoncen
    -   nebo ceka na vstupne vystupni udalost ktera trva delsi dobu -> cekajici
    -   nebo je pripraven (napr. ceka na vstup z klavesnice)

### Pridelovani procesoru

-   planovac - CPU scheduler - vybira vhodne proces pro planovani
-   dispecer - dispatcher - provadi prepnuti kontextu
    -   uvolni proces - ulozi jeho informace (stav registru, zasobnik atd.)
    -   potom zavadime novy proces - nacteni informace o zasobniku, registrech ..
    -   casove kvantum - doba na kterou ma proces pridelen procesor
-   metody vybirani vhodneho procesu
    -   FIFO
    -   FCFS - First Come First Served (v podstate FIFO)
    -   SJF - Short Job First - nejprve procesy s predpokladanou nejkratsi dobou behu
    -   Prioritni fronta
        -   nevyhoda - nizke priority se nemuseji dostat ke zpracovani
        -   reseni - zvysovani priority po nejake dobe
-   priority
    -   linux
        -   **-20** az **19** - zmena pomoci `nice` a `renice`
        -   -20 az 0 - systemove, 0-19 uzivatelske
        -   zvysovat muze pouze root
    -   windows - **0** az **31**, zmena pomoc TaskManageru

### Multitasking

-   nepreemptivni multitasking
    -   pseudomultitasking - MS DOS
        -   pr.: aplikace chce tisknout -> je potreba ovladat tiskarnu a pak se vratit do program
        -   zarizuje system, uzivatel nemuze ovlivnovat
    -   kooperativni multitasking
        -   proces sam preda procesor
        -   prepina zvlastni program - MacOS 8 a 9 - Finder
        -   prepina uzivatel - Windows 3.X, iOS6 a starsi
-   preemptivni multitasking
    -   pravidelne stridani po uplynute dobe
    -   rozhoduje operacni system

### Procesy v Linuxu

-   procesy jadra - absolutni prednost
-   zombie
    -   ukonceny proces, ceka na ukonceni synovskych procesu, az potom se ukonci uplne
    -   nelze se vratit do jineho stavu

## Synchronizace procesu & deadlock

### Synchronizace procesu

-   nutne synchronizovat paralelni procesy
-   v praxi synchronizace probiha na urovni vlaken
-   **casove zavisla chyba**
    -   vznika podle poradi pristupu k procesu
-   teoreticky lze resit pomoci _Bernsteinovych podminek a Petriho siti_
    -   Petriho site - graficky nastroj - nacrtek procesu a jak se ovlivnuji navzajem
    -   Bernsteinovy podminky - pokud proces zapisuje nebo cte z pameti, pak z teto casti nesmi cist/zapisovat nikdo jiny

#### Kriticka sekce

-   takova cast, ktera nelze sdilet
-   je treba zajistit
    -   vzajemne vylouceni - do kriticke sekce muze pouze jeden proces
    -   dostupnost kriticke sekce - kazdy proces ktery do kriticke potrebuje, musi mit moznost se tam dostat
    -   proces je v kriticke sekci konecnou dobu
-   resene problemy
    -   **uvaznuti** - deadlock - procesy vzajemne cekaji na nejakou akci
    -   **blokovani** - proces do kriticke sekce nemuze ikdyz je volna
    -   **starnuti** - prilis dlouhe cekani na pristup do kriticke sekce

##### Algoritmy pro pristup do kriticke sekce

-   Petersonuv (zdvorily) algoritmus
    -   nez chce do kriticke sekce, zepta se ostatnich a pocka, nez tam pristoupi ostatni
-   Bakery (pekaruv) algoritmus
    -   FIFO
-   Hardwarova reseni
    -   funkce TestAndSet - na vsech modernich procesorech
    -   resi si sam procesor
    -   nevyhoda - nadmerne zatizeni procesoru
-   Semafor - celociselna promenna udavajici pristupnost kriticke sekce
    -   kladne cislo - ukazuje kolik procesu muze vstoupit do kriticke sekce
    -   nula - kriticka sekce je blokovana, ale pred nami nikdo neceka
    -   zaporne cislo - kolik pred nami ceka procesu
    -   na rizeni semaforu se pouziva TestAndSet
-   Monitor - outsourcing
    -   specialni program, ktery hlida pristup do kriticke sekce a programy pristupuji pres nej

##### Synchronizacni problemy

-   producent a konzument - pasova vyroba
    -   jeden proces data produkuje, druhy konzumuje
    -   mezi nimi pamet s urcitou kapacitou
    -   producent vyrabi prilis rychle a ceka na konzumenta
    -   konzument spotrebovava prilis rychle a ceka na producenta
    -   -> je treba synchronizace
-   model a obraz - letadlo
    -   napr. obnova displeje
        -   moc rychly (nelze precist)
        -   moc pomaly (neaktualni)
-   ctenari a pisari
    -   napr. internetove noviny - ctenari ctou a pisari kteri meni informace podle aktualnosti -> ctenari ctou neaktualni informace nebo 2 pisari meni stejny clanek
    -   kdyz k sekci pristoupi pisar - nesmi k ni nikdo jiny
    -   pristup vice ctenaru k jedne kriticke sekci nevadi
-   hladovi filozofove - deadlock
    -   napr. kolem kulateho stolu sedi filozofove - uprostred je miska ryze a kazdy filozof ma jednu hulku -> kdyz dostanou hlad, zadny se nechce vzdat hulky, vsichni umrou -> deadlock

### Deadlock

-   cekani na udalost, ktera nemuze nastat
-   priklady
    -   hladovi filozofove
    -   dva procesy, kde kazdy drzi otevreny soubor a potrebuje zapsat do souboru druheho procesu
    -   zakon statu Kansas o vlacich - pri krizeni trati se ceka az odjede ten druhy
    -   dopravni situace prednosti zprava, kde zadne auto nemuze jet
-   sekvence pouziti zdroje
    -   dotaz na dostupnost - kriticke
    -   uziti zdroje
    -   uvolneni zdroje - kriticke
-   podminky vzniku
    -   vzajemna jedinecnost - existuji nesdilitelny zdroj
    -   drzi a ceka - proces drzi zdroj a ceka na zdroj, ktery drzi jiny proces
    -   nepreemptivnost - zdroj muze byt uvolnen pouze po ukonceni ulohy
    -   kruhove cekani - procesy cekajici vzajemne na zdroje
-   typickym priznakem je spusteno hodne procesu a zaplnena RAM, ale malo vytizeny procesor
-   metody reseni
    -   prevence deadlocku
        -   v praxi neni jednoduche
        -   nesdilitelne zdroje potrebujeme
    -   vyhybani se deadlocku
        -   deadlock povolime ale snazime se system udrzovat tak, aby deadlock nenastal
        -   bankeruv algoritmus - nemuze pujcovat, pokud nemuze zajistit na vyber vsech vkladu
            -   v praxi - proces deklaruje vsechny pozadavky a pokud nejsou dostupne - nespusti se
        -   pouziva se u realtime operacnich systemu
    -   detekce deadlocku a zotaveni po uvaznuti
        -   pravidelna kontrola deadlocku a automaticke zotaveni
        -   prilis narocna na sys. zdroje, nerozsirila se
    -   ponechani reseni deadlocku na lidske obsluze
        -   pouzivana u vsech soucasnych nerealtimovych procesu
        -   zkusime odhalit ktere procesy se nachazi v deadlocku a ukoncujeme je
        -   nejjednoduse je restart a opetovne spusteni aplikace -> pravdepodobne stejny prubeh

## Odkladaci zarizeni

-   hardware na ktery muzeme ukladat data a zase je nacist i po vypnuti pocitaci
-   pevna - HDD, SSD ..
-   vymenna - CD, DVD, flash disk ..

**Soubor**

-   zakldani jednotkou pro ukladani dat
-   nejmensi cast dat, kterou muzeme adresovat

**Adresar**

-   soubory se slucuji do adresaru
-   tvori strukturu
-   root - korenovy adresar
-   z pohledu OS je to taky typ souboru

### Deleni adresarovych struktur

-   jednourovnova - vse v korenovem adresari (CP/M)
-   dvourovnova - v rootu jsou adresare, ale ty uz maji pouze soubory (RSX)
-   stromova - klasicka (FAT)
-   acyklicka
    -   podobna stromove, ale navic muze odkazovat na adresare a soubory
    -   nemuzou vznikat cykly
    -   NTFS, Extended
-   cyklicka

### Soubor

-   operace
    -   vytvoreni
    -   cteni
    -   zapis
    -   zmena pozice v souboru
    -   smazani
-   druhy
    -   standartni - dokumenty, programy
    -   adresare - zvlastni typ souboru
    -   simulovane - pristup k I/O zarizenim
    -   odkladaci soubory - virtualni pamet
-   fragmentace
    -   vnejsi - soubory rozkouskovany po pametovem mediu (na vice clusterech)
    -   vnitrni - soubor je mensi nez cluster (jednotka pro ukladani)
-   podle pristupu
    -   prime - CD, disk - kterykoliv soubor muzeme kdykoliv precist
    -   sekvencni - magneticka paska - musime precist celou pasku pred pozadovanym ctenim
    -   indexovy - lze primo adresovat nektera mista

### HDD

-   disky obsahujici pohyblive casti
-   nasledujici plati pro MBR a BIOS (u SSD - GPT a EFI)
-   slozeni
    -   stopy - samotne disky
    -   sektory - vysece disku, na vnejsich plochach je vice sektoru
    -   desky - povrch
    -   hlavy - cteci zarizeni
    -   cylindry - stopy nad sebou
-   rychlost
    -   cas vyhledavani - seek time - nastaveni hlavy nad spravnou stopu
    -   cekaci doba - latency time - doba nez se pozadovany sektor natoci pod hlavu
    -   cas prenosu - trasfer time - doba prenosu mezi hdd a ram
-   planovani disku
    -   cinnost, ktera rika jakym zp. se budou vyrizovat pozadavky na disku (tak aby se nemusela hlava vracet)
    -   typy
        -   FCFS - First-Come First-Served - pozadavky zpracovany tak jak prijdou
        -   SSTF - Shortest-Seek-Time-First - nejdrive vyrizen pozadavek s nejkratsim presunem hlavy
        -   SCAN - planovani - hlava projizdi diskem tam a zpet a cestou vyrizuje pozadavky
        -   C-SCAN - hlava po dojeti nakonec se vrati na zacatek - jakoby navazoval na konec - nejlepsi prumerne vysledky (vylepseni - preskakuje se konec na ktery uz nejsou pozadavky)
-   ulohy OS
    -   pocatecni inicializace
    -   bootovani z disku
    -   prace s vadnymi bloky
-   formatovani
    -   nizkourovnove - priprava stop a sektoru
    -   vysokourovnove - priprava struktury dle zvoleneho formatu - FAT, NTFS, ext4

#### Cleneni na oddily

-   windows - zavadeci a system (vcetne dat)
-   linux - boot, swap a system (vcetne dat)
-   bezne 2 typy
    -   MBR - Master Boot Record
        -   uziva BIOS
        -   pouze na HDD
        -   pouze 4 oddily
    -   GPT - GUID Partition Table
        -   EFI nebo UEFI
        -   SSD i HDD

## Souborove systemy

-   vlastnosti
    -   okamzity zapis
        -   v danem okamziku probiha pouze jedna operace, dalsi musi cekat
        -   data se nemohou ztratit, ale pomalejsi
        -   FAT
    -   opatrny zapis - bezpecna posloupnost opraci, jako u databazi
    -   opozdeny zapis - cache pamet, na disk se ukladaji az v okamziku kdy je to mozne -> rychlejsi, ale data se mohou ztratit
    -   zurnalovaci systemy - uchovavaji se informace o operacich - pri vypadku je mozne data obnovit - NTFS a vetsina linuxovych

### FAT

-   starsi (pouzit u MS DOST)
-   nepouziva zurnalovani
-   pouziva se tabulka FAT - File Allocation Table - informace kde je co ulozne
-   stromova struktura
-   maximalne 255 polozek v rootu
-   FAT N (12, 16, 32) umoznuje adresovat 2^N clusteru
-   maximalni 4GB soubor
-   neumi prava

### NTFS - New Technology File System

-   prvni u Windows NT
-   neumoznuje male disky < 4GB
-   umoznuje nastavit prava, indexovat soubory
-   pouziva zurnalovani
-   nazvy v Unicode
-   dynamicke premapovani vadnych sektoru
-   sifrovani a komprese
-   prevne odkazy - funkcni i po presunu odkazovaneho souboru
-   ridke soubory - lepsi ulozeni

### Extended

-   unixove systemy
-   ext (jiz se nepouziva), ext2 (pouzivana u zavadejicich souborove sys.), ext3 a ext4
-   velikost bloku: 512, 1024, 2048, 4096

### Dalsi UNIXove

-   XFS
    -   zurnalovaci system, 64bit, temer neomezena velikost adresovaneho souboru
    -   jsou zurnalovana metadata ale ne data -> rychlejsi ale nebezpecnejsi
-   ZFS - Zettabyte File System, 182bit,
-   APFS - Apple File System

### ISO 9660

-   zapis na datova CD, DVD, Blue Ray
-   maximalni hloubka adresaru 7
-   maximalni jmeno souboru 32 znaku
-   rozsireni
    -   Joliet - dlouha jmena pro Windows
    -   El Torito - moznost bootvani
    -   Rock Ridge - dlouha jmena pro UNIX

### Dalsi pojmy

-   RAID - Redundant Arrays of Independent Disks
    -   diskova pole
    -   RAID 0 - stripping - data stridave zapisovana na 2 nebo vice disku (zapis/cteni rychlejsi, ale po ztrate 1 disku - ztrata disk)
    -   RAID 1 - mirroring - zapisovani na vice disku (rychlejsi cteni, pomalejsi zapis?)
    -   RAID 5
        -   data zapisovana na vice disku a jeden pouzit pro kontrolni soucty
        -   data lze obnovit, ale pomalejsi zapis
    -   RAID 6 - pro kontrolni soucty pouzity 2 a vice disky a metody se mohou lisit
    -   softwarovy RAID - operacni system - jednodusi, ale mene bezpecne
    -   hardwarovy RAID - HW zarizeni ktere se stara o pouziti RAID - rychlejsi
-   sitove souborove systemy
    -   z vice ulozist na vice pocitacich muzeme vytvorit neco jako RAID (0, 1)
    -   rozdeleni dat ve vice serverovnach
    -   GlusteFS, Ceph
-   LVM - Logical Volume Manager - vytvoreni logickych disku z fyzickych
-   VFS - Virtual File System - definici operaci, ktere musi zvladnout kazdy souborovy system bez ohledu na skutecne hw umisteni
