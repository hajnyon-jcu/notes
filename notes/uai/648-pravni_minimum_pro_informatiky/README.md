# 648 - Pravni minimum pro informatiky

[[TOC]]

## Teorie prava, ustavni pravo

-   vice uhlu pohledu
    -   zavazna pravidla chovani, ktera jsou statem vynutitelna
    -   soubor pravnich norem, platnych pro dane uzemi

### Prameny

-   normativni pravni akty
-   soudni a spravni precedenty
-   pravni obyceje (dlouhotrvajici masove chovani)
-   normativni smlouvy
-   v CR
    -   NPA - Normativni pravni akt
        -   zavazne
        -   ustava, ustavni zakony, zakony, zakonna opatreni, vyhlasky, narizeni
    -   judikatura - neni pramen, pouze dotvari (vyznam v praxi)
    -   pravni obyceje - nejsou pramen (vyuziti u mezin. prava)
    -   normativni smlouvy - jsou pramen
        -   kolektivni, verejnopravni a mezinarodni smlouvy

### Pravni norma

-   statem vydavane a chranene obecne zavazne pravidlo chovani (uklada prava a povinnosti)
-   pozadavky
    -   vseobecne zavazny akt
    -   od organu s legislativni pravomoci
    -   obsahuje pravni normy
    -   nalezi formalne publikovany
-   prvky
    -   subjekt - osoby kterych se tyka
    -   objekt - urcite chovani nebo objekt tohoto chovani
    -   obsah - souhrn opravneni a pravnich povin. vyplivajicich subjektum z daneho vztahu
-   rozdeleni pravnich norem
    -   nizsi museji byt v souladu s vyssi

![rozdeleni norem](./rozdeleni-norem.png)

-   vydavani pravnich norem
    -   PS + Senat = ustava, LSZP (listina zakl. prav a svobod), ustavni zakon, zakon
    -   Senat = zakonna opatreni
    -   Prezident + vlada = rozhodnuti prezidenta
    -   vlada = narizeni vlady
    -   ministr = vyhlaska ministerstva
    -   krajske zastupitelsvo = vyhlaska kraje
    -   rada kraje = narizeni kraje
    -   obecni zast. = vyhlaska obce - samostatna pusobnost
    -   obecni rada = vyhlaska obce - prenesena pus.
    -   tzv negativni zakonodarce = ustavni soud (zruseni zakonu ktera jsou v rozporu s ustavou)

### Individualni pravni akty

-   vydavany organy statni moci - soudy, spravni urady ...
-   nepusobi vuci vsem ale pouze mezi stranami
-   deleni
    -   konstitutivni - vznik, zanik, zmena pravnich vztahu (rozvod, stavebni povoleni..)
    -   deklaratorni - nezakladaji ani nemeni, pouze existujici vztahy potvrzuji (vetsina rozsudku soukromeho prava)

### System prava

-   soukrome
    -   obcanske
        -   rodinne
        -   autorske
        -   obcanske procesni
    -   obchodni
    -   pracovni
    -   MPS - ZMPS - mezinarodni pravo soukrome a mez. smlouvy
    -   pozemkove
-   verejne
    -   ustavni
        -   spravni
        -   trestni
        -   finacni
        -   zivotniho prostredi
        -   socialniho zabezpeceni
        -   MPV - mezinarodni pravo

### Ustavni poradek CR

-   souhrn vsech ustavnich zakonu
    -   ustava
    -   LZPS
    -   ustavni zakony - pr. Ustavni zakon o opatrenich souvisejicich se zanikem CSFR, o zmenach hranic s Rakouskou rep. apod

### Ustava CR

-   zakladni zakon CR
-   psana ustava ktera je rigidni (lze ji menit jen sloziteji nez obycejne zakony)
-   113 clanku v 8 hlavach
    -   prvni
        -   14 cl., upravuje zakl. system fungovani republiky
        -   demokraticky stat, ucta k pravum a svobodam obcana
        -   dodrzovani zavazku mezinarodniho prava
        -   zdrojem moci je lid
        -   Kazdy muze cinit, co neni zakazano zakone a nikdo nesmi byt nucen cinit, co zakon neuklada.
        -   zakotveni LSZP
        -   dobrovolny vznik a soutez politickych stra
        -   zmeny a doplneni ustavy pouze ustavnimi zakony
        -   uzemi tvori nedilny celek
        -   statni obcanstvi, hlavni mesto, statni symboly
    -   druha
        -   zakonodarna moc nalezi parlamentu tvorenym
            -   PS - 200 poslancu, 4 roky, min 21 age
            -   Senat - 81 senatoru, 6 let, kazde 2 roky obmena 1/3, min 40 age
            -   nelze byt clenem obou komor najednou, neslucitelne s dalsimi fcemi (vyjmenovane v jinem zakone, nepr. prezident)
        -   zakotveno volebni pravo
            -   tajnost, vseobecnost, rovnost
            -   18+let
        -   imunita poslance/senatora
            -   nelze stihat za projevy, hlasovani, za trestny cin lze stiha se souhlasem komory
        -   komory jsou zpusobile se usnaset za pritomnosti aspon 1/3 clenu
        -   pro prijeti rozh. je treba vetsina pritomnych (vyjimky napr: veta prezidenta, prehlasovani senatu)
        -   navrh zakona smi predlozit
            -   poslanec, ci skupina poslancu
            -   senat
            -   vlada
            -   zastupitelstvo VUSC (kraj)
    -   treti
        -   moci vykonnou je v CR prezident a vlada
        -   prezident
            -   je hlavou statu, neni z vykonu fce odpovedny
            -   volen primo na 5 let
            -   navrhovat muze
                -   kdokoliv starsi 18 let s petici 50k+ podpisu volicu
                -   skupina 20 poslancu nebo 10 senatoru
            -   40+ let, ne 2x za sebou
            -   nelze jej stihat ani zadrzet (jen pro velezradu nebo hrube poruseni ustavy)
        -   vlada
            -   vrcholny organ vykonne moc
            -   tvori predseda, mistopredsedovea a ministri
            -   odpovedan PS, jmenuje prezident
            -   musi pozadat PS o vysloveni duvery do 30 dnu, jinak nutno jmenovat nove
            -   mozne vyslovit vlade neduveru
                -   pri neduvere musi predseda podat demisi za celou vladu, jinak ovolana prezidentem
            -   na navrh predsedy vlady take prezident odvola clena vlady
            -   ministerstva zrizovana zakonem
        -   statni zastupitelstvi zastupuje stat v trestnich rizenich
    -   ctvrta
        -   moc soudni
        -   soudce je nezavisly, nestrannost nesmi nikdo ohrozovat
            -   nelze proti jeho vuli odvolat ci prelozit
            -   zakonem zaruceny plat
            -   fce neslucitelna s fci prezidenta, clena parlamentu ani jinou fci ve verejne sprave
        -   ustavni soud
            -   nespada pod soustavu obecnych soudu
            -   15 soudcu na 10 let
                -   jmenuje je preziden
                -   osoba volitelna do senatu, pravnicke vzdelani, 10 let cinnosti v pravnickem povolani
                -   soudce nelze trestne stihat bez souhlasu senatu
        -   obecne soudy
            -   nejvyssi soud, nejvyssi spravni soud
            -   vrchni soudy
            -   krajske soudy
            -   okresni soudy
        -   soudci bez casoveho omezeni
            -   bezuhonni, vs pravnicke vzdelani
            -   vazan zakonem a mezinarodni smlouvou
        -   vsichni ucastnici soudniho jednani maji rovna prava, jednani je ustni a verejne (vyjimky), rozsudek vzdy verejne
    -   pata
        -   NKU - nejvysssi kontrolni urad
        -   kontrola nad hosp. se statnim majetkem
        -   predstavitele jmenuje prezident an navrh posl. snem.
    -   sesta
        -   CNB - ceska narodni banka
        -   pece o cenovou stabilitu
    -   sedma
        -   cleneni republiky na obce a kraje
        -   USC - uzemne samospravne celky
            -   vlastni majetek a rozpocet
            -   stat muze zasahovat do cinnosti pouze vyzaduje-li to ochrana zakona
            -   clenove zastupitelstev voleni na 4 roky
-   osma
    -   prechodna ustanoveni, ktera povetsinou uz dnes nemaji vyznam
    -   vyjimka cl. 112 - ustavni poradek CR
    -   vyjimka cl. 113 - nabyti ucinnosti

### LZPS - listina zakladnich prav a svobod

-   soucast ustavniho poradku
-   zajistuje dodrzovani zakl. lidsk. prav v CR
-   deleni
    -   lidska prava a zakl. svobody
        -   zakl. lidska prava a svobody
        -   politicka prava
    -   prava narodnostnich a etnickych mensin
    -   hospodarska, socialni a kulturni prava
    -   pravo na soudni a jinou pravni ochranu
    -   ustanoveni spolecna

## Obcanske pravo - zaklady

-   obcansky zakonik, tzv. Novy obcansky zakonik - NOZ
    -   rosahla rekodifikaci temer celeho soukr. prava
    -   upraven kvuli zprehlednini, duplikaci, upraveni neupravenych otazek soukr. prava
    -   od 1.1. 2014
    -   upravuje vesmes vsechny aspekty soukr. zivota obcanu
    -   obcanske, obchodni a rodinne pravo

### Obecna cast

-   zakladni zasady a vychodiska
-   uprava osob - fyzicka, pravnicka
-   pravni osobnost - zpusobilost mit prava
-   svepravnost - zpusobilisto nabyvat pravnim jednanim prava a zavazovat se povinnostem
-   nelze se vzdat osobnosti ani svepravnost

#### Fyzicka osoba

-   pravni osobnost od narozeni do smrti
-   svepravnost - 18 let, 16 let (snatkem, nebo podnikani?)
-   nasciturus - hledi se na nej jako na jiz narozene (dedeni atd)

-osobnost cloveka

-   chraneni vcetne vsech prirozenych prav
-   zivot, dustojnost, zdravi a pravo zit v priznivem ziv. prostredi, vaznost, cest, soukromi a projevy osobni povahy
-   moznost domahani se soudne, po smrti i kdokoliv z blizkych
-   do soukromi nesmi zasahovat nikdo (mimo zakonny duvod)
-   upraven zasah do dusevni a telesne integrity
-   casti lidskeho tela
    -   pravo dozvedet se jak bylo s casti tela nalozeno
    -   prenechat jinemu lze jen za podminek stanovenych v zakone

#### Veci

-   vse co je rozdilne od osboy a slouzi potrebe lidi
-   vec urcena k obecnemu uzivani je verejnym statkem
-   hmotne
-   nehmotne - prava, patenty ..
-   nemovite veci - pozemky, stavby
-   movite veci - ty ktere nejsou nemovite
-   pozemek - soucasti je prostor pod i nad povrchem, stavby, rostliny

#### Promlceni / prekluze

-   promlceni - pravo dale existuje, je vymahatelne, dokud protistrana nenamitne
    -   majetkova prava
-   prekluze - pravo zanika, soud prihlizi
-   lhuta
    -   pocatek ode dne, kdy mohlo byt pravo vymahano u soudu
    -   obecne 3 roky, nejpozdeji se majetkove pravo promlci za 10 let

## Rodinne pravo

-   upravuje manzelstvi, pribuzenstvi a svagrovstvi, porucenstvi a jine formy pece o dite
-   registrovane partnerstvi

### Manzelstvi

-   trvaly svazek muze a zeny
-   vznik svobodnym a uplnym souhlasnym projevem vule (za jednoho muze provest zmocnenec)
-   obrad verejny, pritomnost svedku
-   obcansky x cirkevni
-   prijmeni
    -   jedno spolecne, kazdy si necha sve (vyber prijmeni deti) nebo jedno spolecne a druhy si ponecha i rodne
-   18+, s vyjimkou 16+ let
-   povinnosti a prava
    -   rovna
    -   ucta, zit spolu, vernost, respekt dustojnosti, podporovat se, pece o deti ...
    -   pravo na sdeleni prijmu a jmeni, stavajicich pracovnich, studijnich a podobnych cinostech
    -   prispivani na potreby rodiny
    -   dohoda o zalezitostech rodiny - pri nedohode nahrazuje soud
-   majetkove pravo
    -   zakonny rezim
        -   spolecne jmeni vse, s vyjimkou veci osobni potreby, daru nebo dedictvi
        -   sprava: oba nebo jeden dle dohody
    -   smluveny
        -   upravi prava a povinnosti
        -   formou verejne listiny
        -   lze zmenit dohodou manzelu nebo soudem
    -   zalozeny na soudu
    -   oddelena jmeni
        -   naklada kazdy se svym majetkem
-   zanik
    -   rozvod
    -   rozvod nesporny
        -   oba souhlasi, manz. musi tvat alespon rok a manzele spolu 6+mesicu neziji
        -   dohodli se s detmi, na rozdeleni majetku
        -   pisemne dohody

### Rodice a deti

-   matkou je zena ktera porodila
-   otec
    -   za trvani manzelstvi manzel (+ 300 dni potom)
    -   pokud znovu provdana novy manzel
    -   umele oplodneni - darce
    -   pokud ani jedno - souhlasne prohlaseni matky a muze
    -   popirani
        -   do 6 mesicu ode dne kdy se dozvedel pochybnost o otcovstvi, nejpozdeji 6 let od narozeni
        -   nelze poprit diteti narozenemu v dobe mezi 160-300 dni od umenleho oplodneni
        -   matka muze poprit otce do 6 mesicu od narozeni

## Dedicke pravo

-   vznik smrti zustavitele
-   pravo na pozustalost nebo pomerny podil z ni

### Pozustalost

-   jmeni zustavitele k okamziku smrti
-   jmeni - aktiva - pasiva
-   nespadaji prava povinnosti vazana vylucne na osobu (jen kdyz byly uznany nebo uplatneny u organu verejne moci)
    -   pr. zustavitelova prava na bolesnte, satisfakci v penezich apod, byla-li za jeho zivota uznana nebo zazalovana
    -   nelze zdedit osobnostni pravo autorske
        -   pouze majetkova prava autorska

### Subjekty

-   zustavitel
    -   fyzicka osoba, jejiz majetek je v okamziku smrti predmetem dedickeho rizeni
-   dedic
    -   osoba, na niz ma prejit pozustalost (cela, cast)
    -   fyzicka i pravnicka osoba (pravnicka musi vzniknout do 1 roku od smrti - fondy, nadace)
    -   je vyloucen:
        -   ten kdo se dopustil cinu povahy umyslneho trestneho cinu proti zustaviteli, jeho predkovi, potomkovi nebo manzelovi
        -   ten kdo se dopustil zavrzenihodneho cinu proti posledni vuli zustavitele
        -   zakonny dedic manzel, ktery se dopustil vuci zustaviteli cinu naplnujiciho znaky domaciho nasili
        -   rodic, ktery byl zbaven rodicovske odpovednosti kvuli zneuziti nebo zanedbani
        -   ten kdo zemrel soucasne se zustavitelem

### Dedicke tituly

-   dedicka smlouva
-   zakonna dedicka posloupnost
-   zavet
-   odkaz

## Pracovni pravo

## Autorske pravo

## Trestni pravo
