# 700 - Agilni metody vyvoje software

-   rizeni softwaroveho programu
-   tradicni vs agilni pristup
    -   tradicni - vodopadove
    -   adhoc - nepouzivam metodiku (proramuj a opravuj)
    -   agilni - iterativni
    -   uspesnost 2014 - agilni cca o 20% vetsi
-   procesy zivotniho cyklu (vodopadova metoda)
    1. zadani - specifikace pozadavku
    2. analyza
    3. navrh/design
    4. implementace
    5. integrace (do jinych systemu)
    6. testovani
    7. produkce - nasazeni
    8. provoz/udrzba
    9. odstraneni
-   projektove procesy
    -   planovani projektu
    -   hodnoceni a rizeni
    -   rizeni rizik, konfiguraci, informaci
    -   mereni
-   **iron triangle**
    -   naklady (cena, co jsem slibil)
    -   cas (termin, co jsem slibil)
    -   rozsah (funkcionalita, co jsem slibil)
    -   2012 - 40% projektu splnilo vsechno, 40% aspon neco, 20% ani jedno (rozdil podle velky/maly projekt)
    -   typicky kdyz hnu s jednou velicinou, zmeni se mi i ostatni
    -   casem se pridala jeste kvalita
-   **vyuziti funkci systemu**
    -   podle pruzkumu 2002
    -   64% nikdy nebo vyjimecne pouzivane, 20% casto nebo denne pouzivany
    -   agilni metodky se snazi tohle zlepsit (prioritizovani funkcionalit s rychlou zpetnou vazbou)
-   procesni (narocne - typicky potreba skoleni) vs bezprocesni (Scrum)

## Modely zivotniho cyklu

-   **programuj a opravuj**
    -   zakladni model, ktery praktikujeme i nevedomky
    -   iterace programovani a opravovani chyb, konci nejakym "hotovym" stavem
    -   _vyhody_: jednoduchy
    -   _nevyhody_: obtizne pouzitelny na vetsi systemy
-   **vodopadovy model**
    -   vznikl v 60. letech, standard cca pred 20 lety, nyni vytlacovan agilnim pristupem
    -   body viz nahore - rozdeleni do pojmenovanych fazi, ktere se jedna podruhe delaji a pokracuje se dolu (typicky schuzky se zakaznikem mezi jednotlivymi fazemi)
    -   _vyhody_: dobra predstava o rozsahu reseni, jasne rozdeleni fazi projektu (vim, ktere lidi kdy potrebuju)
    -   _nevyhody_:
        -   pokud se zmeni zadani, musim se vratit az na zacatek k analyze a navrhu -> nemuzu se vratit o vice kroku vzad (resp. dost nakladne casove i financne)
        -   chybi zpetna vazba od zakaznika, kdy to implementuju rok a az potom to vidi zakaznik
-   **v-shape model**
    -   tento model neni tak dulezity
    -   zavadi ruzne typy testovani, v podstate vodopad s pridanymi testy
    -   podobne jako TDD - v jednotlivych bodech pridavam testovani
    -   _vyhody_: duraz na testovani, trasovatelnost pozadavku
    -   _nevyhody_: porad podobne vodopadu
-   **spiralovy model**
    -   tento model neni tak dulezity
    -   zacina pouzivat iterace, prototypy, testovani
    -   prechod mezi vodopady a agilnim vyvojem
    -   relativne slozite pouziti
    -   rozdeleni do 4 fazi + iterace
    -   _vyhody_: zavedeni iteraci, duraz na analyzu rizik, prototypovani
    -   _nevyhody_: pozadavky az ve druhe iteraci, testovani az ve ctvrte
-   **inkrementalni model**
    -   rozdelni vodopadu na N inkrementu - mensi faze v kterych prochazim vodopad (ale ne cely - specifikace pozadavku je jen na zacatku)
    -   _vyhody_: zpetna vazba - po kazdem inkrementu
    -   _nevyhody_: ne vzdycky to jde rozdelit na inkrementy, porad obtizne specifikace pozadavku
-   **iteracni model**
    -   kazda iterace obsahuje vsechny faze vodopadu, dokaze reagovat na zmeny pozadavku a typicky se na konci dostaneme k jinym pozadavkum
    -   na nem jsou zalozene agilni metodiky
    -   _vyhody_:
        -   cetna zpetna vazba
        -   zmena pozadavku v kazde iteraci - mozna realizace zmen
        -   vcasna a cetna integrace
    -   _nevyhody_:
        -   mensi predstava o rozsahu celeho projektu (nevim co se zmeni)
        -   vysoke naroky na dostupnost zakaznika

## Agilni pristup

-   **Agilni manifest**
    -   sepsan v 1999 na konferenci v USA, kde se seslo nekolik IT firem/odborniku a shodli se, ze se SW da vyvijet lepe
    -   https://agilemanifesto.org + https://agilemanifesto.org/principles.html
    -   dohodli 4 hlavni body/principy:
        -   **jednotlivci a interakce** je dulezitejsi nez **procesy a nastroje**
        -   **fungujici software** je dulezitejsi nez **detailni dokumentace** (spis mensi dokumentace, ale nerika to ze zadna nema byt)
        -   **kolaborace se zakaznikem** je dulezitejsi nez **domluva smlouvy** (win-win situace)
        -   **reakce na zmenu** (pozdavku) je dulezitejsi nez **dodrzovat plan**
    -   k tomu se snazi ruznymi metodami a principy dospet agilni metodiky
    -   po nem vznikaji nove metodiky jako Scrum, Extreme programming
-   upraveny **iron trojuhelnik**
    -   agilni pritup pridava k zakladnimu trojuhelniku jeste **kvalitu** a **hodnotu pro zakaznika**
    -   porovnani
        -   rigorozni (tradicni)
            -   fixni: rozsah
            -   variabilni: termin, naklady
        -   agilni pristup
            -   fixni: termin, naklady
            -   variabilni: rozsah
-   **Paretovo pravidlo** - 80:20
    -   plati v ruznych pripadech
    -   80% bugu je ve 20% kodu
    -   agilne nas zajima 80% pridane hodnoty je ve 20% funkcionality
        -   takhle muzeme v iteracich pridavat hodne funkcionality uz na zacatku -> lepsi zpetna vazba
        -   vyuziva se v agilnim pristupu - prioritizace funkcionalit
    -   nektere firmy pouzivaji jako moznost prerusit projekt v kterem koliv stavu, kdy uz je to pro zakaznika dost (eg. pokryti 80% funkcionality)
-   metodiky - cca 10, z toho se pouzivaji ~3 (scrum a ruzne hybridy, kanban, extreme programming)

### Scrum

-   r. 2000 Ken Beck (knizka), caste pouziti (60-70% pouziti v agilnich metodach)
-   zalozeno na iteracich - **sprinty**
    -   typicky tyden (mensi projekty) az mesic (vetsi projekty)
-   zakladni proces
    -   product backlog
    -   sprint backlog
    -   sprint samotny
        -   daily stand up/scrum (ruzne nazvy)
    -   dilci nasaditelny produkt
        -   ukazka zakaznikovi, pouzitelna cast
-   scrum
    -   **artefakty** - prvky metodiky
        -   _product backlog_
            -   seznam pozadavku na vyvijeny system
            -   kazdy pozadavek - popis, priorita, narocnost (clovekodni, story pointy) atd
            -   ve forme user stories - muzou se meni a aktualizovat
            -   user stories
                -   typicky kratka veta, ktera rika _kdo_, _co_ a _za jakym ucelem_ potrebuje udelat
                -   typicky dalsi sloupce:
                    -   kod - zkratka (pouziva se jako reference napr na scrum boardu)
                    -   nazev - popis vetou
                    -   popis neboli akceptacni kriteria - body, ktere kdyz jsou splnene, tak je user story hotova (melo by pokryt hlavni casy)
                    -   priorita - dulezitost pro zakaznika
                    -   estimace - hruby casovy odhad, nekdy se pouzivaji story pointy
                    -   iniciator - kdo s user story prisel (koho se zeptat na detaily)
                    -   status - "vyreseno ve sprintu 1", "todo", "hotovo" atd.
        -   _sprint backlog_
            -   seznam user stories pro dany sprint
            -   stories se rozdeli na tasky (analyza na zacatku sprintu)
            -   rozdeluje se na zacatku kazdeho sprintu
            -   _task_ - uloha pro jednoho cloveka, typicky max 8 hodin na ulohu
        -   _burndown chart_
            -   kolik tasku nam chybi v danem sprintu
            -   pomer clovekohodin / dni ve sprintu
            -   v kazdem okamziku vim, jak jsem na tom v ramci sprintu
            -   podle prubehu vim, jak upravovat vahy tasku (jestli tam neni problem)
            -   muze byt burnout chart i pro cely projekt
        -   _scrum board_ - evidence tasku v ramci sprintu
            -   typicky tabule (idealne fyzicka, ale muze byt trello, jira atd)
            -   evidence tasku v ramci sprintu a kdo je dela
            -   sloupce: todo, doing, done
            -   radky: user story - serazene podle priority pro zakaznika (vybiram podle toho tasky, co jdu delat)
            -   kontroluje se na daily standupu
            -   muze mit vice sloupecku: testovani, review
            -   muze se na ukolech znacit, ze task je tam vice jak 1 den, vice jak 2 dny, aby bylo videt ze je problem
    -   **role**
        -   _product owner_
            -   zodpovednost za backlog a produkt jako takovy, co se bude delat
            -   prioritizace user stories (podle pridane hodnoty pro zakaznika), aktualizace user stories (zmena pozadavku)
            -   ovlivnuje vysledny produkt a je za nej odpovedny
            -   idealni je, kdyz je product owner z tymu zakaznika (ne vzdy to jde)
            -   muze sbirat pozadavky od jednotlivych lidi (napr. delame pro vetsi firmu, ktera ma vice oddeleni, kazde svoje pozadavky)
        -   _scrum master_
            -   alternativa projektoveho vedouciho, ale neni to projektak - neurcuje kdo ma co delat
            -   zodpovednost za scrum - dodrzovani scrum metodiky a jeji rizeni
            -   vedeni schuzek, spravuje sprint backlog a burndown chart
            -   snazi se udrzet idealni podminky v ramci tymu (resi hw, blokace teamu, konflikty, "stit tymu" pred ostatnimu)
            -   nemel by programovat, ani urcovat co kdo programuje (estimace urcuje team, samy lidi v teamu si vybiraji tasky)
        -   _tym_
            -   IT specialiste kteri pracuji na vyvoji
            -   idealne 5-9 (mene nez 4 ztraci trochu vyhody, vice nez 10 ztraci taky vyhody - schuzky a celkove organizace)
            -   ruzne role, podle projektu - typicky: programatori, designery, analytiky, testeri (lidi muzou sdilet role)
            -   odpovednost za dany sprint a jeho dokonceni v terminu (striktne dany) - odlisne od standardnich metodologii
                -   zalozeno na psychologii - kdyz si sam vyberu ukol, tak mam vetsi motivaci ho dodelat do terminu
                -   kdyz se neco nedodela, nehledaji se jednotlivci, ale odpovednost ma cely tym
            -   kdyz je potreba zmenit tym - musi se udelat mezi sprinty
            -   velocity - cas, ktery dohromady tym ma na jeden sprint
    -   **meetingy**
        -   _naplanovani sprintu_ - sprint planning meeting
            -   na zacatku kazdeho sprintu
            -   vybrani user stories, analyza a navrh, rozdeleni do tasku
            -   nekdy nasleduje jeste Sprint analysis meeting a Sprint design meeting (nekdy zahrnuto v jednom)
        -   _daily standup/scrum_
            -   ~15 min schuzka pro denni update (stiha se/nestiha/neco me blokuje atd)
            -   presne dany cas (typicky 9 rano)
            -   schuzka celeho tymu krome product ownera
            -   kazdy clen ma typicky 1min na 3 otazky
                -   co delal vcera
                -   co bude delat dnes
                -   jestli je necim blokovany
            -   detaily se neresi tady, ale na separatnich schuzkach - dulezite, aby byl dodrzovany cas
        -   _sprint demo_ - spint review
            -   na konci sprintu si tym pripravi prezentaci pro product ownera
            -   maximalni snaha aby se prezentoval fungujici projekt (ne screenshoty, ale realna aplikace nebo alespon video), idealni pripad je i poskytnout demo verzi (review appka)
            -   ziskani feedbacku
        -   _retrospektiva_
            -   pred zacatkem dalsiho sprintu, po demu
            -   slouzi k optimalizaci procesu, optimalizace vah tasku
            -   kazdy clen by mel rict co se mu libi/nelibi, co by se melo zmenit do dalsho sprintu
            -   nekolik ruznych praktik jako: kvadrant (do more, do less, stop doing, start doing), anonymni anketa
-   neni pro kazdeho, nutne aby lidi prijmuly zakladni myslenky - standupy, prioritizaci atd a aby jim to vyhovovalo
-   scrum je typicky pro samotny proces vyvoje, ale jsou jeste dalsi faze
    -   predehra
        -   naplanovani celeho projektu, globalni architektura, technologie, standardy, konvence
        -   typicky 1-2 sprinty
    -   dohra
        -   systemovy/zatezovy testovani, finalni integrace, dokumentace
-   praktiky agilniho vyvoje
    -   zjistovani pozadavku
        -   komunikace se zakaznikem/zadavatelem
    -   estimace
        -   odhad pracnosti, kolik mi zabere casu (at uz user story nebo tasku ve sprint backlogu)
        -   radove by mel vychazet `user story == suma(tasku)`
        -   vyuziti v burndown chartu, trackovani progressu
        -   vytvari je cely tym
        -   ruzne typy ohodnoceni
            -   story points - abstraktni skala (neni spojene s casem), idelani stav, ale je narocny to dobre pouzit
            -   clovekohodiny/dny
        -   ruzne praktiky
            -   planning poker
                -   estimace pomoci karetni hry
                -   kazdy clen ma balicek karet
                -   kazdy clen ohodnoti podle sve uvahy dany task
                -   pak reknou sve nazory ty co dali nejvyssi a nejmensi ohodnoceni a hlasuje se znovu
                -   pak se vybere finalni estimace
                -   pouzivaji se karticky s hodnotami podle Fibonaciho posl. - zaokrouhlene vyssi hodnoty (lepsi predstava u mensich), otaznik pokud nekdo nevi jak ohodnotit, kava pro prestavku
                -   existuji i jine skaly (velikosti tricek: XS, S, M, L ..)
            -   rozdelovani na skupiny
                -   existuji skupiny (oznacene stejnou skalou jako planning poker)
                -   v kazdem kroku muze clen tymu bud
                    -   vzit karticku s user story a priradit ji do skupiny
                    -   presunout karticku do jineho sloupce
        -   typy
            -   product backlog
            -   sprint backlog
    -   retrospektiva
        -   ruzne typy a verze
            -   quadrant: do less, do more, start doing, stop doing
            -   sailboat anchor: predstavim si tym jako lod (kotva me drzi zpet, vitr me fouka dopredu, kameny jsou prekazky, ostrov kam se chceme dostat)
    -   prioritizace
        -   typicky product owner radi user story podle priority

### Kanban

-   jednodussi nez scrum, ze ktereho vychazi, ale zjednodusil to (vychazel nektere kroky)
-   hlavne pouziva kanban - upraveny scrum board
    -   _backlog_ - tasky
    -   _selected_ - vybrane tasky, omezene poctem
    -   _develop_ - omezene poctem
        -   _doing_ - WIP tasky
        -   _done_ - hotove tasky
    -   _deploy_ - tasky pripravene na deploy, omezene poctem
    -   _live!_ - hotove tasky
-   workflow - pohyb task v ramci kanbanu
    -   zakaznik vybere ukoly (ale max podle omezeni sloupecku _selected_)
    -   tym si vybere ukoly do _doing_ (max podle omezeni)
    -   tym splni ukol
    -   deploy tym si veme splneny ukol do _deploy_ a nasazuje
    -   pokud se nekde objevi problem, volny lidi jdou na tom pomahat, aby se workflow nezasekl
-   zakladni pravidla
    -   duraz na workflow - abych videl, ve kterem stavu jsou ukoly
    -   omezeni rozpracovanych ukolu - co nejmensi pocet rozpracovanych
    -   mereni prumerneho casu workflow (z TODO do DONE)
-   zadne role, meetingy, ani iterace (nejsou sprinty)
-   hodi se na nektere typy projektu - mensi nezavisle projekty, marketing (a jine odvetvi)
    -   kombinace: scrum na hlavni vyvoj, kanban na udrzbu hotoveho projektu (sprava bugu, feature requestu atd)

### Extreme programming - XP

-   r. 1999, jedna z prvnich agilnich metodik, sepsana jako zkusenosti
-   programatorska metodika, jeji casti se pouzivaji i v ostatnich metodach (nebo se pouzivaji nevedomky), best practices
-   striktne nepopisuje zadny meetingy ani artefakty, ale pravidla v praktikach je zahrnuji (iterace, male releasy)
-   hodnoty
    -   communication - komunikace - jak komunikuji programatori v tymu, se zakaznikem atd
    -   simplicity - jednoduchost - premyslet nad tim jak to delat spis jednoduse - navrh, programovani, testovani
    -   feedback - zpetna vazba
    -   courage - odvaha
    -   respect - respekt (pridan pozdeji)
-   praktiky
    1.  whole team - cely tym je zodpovedny za cely projekt (souvisi s 5.)
    2.  planning game - neco jako sprint planning (pred zacatkem iteraci)
    3.  small releases - iterativni pristup, mensi casti, ktere se daji prezentovat
    4.  system metaphor - vsichni v tymu (i manazeri, zakaznik) vi a dokazi rict jak pouzit system
    5.  collective code ownership - cely tym zodpovedny za kod (codereview, zastupovani clenu)
    6.  continous integration - automaticka integrace do celeho systemu
    7.  coding standards - dane konvence
    8.  sustainable pace (40h week) - dany casovy rozsah (aby se nestalo ze 3 dny nic nedelam a dohanim posledni 2)
    9.  pair programming
    10. simple design - delat veci spis jednoduse, omezit veci ktery by se nepouzivali
    11. refactoring
    12. test-driven development

### Skalovani agilnich metod

-   drive pohled ze agilni metodiky nejdou pouzit na vetsi projekty:
    | | Rigorozni metodiky | Agilni metodiky |
    | -- | -- | -- |
    | **Predpoklady** | SW procesy lze popsat, pozadavky mozne predem definovat | SW procesy nelze popsat, predem jen hrube pozadavky |
    | **Obsah** | presne definovane procesy, cinnosti, artefakty | jen generativni pravidla |
    | **Pouziti** | | |
-   dnes uz prekonany a existuje naskalovane agilni metodiky
-   problemy pri skalovani:
    -   role architekta
        -   pocatecni analyza, definice globalni architektury a navrh
        -   je na vsech schuzkach sprintu
        -   je na vsech schuzkach predehry a sprintu
    -   backlog
        -   muze dost narust na vetsich projektech
        -   pridaji se dalsi polozky jako: priorita (napr. vazena z X zdroju), estimace, verifikace, stav (verified, registred, drafted)
    -   paralelni prubeh sprintu
        -   sekvencni - sprinty jedou po sobe
        -   prekryvajici - jednotlive sprinty se prekryvaji (vzdy max 2)
            -   duvod je typicky, ze behem dema/deploye nejsou vsichni clenove tymu vytizeni a proto muzou uz delat na dalsim sprintu
        -   prekryvajici se Xkrat - muze se prekryvat az X sprintu
            -   duvod je typicky nasazeni se zatezovym testovanim, kdy deploy a nasazeni trva dele
    -   distribuovany tym
        -   ruzne casove zony tymu - resi se kdy delat schuzky, standupy atd
        -   komunikace - napr. mam tym na UX a na backend a potrebuju je sesynchronizovat
-   metody
    -   scrum of scrums
        -   metoda pouzivani scrumu pro vetsi tymy
        -   scrum tymy normalne, ale po daily standupu je jeste standup scrum masteru
    -   spotify model
        -   nestacil jim scrum, takze ho rozsirili
        -   pojmenovaly organizacni skupiny a popsali jak spolu skupiny funguji
        -   skupiny
            -   squad - obdoba scrum teamu - ma sveho produkt ownera
            -   tribe - skupina squadu, ktere resi stejny produkt
                -   chapter - je v tribu, ale jde napric squadama v tribu (spojuje lidi stejneho zamereni)
            -   guild - spojuje lidi napric tribama, ktery maj stejny zajmy (napr. konicky), hlavne podpora rustu, vzdelavani (1 mesicni schuzky, vzdelavani atd)
        -   podobny jako maticovy model (korporaty, kde jsou pooly lidi z kterych taham tym), ale tady je rozdil, ze lidi nevytrhnu z tymu
    -   LeSS (Large-Scale Scrum)
        -   nekomercni intuitivni metoda, typicky kdyz ma firma jeden projekt, ale vice tymu
        -   soubor navodu a popisu
        -   jeden product owner, 1 az 8 tymu (kazdy scrum mastera, popr. sdileny)
        -   sprint
            -   spolecne planovani
            -   potom kazdy tym svuj scrum (sprint board atd.)
            -   spolecna sprint review, retrospektiva (nejdriv per tym a pak spolecna)
            -   vysledek je nova verze produktu
        -   dulezite nastavit synchronizaci tymu (at uz integrace, nebo ruzne estimace atd)
    -   SAFe (Scaled Agile Framework)
        -   komercni framework pro enterprise spolecnosti
        -   zamereno na procesy, robustni system, ale porad jsou tam casti agile (obsahuje tymy scrum, kanban atd.)
        -   napr. Skoda

### State of agile - pruzkum

-   celosvetovy pruzkum, 40k respondentu
-   respondenti
    -   nejak praktikuji agilni metody - 95%
    -   mene nez pulka tymu je agilni - 44%
    -   software dev., 26% IT - 37%
    -   duvody pro pouzivani agile
        -   zrychleni dodavani a vyvoje SW
        -   zvetseni moznosti meni priority
        -   zvetseni produktivity
    -   vyhody po pouzivani
        -   moznost menit priority - reakce na zmeny
        -   viditelnost do projektu
        -   srovnani businessu a IT oddeleni (synchronizace)
        -   zrychleni dodani
        -   zvyseni tymove moralky
    -   pouzivani metodik
        -   Scrum - 58%
        -   ScrumBan - 10%
        -   hybrid - 9%
        -   Scrum/XP hybrid - 8%
        -   Kanban - 7%
    -   pouzivane techniky
        -   daily standup - 85%
        -   retrospektivy - 81%
        -   sprint planning - 79%
        -   sprint review - 77%
    -   XP
        -   unit testing
        -   coding standards
        -   CI
        -   refactoring
    -   hodnoceni prechodu na agile
        -   uspokojeni zakaznika/zadavatele - 58%
        -   pridana hodnota pro business - 54%
        -   splneni terminu - 48%
        -   kvalita - 45%
    -   metody na skalovani agile
        -   SAFe - 35%
        -   Scrum of Scrums - 16%
    -   problemy a prekazky pri prechodu na agile (spis vetsi firma)
        -   braneni se zmene obecne
        -   braneni se zmene ve vedeni
        -   nekonzistentni procesy a praktiky pres tymy
        -   kultura firmy neni sladena s agile
    -   pouzivane nastroje
        -   kanban board
        -   taskboard
    -   pouzivane programy: JIRA, excel, gdocs
