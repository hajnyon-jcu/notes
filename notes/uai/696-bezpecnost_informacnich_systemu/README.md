# 696 - Bezpecnost informacnich systemu

## Uvod

-   co chranime
    -   aktiva (vse co ma pro nas nejakou hodnotu)
    -   data (osobni udaje, know how, dobra povest)
        -   chranena ze zakona (osobni udaje)
        -   provozni data
        -   data zakazniku nebo informace o zakaznicich
        -   informace o provozu firmy
        -   know how
        -   prezentace pro verejnost
    -   majetek (penize na ucte, hw a sw, budovy a jejich zarizeni)
    -   lidi (zamestnance)
-   proc chranime
    -   ztrata penez/zakazniku
    -   ztrata know how
    -   dobre povesti (unik dat)
    -   chceme se vyhnout poruseni zakona

### Aktiva

-   vse co pro nas ma nejakou cenu (data, fyzicky majetek, dobra povest atd.)
-   Zákon č. 40/2009 Sb., trestní zákoník
    -   meli bysme umet
    -   § 230 Neoprávněný přístup k počítačovému systému a nosiči informací
        -   az 3 roky vezeni
    -   § 231 Opatření a přechovávání přístupového zařízení a hesla k počítačovému systému a jiných takových dat
        -   napr. najdu na internetu db uniklych hesel a ulozim si je na disk
    -   § 232 Poškození záznamu v počítačovém systému a na nosiči informací a zásah do vybavení počítače z nedbalosti
    -   § 182 Porušení tajemství dopravovaných zpráv - odchyceni komunikace na siti
-   Zakon o ochrane osobnich udaju
    -   “osobním údajem jakákoliv informace týkající se určeného nebo určitelného subjektu údajů. Subjekt údajů se považuje za určený nebo určitelný, jestliže lze subjekt údajů přímo či nepřímo identifkovat zejména na základě čísla, kódu nebo jednoho či více prvků, specifckých pro jeho fyzickou, fyziologickou, psychickou, ekonomickou, kulturní nebo sociální identitu.”
-   GDPR
-   Zákon o kybernetické bezpečnosti (NBÚ).
-   hrozba - napr. zemetreseni
-   riziko - pravdepodobnost, ze se hrozba nastane a jak se projevi
-   kde se nachazeji
    -   servery, klienti, media, sit, internet, papirove dokumenty
-   jak aktiva chranime
    -   analyza rizik (napr. datove centru mimo zaplavovou oblast, zalohovani, zalozni zdroj atd)
    -   pojisteni

## ISMS

-   Information Security Management System
-   system politiky, praktiky a postupu, ktere maji chranit aktiva spolecnosti
-   nekolik kroku nutnych k udrzeni ISMS
    -   identifikace aktiv - co chceme chranit a kde to je
    -   analyza rizik a jejich osetreni
        -   rizika take muzeme akceptovat - tj vime o nem ale neosetrime (napr. moc drahy osetreni)
    -   vyber a implementace opatreni k osetreni rizik (napr. zaheslovani pocitace s aktivem, 2FA atd.)
    -   monitorovani, udrzba a zlepsovani opatreni urcenych k ochrane aktiv
-   kroky se opakuji - bezpecnost neni nikdy hotova
    -   vznikaji nove hrozby/utoky atd
-   bezpecnostni pozadavky mohou byt identifikovany na zaklade
    -   identifikovani informacnich aktiv a jejich hodnoty
    -   pozadavky organizace na zpracovani informaci, jejich ukladani a prenaseni (komunikace)
    -   pravnich, regulacnich a smluvnich pozadavku
-   metodologie analyzy rizik je nastinena v _ISO/IEC_ 27005 a v lekci “Analýza rizik”
    -   ISO - mezinarodni organizace pro standardizaci
    -   nas zajimaji 2700X - normy nemusime znat zpameti
    -   v norme se resi co musi splnovat ISMS, aby organizace mohla byt povazovana za chranenou
-   kazde identifikovane riziko musi byt osetreno
    -   aplikace odpovidajiciho opatreni snizujici riziko
    -   akceptace rizika
    -   vyhnuti se riziku (napr. zakaz nejake akce)
    -   sdileni rizika - pojistovny
-   opatreni by mely byt zahrnuty jiz do vyvoje systemu
-   faktory uspesne implementace ISMS
    -   politika informacni bezpecnosti , cile a aktivity nutne k dosazeni cilu
    -   postu navrhu, implementace, monitorovani, udrzovani a zlepsovani informacni bezpecnosti v soulad s firemni kulturou
    -   viditelna podpora ze vsech urovni systemu (top management..)
    -   povedomi o informacni bezpecnosti v ramci organizace (skoleni - pouzivani hesel, predchazeni viru atd)
    -   efektivni proces rizeni incidentu (pripravene postupy, tymy a lidi ktere je resi)
    -   proccess business continuity - zalohovani, zalozni zdroj energie atd
    -   system metrik umoznujici merit vykonnost a efektivitu systemu rizeni bezpecnosti
-   kostra:
    -   identifikace aktiv
    -   oceneni aktiv
    -   analyza rizik
    -   opatreni rizik
    -   monitorovani systemu
    -   reakce na incidenty
-   cyklus politiky
    -   vyhodnoceni
        -   kategorizace dat
            -   kde se nachazeji
            -   kdo s nimi pracuje (kdo ma pristup)
            -   kdo je za ne zodpovedny
            -   jak jsou dulezita
        -   vyhodnoceni hrozeb
            -   hrozby vnejsi a vnitrni (z vnitrku organizace)
        -   dopad hrozeb na fungovani firmy
            -   financni ztraty, propad prodeju
            -   ztrata dobreho jmena
            -   nedodrzeni legalnich pozadavku
        -   klasifikace dopadu
            -   ocekavany/nejhorsi mozny/nejlepsi mozny - melo by se promitnout do financnich nakladu
        -   potreb uzivatelu
        -   bezpecnostnich nastroju
        -   autentizace (+ identifikace)
        -   autorizace (rizeni pristupu k datum)
        -   ochrana integrity dat (jestli se nezmenili - pri komunikaci po siti atd)
        -   fyzicka bezpecnost (zabezbeceni budov a objektu, recepce, ochranka atd)
        -   pravidelne skoleni zamestnancu
        -   monitorovani systemu
        -   bezpecnostni testy
        -   analyza zhodnoceni investic
    -   tvorba politiky
        -   **musi obsahovat**
            -   pruvodni informace od vedeni
            -   ucel politiky
            -   odpovednost a pravomoci (koho se tyka, kdo muze kontrolovat a stanovovat sakce)
            -   zakladni pojmy
            -   informace o vlastnictvi a pristupu k datum
            -   pravidla pouzivani vypocetnich systemu
        -   klicove body, ktere **muze obsahovat**
            -   rizeni pristupu (identifikace, autentizace, bezpecne uchovavani udaju, administrace uctu, privilegovany pristup, pristup cizich osob - ktere nejsou zamestnanci, vzdaleny pristup k siti ..)
            -   email (privatnost informaci, sifrovani zprav)
            -   prenosna zarizeni (prevence kradeze, odpis zarizeni, rizeni pristupu, skodlivy kod)
            -   software (pristupy, licence, aktualizace)
            -   verejne site (upload/download, sifrovani, privatnost)
            -   sit (rozdeleni na zony, modemy a access pointy, rizeni pristupu)
            -   fyzicka bezpecnost (pracovni stanice, servery, infrastruktura siti)
            -   auditing a monitorovani (auditni zaznamy a logy)
            -   vzdelavani uzivatelu
            -   havarijni plany - zalohy a obnova
            -   disciplinarni rizeni
    -   implementace politiky
        -   nesmi byt tak prisna aby omezovala chod spolecnosti
        -   podpora spolecnosti (vedeni) + vseobecna informovanost
        -   zavedeni politiky a jeji prehodnocovani

### Analyza rizik

-   identifikuje rizik, ktera je nutne kontrolovat nebo akceptovat
-   zahrnuje analyzu hodnot aktiv. hrozeb a zranitelnosti
-   metody analyzy
    -   kvalitativni
        -   mira rizika je urcovana kvalifikovanym odhadem (nizka, stredni, velka)
        -   nekdo rika na zaklade zkusenosti cloveka
    -   kvantitativni
        -   matematicky vypocet rizika z frekvence vyskytu hrozby a jeho dopadu
        -   vyjadruje se jako mnozstvi financnich ztrat za obdobi
-   typy analyzy
    -   orientacni - rozhodnuti jaky typ analyzy zvolit
    -   elementarni
        -   aplikuje se zakladni bezpecnost za pouziti standardnich opatreni (katalogy ETSI, IT Baseline PRotection Manual)
        -   neprovadi se detailni hodnoceni
        -   vyhody: minimalni pouziti zdroju k implementaci
        -   nevyhody: velke naklady jeli zakladni uroven prilis vysoka nebo nedostatecne zabezpeceni kdyz je moc nizka
    -   neformalni
        -   vyuziva zkusenosti jednotlivcu - vhodne kdyz je treba rychle resit bezpecnost a jsou k dispozici specialiste
        -   vyhody: rychlost a fin. nenarocnost
        -   nevyhody: zavisle na odbornych znalostech
    -   podrobna
        -   podobna identifikace a ohodnoceni aktiv, odhad hrozeb a zranitelnosti
        -   to je nasledne pouzito pro ohodnoceni rizik a navrhu opatreni
        -   postup: identifikace aktiv, jejich ohodnoceni, ohodnoceni hrozeb, zranitelnosti, bezp. opatreni a odhad rizik
        -   vyhody: identifikovana vhodna opatreni
        -   nevyhody: casova narocnost a potreba expertu
        -   nastroje: CRAMM, CORAS
    -   kombinovana
        -   nejdrive orientacni pro vsechny systemy
        -   na dulezite sys nebo sys s velkym rizikem je pouzita podobna analyza
        -   ostatni jsou osetreny elementarni
-   pri tvorbe bezp. politik se vychazi z analyzy rizik
-   plan bezpecnosti - dokument popisujici kroky a opatreni, ktere je treba vykonat pro implementaci ochrannych opatreni
    -   rozvrzeni opatreni pro kratkodoby (napr. momentalni hrozby) / strednedoby (napr. detekce utoku)/dlouhodoby plan (napr. behavioralni analyza - detekce anomalii)

## Opatreni

### Autentizacni mechanizmy

-   ridi pristup k aktivum
-   identifikace osoby a autentizace, autorizace - pristup k aktivu
-   autentizace
    -   neco vime
        -   heslo
            -   dostatecna delka, slozitost/nahodnost
            -   utoky: hadani na zakladi infa (jmeno manzelky atd), slovnikovy utok, brute force, soc. inz., odposlech v siti, ziskani z pameti, keylogger ...
            -   implicitni heslo - prednastavene heslo v systemu
        -   OTP
    -   neco mame - klic (kryptograficky klic - cipova karta, token), OTP, biometrie
    -   kombinace: 2FA

#### Jméno a heslo

-   něco víme
-   uživatel má přidělené unikátní přihlašovací jméno a autentifikuje se pomocí zadání hesla, které zná jen on
-   celé to stojí a padá na hesle, většinou kombinované s dalšími metodami (2FA)
-   **výhody:** při volbě dobrého hesla špatně prolomitelné
-   **nevýhody:** dobré heslo může být hůře zapamatovatelné, lze odposlechnout (key-logger, nezabezpečená síťová komunikace, kolega kouká přes rameno)

#### Otisk prstu

-   něčím jsme
-   uživatel naskenuje do systému pomocí čtečky otisk svého prstu/ů a poté se jejich opětovným oskenováním autentifikuje
-   **výhody:** mám pořád u sebe, relativně těžko zkopírovatelné a malé riziko zcizení
-   **nevýhody:** nutnost HW na skenování, není 100%

#### Scan obličeje

-   něčím jsme
-   podobně jako u otisku uživatel naskenuje pomocí fotoaparátu svůj obličej a opětovnám skenem se autentifikuje
-   **vyhody a nevyhody**: obdoba otisku prstu

#### PIN

-   něco víme
-   kratší, číselné heslo, typicky 4, 6 nebo 8 čísel
-   **výhody:** kratší než heslo (snadno zapamatovatelný)
-   **nevýhody:** lehce uhodnutelný (otisky na displeji, někdo kouká přes rameno)

#### SSH klíč

-   něco víme
-   uživatel si vygeneruje soukromý a veřejný klíč, který si vymění s protistranou (typicky server), autentizace se pak provádí pomocí veřejného a privátního klíče
-   **výhody:** bezpečný, generovaný (odpadají slabá hesla)
-   **nevýhody:** složitější nastavení, ukládání privátního klíče (problém, když mi někdo zcizí notebook/pc)

#### OTP

-   něco mám
-   typicky generátor tokenů (vlastním generátor) nebo SMS ověření (vlastním mobil)
-   generátor tokenů se spáruje se systémem, při přihlášení pak zadávám token generovaný často jen na omezenou dobu
-   telefonní číslo se spáruje se systémem, při přihlášení pak zadávám kód z SMS
-   **výhody:** typicky použití jiného kanálu, v kombinaci s další metodou zvyšuje bezpečnost
-   **nevýhody:** menší komfort, možné větší náklady

#### 2FA

-   kombinace typicky 2 způsobů (může být i více, pak multifactor autentification - MFA)
-   typické kombinace: jméno a heslo + OTP generátor, jméno a heslo + SMS, jméno a heslo + kód poslaný emailem
-   **výhody:** zvyšuje bezpečnost
-   **nevýhody:** snižuje komfort

#### RFID karta

-   něco mám
-   na kartě čip s uloženým klíčem, kterým se autentizuji proti systému
-   **výhody:** bezpečná
-   **nevýhoda:** musím mít fyzicky u sebe, mohu lehce ztratit/přijít o ni

### Zvýšení bezpečnosti uživatelských účtů

-   cim mene uctu tim lip
    -   ruseni nebo blokovani nepotrebnych uctu vzniklych po instalaci OS
    -   ruseni nebo blokovani uctu, ktere do systemu jiz nemaji pristup (zmena pozice, ukonceni prace atd)
-   IAM - Identity Access Management
    -   automatizovany system pro spravu uctu
    -   system, jehoz ucelem je zajistit ze spravni uzivatele maji odpovidajici pristup k definovanym prostredkum
    -   resi i opravneni uzivatelu
-   zamezeni uniku informaci o uzivatelskych uctech
    -   casto pouzivana opakujici se schemata (jmenoprijmeni, email ..)
    -   nezverejnovat informace o zamestnancich
    -   zamestnanci by nemeli verejne pouzivat firemni ucty/maily
    -   heslo by nemelo byt pouzite 2x, ani v systemech v ramci jedne site
    -   zakaz volatilnich sluzeb nebo alespon filtrovat pristup k nim
    -   pozor na metadata fotografii a dokumentu
    -   zamezeni sdileni uctu

### Zabezpečení síťových služeb

-   protokoly
    -   vymena klicu (handshake)
    -   prenost dat
    -   sifrovani (TLS 1.3)
-   algoritmy
    -   sifry - RSA, AES
    -   delka klice - cim delsi, tim bezpecnejsi
-   omezeni poctu sluzeb a jejich pravidelna aktualizace
-   zvyseni bezpecnosti samotnych sitovych protokolu
-   firewall
    -   lokalni (na zarizeni), sitovy (na siti)
    -   brana mezi 2 sitemi (intranet <-> internet, domaci sit <-> internet)
    -   specifikace IP adres a portu, ktere jsou pristupne
    -   IP filtr - zahazuje pakety na zaklade portu a IP adresy
        -   problem: dynamicke odchozi porty - musime vytvorit pravidlo s rozsahem portu
    -   **bezstavový firewall** hlídá komunikaci mezi 2 sítěmi a pomocí adresy a portu blokuje nebezpečná spojení
    -   **stavovy firewall** si navíc oproti bezestavovému drží záznamy o procházejících spojeních a ví jestli je spojení nové (handshake) nebo stávající
    -   **aplikacni firewall** brani pred utoky na aplikaci
        -   sleduje obsah a zpravy ruznych protokolu a kontroluje dotazy, jestli jsou adresare pristupne verejnosti, jestli se nejedna o SQL injekci atd
        -   Cloudflare WAF, Azure Firewall
    -   aktivni/pasivni firewall
        -   pasivni - loguje a informuje administratora
        -   aktivni - hlida a blokuje

### Opatření proti průniku škodlivého kódu (aktualizace, anitivirová ochrana)

-   skodlivy kod - ma za dusledek neopravneny pristup k pocitaci
-   opatreni
    -   aktualizace - systemu a programu
    -   anitivirus
    -   vzdelavani uzivatelu
    -   filtrovani, omezovani sitovych sluzeb
-   proniknuti skodliveho kodu
    -   ziskani pristupu
    -   vylakani autentizacnich udaju
    -   zneuziti chyby (exploit, backdoor, neaktualizovany system)
    -   interakce uzivatele

### Souborový systém (autorizace, šifrování)

-   autorizace - prava na cteni a zapis do souboru
-   linux - `chmod` zapis prav ([r], [w], [x]) pro 3 skupiny: vlastnik [u], skupina [g], ostatni [a]
-   windows - ACL (Access Control List) - seznam opravneni pro objekt (soubor, slozku)

### Zabezpečení síťových protokolů

### Logování a Auditing

-   skodlivy kod muze po sobe mazat logy -> vyhodnejsi ukladat jeste jinam
-   k tomu slouzi SIEM - Security information and event management
-   co logovat?
    -   kdo, kdy, jaka akce, odkud (z jake site)
    -   prihlaseni k uctu (i neuspesne)
    -   zmeny v uctech (zakladani, ruseni, zmena)
    -   pristup k objektum
    -   zmeny politik
    -   pouziti privilegii (pristup k necemu, k cemu uzivatel nema pristup)
    -   systemove udalosti

### Šifrování

-   dostupnost
-   duvernost
    -   ochrana dat pred ziskanim
-   integrita
    -   nechceme aby data mohli byt zmeneny
-   rozdil od kodovani (zmena dat na zaklade pravidel - pr. RLE, base64, utf)
-   algoritmus + klic -> zasifrovana data
-   2 urovne
    -   jednotlive souboru - crypt/decrypt
    -   cely disk / sekce - bitlocker, truecrypt
-   problem distribure klice - resi asytmetricka kryptografie

#### Symetricka kryptografie

-   symetricka kryptografie (jeden klic sifruje i desifruje)

#### Asymetricka kryptografie

-   2 klice - jeden na sifrovani, druhy na desifrovani
-   elektronicky podpis
    -   verejny klic - kontroluju podpis
    -   privatni klic - podepisuju data
    -   slabina - podvrzeni verejneho klice (odchytim zpravu a podvrhnu verejny klic, zpravu zasifruju s odpovidajicim privatnim)
        -   reseni -> certifikacni autorita -> certifikat (overeni ze dany klic patri dane osobe/entite atd)
-   https - TLS protokol
    -   mozny odposlech dat - chranime sifrovanim
        -   MIM - man in the middle - muze podvrhnout klice a navazat tak "bezpecnou" komunikaci, ve ktere odposlouchava zpravy -> overovani verejneho klice pomoci certifikatu u autority
    -   nesifrujou se prenasena data (vypocetne narocny)
    -   sifruje se jen symetricky klic a s nim se pak sifruje (mene narocny)

#### Utoky mezi klientem a serverem

-   odposlouchavani komunikace - **sifrovani komunikace**
-   spoofing (fake email) + phishing (fake stranka) - **overovani certifikatu - musi vlastnit banka**
-   MIM - man in the middle
    -   prostrednik, kterej zachytava komunikaci
-   proxy server
    -   komunikace mezi intranetem a internetem
    -   proxy server se prida do povolenych cert. autorit

#### Protokoly a algoritmy

-   protokoly
    -   navazani spojeni, vymena klicu, handshaking, vymena dat atd
    -   TSL 1.3 - doporuceny (mensi verze nebo SSL uz se nemaji pouzivat)
-   algoritmy
    -   RSA, AES, 3DES
    -   PGP - pretty good privacy - sifrovani souboru
    -   delka klice v bitech (ruzne delky a doporuceny delky pro kazdy algoritmus)
-   hashovaci algoritmy
    -   SHA512, bcrypt
    -   prekonany: MD5, SHA1

### Opatření proti útokům prostřednictvím klientů

-   utoky na sw (klienty) jsou vedeny prevazne pomoci skodlivych souboru (dokumenty, videa, obrazky atd)
-   opatreni
    -   aktualizace sw, antivir, vhodna konfigurace (vypinani nepotrebnych sluzeb ..)
    -   spousteni klienta v sandboxu, virtualni OS nebo OS z prenositelneho media

### Fyzické zabezpečení

-   kdyz se utocnik dostane k technologiim, muze instalovat hw backdoory, krast aktiva, ziskat pristupove informace atd.
-   nutne adekvatne chranit
    -   pristupove systemy (vratnice, turnikety)
    -   zabezpeceni budov
    -   kamerove systemy
    -   dektektory pohybu atd

### Ověření zabezpečení

-   kdyz jsou definovana a nasazena vsechna opatreni z politiky
-   overeni zevnitr (whitebox) - konfigurace, nastaveni
    -   CIS benchmarky - dokumenry o podrobnostech nastaveni danych systemu
-   scan zranitelnosti - vulnerability scan (blackbox) - automatizovany scan
    -   kontroluje sitove sluzby (porty) a pote pomoci db jejich zranitelnosti
    -   OpenVAS, Nessus
    -   obvykle provaden pravidelne (napr jednou mesicne)
    -   zname chyby v programech (databaze)
-   penetracni testy
    -   utok ze zkousi z pohledu samotneho utocnika (tester se snazi prolomit zabezpeceni)
    -   testovaci metodiky: OSSTMM, PTES, OWASP (testovani aplikaci z venku), Mithre attack (navody z pohledu hackera)
    -   vysledkem je popis nalezenych nedostatku, ale i vsech provedenych testu a jejich vysledku

### IDS/IPS/Honeypot

-   IDS - Intrusion detection system
-   IPS - Intrusion prevention system
    -   detekuji (IDS) nebo i blokuje (IPS) utoky na siti
    -   podle chranene vrstvy: sitove nebo aplikacni (web, db) - v posledni dobe prolinani - analyzuje se vsechno
    -   architektura
        -   senzor, agent: sbira a analyzuje sitovou komunikaci
    -   ridici centrum (konzole)
        -   sprava dat
        -   generovani vystrahy
        -   korelace udalosti
        -   monitorovani ostatnich komponent
    -   pravidla, jak utok muze vypadat - pak se podle toho hledaji skodlive pakety
    -   zpusoby zapojeni
        -   TAP - mezi 2 sitemi, vetveni site, monitoruje kazdy paket
        -   MIRROR PORT - provoz zrcadlen na jiny port
        -   IPS take blokuje, ale nemuze byt zapojen na MIRROR PORT
    -   tvorba pravidel
        -   negative security model - blacklist
        -   positive security model - whitelist
-   honeypot
    -   demoni simulujici realne sluzby, ktere umoznuji detektovat a analyzovat utoky
    -   tvari se jako bezny server
    -   casto nasazovany ve forme celych siti
    -   provoz, ktery smeruje do techto siti je pak povazovan za skodlivy/utok
    -   typy
        -   nizkointeraktivni - nelze se dostat dovnitr
        -   vysceinteraktivni - lze se dostat dovnitr (zamer, pak se sleduje co tam utocnik dela, co chce dostat atd)

### SIEM

-   Security information and event management
-   agregace dat, korelace dat, generovani poplachu, analyzovani dat
-   compliance - shoda s normami a pozadavky zakonu
-   centralni uloziste pro logy, ktere se zde i podle pravidel analyzuji
-   prijima data z: antiviru, honeypotu, IPS, IDS, dochazkovych systemu, sitovych komponent (DNS), web serveru, databazi ..

### Reakce na incident

-   jak se o nem dozvedet
    -   SIEM, zamestnanci
-   jak analyzovat
    -   shromazdeni informaci o incidentu
    -   analyza dukazu (policie)
    -   izolace napadeneho systemu
    -   umoznit stihani utocniku
    -   vytvaret podrobne protokoly
-   metodologie:
    1. priprava
        1. priprava pred incidentem
        2. vytvoreni tymu, ktery bude problem resit
        3. definice potrebnych nastroju a pomucek
        4. pravidelne treninky
    2. detekce incidentu
        1. urcit zda jde opravdu o incident (kdy, co, kdo, jaky hw, ohrozene systemy, kontakty na zucastnene osoby)
    3. pocatecni reakce
        1. zajisteni informaci
        2. zjisteni dulezitosti napadenych systemu
        3. zjisteni potencionalniho utocnika
        4. zjisteni neopravneneho pristupu (kam se dosta a jaka prava ma)
        5. odhad financni ztraty
    4. formulace strategie
        1. sestaveni strategie reseni incidentu
        2. musi byt odsouhlasena vrcholem firmy
    5. duplikace
        1. duplikace kritickych dat (aby nedoslo k prepsani informaci, ktere zanechal utocnik)
        2. ziskani zivych dat (RAM, registry, vyrovnavaci pamet, disky)
        3. duplikace kazdeho bitu (od zacatku az po sluzebni stopu)
        4. duplikace:
            - vyjmeme medium a zduplikujeme v pocitaci
            - duplikat provedeme v nenapadenem systemu
            - duplikujeme v uzavrene siti
    6. patrani
        1. analyza dukazu o incidentu
        2. chyby pri zpracovavani dukazu:
            - zmena casovych znacek
            - zruseni podezrelych procesu
            - aplikace zaplat pred dokoncenim analyzy
            - nezaznamenavani prikazu behem vysetrovani
            - pouziti neproverenych prikazu
            - prepsani potencionalnich dukazu
    7. implementace bezpecnostnich opatreni
        1. izolace utocnika - odpojeni od site, izolace pocitace, filtrovani provozu
        2. dukazy shromazdovat pred implementaci bezpecnostniho opatreni
    8. monitorovani site
    9. obnova
        1. puvodni podoba systemu
        2. zaplatovani
    10. protokolovani - zdokumentovani patrani
    11. pouceni se z chyb
