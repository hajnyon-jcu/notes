# 685 - Objektove programovani 2

## Zpracovavani chyb

-   vyjimky

### Chybove situace

-   nespravna implementace
-   nepatricna zadost o objekt - pr. null pointer, neplatny index
-   nekonzistentni nebo nevhodny stav objektu - pr. vice vlaken

-   ne vzdy jde o chybu programatora - napr. nespravny vstup, prava na soubor, obsazeny port..
-   faze
    -   objeveni chyby
    -   ohlaseni
    -   zpracovani

### Defensivni programovani

-   spis predpokladame, ze chyba nastane a reagujeme na ni
-   napr. ma server predpokladat, ze se klienti chovaji spravne? (validni dotazy)

### Vyhazovani, zpracovani vyjimek a oznamovani chyb

-   jak hlasit nevalidni argumenty
    -   uzivateli - jen kdyz existuje a muze s tim neco udelat (spatne vyplneny formular)
    -   klientskemu objektu - vyhozeni vyijimky nebo diagnosticka chyba (true/false, 0/1-255 etc)
        -   klient pak kontroluje -> vyjimky vhodnejsi
-   u vyjimek neni potreba spec. navratova hodnota - cistejsi kod
-   chyby nemohou byt ignorovany
-   syntax
    -   `throw new Exception(..)`
    -   javadoc anotace `@throws Exception ..`
    -   metoda `public void metoda() throws Exception {}`
    -   klauzule `try {} catch() {}`
        -   chyta v try a predava do catch
        -   muze chytat vice ruznych vyjimek
        -   `finally {}` provede se vzdy (ikdyz ne/nastal catch)
-   hierarchie v Jave
    -   `Throwable`
        -   `Error`
        -   `Exception`
            -   `MyCheckedException` - predvidatelna selhani - mozne zotaveni
            -   `RuntimeException` - nepredvidatelna selhani - zotaveni je nepravdepodobne
                -   `MyUncheckedException`
-   ucinek vyjimky
    -   metoda vyhazujici vyjimku je predcasne ukoncena
    -   nevraci se hodnota
    -   rizeni je predano do bloku `catch` (program nepokracuje na miste volani metody, ktera vyhodila)
-   nekontrolovane vyjimky
    -   pouziti neni kontrolovano kompilatorem
    -   kdyz nejsou odchyceny zpusobi konec programu
    -   napr. `IllegalArgumentException`
    -   extenduje se `RuntimeException`
-   kontrolovane vyjimky
    -   urcene k odchyceni a reakci na ne
    -   kompilator je kontroluje
    -   lze provest zotaveni
    -   extenduje se `Exception`
-   aserty
    -   pouziti pro kontrolu vnitrni konzistence
    -   prikaz `assert`
    -   odstranuji se v produkcni verzi
    -   `assert <bool>` nebo `assert <bool> : <vyraz>`
    -   vyhazuje `AssertionError`

## Prace se soubory

-   nachylne na chyby - interakce s externim prostredim (prava, smazane soubory atd)
-   `java.io`
-   `Reader` a `Writer` - pracuji s char
-   `Stream` - pracuji s byte
-   ARM - Automatic Resource Management
    -   zajisteni ze zdroje jsou po pouziti uzavreny
    -   try - with - resource
    -   `try(FileWriter writer = new FileWriter("file-name")) {} catch(IOException)`
        -   neni pozadovane volani `close` metody na soubor
-   vstup ze souboru
    -   `FileReader`
    -   `BufferedReader` - radkovy vstup

## Vicevlaknove programovani

### Procesy

-   vlastni prostredky pro beh (hlavne pamet)
-   pri behu programu obecne kooperuje mnozina procesu (pomoci sockets a pipes)
-   vetsina implementaci JVM bezi jako jeden proces
-   moznost vytvaret systemove procesy jako instance tridy `ProcessBuilder`
-   **vlakna - threads**
    -   poskytuji prostredi pro beh programu
    -   existuji v ramci procesu a sdili zdroje procesu (pamet, otevrene soubory - kontext)
    -   vytvoreni vlakna vyzaduje mene zdroju nez procesu
    -   main thread - spusteni aplikace, vytvareni dalsich vlaken
    -   vyhody
        -   vyuziti zdroju (vice vlaknove procesory)
        -   zjednoduseni nekterych aplikaci
    -   nevyhody
        -   slozitejsi navrh
        -   rezie prepinani vlaken
        -   synchronizace
    -   vlakno je instance tridy `Thread`
        -   lze pimo vytvaret instance a resit synchronizaci
        -   nebo abstrahovat reseni napr. pomoci `Executor`
    -   interface `Runnable` nebo extendovani `Thread` -> metoda `run` a `start`
    -   `sleep` - docasne pozastaveni vlakna (muze byt ukoncena prerusenim)
    -   preruseni - interrupt
        -   indikace pro vlakno, ze by melo skoncit
        -   vlakno musi podporovat i vlastni preruseni
        -   vyjimka `InterruptedException` a metoda `interrupted`
        -   nastaveni preruseni pomoci `interrup` metody
    -   `join`
        -   umoznuje jednomu vlaknu pockat na druhe
        -   na preruseni reaguje vyhozenim vyjimky `InterruptedException`
    -   stavy vlakna
        -   new - nove, nebezi
        -   runnable - zavolani `start` - bezi
        -   blocked - je necim blokovano (napr. ceka na zamek)
        -   waiting - vlakno ceka - `wait`
        -   timed waiting - vlakno ceka nejaky cas - `sleep`
        -   terminated

### Problemy synchronizace

-   sdileni pristupu ke zdrojum
-   nekonzistence sdilene pameti
    -   vice vlaken ma pohled na data (prectena hodnota), ktere se ale lisi
-   synchronizovani metod
    -   synchronizovani metod pomoci `synchronized`
    -   nebo synchronizovani bloky (musi specifikovat obje,t ktery poskytne zamek)
        -   vlakno pak ceka, pokud jine vlakno pracuje v synchronized bloku/metode
    -   postaveno na vnitrnich zamcich (vlakno nejdriv musi zamek ziskat, pak muze provest operace a pak ho vraci)
    -   reentrantni synchronizace - vlakno uz ma zamek a znovu vola metodu, ktera ho potrebuje (ten samy zamek objektu) - pak je to ok
-   atomicky pristup
    -   atomicka akce se bud vykona cela nebo vubec
    -   napr
        -   cteni a zapis z/do promenne (krome long a double)
        -   cteni a zapis z/do promenne deklarovane jako volatile (vcetne long a double)
    -   atomicke akce se nemohou prolinat - stale nutna synchronizace
-   deadlock
    -   dve ci vice vlaken jsou navzdy navzajem blokovana (vzajemne na sebe cekaji)
-   starvation - vlakno neni schopne ziska pristup ke zdroji a neni schopno udelat pokrok
-   livelock - vlakna jsou prilis zamestnane (ne blokovane) a cekaji bez moznosti udelat pokrok
-   guarded blocks
    -   zacina podminkou, ktera umoznuje jeho splneni (ceka az se bude moci provest)
    -   vyuziva metodu `wait` pro pozastaveni aktualniho vlakna (metoda uvolni vnitrni zamek a pozastavi se)
    -   pozdeji jine vlakno ziska zamek a pomoc `notifyAll` o tom da vedet ostatni
    -   typicke pouziti - producent - konzument
        -   pro tento priklad lze pouzit `SynchronousQueue`, ktera producent - konzument resi za nas

### Immutable objects

-   objekt, ktery nemeni svuj stav po vytvoreni (inicializaci)
-   hodi se ve vlaknovych aplikacich
-   nema verejne metody typu set
-   pouziva `final` a `private`

### Balicek `java.util.concurrent`

-   `Lock`
    -   sofistikovanejsi zamky
    -   muze byt vlastnen jednim vlaknem v case
    -   podporuje wait/notify
-   `Executor`
    -   umoznuje spusteni novych uloh
    -   oddeleni managmentu a vytvareni vlaken od zbytku aplikace
    -   `ThreadPool`
        -   minimalizuje rezii spojenou s vytvarenim vlaken (drzi vlakna ziva a znovupouziva je)
    -   `ScheduledThreadPool` - zpodene nebo opakovane akce
    -   `Fork`/`Join` - efektivni vyuziti vice procesoru
        -   distribuuje ulohy mezi vlakna v poolu
        -   pouziva work-stealling algoritmus (volne vlakno muze ukrast praci jinemu)
        -   kdyz je prace dost mala, vlakno ji udela, kdyz je velka - rozdeli ji a muze si ji vzit jine vlakno
-   Concurrent kolekce
    -   interface: BlockingQueue, ConcurrentMap ..
    -   class: ArrayBlockingQueue, ConcurrentHashMap ..
-   Atomicke promenne
    -   AtomicInteger, AtomicLong..
-   `ThreadLocalRandom`
    -   hodi se kdyz nekolik uloh pouziva nahodna cisla paralelne v thread pools
    -   neni kryptograficky bezpecne
    -   mensi overhead nez vlaknove pouziti `Random`

### Vlakna ve Swing a JavaFX

#### Swing

-   druhy vlaken
    -   initials - pocatecni vlakna
        -   vytvori objekt typu `Runnable`, ktery inicializuje GUI a vlastni beh `run` uz obstarava EDT
    -   event dispatch thread (EDT) - provadi obsluhu udalosti a akce s komponentami
        -   obstarava tvorbu GUI - swing neni thread safe
    -   worker threads - casove narocne ulohy na pozadi
        -   `SwingWorker`
            -   `done` metoda provedena po skonceni pracovniho vlakna vlaknem EDT
            -   `doInBackground` metoda spoustena v pracovnim vlakne vracejici hodnotu
            -   `publish` metoda muze poskytnout mezivysledek (vola metodu `process` v EDT)
            -   muze definovat vlastnosti, jejichz zmena generuje udalosti zpropagovany v EDT
            -   `cancel` metoda zrusi ulohu

#### JavaFX

-   obdoba swingu
-   graf sceny neni thread safe - mel by byt modifikovan pouze JavaFX Application Thread (JAT) vlaknem
-   narocne ulohy zase pomoci pracovnich vlaken

    -   interface `Worker` – komunikace pracovních vláken s UI
    -   třída `Task` – implementace FutureTask
    -   třída `Service` – provádí instance třídy Task
    -   třída `WorkerStateEvent` – událost při změně stavu objektu Worker
    -   interface `EventTarget` – implementován v Task a Service – umožňuje naslouchání stavovým událostem
    -   ukonceni - `canceled` metoda, vlakno musi souhlasit - `isCanceled`

-   JConsole - program pro management Java aplikace
    -   moznost pripojeni na lokalni proces
-   JMC - JDK Mission Control - sbira informace o chodu programu
-   Java Flight Recorder - program pro zaznam chodu aplikace

## Sitova komunikace

### Zabezpeceni

-   objekt typu `SecurityManagr` - nastaveni security policy
    -   kazdy projekt muze mit svoji, defaultni v instalacni slozce javy (od v8+ je editovatelne v textaku)
    -   akce ktere nejsou povolene managerem vyhodi `SecurityException`
-   overeni bezp. omezeni
    -   `System.getProperties`

### Protokoly

-   budeme se bavit o transportni (udp, tcp, ...) a castecne o aplikacni (http, ftp, ...)
-   palikacni vrstva
    -   `java.net`, `java.io`, `java.nio`
-   porty
    -   umoznuji beh vice aplikaci vyuzivajicich sit. adapter
    -   mapovani prichazejicich dat
    -   0..65535
        -   0..123 - well known - rezervovany
        -   1024..49151 - registrovane
        -   49152..65535 - dynamicke a soukrome porty

#### TCP

-   garance doruceni paketu
-   url - Uniform Resource Locator
    -   adresa zdroje na internetu
    -   `URL`, `URLConnection`
    -   `http://example.com?aa=true` - protokol, jmeno zdroje, parametry
-   lze cist pomoci `InputStream`
-   lze zapisovat pomoci URL a odesilat tak data
-   socket
    -   predstavuje koncovy bod obousmerne komunikacni linku meze 2 programy v siti
    -   `Socket`, `ServerSocket`
    -   `Socket` reprezentuje spojeni klient - server
        -   server nasloucha a ceka na zadost klienta
        -   klient se pripoji, server akceptuje a vytvori novy socket, aby mohl porad poslouchat
        -   pak uz spolu komunikuji

#### UDP

-   vez garance doruceni datagramu (paketu)
-   `DatagramPacket`, `DatagramSocket`
-   `MulticastSocket` - odesilani vice prijemcum poslouchajicich na multicastu

-   `NetworkInterface` - poskytuje informace o aktivnich sitovych spojenich
    -   lze zjistit informace o sitovych rozhrani pocitace

## Databaze

### JDBC

-   zakladni framework pro praci s db
-   `Connection` - navazani spojeni (je potreba jdbc ovladac - mysql, sqlite etc)
-   datovy zdroj
    -   `DriverManager` - pripojeni pomoci url (napr `jdbc:mysql://localhost:3306/`)
        -   `Statement` - objekty schopne dotazovat se do db
            -   `executeUpdate`, `executeQuery` ..
            -   moznost zpracovavat davky
        -   `Prepared Statement` - ochrana sql injekce
        -   `ResultSet` - vystup z db - tabulka
            -   data pristupna pres `Cursor` - ukazatel na radek
            -   mozno modifikovat - pak zavolat `updateRow` -> update v db
        -   moznost ovladat `commit` a `rollback`

### ORM

-   vazba relacni db <-> objekty
-   nastroje
    -   JDO - Java Data Objects
    -   JPA - Java Persistence API (Hibernate, EclipseLink ..)
        -   standard popisujici API pro ORM
        -   tridy
            -   anotace `@Entity`
            -   alespon jeden constructor bez parametru
            -   metody ani trida nesmi byt final
            -   neverejne atributy + getter/setter
            -   muzou dedit z entitni i neentitni tridy
        -   relace
            -   one-to-one
            -   one-to-many
            -   many-to-one
            -   many-to-many
        -   konfigurace v `persistence.xml`
        -   stavy entity
            -   persist - ulozeni do db - `INSERT`
            -   remove - `DELETE`
            -   merge - `UPDATE`
            -   find - `SELECT`

#### Hibernate

-   konfigurak v xml (url db, drivery, cesta na entity atd)
-   POJO - Plain Old Java Objects
    -   jednoduche objekty primarne k ukladani dat

### Grafova databaze

-   neo4j
-   uzly a hrany (volitelne atributy)
-   implementace grafovych algoritmu pro hledani atd.
-   ma svuj jazyk na dotazovani
-   java komunikace pomoci protokolu `bolt` nebo jazyku `Cypher`

## XML

-   popis viz [687 - Znackovaci jazyky](../687-znackovaci_jazyky/README.md)
-   vyuziti DTD a XSD schemat pro validaci
-   parsery
    -   cteni a kontrola
    -   extrakce hodnot
    -   proudove cteni - SAX (Simple API for XML)
        -   postupne se cte XML a pro kazdou cast se generuje udalost, na kterou muze programator reagovat
        -   rychlost, mala pametova narocnost
        -   nelze se vracet, nizkourovnovy zpusob zpracovani
    -   stromova reprezentace - DOM - Document Objet Model
        -   cely dokument se nacte do pameti ve stromove strukture
        -   cely dokument k dispozici, umoznuje cteni i zapis, spoluprace s Xpath
        -   pomale nacitani a velka pametova narocnost
    -   JAXP - Java API for XML Processing
        -   push parsery - napr SAX, po precteni casti se generuji udalosti
        -   pull parsery - napr StaX, cteni probiha na zadost, udalosti generujeme a parser na ne reaguje vracenim casti dokumentu
    -   JAXB - Java Arch. for XML Binding
        -   objektove mapovani elementu XML na objekty

## JSON

-   API
    -   objektovy - prace s modelem v pameti
    -   proudovy - parsovani

## Lambda vyrazy

-   od java 8
-   vyuzivaji rysy funkcionalniho programovani
-   funkcionalita se predava jako argument
-   syntax: `(param1, param2) -> {//code}`
-   oddelujeme tim co se ma delat a jak se to ma delat
-   `Predicate` - testuju podminku
-   `Consumer` - rikam co delat, kdyz to splnuje podminku
-   `Function` - vraci hodnotu
-   priklad

```java
public static <X, Y> void processElements(Iterable<X> source, Predicate<X> tester, Function <X, Y> mapper, Consumer<Y> block) {
    for (X p : source) {
        if (tester.test(p)) {
            Y data=mapper.apply(p);
            block.accept(data);
        }
    }
}

processElements(
    roster,
    p -> p.getGender() == Person.Sex.MALE && p.getAge() >= 18 && p.getAge() <= 25,
    p -> p.getEmailAddress(),
    email -> System.out.println(email)
);
```

-   vyuziti agregovanych operaci, ktere akceptuji lambda vyrazy

```java
roster
    .stream()
    .filter(p -> p.getGender() == Person.Sex.MALE && p.getAge() >= 18 && p.getAge() <= 25)
    .map(p -> p.getEmailAddress())
    .forEach(email -> System.out.println(email));
```

### Agregovane operace

-   pipeline - sekvence agregovanych operaci
-   interni iterovani - moznost paralelne

```java
double average = roster
    .stream()
    .filter(p -> p.getGender() == Person.Sex.MALE)
    .mapToInt(Person::getAge) // namapuj age na integer
    .average() // redukcni operace
    .getAsDouble();
```

-   redukcni operace (average, min, max, sum, count)
    -   obecne:
        -   reduce - napr. pocitani sumy - 1. arg. vysledek a 2.arg current value
        -   collect - modifikuje current value

### Paralelismus

-   paralelni reseni vypoctu
    -   rozdeleni na podproblemy
    -   sestaveni reseni z podproblemu
-   agregovane operace a paralelni streamy umoznuji impl. paralelismu u kolekcich (ktere nejsou thread safe) pokud se kolekce nemodifikuje pri zpracovani
-   otazkou je zda se vyplati - hw, velikost ulohy atd
-   paralelni zpracovani streamu
    -   rozklad na podstreamy - temi se iteruje a pak se kombinuji vysledky
    -   `Collection.parallelStream`
-   poradi zpracovani zavisi na zpracovani (ser/paral), zdroji streamu a mezioperacich
-   vedlejsi efekt
    -   interference - nepouzivat `filter` a `map` - modifikace
    -   stavove lambda vyrazy - vysledek zavisi na stavu, ktery se muze menit behem provadeni pipeliny
-   vsechny mezioperace jsou lazy
