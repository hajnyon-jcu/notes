# 789 - Multimedialni technologie

## Multimedia

-   sdeleni - informace pro prijemce
-   medium
    -   vlastni forma sdeleni
    -   typicky cleneno podle lidskych smyslu
-   komunkacni kanal - zpusob prenostu sdeleni (kodovani, protokol...)
-   druhy
    -   text (linearni medium)
    -   hypertext (nelinearni medium - texty propojene odkazy)
    -   grafika (fotografie, obrazky)
    -   zvuk (audio, rec, hudba ..)
    -   video (animace, video)
-   dalsi media: hmat (Brailovo?), cich?, chut?
-   multimedia
    -   kombinace ruznych medii ve sdeleni
    -   pro nase ucely hlavne elektronicky prenositelna
    -   obecny pojem pro sdeleni zejmena zvukem a videem
-   hypermedia
    -   rozsireni pojmu hypertext pro medialni oblast
    -   nelinearita
    -   interaktivita
-   atributy medii
    -   forma: text, hypertext, video, audio ...
    -   tok: jednosmerny, obousmerny
    -   ucastnici: asynchronni, synchronni (online)
    -   kardinalita: 1:1, 1:N, N:N
-   vnimani medii
    -   zpusob komunikace s okolim
    -   fyziologicka faze - receptory, transformace
    -   psychicke zpracovani - uvedomeni, interpretace
-   psychologie
    -   clovek vnima to co chce
        -   hist. zkusenost, podprahove vnimani, kontext
    -   genderove odlisnosti, vek, kultura ...

### Text

-   umele vytvorene medium
-   zalozen na (obvykle) konecne abecede
-   je nutne se ho naucit
-   lehce elektronicky prenositelne (kodovani)
-   mnohoznacnost
    -   vice moznosti pro vyjadreni tehoz, pochopeni tehoz
-   i graficke atributy (font)
-   obsah - stylistika, emocni zabarveni, terminologie
-   forma - velikost, barva ..

### Hypertext

-   nelinearni medium zalozene na blocich textu a jejich propojeni odkazy
-   priblizeni chovani cloveka
    -   asociace
    -   pokud me neco zajima, chci vedet vice
-   rozvej hlavne s WWW (html)
-   odkaz muze byt i graficky (obrazek)

## Pocitacova grafika

-   cleneni
    -   vnitrni reprezentace v PC
        -   znakovy rezim - matice znaku (80x25), pouziva kodovani (ASCII, UTF8..), DOS, UNIX (cmd)
        -   graficky rezim - ovladani kazdeho elementu zvlast, rozliseni v bodech (1920x1080), nutny lepsi hw
            -   standardy: CGA, EGA, VGA ..
    -   reprezentace barev
    -   typy grafiky
    -   pocet dimenzi
-   internet
    -   minimalizace toku dat
    -   podporovane: jpg, png, gif, svg
    -   3d grafika - ruzne standardy

#### Terie barev

-   propustene a odrazene svetlo
-   barevne hranice technologii - podle poctu odstinu zakl. barev
-   zakladni model
    -   ruzny pocet zakladnich barev
    -   kazde zarizeni svuj gamut
-   RGB - red, green, blue
    -   pro zarizeni vyzarujici svetlo
    -   odpovida fyziologii oka
    -   je aditivni (pridani barev)
-   CMYK - cyan, magenta, yellow, black
    -   tiskarny
    -   pohlcovani ruznych delek svetla pigmenty
    -   je subtraktivni (ubirani barev)
-   HSB - hue, saturation, brightness
    -   malirske michani barev
    -   lepe odpovida lidskemu vnimani
-   primo pojmenovane bravy
    -   standardy
-   reprezentace barev
    -   pro kazdy bod nutny udaj o barve
    -   kriteria
        -   pocet zobrazitelnych barev - napr 16.7 mil
        -   pocet soucasne zobrazitelnych barev
    -   dva hlavni pristupy
        -   indexove
            -   barvy nastaveny predem z sirsi palety (RGB paleta - pro kazdou barvy 1B)
            -   na konkretni barvu odkazovano indexem
            -   od poctu barev je odvozen index (16 barev => 4bit index, 256 barev => 1B index)
        -   obecne
            -   RGB jednotlive pro kazdy bod
            -   podle poctu bitu na jeden bod je dan pocet moznych barev (16b => HiColor - RGB 5,6,5b, 24b => TrueColor - RGB po 1B)
-   vnimani barev
    -   receptory - oko
        -   tycinky - 120-130mil
        -   cipky - 5-6mil
        -   rozliseni cca 70Mpix
    -   procesor - mozek
-   psychologie barev
    -   ruzne barvy ruzne pusobi na lidi

### Bitova grafika

-   bez doplnkove informace
-   udaje o kazdem bodu samostatne
-   hlavni pouziti u fotografii (vytvarena mimo digitalni prostredi)
-   typicke transformace
    -   barevne - jas, kontrast, efekty
    -   prostorove - spirala, morphing
-   soubory
    -   konstantni rozliseni
    -   velikost souboru
    -   komprese
        -   bezeztratova
            -   BPM, TIFF, TGA, PCD
        -   ztratova
            -   GIF, PNG, JPG
        -   kompresni pomer = vystup/vstup
        -   ucinnost, casova a pametova narocnost, kvalita
        -   metody - RLE, slovnikove, rozdilove kodovani, prediktivni kodovani
    -   WebP - google, ztratova i bezeztratova
-   import
    -   fotoaparat - clona, expozice, citlivost, snimac, svetelnost..
    -   scanner - rozliseni, barevna hloubka
    -   screenshot
-   transformace
    -   barevne
        -   snizeni poctu barev
        -   barevny odstni
        -   jas, kontrast
        -   histogram
    -   geometricke
        -   zmena rozliseni, manipulace s casti obr. nebo celkem, simulace nastroju (stetec, pero)
-   programy
    -   jednoduche editory - malovani, picasa3 ..
    -   retuze a montaz - photoshop, gimp ..

### Vektorova grafika

-   objektovy pristup
    -   strukturalni popis objektu (primka, souradnice, barva ..)
-   doplnkova informace
-   jiny sw - prevod popisu na zobrazeni
-   z principu bezeztratove
-   uklada se pouze popis
-   formaty: WMF (Win), CDR (Corel), SVG (web)
-   programy CorelDraw, Illustrator, Inkscape

#### SVG

-   zakladem je xml
-   napr. inkscape

### 3D grafika

-   obvykle zvlastni pripad vektorove
-   strukturalni popis ve 3 souradnicich
-   nutny sw s vizualizacnimi schopnostmi (blender, 3d studio max)
-   modelovani "realnych" objektu - terragen
-   udaje
    -   popis objektu - umisteni, povrch
    -   opticke vlastnosti povrchu - textury
    -   zdroje svetla - umisteni, barva, tvar a typ
    -   pozorovatel
        -   pozice, smer pohledu, sire zaberu
-   narocnost na hw
    -   akceleratory
        -   urychleni vykreslovani hlavni 3D
        -   pouze v nekterych pripadech (aplikace to musi vyuzivat)
        -   Direct3d, OpenGL, DirectX, WebGL

## Tvorba medialnich del

-   psychologie - obsah vs forma
-   dila umelecka
    -   zamerena na umelecke ztvarneni nametu (predani myslenky, pohledu)
    -   drama, komedie, akce ...
-   reklama
    -   specificka forma - zkratkovitost, rychlost ..
    -   hlavni cil je zaujmout
-   vzdelavaci dila - odborna, popularne naucna
-   zpravodajska a publicisticka
    -   nestrannost, fakta, udalosti, vysoka mira improvizace
-   dokumentarni - fakta, muze byt umelecky ovlivneno
-   prubeh
    -   namet
    -   (projektovy) zamer
        -   ucel, cilova skupina, distribucni strategie
        -   financi zajisteni (investice)
    -   preprodukce - priprava na porizeni medialnich podkladu
        -   scenar
        -   produkcni prehled - seznam vsech elementu pro produkcni fazi
        -   priprava produkce (lokality), administrativa (povoleni, uzavreni objektu)
        -   stab, herci, zvukovy navrh, vybaveni sceny, kulisy
        -   casting
        -   logistika produkce (casovy plan, doprava, technika)
    -   produkce
        -   porizeni samotneho materialu
        -   kontinuita a integrita
        -   obraz - zlaty rez, poloprostor (orientace divaka)
        -   prace s kamerou (pozadi, popredi, zaostreni ..)
        -   osvetleni, zvuk, rezie, specialni efekty, kaskaderi
    -   postprodukce - zpracovani a uprava
        -   editace obrazu a zvuku - synchronizace, vylepseni, slazeni ..
        -   finalni produkt - integrita zvuku a obrazu
        -   sprava medii - ulozeni na medium (kazety, film, disk ..)
        -   finalni uprava (DVD)
    -   distribuce - sireni produktu
        -   kombinace ruznych forem
        -   marketing a propagace
        -   kina, online distribuce, dvd

### Scenar

-   chronologicky popis obsahu dila
-   slozky
    -   umelecka
    -   technicko-formalni - sceny, zabery, zvuk
-   pozadavky
    -   jednotna podoba
    -   pevny vztah mezi strankou a delkou
    -   neuvadet zbytecne detaily
-   obsah
    -   popis sceny
    -   popis situace na scene
    -   dialogy
    -   pokyny pro kameru
    -   zpusob strihu
-   spec. sw - napr Celtx

## Audio

-   mechanicke vlneni
-   analogove vs digitalni
    -   kontinualni vs diskretni
    -   vzdy ztratovy prevod
-   praze se zvukem
    -   zaznam - digitalizace A/D
    -   ulozeni - formaty
    -   zpracovani - upravy
    -   prezentace - D/A
-   vnimani zvuku
    -   clovek: 20Hz - 20kHz
    -   mensi rozsah intenzit nez zrak
    -   logaritmicka citlivost
        -   prah slysitelnosti, bolesti
    -   frekvencni spektrum - zastoupeni frekvenci
    -   prostorovy vjem - stereo
    -   kvalita sluchu - maskovani frekvenci, casove maskovani
-   dB korekce A,B,C,D
    -   prizpusobeni lidskemu vnimani hlasitosti
-   intenzita zvuku
    -   objektivni vs subjektivni metriky
    -   merici kmitocet - 1000Hz, sinusovy prubeh
    -   hodnoceni hlasitosti - Fon [Ph] - logaritmicka
    -   subjektivni hodnoceni hlasitosti - Son [son] - linearni
-   dB v PC - 0 je maximum - technocka mez zarizeni
-   druhy
    -   mira podilu ruznych frekvenci jako u svetla
    -   cisty ton - jedina frekvence
    -   ton s harmonickymi slozkami a dominantni frekvenci - jiny tvar vlny
    -   bily sum - nahodny signal s rovnomernou spektralni hustotou
    -   ruzovy sum - vykonova frekvencni hustota je primo umerna prevracene hodnote frekvence
-   sireni zvuku
    -   rychlost sireni
    -   pohlcovani zvuku - selektivni na zaklade kmitoctu
    -   odraz, rezonance, dozvuk
-   snimani
    -   mikrofony
        -   dynamicky - magnet v civce
        -   kondenzatorovy - zmena vlastnosti dielektrika
    -   typy
        -   frekvencni charakteristika
        -   smerova charakteristika
        -   provedeni
-   vystup
    -   piezoelektricke menice - zejmena vyssi kmitocty
    -   reproduktory - civka v magnetickem poli
    -   sluchatka - eliminace ruchu
-   zesilovani
    -   elektronicke, analogove, digitalni

### Obecny zvuk

-   nestrukturovany - lze zaznamena jakykoliv zvuk
-   ruzne formaty zaznamu
    -   PCM - pulsne kodova modulace
        -   snima okamzite hodnoty napeti v danych intervalech
        -   vzorkovaci frekvence
            -   pocet sejmnutych vzorku za vterinu (Hz)
            -   musi byt 2x vetsi nez nejvyssi zaznamenavana frekvence zvuku (pouzivani 44.1KHz)
        -   pocet rozlisitelnych urovni signalu - kvantovani
            -   pocet bitu pro ulozeni tohoto udaje - 16b => 65k urovni
        -   mono vs stereo vs 4.1 vs 5.1 vs 7.1
            -   nasobne pozadavky
        -   pr.: 1s zvuku ve stereo v CD -> `44100 Hz x 2B x 2(stereo) = 176 400 B (150 kB/s)`
    -   ADPCM, Delta, CCITT
-   prace se zvukem
    -   na urovni prubehu signalu - casova osa, transformace
    -   visestopy zaznam a jeho zpracovani
    -   scenar
    -   audacity, razor, cakewalk
-   transformace
    -   amplitudove - uprava obalky, ztlumeni, zesileni, normalizace, fadein/out
    -   casove - dozvuk, echo, zrychleni
    -   frekvencni - fourierova transformace, propuste, zadrze
    -   vzorkovaci - uprava vzorkovaci frekvence
-   ukladani dat
    -   kontejner - struktura pro ukladani dat o zvuku
        -   obecne pro jakakoliv data
        -   Ogg, AVI, MP4, WAV
    -   kodek - algoritmus pro transformaci dat pro ulozeni a zpet
    -   nelze libovolne kombinovat
-   souborove formaty
    -   ruzny datovy tok, komprese, ztrata informace
    -   WAV, RAW, CD - velikost, ale mozna prima konverze
    -   MP3 (vyuziva nedokonalosti lidskeho sluchu), AC3 - ztrata informace
-   generovani
    -   nove zvuky nebo napodobeni originalu
    -   generatory a syntezatory

### Strukturovany zvuk

-   prace s doplnkovou informaci
-   omezena abeceda
-   hudba - MOD, MIDI
-   analyza a synteza reci
-   MOD - Amiga
    -   uklada se samotny prubeh skladby
    -   sejmute vzorky nastroju
    -   stopa, kde bude vzorek prehravan
    -   cislo vzorku
    -   cas pocatku prehravani
    -   doba trvani prezentace vzorku
    -   frekvence vzorku, hlasitost
    -   4 samotne kanaly
    -   zpracovani - na urovni not, moznost vymeny nastroju
    -   tvorba - samplery, syntezatory
-   MIDI
    -   strukturovana zvukova data
    -   v souboru pouze prubeh skladby (uspora mista za cenu omezeni moznosti)
    -   16 kanalu
    -   zpracovani - komfortni, notovy zapis, zmeny nastroju
    -   trackery - editace strukturalniho popisu
    -   sequencery - editace hudebnich souboru vcetne not, vicestopy, smycky
    -   syntezatory - prace s oscilatory
-   standardy
    -   VST - virtual studio technology
        -   pluginy pro ruzne ucely
    -   LADSPA - Linux Audio Developers Simple Plugin API

#### Analyza reci

-   identifikace predem znamych zvukovych vzorku
    -   detekce povelu, ovladani hlasem
-   syntakticke zpracovani mluveneho slova - diktovaci sw
-   semanticke zpracovani - detekce vyznamu, automaticky preklad
-   deep learning
-   synteza reci
    -   prirazeni fonemu
    -   telefonni systemy
    -   hromadne IS
    -   pomucka pro nevidome

### Sonic PI

-   nastroj pro strukturalni popise hudby (programovani hudby)
-   kombinace obecneho a strukturovaneho zvuku
-   nastroje
    -   generovani zvuku
    -   transformace
    -   zvukove efekty
    -   sestavovani kompozic
-   prikazy
    -   `play`, `sample`, `sleep`
    -   funkce, promenne, smycky, vlakna

## Video

-   sekvence rychle se stridajicich obrazku
-   frekvence 25-30fps, dnes vice
    -   vychazi z normy (h.264, MPEG2)
-   formaty
    -   lisi se zpusobem komprese (kvalita, velikost)
    -   podle informace
        -   bez doplnkove informace - AVI, MPEG, QuickTime
        -   s doplnkovou informaci - nektere animace
    -   podel datoveho toku
        -   CBR, VBR
-   animace
    -   priklad videa s doplnkovou informaci (flash) - neni to podminka (GIF)
    -   digitalni vs klasicka
    -   ruzny postup
    -   prace se scenou - casto 3D
        -   meni se parametry v zav. na case
        -   snimek se vytvori a zaradi do video sekvence
-   transformace
    -   casova osa
    -   strih, hudba, vkladani ruznych medii
    -   titulky
    -   zmena kodovani
-   porizeni
    -   kamery - ruzny ucel, snimace, pametove medium, rozliseni, format
    -   screencast
    -   konvertory
-   strih - postprodukce
    -   kombinace nekolika zdroju do vysledneho produktu
    -   strih, titulky, dabing

### AVI

-   Audio Video Interleave
-   kontejnerovy format
-   vice datovych stop
-   ruzne kodeky
-   streaming
-   starsi format - nestandardni info o pomeru stra, casove znacky

### QT - Apple Quick Time Movei

-   soucast sirsiho rozhrani
-   kontejnerovy format
-   zaklad MPEG4
-   `.mov`
-   rada kodeku

### MPEG

-   Motion Pictures Experts Group
-   komprese
    -   I-frame - plny snimek
    -   P-frame - rozdilovy (odlisnost mezi aktualnim a predchozim snimkem)
    -   B-frame - rozdilovy (rozdil mezi dvema nejblizsimi I nebo P snimky)
    -   nutne mit vzdy potrebne snimky
    -   problem pri editaci
-   MPEG1 - CD, 1.5Mb/s
-   MPEG2 - DVD, 15Mb/s
-   MPEG4
    -   kontejnerovy format MP4
    -   H.264/AVC - 50% uspora objemu dat
    -   HEVC/H.265 - dvojnasobna ucinnost proti H.264
    -   zvuk v mp3, aac
-   MPEG7 - nastupce MPEG4
    -   archivace multimedii + vyhledavani
-   MPEG21 - pro 21. st.
    -   obecny kontejner
-   MJPEG - samotne obrazku v jpeg

### Dalsi formaty a kodeky

-   DivX - firemni kode, vysoka komprese
-   Xvid - odnoz DivX 4
-   RealMedia - streaming
-   WebM - pro HTML5
-   archivace
    -   VCD
    -   XVCD
    -   SVCD
    -   DVD
    -   Blue-Ray

## Multimedia a internet

-   online distribuce multmed. obsahu
-   duraz na kompresi
-   streaming
    -   unicast - 1:1
    -   multicast - 1:N
    -   kombinace
-   dnes HTML5
-   video na internetu
    -   distribucni server - moznost vice zdroju, unicast i multicast, cloudova reseni
    -   HTML5 - WebRTC
-   komunikace
    -   obousmerny tok dat
    -   ICQ - specificky protokol, puvodne pouze text
    -   Skype - vlastni protokol, puvodne jen audio
    -   VoIP - Voice over IP - standardni protokol, ruzne kodeky, siroka podpora
-   videokonference
    -   dostatecna zvukova kvalita a primerena kvalita obrazu
    -   skype, hangouts, teams
-   SMIL - Synchronized Multimedia Integration Language
    -   predevsim pro distribuci po sitich
    -   oddeleni scenare od dat
    -   propojeni ruznych technologii - viedo, audio, www, grafika

## Pokrocile zpracovani medii

-   nove metody a technologie
-   umela inteligence
    -   neronove site a deeplearning
    -   ruzne techniky
-   klasicke techniky analyzy dat
    -   text - identifikace autora, pokusy o umely text
    -   grafika - identifikace osob, podobnost obrazu
    -   zvuk - analyza a synteza
-   nove pristupy zalozene na princ. neur. siti
    -   text - analyza nalady, preklad, generovani textu
    -   grafika - rozpoznani objektu, generovani grafiky
    -   video - predikce
    -   zvuk - generovani obecneho i strukturovaneho zvuku
-   uceni
    -   bez ucitele
        -   podobnost v datech
        -   pouze vstup
    -   s ucitelem
        -   klasifikace, predikce
        -   vstup a pozadovany vystup
    -   posileni uceni
        -   generovani
        -   kombinace predhozich
-   technologie
    -   data -tensory
    -   sw - tensorflow, teano
    -   jazyky - python (pycharm, pytorch)
    -   hw - potreba vykonu gpu - nvidia TPU
