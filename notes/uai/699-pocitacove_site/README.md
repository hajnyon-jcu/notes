# 699 - Pocitacove site

## Uvod

### xAN - x Area Network

-   LAN - Local
-   WLAN - Wireless Local
-   WAN - Wide
-   PAN - Personal
-   MAN - Metropolitan
-   CAN - Controller (CAN bus - Bosch)
-   CANaerospace
-   VAN - Virtual/Value-Added

### Zakladni pojmy zpusobu prenosu informaci

**Synchronni prenos**

-   data prenasena v paketech stejne delky, rozdeleny na sloty
-   sloty prirazujeme aplikacim a tak garantujeme siry prenosoveho pasma (aplikace muze mit vic slotu => vice dat)

**Paketovy prenost**

-   ruzne dlouhe pakety
-   zahlavi-data-zapati na linkove vrstve
-   pak ale nemuzeme garantovat siry prenosoveho pasma

**Virtualni okruh**

-   historicky telefonni okruh
-   nejdrive se sestavi cesta a pak se po ni prepojuji hovory
-   dnesk se nepouziva
-   IP protokol pouziva - kazdy paket svou informaci a muze jit jinou cestou nez ostatni
-   TCP protokol - zajistuje serazeni paketu (pri ruzne rychlem doruceni)

## Sitove vrstvy

Ruzne modely

![modely](./tcp_osi.png)

### TCP/IP

-   nejbeznejsi
-   4 vrstvy
    -   linkova a fyzicka
    -   IP
    -   TCP/UDP
    -   aplikacni

![pakety](./tcpip.svg)

#### 1. Fyzicka vrstva

-   vrstva zajistuje komunikace meze 2 fyzickymi zarizenimi

**Protokoly specifikuji:**

-   elektricke signaly pro prenos informace, tvary konektoru atd.
-   typ media
    -   kroucena dvojlinka
    -   koaxialni kabel
    -   opticke vlakno
-   prenosova rychlost (MB/s ..)
-   modulace - jakym zp. jsou modulovany informace
-   kodovani - jak jsou data kodovana
-   synchronizace

##### Obchodni pohled

-   spojeni posledni mile
    -   retail (uzivatele) - pevne linky, wifi, 4g,5g
    -   korporace - optika, metalicke spoje..
-   infrastruktura poskytovatelu
    -   metalicke spoje, optika, radiove spoje
-   subjekt NIX.cz
    -   jednotlivi poskytovatele si pres nej vymenuji data
-   mezikontinentalni spoje - podmorske kabely / druzice

##### Pevna linka

-   modulace do hlasoveho pasma -> demodulace
-   vyuzivala pouze zlomek zakladniho pasma kroucene dvoulinky
-   pasmo se rozdelilo na cast pouzivanou pouze pro hovory a cast pro data => ADSL
-   **ADSL** - Asymmetric Digital Subscriber Line
    -   obvykle vetsi potreba downloadu
    -   datove pasmo se rozdelilo na upstream (upload) a downstream (download)
    -   _FDM_ - frequency division multiplexing
        -   zacatek pro upstream a zbytek pro downstream
        -   problem je ze kroucena dvoulinka nemela vsechny frekvence stejne kvalitni
    -   _EC_ - echo cancellation
        -   najde si vhodne prenosove kanaly a ty pak pouziva (upstream nemusi byt uplne na zacatku ale posunuty) ![FDM & EC](./fdm-ec.png)
    -   dvoulinka domu -> splitter(pasivni zarizeni ktere oddeluje frekvence pro hovory a data) -> modem ADSL -> ethernet -> switch -> pc

![ADSL](./adsl.png)

##### Kroucena dvojlinka

-   zaklad v telefonnim vedeni (puvodne jednodratove)
-   nevyhody pokud by vedla prilis dlouho vedle sebe => reseni jejich vzajemne prekrizeni => vyruseni elektromagneticke indukce

**Kategorie**

-   4 pary kroucene dvoulinky

-   5e - nestinena, 100MHz ~ 100Mbit
-   6a - stinena hlinikovou folii + vnejsi stineni, 500MHz ~ 500Mbit
-   7 - stinena paskou s hlinikovou folii + vnejsi stineni, 600MHz ~ 600Mbit
-   7A, 8.2, 8

![UTP](./utp.png)

-   kabel zakoncem konektorem, typicky RJ-45 - 8 pinu

##### Opticke vlakno

-   2 sklenene trubicky, vnoreny do sebe a kazda ma jinou hustotu
-   nasvecuje se diodou -> paprsek se uvnitr lame -> odrazi se -> opticky prijimac
-   vlakno pro kazdy smer (tam a zpet)

**Typy**

-   vicevidove vlakno - 50/62.5 mikrometru
    -   paprsek se lame
-   jednovidove vlakno - 9 mikrometru
    -   tak tenke, ze paprsek jde temer rovnobezne

**Frekvence**

-   opticke vlakno klade ruznym frekvencim svetla ruzny odpor -> nejmensi odpor, tzv okna se pouzivaji pro prenos
-   vyuzivano infracervene zareni

![Optika](./optika.png)

**Napojovani**

-   navarovanim specialni svareckou
-   kabel se oholi az na sklo -> nasadi se keramicka ferula -> ktera drzi oba konce

**Dark fiber**

-   pronajimani optickych vlaken

**Strukturovana kabelaz**

-   budovy vybaveny kilometry kabelu
-   rack s propojovacim panelem (patch panel) + aktivni prvky -> napojeny do distribucniho boxu optiky

##### Podmorsky kabel

1. Opticka vlakna
2. vazelina
3. medena/hlinkova trubka
4. polykarbonat
5. hlinikova vodni bariera
6. ocel
7. dalsi vrchni ochrana

-   napaji se pomoci medenou trubkou - odporovy delic

##### Radioreleove spoje

-   max 50km

##### Geostacionarni druzice

-   35k km nad rovnikem - zpozdeni 0.25s
-   zemi lze pokryt uz 3 druzicemi

#### 2. Linkova vrstva

-   spojeni meze sousednimi pocitaci (lokalni i rozsahle site)
-   spoleha na fyzickou vrstvu a neresi jeji problemy
-   reseni posledni mile - tj. napr mezi poskytovatelem internetu a uzivatelem

##### Ethernet

-   sdilene prenosove medium s nahodnym pristupem
-   puvodne tlusty ethernet -> tenky ethernet
-   CSMA/CD - sdilene => kolize a ruseni
    -   Carrier Sense Multiple Access - nez vysilam, zkontroluju ze nikdo nevysila
    -   Collision Detection - odposlouchavam jestli nevznikly kolize - kdyz jo, dovysilam zpravu a pak nahodne dlouho cekam
-   efektivne se sdilene medium dalo vyuzit na ~20%

###### Ethernet II

-   zakladni linkovy protokol
-   **ramec**:
    -   adresa prijemce 6B
    -   adresa odesilatele 6B
    -   protokol: 2B
        -   jaky protokol vyssi vrstvy je v datove casti
        -   IPv4, IPv6, ARP, .. IEEE 802.3, VLAN
    -   data 46-1500 B
    -   kontrolni soucet - CRC - 4B

![Ethernet II](./eth2.png)

**Linkova adresa - MAC**

-   6B
-   nejnizsi 2 bity prvniho bajtu
    -   00 - prvni 3 - vyrobce, druhy 3 vyrobek
    -   01 - obeznik (skupinova adresa), ktery je urcen
    -   1x - adresa vlozena softwarem

**Linkove ramce - Pakety**

-   zakladni prenosova jednotka dat
-   zahlavi (header) - adresu prijemce, odesilatele
-   data (payload)
-   zapati (trailer) - checksum

###### IEEE 802

-   standardy pro lokalni site
-   rozdeleni fyzicke a linkove na 5 podvrstev

![802](./802.png)

**Rodina protokolu 802:**

-   _802.3_ - ethernet - obdobny Eth II - datova cast se sklada ze - zahlavi 802.2 - kdyby zarizeni byli propojene naprimo - zahlavi 802 SNAP - 3B organizacni kod, 2B typ protokolu ![802-3](./eth802-3.png)
-   _802.11_ - wifi
-   _802.15_ - PAN - personalni site (pr. bluetooth)

**Podvrstvy:**

-   Fyzicka: -_MAC_ - tvar konkretniho ramce
    -   _PLCP_ - zabaleni ramce
    -   _PMD_ - kodovani bitu a jejich prenos
-   Linkova:
    -   _802.1_ - prepinani - Bridging
    -   _802.2 LLC_ - Logical Link Control

###### VLAN

-   virtualni lan
-   IEEE 802.1Q - vklada za adresu odesilatele jeste svoje zahlavi:
    -   4B typ protokolu, 2B: 3bit - prioritni pole, 1bit muze byt zahozen ramec (pri prehlceni), 12bitu na identifikaci v ramci VLAN

![802.1Q](./802q.png)

##### WiFi - Wireless Fidelity

-   bezdratove lokalni site
-   IEEE 802.11 - WLAN
    -   cela rad standardu (a,b,g ...), ruzna pasma, rychlosti
    -   cesky telekomunikacni urad - urcuje ktere pasma se maji pouzivat (mohou nastat kolize pri pouziti produktu z jine zeme)

**Typicke konfigurace**

-   _Ad-hoc_ - stanice komunikuji navzajem, narocna konfigurace
-   _point-to-point_ - dvoubodove linky
-   _infrastrukturni site_ - rizena jednim access pointem
-   _roaming_ - virtualni oblast (ESS) vykryta vice access pointu
    -   stanice - zarizeni pristupujici k AP
    -   BSS - Basic Service Set - skupina stanic sdilejicich AP
    -   DS - Distrbucni system - abstraktni sit ktera bud neexistuje nebo je realizovana jinou siti (vice AP)
    -   AP - Access Point
    -   ESS - Extended Service Set - skupina BSS, tj. oblast ve ktere funguje sit
    -   portal - komunikace mezi DS a venkovni siti

![BSS](./bss.png)

**Zakladni sluzby**

-   _autentizace/de_ - prihlaseni
-   _asociace/di/re_ - asociace k AP
-   _vymena_ ramcu v ramci BSS
-   _distribuce_ - prenos ramcu mezi jedn. AP v ramci ESS
-   _integrace_ - prenost ramcu pres portal
-   _soukromi_ - sifrovani aj

**Stavy stanice**

-   _pocatecni_
-   _autentizovany_ - prihlasena
-   _asociovany_ - prihlasena + asociovana

**Typy ramce**

-   _datove_
-   _ovladaci_ - rizeni komunikace v ramci BSS
-   _ridici_ - rizeni site

###### Ramce

**Datovy**

-   2B ridici pole
    -   2b - verze
    -   2b - typ
    -   4b - subtyp
    -   1b - to DS
    -   1b - from DS
    -   1b - more fragment - ramec je rozdelen do vice fragmentu
    -   1b - retry - je ramec opakovany
    -   1b - power mngmt - po preneseni stanice prejde do usp. rez.
    -   1b - more data - stanice ma ve vyrovn. pameti vice ramcu
    -   1b - WEP - je zabezpeceno WEP - nepouziva se
-   doba platnosti
-   4x4B adres
    -   prijemce, odesilatel
    -   vysilajici stanice, prijimaci stanice
    -   5. typ je BSS id - identifikace konkretniho BSS
-   2B - cislo fragmentu, cislo ramce
-   0-3212B - data
-   4B CRC

**Ovladaci**

-   CSMA/CA
-   _CTS_ - Clear To Send
    -   2B - typ
    -   2B - doba trvani
    -   6B - cilova adresa
    -   4B - CRC
-   _ACK_ - Acknowledgement - stejny jako CTS
-   _RTS_ - Request To Send
    -   2B - typ
    -   2B - doba trvani
    -   6B - cilova adresa
    -   6B - vysilaci adresa
    -   4B - CRC
-   nez stanice zacne vysilat, tak posloucha jestli je medium volne -> pak zacne vysilat, pokud cilova stanice prijme -> posle ACK, pokud neprijme pak opakuje vysilani
-   stanice ktera chce odesilat vice ramcu, posle RTS a rezervuje si medium na konkretni casovy interval -> cilova odpovi CTS

**Ridici**

-   format
    -   2B - typ
    -   2B - doba trvani
    -   6B - cilova adresa
    -   6B - vysilaci adresa
    -   6B - BSS id
    -   2B - sequence control
    -   0-2312B - data
        -   1B - id
        -   1B - delka
        -   informace
    -   4B - CRC
-   _SSID_ - Service Set IDentifier
    -   logicke jmeno site
-   _Majakovy ramec - Beacon frame_
    -   nabizeji potencionalnim uzivatelum informaci o siti
-   _Otukavaci ramec - Probe frames_
    -   probe request - stanice zada o parametry AP
    -   probe response - parametry
-   _asociace/re/dis_
-   _autentizace_
    -   otevrena sit
    -   autentizace pomoci sdileneho tajemstvi
    -   dalsi

##### Rozsirene LAN

-   soustavy jednotlivych LAN
-   problem vice switchu - jejich propojeni do smycky
    -   obeznikove boure
    -   problemy s konektivitou (zprava prijde z vice zdroju - nevi jaka spravna adresa)
    -   nekolikanasobne doruceni

###### Spanning Tree Protocol

-   kostra grafu - acyklicky graf
-   cilem je vypnout opakovani ramcu na rozhrani, ale pritom moznost dorucit zpravu kamkoliv
-   BPDU - Bridge Protocol Data Units
-   hledani nejkratsi cesty, ostatni porty oznacime, aby se nepouzivali pro prenos dat
    -   hrany ohodnoceny podle rychlosti spojeni
-   nekolik standardu
-   nevyhody
    -   vypnute cesty nelze vyuzit napr. k load balancingu
    -   nevyuzivaji se nektere cesty (ikdyz by byli vyhodne)

###### Shortest Path Bridging - SPB

-   nejkratsi cesta - Djiskstruv alg.
-   drzi si info o delce cest a muze pak pouzit tu nejvyhodnejsi
-   pri vypadku cesty se najde jina
-   nektere switche se oznaci jako designated
    -   koncentruji informace o dane casti site
    -   designated switche si vymeni svou tabulku s informacemi a tak znaji celou sit
-   narocna konfigurace

##### Propojovaci kabel RJ-45

-   pro 10 a 100 Mb se pouzivaji pouze 4 vodice
-   pro 1 gb se pouziva vsech 8

##### Repeater

-   pocitace pripojeny a repeater opakuje jeho zpravy na vsechny pripojene zarizeni
-   nutna CSMA/CD

##### Switch

-   propojuje jen pocitace, ktere spolu chteji komunikovat
-   lze vypnout CSMA/CD
-   prepinaci tabulka - linkovych adres pripojenych zarizeni
-   muze propojovat i ruzne linkove protokoly (napr. ethernet 1gbit a 10gbit)

#### 3. Sitova vrstva - IP

-   komunikace mezi 2 vzdalenymi pocitace (kdekoliv po internetu)

**IP datagram**

-   IP-zahlavi
-   data

##### Smerovac - Router

-   predava IP datagramy mezi jednotlivymi ip sitemi

##### Internet

-   puvodne ARPANET 1967 - prvni sit
-   **host** - hostitelsky pocitac
-   **hop** - vzdalenost mezi 2 spojenymi pc
-   supersit, sit, subsit

###### Autonomni system

-   poskytovatele internetu ruzne propojovani - tvori internet
-   nektere propojeni nejen podle algoritmu ale i podle politik
-   typy
    -   stub - pahylovy, jedna linka na AS
    -   multihomed - AS propojen s vice AS
    -   tranzit - tranzituje data jinych pokytovatelu
-   je identifikovan `2B`, dnes `4B`

##### IPv4

-   2 sluzebni protokoly - ARP, RARP(uz se nepouziva -> nahrazen DHCP)
-   ICMP a IGMP
-   4B adresy

**IPv4 Datagram**

-   ip zahlavi
    -   `4bity` - verze
    -   `4bity` - delka zahlavi
    -   klasifikace - sousedni pocitace se muzou dohodnout na priorite paketu (pr. voice over ip pakety by se mely vyrizovat prednostne)
    -   ECN - co s paketem v pripade zahlceni site
    -   `2B` - celkova delka ip datagramu
    -   identifikace ip datagramu
    -   `3bity` - priznaky fragmentace
        -   kdyz dostanu velky paket, ktery nedokazu poslat, tak ho rozdelim na mensi podle priznaku
    -   fragmentace - posunuti fragmentu
        -   offset - v podstate poradi fragmentace
    -   `1B` - TTL - time to live
        -   kazdy smerovac na ceste polozku snizuje a kdyz prijde na 0 tak ji smerovac zahodi a posle zpravu 'time exceeded'
        -   `tracert/traceroute` - ICMP
    -   `1B` - protokol vyssi vrstvy - TCP, UDP, SCTP, ICMP ..
    -   checksum zahlavi
    -   `4B` adresa odesilatele
    -   `4B` adresa prijemce
    -   volitelne polozky zahlavi - dnes uz se nepouzivaji (povazovany za nebezpecne)
-   data ip datagramu

###### ARP - Address Resolution Protocol

-   ziskani linkove adresy sitoveho rozhrani protistrany ve stejne podsite pomoci IP adresy (v eth ziskavame MAC)

###### IGMP - Internet Group Management Protocol

-   sireni skupinovych obezniku v LAN
-   stanice se prihlasi k prijmu a pak je odbira
-   v internetu se siri po obeznikove kostre (MBONE)
-   bali se do IP zahlavi a obsahuje jednoduchy paket typu:
    -   dotaz na skupiny
    -   zadost o clenstvi ve skupine
    -   opusteni skupiny

##### IPv6

-   samotny protokol IPv6
-   ICMPv6 - sluzebni protokol
-   prvni specifikace uz v r. 95, nejnovejsi r. 2017

**IPv6 Datagram**

-   `4bity` - verze
-   `1B` - trida
    -   co s paketem pri zahlceni
    -   priorita
-   `2B` - identifikace toku dat
    -   spolu s IP adresou identifikuji smer toku dat
-   `2B` - delka dat
-   `1B` - dalsi hlavicka
    -   odkaz na dalsi hlavicku, ktera muze nasledovat za adresou
    -   dalsi hlavicka vzdy obsahuje delku a odkaz na dalsi hlavicku
    -   napr. informace pro smerovace, smerovaci informace, zahlavi fragmentu (smerovace po ceste nemuze fragmentovat), autentizacni hlavicka (IPSec), 58 - ICMPv6
-   `1B` - pocet hopu - obdoba TTL
-   `16B` - adresa odesilatele
-   `16B` - adresa prijemce

###### ICMPv6

-   obdoba ICMPv4
-   echo request/reply
-   navic:
    -   zadost/oznameni o linkovou adresu (nahrazuje ARP)
    -   zjisteni adresy smerovace na LAN (nahrazuje DHCP)
-   obsah
    -   typ, kod, checksum a telo podle typu

##### IP adresy

-   2 adresy - adresa site, adresa rozhrani
-   `<ADRESA>/<MASKA>`

**Maska**

-   rika nam, ktera cast IP adresy je pouzita pro identifikaci site a ktera pro stanice v lokalni siti

**Typy adres**

-   _unicast_ - konkretni adresa
-   _multicast_ - skupina adres
-   _broadcast_ - vsechny adresy v LAN
-   _anycast_ - IPv6 - adresuje se skupina, ale zpravu staci dorucit na jeden z nich

| adresa          | IPv4            | IPv6      |
| --------------- | --------------- | --------- |
| tento pocitac   | 0.0.0.0         | ::/128    |
| loopback        | 127.0.0.1       | ::1/128   |
| lokalni adresy  | 10., 172., 192. | FC00::/7  |
| linkove lokalni | 169.254.0.0/16  | FE80::/10 |
| multicast       | 224.0.0.0/4     | FF00::/8  |

-   lokalni adresa - muze platit i pro intranet cele firmy
-   linkova lokalni - adresa na linkove lokalni siti

**Prikazy**

-   `ipconfig/ifconfig`

###### Alokovane verejne adresy

-   dostaneme od poskytovatele internetu
-   IANA - organizace, rozdeleni adres do bloku (podle kontinentu vicemene)
-   IPv4 - v podstate vycerpany
-   IPv6 - od bloku 2000::/3

###### IPv4

-   `4B`, pouziva se v desitkovem zapisu
-   specialni adresy - broadcast, tento pocitac(0.0.0.0), loopback (127.0.0.1), obezniky
-   intranet adresy
    -   10.0.0.0/8
    -   172.16.0.0/12
    -   192.168.0.0/16

###### IPv6

-   `16B`, pouziva se v sestnactkovem zapisu
-   vedouci nuly se nemusi zapisovat
-   zdvojena `::` nahrazuje libovolne mnozstvi nul
-   _unicast_
    -   sklada se z adresy site (64bitu) a adresy sitoveho rozhrani (64bitu)
    -   globalne jednoznacne adresy
    -   lokalne jednoznacne adresy - v ramci LAN
-   pridelovani - rucne, nahodne nebo EUI-64
-   _multicast_
    -   `8bitu` - FF
    -   `4bity` - trvaly/docasny obeznik
    -   `4bity` - rozsah skupiny tvorici obeznik (lokalni uzel, LAN, globalni..)

**EUI-64**

-   namapovani 6B linkove adresy na 8B EUI-64
    -   mezi puvodnim 3. a 4. bajtem se vlozi 2 bajty `FFFE`
-   prevod na IPv6
    -   invertuje se druhy nejnizsi bit v prvnim bajtu

##### Smerovani - Routing

-   router - ma **smerovaci tabulku**
    -   seznam cilovych sitich (adresa + maska) a rozhrani na kterych se site nachazeji a take metrika
    -   posledni radek vetsinou - sit 0.0.0.0/0 - default gateway
    -   `netstat -r` - vypis tabulky

###### Protokoly

**Redistribuce smerovacich informaci**

-   _Interier Gateway Protocols - IGP_
    -   v ramci autonomniho systemu
    -   RIP II, IS-IS, OSPF
-   _Exterier Gateway Protocols - EGP_
    -   mezi autonomnimi systemy (politiky atd.)
    -   pracky DDOS utoku - na hranici AS se mohu ptat odkud prichazeji packety a blokovat cely AS
    -   EGP, BGP (Boulder Gateway Protocol)

**RVP - Routing Vector Protocol**

-   Bellman-Forduv alg.
-   smerovace maji informace o sousedech, pak si sousedi vymeni informace -> az maji info o cele siti
-   muzou vznikat smycky - resi RIP protokol (omezeni hopu)

**LSP - Link State Protocols**

-   alg nejkratsi cesty v grafu (Djikstra)
-   routery tvori graf, linky ohodnocene metrikou
    -   1. krok - zjistovani dostupnosti sousedu (hrany)
    -   2. krok - uzel si doplni smerovaci tabulku
-   nevyhoda - kazdy clen zaplavuje tabulku sousedum (pri velke oblasti)
    -   dvojstupnove reseni - urci se nizsi urovne -> _SPB_, _IS-IS_

**IS-IS - Intermedite system to Intermedite system**

-   sit se rozdeli na oblasti
-   uvnitr oblasti jsou routery Level 1 (uvnitr oblasti) a Level 2 (mezi oblastmi) - nadefinuje admin
-   resi smerovani pro site, ktere podporuji vice sitovych protokolu (ipv4, ipv6)

**OSPF - Open Shortest Path First**

-   admin urci paterni oblast a na ni napojene dalsi oblasti

###### Anatomie sitoveho rozhrani

-   stanice muze byt cilem, ale muze slouzit i jako router (preposilani packetu - forwarding)
-   pri doruceni IP datagramu
    -   je urcen pro nas - predan vyssi vrstve
    -   neni urcen pro nas -> source routing (resi do ktereho interfacu) / forwarding (predavani z jednoho interfacu do druheho)

![Anatomie](./anatomie.png)

#### 4. Transportni vrstva - TCP/UDP

-   slouzi aplikacni vrstve pro komunikaci mezi aplikacemi na vzdalenych pocitacich

**TCP segment/UDP datagram**

-   TCP/UDP zahlavi
-   data

**Identifikace dat mezi aplikacemi**

-   IP adresa odesilatele
-   IP adresa prijemce
-   port odesilatele
-   port prijemce
-   protokolem TCP/UDP

**Port**

-   sada pro UDP a TCP - `2B` => 2x 2^16 = 65k
-   privilegovane - 0-1023
    -   25/tcp - SMTP
    -   80/tcp - HTTP
    -   443/tcp - HTTP pres TLS
    -   53/udp i tcp - DNS

##### UDP - User Datagram Protocol

**Packet**

-   `2B` - zdrojovy port
-   `2B` - cilovy port
-   `2B` - delka dat
-   `2B` - checksum

##### TCP - Transmission Control Protocol

-   dvoulinka - od odesilatele k prijemci a zpet
-   jednotlive bajty cislovany a potvrzovany

**Packet**

-   `2B` - zdrojovy port
-   `2B` - cilovy port
-   `4B` - poradove cislo odesilaneho bajtu
-   `4B` - poradove cislo potvrzovaneho bajtu
-   `5bitu` - delka zahlavi
-   `3bity` - 000
-   `1B` - priznaky
    -   NS - ECN (Explicit Congestion Notification)
    -   CWR
    -   ECE - prvni tri se pouzivaji pro kontrolu zahlceni site
    -   URG - TCP segment nese nalehava data
    -   ACK - TCP segment ma platne pole 'poradove cislo prijateho paketu'
    -   PSH - signalizace ze segment nese aplikacni data
    -   RST - odmitnuti TCP spojeni
    -   SYN - odesilatel zacina novou sekvenci cislovani
    -   FIN - odesilatel ukoncil odesilani dat
-   `2B` - delka okna - pocet bytu, ktere je mozne poslat (nez potrebuju potvrzeni)
-   `2B` - checksum
-   `2B` - ukazatel nalehavych dat - zpracovava se prednostne
-   volitelne polozky zahlavi
    -   MSS - maximalni delka segmentu

**Okno - Window**

-   maximalni mnozstvi dat, ktere muzu poslat aniz by byli potrvzena prijemcem
-   pr.: prijemce a odesilatel se dohodnou na segmentu delky 1024 a okne 4kB
    -   tj odesilatel muze odeslat 4x 1024 bez potvrzeni
    -   okno se posouva o potvrzene segmenty

**Klient server**

-   _server_
    -   ocekava pozadavky na portu (LISTEN), dostane je (SYN_RCVD)
    -   pak odesila odpoved
    -   a ceka na dalsi pozdavky
-   _klient_
    -   posila pozadavky na server (SYN_SENT)
    -   pak prijima odpoved (ESTABLISHED)
-   odmitnutu spojeni - priznak RST
    -   klient pozaduje spojeni na portu na kterem nic neni
    -   aplikace potrebuje rychle ukoncit spojeni
    -   podezreni na utok

**Segmentace a fragmentace**

-   prenase na data se rozsegmentuji do segmentu a prida se jim TCP zahlavi
-   pak se to rozfragmentuje a kazdy fragment dostane jeste IP zahlavi
    -   v prvni fragmentu je TCP zahlavi v druhem uz ne

**Photuris**

-   jiz nepouzivany protocol z 1999
-   server nejdrive overi klienta a az pak mu alokuje prostredky

##### SCTP

-   alternativa k TCP i UDP
-   dokaze propojit mnoho relaci jednim spojenim
-   asociace dvou stran - _peer to peer_
-   komunikace dvou peeru
    -   init ->
    -   init-ack <-
    -   cookie-echo ->
    -   cookie-echo <-
    -   komunikace ..
    -   shutdown ->
    -   shutdown-ack <-
    -   shutdown-complete ->
-   vyhoda
    -   nekolik streamu dat (muzou byt potvrzovane i nepotvrzovane)
    -   zachovava zpravy ve streamech
    -   multi-homing - mezi peery muze existovat vice spojeni

#### 5. Aplikacni vrstva

-   zajistuje komunikace 2 aplikacich na ruznych pocitacich

-   protokoly ke kterym bezny uzivatel neprijde:
    -   DHCP (automaticke prideleni adres), TLS a DTLS (zabezpeceni), Smerovani, SNMP (Simple Network Management Protocol)
-   protokoly, ktere vidi koncovy uzivatel v aplikaci
    -   HTTP, SIP a RTP (voice over ip), mail, FTP, ..

##### DNS - Domain Name System

-   preklada domeny na IP
-   domeny tvori stromovou strukturu
    -   korenova - .
    -   TLD (top level domains) - com, cz, net
    -   domeny druheho radu - google, seznam
    -   domeny tretiho radu ...
-   domenova jmena v ASCII - abeceda + cisla + pomlcka
-   IDN - Internationalized Domain Names
    -   domenova jmena jakekoliv znaky
    -   `xn--` - prefix pro neASCII znaky zakodovane (pr.: '.xn--fiqs8s')
-   u nas domeny spravuje nic.cz
    -   domeny v ASCII
-   v EU - eurid.eu
    -   podpora latinky, azbuky a novorectiny
-   specialni domeny
    -   in-addr.arpa a ip6.arpa - preklad domen
-   name servers - jmenne servery
-   resolver - klient - knihovna kterou volaji programy pro preklad domen
-   pouziva se UDP (pri mensim dotazu TCP)
-   typy
    -   pahylovy - rovnou dotaz na DNS
    -   s cache - drzi si cache
-   `nslookup` - utilita pro pouzivani DNS resolveru
-   zony - cela domena nebo jeji casti
-   jak probiha preklad
    -   resolver da pozadavek na preklad jmena - kdyz nedostane odpoved do timeoutu tak dava pozdavek na dalsi server
    -   DNS server jde reverzne
        -   kdo vlastni com?
        -   kdo vlastni google?
        -   kdo vlastni www? -> vraci IP adresu
-   nameserver
    -   autoritativni
        -   zonova databaze
        -   typy
            -   primarni/master - admin vyplnuje informace o domenach a IP adresach
            -   sekundarni/slave - prebira info z masteru
            -   stealth - neverejny
    -   neautoritativni
        -   cache
        -   typy
            -   caching only
            -   autoritativni server pro jinou zonu
    -   kazdy DNS server je primarnim server pro loopback
    -   informace ulozeny v RR vetach - Resource Record
        -   autoritaticni data se nactou z databaze do cache
        -   neautoritaticni data se nactou ze sitoveho provozu
        -   ruzne typy
            -   **A** - host address - preklad jmeno na ip adresy
            -   **NS** - Name Server - domenove jmeno nameserveru
            -   **CNAME** - Canonical NAME - domenove jmeno specifikujici synonymum k domene (jiz existujici)
            -   **SOA** - Start Of Authority
                -   vzdy prave jedna
                -   informace o zone
            -   **PTR** - reverzni preklad - preklad IP na domenove jmeno -> in-addr.arpa
            -   **MX** - Mail Exchange - dorucovani posty v internetu - skryvani subdomeny (pr.: user@my.comp.cz -> user@comp.cz)
            -   **AAAA** - preklad ip adresy v6
        -   format: [name][ttl] IN typ [data zavysla na typu]
-   **packet dns protokolu**
    -   zahlavi
        -   `2B` - identifikace
        -   QR - query nebo response
        -   OPCODE - typ otazky
        -   AA - autoritativni nebo ne
        -   TC - truncation - zprava zkracena
        -   `1bit` - RD - klinet chce rekurzivni preklad
        -   `1bit` - Ra - server umoznuje rekurzivni preklad
        -   pocet dotazu
        -   pocet odpoved
    -   dotaz - query
    -   odpoved - response
    -   autoritativni servery
    -   doplnujici info
-   kdo spravuje zony
    -   .
    -   TLD - NIC - Network Information Center
    -   dalse level - uzivatele
-   kdo spravuje reverzni zony
    -   in-addr.arpa a ip6.arpa - IANA alokuje adresy
    -   prideluji poskytovatelum
    -   ti prideluji uzivatelum

##### Mail

-   vznik 1982
    -   RFC 821 - SMTP
    -   RFC 822 - format internetove zpravy - `adresat@domena`
        -   zahlavi
            -   from:
            -   to:
            -   sender - kdo vyrizuje
            -   date
            -   reply-to
            -   in-reply-to
            -   cc - Carbon Copy
            -   Bcc - Blind Carbon Copy
            -   Message Id
            -   Subject
            -   X- - uzivatelem definovana hlavicka
            -   Resent- - hlavicky puvodni zpravy
            -   Received - zvlastni hlavicka
                -   jak mail prochazi servery - ty pridavaji info
                -   from, by, via, with, id, for
        -   prazdny radek
        -   telo zpravy

###### SMTP - Simple Mail Transfer Protocol

-   puvodne: textovy editor -> ulozeni do fronty -> SMTP client to periodicky checkoval a kdyz tam ceka mail -> pokusi se ho dopravit k adresatovi -> zase z fronty se zprava vezme a preda adresatovi (inbox)
-   postovni server
    -   klient
    -   fronta, inbox
    -   privatni inboxy uzivatele
-   port 25/tcp
-   komunikace
    -   klient <-> server - navazani spojeni
    -   220 SMTP Ready <-
    -   HELLO ->
    -   250 OK <-
    -   MAIL from a@b.c, RCPT To a@b.c ->
    -   250 OK <-
    -   DATA ->
    -   354 <-
    -   CRLF.CRLF ->
    -   250 OK <-

###### ESTMP - Extended SMTP

-   podobna komunikace
-   umi navic (rozsireni)
    -   SIZE - vetsi delka mailu
    -   DSN - podpora notifikace o doruceni na server adresata
    -   8BITMIME -
    -   CHUNKING - rozsekani dat
    -   AUTH - podporovane zpusoby autentizace
    -   STARTTLS - podpora SSL/TLS

###### POP - Post Office Protocol

-   klient
    -   pouziva SMTP na postovnim serveru pro odchozi emaily
    -   pouziva POP pro prijimani a manipulaci s inboxem

**POP3**

-   prikazy v protokolu
    -   CAPA - capability - co umi
    -   TOP - vypis zacatku zpravy, USER (autentizace), PASS, EXPIRE
    -   STAT - vypis poctu zprav, LIST - list zprav s velikosti
    -   RETR - stazeni mailu, DELE, RSET, QUIT

###### MIME - Multipurpose Internet Mail Extension

-   MIME-Version - indikuje pouziti MIME
-   Content-Type - podtyp dat posilanych ve zprave, eg: `text/plain`
    -   text - podtypy: plain, richtext, html, xml ..
    -   image, audio, video
    -   kompozitni typy - cela skala:
        -   `Multipart/Mixed` - zprava obsahuje nekolik dilcich zprav (zprava + attachment)
        -   `Multipart/Alternative` - casti v ruzne kvalite
-   Content-Transfer-Encoding - specifikuje pouzite kodovani
    -   `text/plain;charset=ISO-8859-2`, `7bit` - ASCII, `8bit`, `binary`
    -   `base64` - bity se deli na sestice -> hodnota 0-63 - 'abeceda' + 'cisla' + 'lomitko' + '='
        -   '=' se doplnuje
-   Disposition-Notification-To - notifikace o otereni zpravy
-   dalsi hlavicky: Content-ID, Content-Description ..

###### IMAP - Internet Message Access Protocol

###### WebMail

-   webovy server ktery stoji nad postovnim serverem a email muzu pristupovat pres prohlizec

##### SIP - Session Initiation Protocol

-   aplikacni protokol pro navazovani, modifikaci a ukoncovani relace (telefonni hovory, konference atd)
-   komunikace
    -   REGISTER -> - registrace do site u registratora (ten ma lokalizacni databazi - data kde je uzivatel )
    -   401 Unauthorized <-
    -   REGISTER (authorizace) ->
    -   200 OK <-
-   jiny priklad
    -   uz jsem registrovany v siti a chci se nekomu dovola
    -   je nakonfigurovana Outbound proxy (adresa ziskana z DHCP)
    -   nasleduje SIP proxy ktera to presmeru na Redirect Proxy (ta preda data o tom kde se nachazi volany)
    -   posle se invite na dalsi SIP proxy a nakonec k volanemu
-   entity
    -   user agent - podle toho jestli jsem volany nebo volajici tak server/client
    -   user agent client - inicializuje pozadavky
    -   user agent server - generuje odpoved na SIP pozdavky
    -   SIP server - element ktery prijima pozadavky
    -   proxy - na jedne strane prijima pozadavky na druhe kontaktuje ostatni
    -   outbound proxy - proxy na lokalni siti kterou dostaneme od DHCP
    -   Back-to-Backt user agent - robot ktery prijima hovory
    -   registrator - koncova entita ke ktere se uzivatel registruje pri zapnuti zarizeni
    -   redirect server
    -   gateway - brana do jine site
    -   policy server
-   zprava prokolu SIP - podobna mailu
    -   zahlavi
        -   metoda
        -   Via, To, From, Call-ID ..
    -   prazdny radek
    -   telo
-   odpoved protokolu SIP
    -   zahlavi
        -   stavovy radek `SIP/2.0 200 OK`
        -   To, From, Call-ID ..
    -   telo
-   metody
    -   ACK, MESSAGE (prenos zprav), NOTIFY (informace o zmene stavu), SUBSCRIBE
    -   BYE, INVITE, REGISTER
    -   CANCEL, INFO, OPTIONS, PRACK, PUBLISH, REFER, UPDATE
-   umi i konference
    -   prezentator
    -   pozorovatel/ucastnik - pomoci subscribe
-   muze bezet nad TCP, UDP i SCTP
-   uri () - identifikace ucastnika
    -   `sip:username:password@domena:port;parametry`
    -   `tel:phonenum;parametry`
-   zasilani zprav
    -   message ->
        -   message ->
            -   message ->
            -   ok <-
        -   ok <-
    -   ok <-
-   instant messaging
    -   je treba nejake medium - zpravidla TCP
    -   MSRP - Message Session Relay Protocol

###### RTP

-   protokoly pro samotnou komunikaci

###### RTCP

-   kontrolni protokol pro RTP

###### SBC - Session Border Controller

-   kontroluje komunikaci a pri podezreni ukoncuje SIP komunikaci i tok medii
-   Media Gateway Controller (SIP) a Media Gateway (media)

###### SDP - Session Description Protocol

-   telo zpravy protokolu SIP
-   obsah
    -   popis relace
        -   o - originator (user, protocol, ip adresa)
        -   c - kam se pripojuje (protocol, ip adresa)
    -   tok 1
        -   typ media (audio, video), port, protocol
        -   kodek, vzorkovac
    -   tok 2 ..

##### HTTP

-   aplikacni protokol
-   v1.1 - pribyla hlavicka **Host** - na jednom serveru tak muze byt vice hostu
-   mezi klientem a cilem muze byt cela rada proxy/gateway

*   **URI - Uniform Resource Identifie**
    -   obecny identifikator
        -   URL - Uniform Resource Locator - popisuje jak se dostat k pozadovanemu zdroji (HTTP URI, SIP URI)
        -   URN - Uniform Resource Name - neresi jak se k pozadovanemu zdroji dostat
    -   format: `schema:cast_zavisla_na_schematu`
        -   `user:password@host:port/path?query#kotva` - obvykle user:password vynechano
    -   pouze ASCII znaky - jine znaky kodovany '%znak'
*   metody
    -   **GET** dotazuje se na data
    -   **POST** odesila data
    -   **HEAD** dotazuje se jen hlavicku
    -   **OPTIONS** dotazuje se jen hlavicku
    -   **PUT**
    -   **DELETE**
*   **REST API** - komunikace dvou aplikaci
*   HTTP odpoved
    -   verze, vysledkovy kod, poznamka -> `HTTP/1.1 200 OK`
        -   _1xx_ - informace
        -   _2xx_ - uspech
        -   _3xx_ - presmerovani
        -   _4xx_ - chyba klienta
        -   _5xx_ - chyba serveru
*   cache
    -   muze jich byt vice po ceste
    -   public/private pro ruzne typy informace
    -   age, expires
*   cookie
    -   server si jejich pomoci uklada data u clienta (napr auth token)
    -   cookie prvni strany - serveru
    -   cookie treti strany - nekoho dalsiho (google atd.)
*   **HTTP/2.0**
    -   pipelining - nekolik pozadavku najednou
    -   vice pozadavku skrz jedno TCP spojeni
    -   komprese hlavicek
    -   server push - server muze odeslat data bez pozadavku
    -   TLS 1.2

###### Proxy

-   serverova cast
    -   klient zada serverovou cast
-   logika proxy
-   klientska cast
    -   dotazuje se na cilovy server
-   cache - muze si udrzovat data
-   parsuje url a preklada pres DNS

###### Gateway

-   stahovani pres ftp
-   serverova cast -> logika - preklopi to na ftp -> klientska cast

### ISO OSI

-   na nizsich vrstvach "odpovida" TCP/IP
-   sitovy model - 7 vrstev
    -   fyzicka
    -   linkova
    -   sitova
    -   transportni
    -   relacni - organizuje dialog mez relacnimi vrstvami a ridi vymenu dat mezi nimi
    -   prezencni - sifrovani, konverze, komprimace
    -   aplikacni - udava interface pro aplikace, jakym zpusobem maji komunikovat
