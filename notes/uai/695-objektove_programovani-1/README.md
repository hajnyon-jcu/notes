# 695 - Objektove programovani I

## Testovani

-   JUnit
    -   `setUp`, `tearDown`, `suite`
    -   `assertEquals`, `fail`
-   TDD - test driven development

## OOP

-   OOA(analyza), OOD(design), OOP(programovani)
-   kopiruje do programu model problemu z realneho sveta

-   veci okolo nas modelovany pomoci `class`
-   kazda trida je reprezentovana svymi instancemi, tj objekty dane tridy - instance tridy
-   trida
    -   data
    -   metody
    -   je popisem strukturovaneho datoveho typu (tzv. referencni typ)
    -   2 druhy referencnich typu (opak primitivniho typu):
        -   objekty = heterogeni - polozky ruzneho typu
        -   pole = homogeni - stejneho typu
    -   polozky objektu: clenske promenne, instancni promenne, datove slozky a atributy objektu
-   v jave lze objekty vytvaret pouze dynamicky `new`
-   konstruktor - vytvori objekt
    -   lze pretizit
-   garbage collector - uvolnuje nepristupne objekty z pameti
-   metody
    -   staticke - metody tridy, procedury a funkce
    -   instancni - metody objektu (operace provadena nad objektem - instanci)

**Struktura JAVA programu**

-   class, interface a vycet (specialni trida enum) - zakladni jednotky
-   ukladaji se do balicku - package
-   zakladni - **java.lang**
-   `import`, komentare trid, metod

-   java nema destruktory -> garbage collector

### Abstrakce

-   zobecneni pohledu a vytvareni castecnych reseni
-   tvar stromu - smerem ke koreni abstrahujeme (ztrata deatilu), smerem k listum konkretizujeme (pridavame detaily)
-   abstraktni tridy nelze vytvorit (objekt)
-   u trid muzeme zabranit vytvoreni pomoci `private constructor` (napr. java.lang.Math)
-   `abstract`

### Dedicnost

-   inheritance - ziskavani vlastnosti dedenim (extends) z vhodne tridy
-   `extends`, jinak je primym predkem `java.lang.Object` - ta z niceho jedina nededi
-   `implements` - trida dedi od interfacu
    -   inteface tvori nesouvisly acyklicky graf
    -   hrany jen typu extends
    -   neni zadny zakladni (obdoba `java.lang.Object`)
    -   nemusi mit predka, muze extendovat vice predku, nemuze byt potomkem tridy, nemuze byt final
-   dedi se pouze public a protected cleny
-   rozdil oproti **kompozici**
    -   dedeni - vytvari bohatsi tridy
    -   kompozice - pridava referencni atributy

### Polymorfismus

-   polymorphism - jednotne zachazeni s ruznymi (mnohotvarymi) objekty majici nektere spolecne (zdedene vlastnosti)
-   prekryte nestaticke metody, ktere se chovaji podle tridy
-   je to relace (RE, ASY, TRA) mezi referencnim typem a mnozinou ref. typu, ktere lze onim typem referovat
-   `super` - volani kontruktoru, metod a datove atributy predka

### Zapouzdreni

-   encapsulation - skryvani soucasti trid a interfacu
-   `public`, `protected`, `package private` - viditelny ve vlastnim balicku, `private`

## JCF

-   pole - nemenna velikost, stejny typ polozek
-   kolekce - sady objektu, efektivnejsi prace, promenna velikost, dodatecne fce (search, sort)
-   `java.utils.Array` - staticke metody pro praci s jednoroz. poli
-   JCF - objekty slouzici k ukladani a praci s ne konecnymi daty
    -   rozhrani - interface
    -   tridy - realizuji rozhrani
    -   algoritmy - polymorfni realizace
-   genericita - predani typu tride, ktera se podle toho pak chova `Class<TType>`
