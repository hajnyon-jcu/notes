# 771 - Pokrocile programovani v C++

## Datove typy, ukazatele, reference

-   promenne
    -   prima - `int x = 0` da se na adresu do pameti
    -   operator `&` reference - vraci adresu v pameti (tzv l-value reference)
    -   pointer/ukazatel - prefix `p` - `int *pX = &x` - v promenne `pX` bude adresa promenne typu `int*` - tedy adresa na promennou `x`
    -   dereference - `*pX` pristup na hodnotu na adrese
    -   reference - `T & rX = x` - propojeni na promennou `x`
        -   abych nemusel kopirovat data (pri predavani do fce apod)
        -   muzu se k tomu chovat jako k promenny
        -   navzdy od deklarace spojene s promennou (nemuzu znovu assignout na jinou) - rozdil oproti pointeru - ten se muze menit
        -   modifikator `const` - nemuzu menit referencovanou promennou
-   funkce
    -   ukazatel na funkci - musim typ dat do zavorek
        -   dobre `void (* pVypis)() = &vypis;` - ukazatel na funkci ktera vraci void
        -   spatne `void * pVypis()` - funkce ktera vraci ukazatel na void
        -   volani `cout << (*pVypis)() << endl;`
        -   reference `void (&rVypis) = vypis`
-   move semantika
    -   elegantni obejiti predavani hodnot pres zasobnik
    -   zapis `&&` - tzv r-value reference
    -   prekladaci vlastne rikam: nedavej hodnoty do zasobniku, ale rovnou, tam kde se s ni bude pracovat
    -   neboli vysledek urcite operace se nekopiruje do promenne, ale rovnou presune (vhodne pro praci s velkymi daty)

### Dynamicke datove struktury

-   linearni spojovy seznam
    -   pocatecni a koncovy prvek
    -   jednotlive prvky odkazy na dalsi a predchozi
    -   jednosmerny/obousmerny

## Chyby a vyjimky

-   moznosti
    -   ignorovat
    -   vypis a ukonceni programu
    -   nastaveni navratove hodnoty fce na kod chyby
    -   skok do jine casti programu (jump)
    -   zachyceni vyjimky
-   `try/catch` blok
    -   `throw`
    -   program se ukonci, zavola se destruktor a pokracuje se v nadrizenem bloku
-   rada STL objektu pouziva vyjimky
-   v c++ hierarchie vyjimek podobna jave

## OOP rysy

-   trida - vzor, blueprint/sablona, existuje v kodu
    -   neni referencni typ -> pri predani v argumentu se kopiruje -> nutno pouzivat reference
-   objekt/instance - nese data, existuje v pameti
-   **abstrakce**
    -   kazdy objekt je "cerna skrinka" a pracujeme pouze s public rozhranim
-   **zapouzdreni**
    -   navenek pouze public veci, ostatni pristupne pouze "zevnitr"
-   **dedicnost**
    -   predek -> potomek
    -   dedicnost urcitych atributu je mozne zakazat ci omezit
-   **polymorfismus** umoznuje
    -   jednomu objektu volat jednu metodu s různými parametry (ad-hoc polymorfismus)
    -   objektům odvozeným z různých tříd volat tutéž metodu se stejným významem v kontextu jejich třídy, často pomocí rozhraní;
    -   přetěžování operátorů neboli provedení rozdílné operace v závislosti na typu operandů;
    -   jedné funkci dovolit pracovat s argumenty různých typů (parametrický polymorfismus, ne ve všech programovacích jazycích).
    -   Rozhodnutí o tom, která metoda bude volána, je u polymorfismu prováděno až za běhu programu (tj. dynamicky pomocí virtuálních funkcí). Tím se odlišuje od přetěžování funkcí, kde je rozhodnutí o volání vhodné funkce provedeno již při překladu (tj. staticky).
-   staticke metody ci atributy nejsou vazany na instanci (`static int a;`, `static int pokus();`)
    -   jsou umistene na spec. miste v pameti -> rychlejsi volani
    -   mohou pouzivat pouze staticke metody a atributy, ne tridni
    -   je mozne pristoupit i bez vytvoreni instance
-   konstruktor a destruktor (`clazz()`, `~clazz()`)
    -   v pripade kopirovani pomoci konstuktoru problem - predavani pole vznika jen melka kopie -> je treba copy constructor (v nem pole prekopirovat) -> hluboka kopie (`Line( const Line &obj);`)
-   chybi interface - nahrazeni abstraktni tridou s virtualnima metodama
-   pristupy - oproti jinym jsou rozdeleny do bloku (neni to u kazde metody)
    -   defaultne u trid private a u struktur public
    -   public - pristup z vnejsku
    -   private - pristup z vnitrku
    -   protected - vyuziti pri dedicnosti
-   `friend` funkce - definuju u metody, funkce ma pak pristup k privatnim atributum tridy
-   **virtualni metoda** - `virtual`
    -   vola se v zavislosti na typu instance
    -   metoda nektereho potomka, ktera jakoby zakryva metodu predka
    -   pokud je `virtual` v predkovi je i v potomkovi
    -   zaklad polymorfismu
    -   ciste virtualni metoda - nema ve tride implementaci (abstrakce - neni mozne vytvorit instanci)
    -   virtualni dedeni

## Pretezovani operatoru

-   operatory
    -   aritmeticke: `+`, `-`, `*`, `/`
    -   logicke: `&&`, `||`
    -   relacni: `<`, `>`, `<=` , `>=`, `!=`, `==`
    -   prirazeni: `=`, `+=`, `-=`, `*=`, `/=`
    -   binarni/ternarni
    -   unarni: `+`, `-`, `&`, `++`, `--`, `sizeof`, `(type)` - pretypovani, `!`, `~`
    -   binarni `<<`, `>>`, `%` ..
-   muzeme pretezovat pro vlastni struktury (muzeme scitat dva objekty a to udela nejakou akci)
-   bud ve tride nebo jako funkce (muzeme pouzit jen jeden zpusob - nebylo by jasne co ma prioritu)
-   seznam op. ktere lze pretezovat: `+`, `-` ,.. , `new`, `delete`, `new[]` ...
-   operatory `=`, `new`, `delete` se nededi ostatni ano

## Streamy - datove proudy

-   tridy umoznujici I/O operace
-   pohodlnejsi nez `File` z C
-   vstupni, vystupni, vstupne vystupni
-   cteni/zapis ze souboru -> `ifstream`, `ofstream`
-   pretizeni `<<`
    -   musi byt na bazi friend funkce (nelze uzit metodu)

```cpp
class Cislo{
    friend ostream &operator << (ostream &s, Cislo &c);
};
ostream& operator << (ostream &s, Cislo &c) {
    s << c.hodnota;
    return s;
}
```

## Generiky

```cpp
template <class T>
T soucet(T a, T b) {
    return a + b;
}
```

## Modularnost, jmenne prostory, dynamicka typova kontrola

-   stejne jmeno
    -   hlavickove casti `.h`
    -   implementacni `.cpp`
    -   potom pouziju `#include “název_modulu.h”`
-   problem kdyz modul vkladam vickrat - direktiva `# pragma once` - nestandartni
-   jmenne prostory - dalsi zpusob modularnosti
-   typova kontrola
    -   silna - C, C++, Java, C# - kontrola jiz behem prekladu
    -   slaba - Lisp, Ruby, JS, Prlogo - az za behu programu
    -   pretypovani - implicitni, explicitni (`(int)x`)

## STL

-   list, vector, queue, deque, stack, map, set ..
-   iteratory
-   algoritmy
    -   nemenne - foreach, count, max_element...
    -   menne - copy, merge, replace
    -   sort, find

## Predikaty

-

## Vlakna
