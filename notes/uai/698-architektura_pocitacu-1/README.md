# 698 - Architektura pocitacu I

**1. Popište (formálně), jak je vyjádřeno číslo v poziční soustavě.**

![Pozicni soustava](./pozicni_soustava.png)

ai je z-adická číslice
z je základ soustavy
n je nevyšší řád s nenulovou číslicí
m je nejnižší řád s nenulovou číslicí

**2. Dvojková soustava, reprezentace čísla ve dvojkové soustavě.**

00111001
1*(2^0) + 1*(2^3) + 1*(2^4) + 1*(2^5)

**3. Převeďte číslo z desítkové do dvojkové soustavy (metoda postupného dělení základem).**

![Dec to bin](./dec2bin.png)

**4. Převeďte číslo v intervalu <0,1) v desítkové soustavě do soustavy dvojkové (metoda postupného násobení základem).**

```txt
0.675 * 2 = 1.35 | 1
0.35  * 2 = 0.70 | 0
0.7   * 2 = 1.4  | 1
```

![Dec to bin decimal](./dec2bin_decimal.png)

**5. Sečtěte dvě celá kladná čísla ve dvojkové soustavě.**

```txt
0+0 = 0
1+1 = 10
1+0 = 1
```

**6. Odečtěte dvě celá kladná čísla ve dvojkové soustavě.**

```txt
0-0 = 0
1-0 = 1
110 - 1 = 101
```

**7. Vynásobte dvě celá kladná čísla ve dvojkové soustavě.**

**8. Vydělte dvě celá kladná čísla ve dvojkové soustavě.**

**9. Převeďte kladné číslo ve dvojkové soustavě do soustavy desítkové. Číslo má i zlomkovou část.**

**10. Převeďte číslo ze soustavy desítkové do šestnáctkové.**

dec => bin => hex

**11. Co jsou příbuzné soustavy a co platí o jejich základech.**

-   maji zaklad o mocnine (dvojkova, ctyrkova, osmickova, sestnactkova nebo trojkova, devitkova atd.)
-   pokud nasobim zaklad zakladem => vznikne mi nasledujici soustava (eg: `2*2*2*2 = 16`)

**12. Převeďte číslo z dvojkové soustavy do šestnáctkové.**

**13. Převeďte číslo šestnáctkové soustavy do dvojkové.**

-   1 znak hex == 4 bity dvojkove

**14. Co to je kód znaku a jaká kódování znaků znáte?**

-   znak abecedy zapsany v binarni podobe podle stanovenych pravidel, eg ASCII tabulka
-   8bitove - ASCII, UTF-8, Windows-1250
-   16bitove - UTF-16
-   32bitove - UTF-32

**15. Co je to font a jakou roli hraje při zobrazování znaků?**

-   bitova reprezentace tvaru znaku
-   zobrazuje znaky v nejake podobne na displeji

**16. Co je to ASCII tabulka?**

-   tabulka obsahujici znaky anglicke abecedy a symboly
-   koduje je do 7 bitu => 128 znaku

**17. Jaký je rozdíl mezi kódováním UTF8, UTF16 a UTF32?**

-   pouziti bitu - 8, 16, 32

**18. Co je to řádová mřížka, nejnižší a nejvyšší řád mřížky a modul řádové mřížky?**

-   popisuje format zobrazeni cisel
-   pr. **1000,1**
    -   nejnizsi rad - nejmensi mocnina => 2^-1
    -   nejvyssi rad - nejvetsi mocnina => 2^3
    -   modul - `z^(n+1)` => `2^(3+1) = 16` - nejvyssi cislo, ktere se do mrizky uz nevejde

**19. Vyjmenujte tři metody kódování záporných čísel.**

-   primy, aditivni, doplnkovy

**20. Jak je definován obraz záporného a kladného čísla v doplňkovém kódu (formálně)?**

-   kladne cislo - pouze prevedene do binarni
-   zaporne - binarni zapis znegovan a pote prictu 1
-   rozsah -128 az 127

**21. Uveďe obrazy čísel 0,1,-1,127,-127,-128 v doplňkovém kódu na osmibitové mřížce.**

```txt
0 = 00000000
1 = 00000001
-1 = 11111110 + 1 = 1111111
127 = 01111111
-127 = 10000001
-128 = 10000000
```

**22. Jak lze detekovat přetečení čísel v doplňkovém kódu.**

-   prenos mimo danou mrizku
-   eg preteceni na 9 bit na 8bitove mrizce

**23. Jak je definován obraz záporného a kladného čísla v přímém kódu?**

-   kladne - prvni bit 0
-   zaporne - prvni bit 1

**24. Kolik obrazů nuly je v přímém kódu?**

-   dva - kladna a zaporna
    -   10000000
    -   00000000

**25. Jak je definován obraz záporného a kladného čísla v aditivním kódu?**

-   kladne - 127 + x => prevedu do bin
-   zaporne - 127 - x => prevedu do bin
-   rozsah 128 az -127, nejsou 2 nuly

**26. Jak jsou v počítači reprezentována čísla s plovoucí řádovou čárkou?**

-   `A = m*z^e` - m = mantisa, z = zaklad, e = exponent
-   float 32 - 32bitu
    -   znamenko (1bit), exponent (8bitu - aditivni kod), mantisa (23bitu)

**27. Sečtěte dvě čísla v plovoucí řádové čárce?**

-   prevedeme na stejny exponent
-   u mensiho cisla posuneme des. carku
-   secteme a opet znormalizujeme
-   [calculator](http://weitz.de/ieee/)

**28. Vynásobte dvě čísla v plovoucí řádové čárce?**

-   secteme exponenty a vynasobime mezi sebou mantisy
-   [calculator](http://weitz.de/ieee/)

**29. Uveďte Grayův kód pro čísla 0-7.**

```txt
0: 000
1: 001
2: 011
3: 010
4: 110
5: 111
6: 101
7: 100
```

**30. Popište výrobu integrovaného obvodu.**

-   z kremenneho pisku se vyrobi velmi cista tavenina kreminku
-   za staleho otaceni se tavenina vytahuje
-   cimz chladne a postupne vznikne ingot
-   ten se rozreze na platky - wafery
-   ty se dokonale vylesti
-   pomoci litografickych metod se na wafer nanesou vrstvy ruznych materialu, ktere spolu tvori vodivostni cesty a polovodicove soucastky (tranzistory a jine)
-   na jednom waferu se dela mnoho cipu soucasne -> pote se rozreze (die)
-   cip se spoji s pouzdrem s kontakty

**31. Vysvětlete pojmy monokrystal křemíku, ingot, plátek (wafer), čip (die).**

-   monokrystal kreminku - cisty kremik s pravidelnou krystalickou mrizkou
-   ingot - valec, ktery se tvori vytahovanim z rozzhaveneho kremiku
-   wafer - platek vznikly rezanim ingotu
-   die - rozrezani waferu (s obvody pomoci litografie) na cipy

**32. Co je to litografie, k čemu se používá a co je to rozlišení litografické metody?**

-   tvorba obvodu na platu kremiku pomoci UV svetla
-   lisi se podle velikosti rozliseni (22nm, 14nm, 7nm)

**33. Vysvětlete pojmy fotorezist, maska, leptání.**

-   fotorezist - svetlocitliva polymericka vrstva, ktera reaguje pri osvetleni UV zarenim
-   maska - predloha pro integrovany obvod
-   leptani - tam kde byla vrstva ozarenam je struktura narusena a je mozne pomoci spec. roztoku ji smyt

**34. Jaké typy polovodičů znáte, s jakými typy vodivosti a příměsemi?**

-   typu P - chybi jeden elektron - Bor
-   typu N - jeden elektron prebyva - Fosfor

**35. Jaká je struktura tranzistoru NMOS?**

![nmos](./nmos.png)

**36. Jaká je struktura tranzistoru PMOS?**

![pmos](./pmos.png)

**37. Vysvětlete, jak funguje elektrický obvod se zdrojem napětí v sérii žárovkou, a tranzistorem NMOS/PMOS. Kdy je tranzistor otevřen a zavřen a kdy žárovka svítí a kdy ne.**

-   NMOS - pokud je na G privedeno napeti (>0.7V) tak je otevreny, proteka jim proud a zarovka sviti
-   PMOS - pokud je na G privedeno napeti (<0.7V => 0) tak je otevreny, proteka jim proud a zarovka sviti => obracene

**38. Jak může být technicky reprezentována logická úroveň 0 a 1 (dva způsoby).**

-   logicka nul => zem, logicka jednicka => napajeci napeti
-   rozdil na dvou vodicich
    -   ![rozdil napeti](./napeti.png)

**39. Na čem závisí ztrátový výkon, který se na procesoru mění v teplo?**

-   `P = a * C * U^2 * f`
-   a - zda procesor je aktivní
-   C - parazitní kapacita na tranzistoru
-   U napětí
-   f - frekvence procesoru
-   čím vyšší frekvence, tím více ztrát, můžeme snížit napětí, ale pak bude problém rozlišit log. úrovně

**40. Jakým způsobem můžeme ztrátový výkon na procesoru snížit?**

-   snizime napajeci napeti, drive 5V, dnes 3.3V (sbernice) nebo 1.8V a 1.5V (jadro procesoru, pameti)

**41. Nakreslete vnitřní strukturu (zapojení tranzistorů) hradla NAND a určete a vysvětlete stavy tranzistorů a stav výstupu pro různé logické úrovně vstupů.**

-   dva PMOS paralelne a dva NMOS v serii

![NAND](./nand.png)

**42. Nakreslete vnitřní strukturu (zapojení tranzistorů) hradla NOR a určete a vysvětlete stavy tranzistorů a stav výstupu pro různé logické úrovně vstupů.**

-   dva PMOS v serii a dva NMOS paralelne

![NOR](./nor.png)

**43. Definujte booleovskou funkci a booleovská proměnnou.**

-   promenna - hodnoty 1 a 0
-   funkce bere parametr n promennych a vraci jednu hodnotu
-   pro n promenny existuje `2^2n` funkci

**44. Kolik a jaké jsou funkce dvou proměnných?**

-   AND, OR, NAND, NOR, EQUAL
-   XOR - striktni OR - jen kdyz se obe hodnoty lisi
-   Implikace

**45. Co je minterm a maxterm?**

-   minterm - soucin vsech promennych (bud primych - pokud je 1, nebo negovanych - pokud je 0)
-   maxterm - soucet vsech promennych (bud primych - pokud je 0, nebo nehovanych - pokud je 1)

**46. Co je ÚNDF a ÚNKF?**

-   UNDF - soucet disjukci mintermu
-   UNKF - soucet konjukci maxtermu

**47. Jaké jsou zákony booleovské algebry ?**

![boolova_algebra](./boolova_algebra.png)

**48. Co jsou De Morganovy zákony?**

![de_morgan](./de_morgan.png)

**49. Vyjmenujte typická logická hradla.**

![hradla](./hradla.png)

**50. Pro daný logický výraz nakreslete schéma logického obvodu.**

**51. Jak se chová kombinační obvod v čase ?**

![obvod_v_case](./obvod_v_case.png)

**52. Nakreslete schéma zapojení půlsčítačky.**

![scitacka](./scitacka.png)

**53. Napište pravdivostní tabulku pro půlsčítačku.**

viz 52.

**54. Sestavte úplnou sčítačku ze dvou půlsčítaček.**

![uplna_scitacka](./uplna_scitacka.png)

**55. Nakreslete schéma čtyřbitové (vícebitové) sčítačky složené z úplných sčítaček.**

![4bit_scitacka](./4bit_scitacka.png)

**56. Co je a jak funguje multiplexor?**

-   elektronicky clen, ktery funguje jako prepinac
-   ma 3 vstupy A, B, S a jeden vystup Y
-   Y je bud A nebo B v zavislosti na S

**57. Co je a jak funguje komparátor?**

-   el. obvod, ktery porovnava dva vstupni signaly
-   vystup 1 pokud `A == B`

**58. Jak je hardwarově realizován posuv vlevo/vpravo?**

-   nasobime/delime 2
-   realizovan pomoci multiplexoru

**59. Je hardwarová násobička kombinační nebo sekvenční obvod?.**

-   kombinacni, obsahuje n^2 scitacek

**60. Co je to hodinový signál, jaká je typicky jeho střída a jaké má parametry?**

-   cislicovy signal (0/1), vlastne se meni jako sinusoida
-   vypocet periody -> f= 1/T

**61. Co je vzestupná a sestupná hrana hodinového signálu?**

-   vzestupna - stoupa od minima k maximu
-   sestupna od maxima k minimu

**62. Vysvětlete funkci synchronního hladinového R-S klopného obvodu.**

-   zakladni synchronni klopny obvod (KO)
-   2 vstupy R, S + CLK a 2 vystupy Q a neg. Q
-   pokud S = 1 a R = 0, pak Q = 1 a neg. Q = 0
-   tento stav trva i po skonceni signalu na S
-   neni dovolen stav kdy je na vsech vstupech 1 (R=1, S =1 a CLK = 1)

**63. Vysvětlete funkci hranového klopného obvodu D, který reaguje na náběžnou hranu.**

-   kdyz jde hrana nahoru -> opise se hodnota na vystup Q ze vstupu D

**64. Vysvětlete funkci jednobitového registru se synchronním zápisem a asynchronním nulováním.**

-   klopny obvod D neni moc dobry na dlouhodobou pamet
-   hodinovy signal ma neustale nejake hrany na ktere musi reagovat a proto si pamatuje hodnotu jen do dalsi hrany
-   problem se vyresi tim, ze pred klopny obvod D dame multiplexer
-   na jeho vtup dame nas vstup D a zaroven vustup z klopneho obvodu
-   ten nam pak slouzi pro ukladani hodnoty

**65. Vysvětlete funkci vícebitového paralelního registru.**

-   sada klopnych obvodu D se spolecnym hodinovym vstupem tvori paralelni registr
-   ten umoznuje soucasny zapis i cteni

**66. Co je posuvný registr, z čeho se skládá a jak funguje?**

-   data paralelne vstupuji (a) a seriove vystupuji (out) nebo paralelne vstupuji (in) a ctou se na vystupech klopnych obvodu
-   realizace pomoci seriove zapojene jednobitove registry (multiplexor + klop. obv. D) -> maji spolecny CLK a RST, kazdy multiplexor navic vstup pro paralelni data

**67. Definujte konečný automat.**

-   slozky: mnozina stavu, vstupni abeceda, vystupni abeceda, prechodova funkce, vystupni funkcem pocatecni stav
-   dostava symboly na vstup a produkuje symboly na vystup

**68. Jak může být konečný automat popsán?**

-   graficky
-   prechodovou tabulkou

**69. Jaký je rozdíl mezi konečným automatem Meally a Moore.**

-   lisi se vystupni funkci
-   Meally - vystupy zalezi na stavu a na vstupnim symbolu
-   Moore - vystupni hodnoty ulozene ve stavech v tzv. tabulce vystupu

**70. Popište činnost konečného automatu popsaného grafem přechodů, pro danou vstupní sekvenci symbolů.**

**71. Nakreslete strukturu hardwarově implementovaného konečného automatu typu Meally/Moore.**

![hw konecny automat](./hw-ka.png)

**72. Jak lze implementovat konečný automat v jazyce Java?**

-   Meally - dvourozmerne pole pro tabulku vystupu

```java
int stav = 0;
int prechod(int vstup) {
    int vystup = tabulka_vystupu[stav][vstup];
    stav = tabulka_prechodu[stav][vstup];
    return vystup;
}
```

-   Moore - jednorozmerne pole pro tabulku vystupu

```java
int stav = 0;
int prechod(int vstup) {
    int vystup = tabulka_vystupu[stav];
    stav = tabulka_prechodu[stav][vstup];
    return vystup;
}
```

**73. Co je sběrnice?**

-   skupina vodicu propojujici dve a vice casti pocitace (CPU <-> RAM)

**74. Podle čeho sběrnice rozdělujeme (taxonomie)?**

-   ucelu
    -   adresova - prenos adresy mezi CPU <-> RAM a dalsimi castmi systemu
    -   datova - prenos dat mezi CPU <-> RAM a dalsimi castmi systemu, obecne jakakoliv sbernice co prenasi data
    -   ridici - prenos ridicich signalu (read - RD, write - WR, byte enable - BE)
    -   systemova - prenos dat mezi CPU <-> RAM a periferiemi, typicky zahrnuje adresovou, datovou a ridici sbernici, ale muze byt i jedna (PCI)
-   synchronizace prenosu
    -   synchronni - prenosy dat jsou rizeny (synchronizovany) hodinovymi impulsy (nevhodne pro zarizeni ktera pracuji na ruzne rychlosti)
    -   asynchronni - pouzivaji potvrzovaci signaly
-   smer prenosu dat
    -   jednosmerna - typicky pro adresovane sbernice, 2 jednosmerne sbernice a 1 v opacne smeru - full duplex, umoznuji broadcast
    -   obousmerna - prenos jednim a druhym smerem se multiplexuje v case, soucasne muze jen jeden smer, half duplex
-   zpusob prenosu dat
    -   seriova - sdili prenos dat a rizeni sbernice pomoci jednoho/vice vodicu, vetsinou seriove pomoci jednoho signalu
    -   paralelni - prenos paralelne po vice vodicich (napr 32bitu)
    -   serioparalelni - USB

**75. Vyjmenujte různé druhy sběrnic: adresová, datová, …**

-   DMA - Direct Memory Access - adresova
    -   primy pristup do pameti, rychla obsluha
-   ISA - Industry Standard Architecture
    -   jednoducha synchr. sbernice, puvodne 8bit, pak 16bit, 5,4MB/s
-   MCA - Microchannel
    -   zanikla, IBM, 32bit, 48MB/s
-   EISA - Extended Industry Standard Architecture
    -   rozsirena ISA, 32bit, vysoka cena
-   PCI - Standard Peripheral Component Interconnection
    -   podpora plug and play, 32 a 64bit, 132/264MB/s
-   AGP - Accelerated Graphics Port
    -   intel pro graficke karty, 32bit, az 2.1GB/s

**76. Co je to třístavový budič a jak funguje?**

-   u obousmerne sbernice aby nemohly vysilat obe strany najednou
-   budic rika jestli se zrovna cte nebo zapisuje

**77. Jak je technicky realizována obousměrná sběrnice?**

**78. Jaké vstupní, výstupní, případně obousměrné signály má typicky paměťový blok a jakou mají funkci?**

-   vstupni
    -   adresa - adresovani prvku v pameti
    -   signaly - output enable (povoluje cteni), chip select(vyber cipu), write (zapis)
-   vystupni / oboustranne
    -   data - prenos dat

**79. Co určuje adresa v paměti?**

-   jednoznacne umisteni prvku v pameti -> index

**80. Jaký je rozdíl mezi statickou a dynamickou pamětí RAM?**

-   staticka - uchovava informaci po dobu kdy je pripojena ke zdroji, bunka SRAM je bistabilni klopny obvod - tj 2 stavy 0,1
-   dynamicka - uchovavana informace pomoci elektrickeho naboje kondenzatoru, DRAM, naboj ma tendenci se vybijet - aby nedoslo k vybiti musi se periodicky refreshovat nabiti

**81. Jaký je rozdíl mezi pamětí RAM a ROM?**

-   RAM - libovolna polovodicova pamet, do ktere je mozno libovolne pristoupit a cokoiliv menit, po ztrate napeti se informace ztrati
-   ROM - trvale ulozena data, energicky nezavisle, pouze ke cteni, data jsou ulozena bud vyrobcem nebo u pameti typu PROM je mozne data jednou zapsat

**82. Jaký je typický časový průběh signálů u statické paměti RAM?**

-   cteni
    -   adresa - index pro oznaceni mista
    -   neg CS (chip select) - vyber z cipu
    -   neg OE (output enable) - aktivace vyst. tristavovych budicu sbernice
    -   data
-   zapis
    -   adresa
    -   neg CS
    -   neg WE (write enable) - povoleni zapisu
    -   data

**83. Jaké jsou typické signály dynamické paměti ?**

-   RAS - radkova adresa
-   CAS - sloupcova adresa
-   WE - zapis
-   hodinovy signal CK

**84. Co určují aktivní signály CAS a RAS ?**

CAS - nebo cl, udava zpozdeni dat na vystupu, nebo zpozdeni pro zapis
RAS to CAS - zpozdeni mezi vyberem radku a adresaci sloupce pameti

**85. Co to je blokový přenos (burst) přenos dat z paměti? Kdo je zodpovědný za zvyšování adresy při blokovém přenosu?**

-   zvlastni rezim pameti pouzivanny pri cteni a zapisu bloku dat o predem zname delce
-   diky pouziti hodinoveho signalu je mozne pouze inicializovat prenos a pote v kazdem taktu hodin provest cteni/zapis hodnoty
-   tim pamet pracuje na sve max. rychlosti

**86. Slovně popište posloupnost signálů při čtení z dynamické paměti (SDRAM).**

-   na adresni lince dostaneme adresu
-   pomoci RAS rekneme ze jde o adresu radku
-   pak obdrzime jednu nebo vice adres sloupce, ktere oznacime pomoci linky CAS jako sloupcovou adresu
-   potom se se zpozdenim zapisi data na datovou linku
-   CLK -> RESET -> ADDR -> CMD -> CMDACK -> Data IN/OUT -> Maska dat

**87. Co to je prokládání cyklů paměti, jak musí být paměť konstruovaná a proč urychluje přístup k datům?**

-   prokladana pamet ma svoji kapacitu rozdelenou do stejne velkych casti (bank) ktere pracuje nezavisle, vsechny sdileji jedno zrozhrani, takze cykly se musi startovat postupne
-   pristupova doba do pameti je podstatne vetsi nez prenos dat, lze startovat jednotlive prenosy s prekryvem a tak zvysit mnozstvi prenesenych dat za jednotku casu

**88. Nakreslete základní architekturu von Neumannova počítače.**

-   instrukce a data jsou ulozena v operacni pameti
-   nelze odlisit data a instrukce

![harward](./harward.png)

**89. Nakreslete základní architekturu počítače s harvardskou architekturou.**

-   instrukce a data fyzicky oddelena -> 2 pameti

![neumann](./neumann.png)

**90. Nakreslete blokové schéma prvních PC.**

![schema](./pc-schema.png)

**91. Nakreslete blokové schéma současného PC.**

![schema](./pc.png)

**92. Nakreslete blokové schéma centrální procesorové jednotky?**

![schema](./cpu.png)

**93. K čemu složí registr PC, IR, PSW?**

-   PC - Program Counter
    -   adresa instrukce, ktera je nactena do instrukcniho registru
    -   typicky zvetsuje hodnotu o delku prave provadene instr.
-   IR - Instrukcni registr
    -   nacita se do nej instrukce, ktera bude provedena v ramci instr. cyklu
    -   programatorsky nepristupny
-   PSW - Program Status Word
    -   registr pro ulozeni priznaku a stavove informace procesoru

**94. K čemu slouží Registry (registrové pole), aritmeticko-logická jednotka, řadič procesoru a jednotka správy paměti?**

-   registry - udrzuji stav procesory, odkladani mezivysledku, tvori operandy
-   aritmeticko-logicka jdn. - provadi vypocetni operace (scitani, nasobeni, bit. posuv, logicky soucin ..)
-   radic CPU - realizuje instrukcni cyklus, ridi vykovavani dilcich operaci, generuje ridici signaly, reaguje na stavove signaly (priznaky aritm. op., preruseni apod.)
-   jednotka spravy pameti - preklad virtualni adresy na fyzickou, kontrola koherence cache, prepinani mezi pametovimi bankami

**95. Když mluvíme o osmibitovém, šestnáctibitovém, dvaatřicetibitovém a šedesáti čtyřbitovém procesoru (počítači), jak se tato čísla odrážejí v jeho architektuře.**

-   delka adres, sirka adresove a datove sbernice a maximalni mozna adresace pameti `2^x`

**96. Jaká je struktura jednoho bitu ALU?**

![alu](./alu.png)

**97. Co obsahuje registr PSW a co znamenají jednotlivé jeho bity?**

-   PSW - registr pro ulozeni priznaku a stavove informace procesoru
-   Z - zero - vysledek operace nulovy
-   C - carry - preteceni nejvyssiho radu
-   V - overflow - mimo rozsah (doplnkovy kod)
-   N - negative - vysledek je zaporny
-   I - priznak povoleni preruseni

**98. K čemu slouží řadič počítače, jaký je rozdíl mezi obvodovým a mikroprogramovatelným řadičem.**

-   realizuje instrukcni cyklus, ridi vykonavani dilcich operaci v ramci cyklu, generuje ridici signaly, reaguje na stavove signaly
-   obvodovoy - konecny automat, D klopne ov. + kombinacni logika
-   mikroprogamovatelny - slozitejsi instrukce, ma pamet pro ulozeni instrukci, instrukce proceseru je realizovana vykonanim sady mikroinstrukci

**99. Popište instrukční cyklus počítače včetně jeho jednotlivých fází.**

-   nacteni instrukce - fetch - z oper. pameti se nacte binarne zak. instr.
-   dekodovani instrukci
-   nacteni operandu - nacteni hodnot z registru nebo pameti do ALU
-   vykonani instr. - ALU provede vsechny operace
-   zapis vysledku - zapis do pameti nebo registru
-   test zadosti o preruseni - pokud muze byt zadosti vyhoveno, provede se specialni instr. cyklus odpovidajici volani podprogramu

**100. Vyjmenujte typy instrukcí. Jaké operace skrývají tato symbolická jména instrukcí ADD, SUB, AND, XOR, OR, SHL, SHR, ROL, ROR, RRC, RLC, JMP, JZ, JC, JNZ, IN, OUT, CMP, CALL, RET, RETI,XCH,LD,ST,BR, BRNE (mix více procesorů).**

-   aritmeticke - ADD, SUB
-   logicke - AND, OR, XOR, OR, COM (negace)
-   posuvy- SHL (vlevo), SHR (vpravo), ROL, ROR, RCL, RCR
-   skoky - JMP, JZ, JC, CALL, RET, RETI
-   presuny - MOV, XCH

**101. Co to je přímá konstanta a přímá adresa? Jaký je mezi nimi rozdíl a kde jsou tyto jak konstanta, tak adresa uloženy?**

-   prima konstanta - naplnime ji registr, je soucasti intrukce
-   prima adresa - odkaz do pameti pomoci ciselne adresu

**102. Co je nepřímá adresace? Vyjmenujte všechny varianty.**

-   neprima `MOV rax, [rbx]`
-   neprima s bazovym registrem a posunutim `LD R1, [R2+100]`
-   neprima s bazovym registrem a index registrem `LD R1, [R2+R3+100]`
-   neprima s bazovym registrem, index registrem a posunutim `LD R1, [100+R2+R3*2]; MOV rax,[rbx+rdx*4+offs]`

**103. Co to je nepřímá adresace s inkrementací/dekrementací (post/pre)?**

-   vhodne na prochazeni pole

**104. Uveďte příklady aritmetických a logických instrukcí.**

```txt
// aritmeticke
ADD r1, r2          ; r1 <- r1 + r2
ADD rbx, rax        ; rbx <- rbx + rax
ADD w0, w1, w2      ; w2 <- w1 + w0 (cílový operand vpravo)
ADDC r1, r2         ; r1 <- r1 + r2 + C
SUB r1, r2          ; r1 <- r1 – r2
SUBB r1, r2         ; r1 <- r1 – r2 – C
CP r1, r2           ; r1 – r2, zapisuje pouze příznaky
CPC r1, r2          ; r1 – r2 - C, zapisuje pouze příznaky
INC r1              ; r1 <- r1 + 1
DEC rbx             ; rbx <- rbx - 1
INC w0, w1          ;
IMUL rax, rbx       ; rax <- rax * rbx
IDIV rax, rbx       ; rax <- rax / rbx

// logicke
AND rax, rbx        ; rax <- rax and rbx
OR rax, rbx         ; rax <- rax or rbx
XOR rax, rbx        ; rax <- rax xor rbx
NOT rax             ; rax = ~ rax (bitová negace)

// posuvy
SHL rax, 1          ; logický posuv vlevo
SHR rax, 1          ; logický posuv vpravo
SAR rax, 1          ; aritmetický posuv vpravo

// rotace
ROL rax, 1          ; rotace vlevo
ROR rax, 1          ; rotace vpravo
RCL rax, 1          ; rotace s carry vlevo
RCR rax, 1          ; rotace s carry vpravo
```

**105. Co kromě vlastní operace nastavují aritmetické a logické operace a posuny?**

-   priznaky ve stavovem registru PSW
    -   C - carry
    -   Z - nulovy
    -   OV - overflow
    -   N - negative

**106. Vyjmenujte všechny typy posunů a rotací?**

viz 100

**107. Jaký je rozdíl mezi logickým a aritmetickým posunem vpravo?**

-   logicky - posouva hodnoty vpravo a nalevo pridava vzdy 0
-   aritmeticky - pracuje s cisly v doplnkovem kodu -> chova se jako deleni 2

**108. Jaké máme typy skokových instrukcí?**

-   nepodminene - JMP - provede se vzdy
-   podminene - JZ, JC, EQ - provede se pokud Z bit v PSW == 1
-   absolutni - PC - prima adresa kam se ma skocit
-   relativni - PC + posun

**109. Jaký je rozdíl mezi relativním a absolutním skokem?**

-   absolutni - primo adresa kam se skace
-   relativni - hodnota ktera se pricte k akt. hodnote PC

**110. Mám dva programy. Jeden používá jen relativní skoky a druhý má alespoň jeden skok absolutní. Který z programů mohu libovolně (=na libovolnou adresu) umístit v paměti počítače a bude stále funkční?**

-   ten s relativnimi skoky, u absolutniho nevime kde bude zacatek programu

**111. Jak je realizován v počítačích zásobník?**

-   implementuje ho procesory a uklada se do pamneti
-   registr SP (stack pointer) - ukazuje na vrchol
-   zasobnik roste odshora dolu (x86) nebo zdola nahoru (nektere mikrokontrolery)

**112. K čemu slouží v počítačích zásobník?**

-   ukladaji se na nej navratove adresy pro skoky do podprogramu
-   parametry, lokalni promenne funkci, obsahy registru

**113. Co je to podprogram? Jaký má vztah k funkcím ve vyšších programovacích jazycích?**

-   v podstate funkce, pred skokem do podprogramu se vlozi na zasobnik adresa nasledujici instrukce a po provedeni podprogramu se na ni pokracuje

**114. Popište detailně funkci instrukcí CALL a RET.**

-   `CALL xxx` - vola podpogram s danym jmenem
-   `RET` - vraci se na dalsi instrukci pred volanim `CALL`

**115. Co je přerušení?**

-   schopnost procesoru prerusit prave vykonavany program a zacit jiny (obsluhu preruseni)

**116. Popište chování počítače při obsluze žádosti o přerušení?**

-   radic preruseni, ktery vybral na zaklade priority informuje procesor o tom, ktery zdroj preruseni vybral
-   na zaklade toho procesor urci adresu obsluzneho procesu

**117. Máme-li více zdrojů přerušení, jak určujeme začátky obslužných rutin pro jednotlivá přerušení?**

-   pomoci radice preruseni - vybira adresy podprogramu podle priority

**118. Co jsou vstupně/výstupní operace? Jak se liší od standardních paměťových operací?**

-   operace zapisu a cteni do registru periferii, ktere ovladaji periferie pocitace
-   lisi se tim ze neplati co zapisu, to ctu (narozdil od pametovych operaci)

**119. Jak jsou kódovány instrukce ve strojovém kódu (struktura instrukce)?**

-   instrukce s pevnou a promennou delkou
-   operacni znak - slouzi k jednoznacnemu urceni instrukce, typicky ruzna delka
-   operandy - urceni zdrojoveho registru, ciloveho registru, primou adresu, primou konstantu, adresni mod (primy/neprimy, auto inc/dec ...), relativni nebo absolutni cestu

**120. Co je to Endianess (Little-Big Endian)?**

-   poradi bytu - jak se ukladaji do operacni pameti
-   little endian - na pametove misto s nejnizsi adresou se uklada nejmene vyznamny bajt (LSB)
-   big endian - na pametove misto s nejnizsi adresou se uklada nejvice vyznamny bajt (MSB)

**121. Jakým způsobem probíhá překlad z vyššího programovacího jazyka až do binární formy?**

![preklad](./preklad.png)

**122. Nakreslete hierarchii paměťového podsystému a řádově uveďte přístupovou dobu pro jednotlivé paměťové prvky.**

![hierarchie](./hierarchie.png)

**123. Popište funkci keše (cache) a způsob, jak urychluje provádění programu.**

-   zkraceni pristupove doby do hlavni pameti
-   mala kapacita, ale rychla pristupova doba
-   cache je skryta (procesor nepozna jestli existuje nebo ne)

**124. Co je prostorová a časová lokalita ?**

-   prostorova - v programu se opakovane pouzivaji data z blizkych adres - nactemeli najednou cely radek z cache, pak cteni nekolika dalsich adres nevede k vypadku cache
-   casova - v programu se opakovane pouzivaji data ze stejne adresy - dve po sobe jdouci instrukce v kratkem case pouziji data ze stejne adresy

**125. Nakreslete a popište konstrukci keše jako plně asociativní paměti.**

-   plne asociativni - kazdy radek ve fyzicke pameti muze byt ulozen v jakemkoliv radku cache

![plne_asoc](./plne_asoc.png)

**126. Nakreslete a popište konstrukci přímo adresované keše.**

-   jeden radek fyzicke pameti muze byt jen na jednom radku cache (asociativita 1)

![asoc-1](./asoc-1.png)

**127. Nakreslete a popište konstrukci keše s asociativitou 2 a více.**

![asoc-2](./asoc-2.png)

**128. Jak instrukce prefetch může ovlivnit činnost keše a snížit počet výpadků při zpracování streamů dat.**

-   prefetch a clflush se pouzivaji pri zpracovani vetsich objemu dat
-   dava se jimi pametovemu systemu predem na vedomi, ze urcita data budou brzy potreba a naopak
-   prefetch - naalokujeme si specificke adresy do cache manualne

**129. Co je to adresní prostor, čím je určena jeho velikost?**

-   souvisly rozsah adres generovanych procesorem pro pristup k pameti
-   velikost je dana poctem adresnich bitu

**130. Uveďte příklady adresních prostorů?**

-   adresni prostor programu
    -   procesor odtud cte instrukce, casto povolena jen pro cteni, pocet bitu citace programu musi odpovidat velikosti tohoto prostoru
-   adresni prostor dat
    -   sem se mapuji pameti RAM pro docasne ukladani dat, vzdy povoleno cteni i zapis
    -   maximum z poctu bitu v registrem, ktere jsou urceny pro neprimou adresaci v tomto adr. prostoru a poctu bitu pridelenym primym adresam v instrukcich cteni a zapsu dat, musi odpovidat velikosti tohoto prostoru
-   vstupne/vystupni adresni prostor
    -   sem se mapuji registry periferii
    -   nebyva prilis velky (64K adres u x86), povoleny cteni i zapis
-   fyzicky adresni prostor
    -   sem se mapuji primo pametove cipy
    -   velikost je urcena sirkou adresove sbernice procesoru
-   virtualni adresni prostor
    -   logicky (opak fyzickeho) prostor prirazeny napr. jednomu procesu (programu), sem se mapuje fyzicka pamet z fyz. prostoru, soucet vsech virtualnich adr. prostoru nekolikrat prevysuje fyzicky (swap)

**131. Co je mapa adresního prostoru? Uveďte příklad.**

-   popisuje obsazeni adresniho prostoru fyzickou pameti
-   casto nesouvisla

**132. Popište virtualizaci paměti metodou stránkování.**

-   virtualni adresni prostor je rozdelen do stranek (4KB)
-   OS modifikuje zaznamy v tabulkach a tim mapuje stranky fyz. pameti do stranek ve virt. adres. prostoru
-   kazdy proces ma vlastni tabulky stranek ktere mapuji jinou cast fyzicke pameti, tim se zajisti ze jedem proces nemuze prepsat data jinemu procesu

**133. Jak se určí fyzická adresa z virtuální pomocí tabulek stránek?**

![strankovani](./strankovani.png)

**134. Index do adresáře stránek pokrývá bity 31-22 adresy. Kolik má adresář položek a kolik bytů paměti zabírá.**

-   `22 - 31 = 10` bitu
-   `2^10` = 1024 radku (polozek) v cache
-   velikost = `1024 * 10 / 8 = 1280` Bytu

**135. Index do tabulky stránek pokrývá bity 21-12 adresy. Kolik má tabulka stránek položek a jakou část paměti zabírá.**

viz predchozi

**136. Co je to pagefile nebo swap partition ?**

-

**137. Kdy nastane výjimka pagefault ?**

-   kdyz program pristupuje k pametove strance, ktera neni aktualne mapovana jednotkou spravy pameti

**138. Jak se pozná, že je stránka odložená na disku?**

-   priznak v tabulce stranek

**139. Které z tvrzení platí:**

1. po sobě jdoucí stránky ve virtuální paměti musí být ve stejném pořadí uloženy ve fyzické paměti. - **NE**
2. po sobě jdoucí stránky ve virtuální paměti mohou být v různém pořadí uloženy ve fyzické paměti. **ANO**
3. po sobě jdoucí stránky ve fyzickém prostoru mohou být mapovány do virtuálního prostoru v různém pořadí. **NE**
4. po sobě jdoucí stránky ve fyzickém prostoru musí být ve stejném pořadí mapovány do virtuálního prostoru. **NE**

**140. Jaké informace obsahuje deskriptor segmentu.**

-   pocatecni adresu segmentu ve fyzicke pameti
-   delku segmentu

**141. Co je TLB a k čemu slouží?**

-   asociativni pamet (castecna nebo plna asociativita)
-   arychluje preklad adres - virtualni -> fyzicka

**142. Co je DMA a k čemu slouží?**

-   Direct Memory Access - prenos dat mezi periferii a operacni pameti bez ucasti procesoru

**143. Co je to architektura CISC?**

-   Complex Instruction Set Computer - pocitac s rozsahlym souborem instrukci

**144. Jaké nevýhody má architektura CISC?**

-   slozite instrukce jsou specializovane
-   velky pocet instrukci -> slozity dekoder -> dekodovani jednoduchych trva dlouho
-   nutnost mikroprogramovatelnych radici
-   tezko se zavadi prodouve zpracovani instrukci - trvaji ruzne dlouho

**145. Co je to architektura RISC?**

-   Reduce Instruction Set Computer - redukovana instrukcni sada

**146. Jakou technikou se dosahuje zvýšení výkonu procesorů typu RISC?**

-   prodouvym zpracovanim instrukci

**147. Co je to ISA?**

-   Instruction Set Architecture - soubor instrukci tvori rozhrani mezi HW a SW
-   zasadni vliv na arch. procesoru
-   abstraktni popis instrukci ktere muzeme provadet na procesorech, implementace zavisi na vyrobci (x86 - Intel a AMD)

**148. Jaké typy ISA znáte?**

-   stradacove - mikrokontrolery
-   zasobnikove - Java VM
-   registrova

**149. Čím je charakteristická střadačová architektura?**

-   jeden registr (stradac), ktery je implicitne uzivan aritm. a log. operacemi
-   aritm. a log. operace maji jako jeden operand stradac a druhy je cten z pameti - vysledek ukladan do stradace
-   jednoduchy hw

**150. Čím jsou charakteristické aritmetické instrukce ve střadačové architektuře?**

-   prvni operand je ulozen ve stradaci a vysledek se tam zase uklada, druhy operand z pameti

**151. Čím jsou charakteristické aritmetické instrukce v registrové architektuře?**

-   oba operandy mohou byt z registru, nebo jeden z pameti

**152. Čím jsou charakteristické aritmetické instrukce v registrové architektuře typu load-store?**

-   operandy pouze hodnoty z registru
-   do registru se nacitaji pomoci instrukci load (LD) nebo store (ST)

**153. Jak se liší v registrové architektuře dvouoperandové a tříoperandové instrukce a jak se tyto typy liší v binárním kódování?**

-   trioperandova
    -   prvni operand je misto kam se ulozi vysledek a druhe dva se napr. scitaji
    -   binarni kod je delsi
-   dvouoperandove
    -   vysledek se uklada do prvniho operandu
    -   bitovy kod je krats (kodujeme jen dva operandy)

**154. Čím jsou charakteristické aritmetické instrukce v zásobníkové architektuře?**

-   operandy jsou ulozeny na zasobniku jako vrchol a polozka pod nim
-   vysledek se uklada zase nahoru
-   instrukce se daji volat pomoci jednoho prikazu

**155. Co je spilling a filling v zásobníkové architektuře?**

-   spilling - odkladani zasobniku do pameti
-   filling - naplnovani zasobniku z pameti

**156. Co je to Bytecode?**

-   program prelozeny prekladacem kteremu rozumi CPI
-   prenositelny kod - je to posloubnost bytu

**157. Jaký je rozdíl mezi vícevláknovými a vícejádrovými procesory?**

-   vicevlaknove - kazde vlakno ma svoji sadu registru, ale sdileji vykonne jednotky ALU
-   vicejadrove - kazde jadro vlastni ALU -> paralelizace

**158. Mohou být jádra vícejádrového procesoru vícevláknová ?**

-   ano (4 jadra - 8 vlaken)

**159. Co je to VLIW architektura?**

-   vice paralelne pracujicich jednotek, explicitne vyjadreny paralelismus
