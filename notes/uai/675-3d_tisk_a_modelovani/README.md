# 3D tisk

## Teorie

-   tiskarny: Original Prusa i3 MK3

### Uvod

-   pouziti: prototypovani, vyroba, umeni, stavebnictvi, zdravotnictvi

#### Technologie

##### FFF (FDM) - Fused Filament Fabrication

-   elektronicky rizena tavna pistole

-   pouziva se filament (cesky struna)

        	- PLA, ABS, PET ..

-   x, y, z osy

        	- zalezi na velikosti - x*y se tiskne nasobne dele (nez treba SLA)

        	- kartezsky - jezdi hlava s tryskou

        	- delta - hlava na provazkach se "houpe"

-   presnost tisku: 0.25mm _ 0.05mm (prumer trysky _ vyska vrstvy), typicky 0.4 \* 0.2

-   pomerne levne materialy i tiskarny

-   obvykle pomalejsi nez SLA nebo SLS

-   casto nutny tisk podpor

##### SLA / DLP

-   Stereolithography / Digital Light Processing

-   postupne vytvrzovani polymeru pomoc ruzneho zareni (nejcasteji UV)

        	- LCD displej propousti svetlo na tekutinu, ktera ztvrdne

        	- nezalezi na velikosti x*y - tiskne se stejne rychle

-   tiskne se z pryskyrice (resin)

-   presnost az 0.01 _ 0.01mm (prumer bodu _ vyska vrstvy)

-   drazsi material, tekuty, casto toxicky (ochrane prostredky), obtiznejsi na manipulaci

-   casto nutny tisk podpor (pred vytzenim mekky)

-   objekty musi mit otvory k vypusteni pryskyrice

-   nutne ocisteni a vytvrzeni po tisku (isopropyl alkohol)

##### SLS

-   Selective Laser Sintering - spekanim prachovych castic laserem

-   pouziva se plastovy nebo kovovy prach

-   nanese se vrstva prachu, typicky laserem se castecky specou dohromady

-   nepotrebuje podpory - tiskne se v nadobe s prachem, ktery podpira

        	- volitelny otvor na vyfoukani prachu

-   nutne ochranne pomucky (vdechovani)

-   vysoka cena (500k+), pomaly tisk

#### Rozsireni

-   [https://octoprint.org/](https://octoprint.org/) - webove rozhrani pro tiskarnu (RPi)

### Nazvoslovi

-   krokove motory - mohou byt precizne ovladany
-   warping - kdyz se tisk nepovede
-   sag - kdyz se mi vytisk provesi
-   blob - nahromadeny kousek materialu, ktery muze vadit/znicit tisk
-   hydroskopicky - absorbuje vlhkost

### Hardware

##### Soucasti

-   hlava
    -   extruder - motor a prevody
        -   direct - extruder je primo na hot endu
        -   bouden - extruder je mimo a do hot endu vede trubicka
    -   hot end - tryska a nahrivaci teleso
    -   ventilatory
        -   trysky - chladi okoli trysky
        -   zebrovani (heater block) - aby nesel horky vzduch nahoru k privodu filamentu

#### Udrzba

-   desku cistit nejlepe okenou

-   zavadeni filamentu

        	- vybereme program na vyjmuti

        	- nechame nahrat

        	- kdyz je nahrato - vyjmeme

        	- kdyz nejde lehce - otevreme a ustipneme

#### Ovladani

-   tlacitko zdroje

-   displej

-   otocne tlacitko

-   tlacitko resetu - zastaveni tisku

### Proces tisku

3d model -> `stl/obj` -> slicing -> `gcode` -> tisk

-   tvorba 3d modelu

        	- SktechUp, FreeCAD, OnShape ...

        	- soubor `stl/obj` (Standard Triangle Language)

-   slicing

        	- priprava modelu na tisk a nastaveni parametru (rychlost, vyska vrstvy, material)

        	- Slic3r, Cura, Simplify3d ...

        	- soubor `gcode`

-   tisk

        	- priprava materialu, tiskarny, postprocessing

        	- tisk z SD karty, pres sit, USB atd.

#### Chyby pri tisku

-   odlepeni z podlozky
-   spatne serizena tiskarna (motory, remeny, udrzba, prach ..)
-   problem pri tisku (naraz do jineho objektu ..)
-   ucpana tryska
-   nerovnomerny, zamotany, prekrouceny filament

#### Vicebarevny tisk (Multicolor)

-   vicebarevny filament (moc se nepouziva)
-   svarovani filamentu (dam dohromady vice ruznych barev) - soucast tiskarny/externe
-   obarvovani filamentu v trysce - neexistuje rozumne funkcni reseni
-   promichani filamentu v trysce - napr. CMYK promichani - filament neni kapalina, takze problem s promichanim, zustavaji zbytky atd
-   stridani filamentu v trysce - vymenuju filament
-   vicenasobny extruder (nebo vice trysek) - dokaze stridat X filamentu nebo ma pro kazdy jednu hlavici

### Filament

-   co nas zajima
    -   material, aditiva (karbonove vlakno, med, zelezo, fosforeskujici apod)
    -   prumer typicky 1.75mm nebo 3mm (casto zamerne 1.7mm a 2.95mm)
    -   pevnost, pruznost - stupnice tvrdosti
    -   abrazivnost - tryska se obrusuje o material - pouziti specialni trysky (tvrzena, kalena)
    -   teplota meknuti a tani - tiskova teplota, ruzna podlozka (lak, izolepa ..)
    -   ruzna reakce s materialy - rozpustne ve vode, reakce na svetlo atd
    -   barva, stalost, prusvitnost
    -   splodiny a zapach
    -   vyrobce - spolehlivost, cena, kvalita
-   recyklace
    -   domaci vyrobny filamentu - ne moc funkcni
    -   3d pero, ktere dokaze tisknout z narezane PET lahve

#### ABS

-   historicky nejvice pouzivany, dnes se opousti nebo pouziva ve smesich
-   starsi kosticky lega ciste ABS

#### PLA

-   _Polyactic Acid_
-   typicky z tuzeneho rostlineho skrobu - nezavadne - podle barviva
-   rozrusuje ho skoro vsechno - voda, svetlo, ..
-   kolem 60st zacina meknout, ale jde to obejit (vypekani v troube - zmena krystalicke mrizky a pak vetsi tepelni odolnost)
-   nejlepsi vlastnosti pro tisk - nejsnazsi
-   pomerne levne

#### PET

-   _Polyethylentereftalat (glykol)_
-   nekolik druhu - PET-G (nejvetsi tepelna odolnost - cca 85st), A-PET (nejmekci, ale dobre se s nim pracuje)

#### CPE

-   podobny PET, vetsi pevnost a vetsi tepelna odolnost, ale drazsi

#### CP

-   _Polykarbonat_
-   dobre se micha s ABS
-   celkem hydroskopicky
-   dobre vlastnosti, ale hure se tiskne

#### Nylon (PA)

-   _Nylon (Polyamid)_
-   hlavne na SLS tiskarnach, ale i na FDM
-   vlasce na ryby
-   nehorlavy
-   pruzny, ale mechanicky celkem odolny, hydroskopicky, velka teplotni roztaznost, drazsi

#### TPE/TPU

-   _elastomery_
-   lehka pruznost, mirne hydroskopicky

#### HIPS

-   _High Impact Polystyrene_
-   rozpustny v chemikalii - pouzival se na podpery

#### Pridavani primesi

-   carbon, drevo, smesi podobajici se kamenum/betonu atd
-   vyztuzovani pomoci vlaken (karbon, kevlar, sklo)

### Modelovani

-   3D Reprezentace modelu
    -   povrchova - modeluju jen povrch (uzavrene veci se berou jako plne)
    -   objemova - vidim, co bude vevnitr
-   STL - Standard Triangle Language
    -   objekt tvoreny trojuhelnikama v kartezske soustave
-   CAD - Computer Aided Design
    -   software pro modelovani
    -   ne/parametricky
        -   parametricke
            -   lze zasahovat do historie (menit parametry)
            -   onshape, Sculpris (kreativni CAD), Fusion360
        -   neparametricke
            -   SketchUp
            -   nemuzu menit historii, modeluje se postupne, WYSIWYG
    -   3D CAD
        -   elektronicke socharstvi (kreativni, umelecky sw)
        -   CSG - Constructive Solid Geometry (FreeCAD - skladani zakl. tvaru)
        -   Sketch -> Extrude - 2d scatch se vytahne do 3d prostoru
        -   push/pull - posouvani ploch
        -   scriptovani - matematicke funkce, programovani (SCAD)
    -   3D scan - ne moc presne, specialni scannery nebo z vice fotek
-   kdyz modelujeme
    -   dobra predstava o tom, co chci modelovat (rozmery, scale, udelat nacrtek)
    -   vztahy k jinym objektum (upevneni, napojeni, otvory na matky atd.)
    -   orientace modelu a odkud budeme modelovat
    -   technologie tisku a jeji omezeni (tisk do vzduchu, podpery atd.)
    -   pouzity material a jeho vlastnosti
    -   tisk v celku nebo v castech a jejich napojeni (lepeni, sroubovani ..)
    -   verzovani modelu a jeho dokumentace
-   tipy a triky
    -   je treba promyslet jak polozit model - muze mi zvysit pevnost, snizit pocet vyplne apod
    -   kdyz musim tisknout do prazdna, casto lze vyresit mostem misto podper
    -   schovavani matek - na sestihran je vhodne udelat hvezdu (lepe sedi matka, pri nepresnem tisku)
    -   prvni vrstva je tenci (vice roztahla, aby se dobre chytla na vrstve)
    -   zaobleni - na pulce se prekroci 45 stupnu a nelze vytisknout - resi se zkosenim a naslednym obrousenim

#### SketchUp

-   neparametricke modelovani - nemuzu se vracet do historie, to co udelam, uz lehce nezmenim (velikosti atd)
-   zakladni prace je nakresleni tvaru a jeho nasledne vytazeni do 3d a potom opet nakreslim tvar a vytahnu/zatahnu

#### OnShape

-   parametricke modelovani
-   proces podobny jako sketchup, ale tvary musim ukotvit - tj. ke stredu, presne miry, uhly apod.
-   muzu menit zpetne a pokud mam spravne namodelovano, tak se mi objekt meni soumerne
-   assembly - slozeni vice modelu, moznost simulovat pohyb soucastek a prohlizet jak na sebe ne/sedi
-   drawing - vykres - moznost exportu okotovaneho vykresu (vhodne pro externi vyrobu, patenty atd.), muzu vybirat pohledy jaky chci, ruzne kotovat, priblizovat male casti atd.

#### Problemy pri modelovani komplexnich uloh

Tyka se obecne modelovani - jako 3d modely, tak treba databazove modely, UML atd.

-   duvody
    -   nespravne pochopeni ulohy -> nespravny vnitrni model (mentalni model - moje predstava reseni)
    -   identifikace klicovych casti
-   hlavni strategie - casto kombinace nekterych, ale je treba zezacatku vybrat, kterou pouziji
    -   rozdel a panuj
        -   dobre se kombinuje s ostatnimi
        -   rozdelim na mensi ukoly ktere postupne resim, pr. u hrnecku nejdriv hruby navrh (aby splnoval zadani), pak dam presne miry, pak resim logo, pak napojeni ucha, pak detaily (zaobleni) ..
    -   iterativni pristup
        -   postupuju po krocich, vetsinou to delam intuitivne, pr. u hrnecku zacinam dnem, pak steny, ucho, detaily nakonec ..
    -   important first (nejdriv to hlavni)
        -   pr. u hrnecku nejdriv udelam hrnecek uplne do konce (vsechny detaily, rozmery atd) a az potom resim ostatni - ucho, talirek ..
        -   nebo kdyz zakaznik chce neco, pres co nejede vlak, tak zacnu s tim ikdyz se mi pak dalsi casti treba budou hure navazovat
    -   zleva doprava (od jednoho konce na druhy)
    -   od stredu (od jadra)
        -   oblibene v 3d modelovani
        -   u db se postupuje od hlavni entity k mene dulezitym
    -   trial and error (pokus-omyl)
        -   nevime kde zacit, ale je treba kontrolovat, jestli model porad odpovida zadani
-   podpurne metody
    -   abstrakce
    -   redukce
    -   analogie
    -   koncept (pojmova mapa, nacrtek)

### Slicing

-   perimetr - udava kolik max vrstev ma davat plnych, nez zacne pridavat vypln (kolik sten) - kdyz chci tvrdsi vyrobek radsi pridam perimetr nez vypln
-   blokator podper
-   vynucovac podper
-   modifikator - muzu modifikovat ruznou vypln na ruznych mistech, perimetr a dalsi parametry casti modelu
-   lze nastavit tloustku vrstvy pro jednotlive vrstvy jinak
-   v zobrazeni vrstev muzu nastavit zmenu filamentu
-   nastaveni tiskarny
    -   zakladni veci, ktery jsou vetsinou dany typem tiskarny
    -   lze davat vlastni g-code - tj ruzne upravy na zacatku, po kazde vrstve atd.
    -   extruder - jaka tryska, nastaveni, retrakce (jak se tryska zveda pri prejizdeni)
    -   zavislosti - lze dedit existujici model
-   nastaveni filamentu
    -   nastaveni teplot pro tisk (ruzne pro prvni vrstvu)
    -   nastaveni chlazeni ventilatoru
-   nastaveni tisku
    -   perimetr
    -   vyska vrstvy, prvni vrstvy - nemela by prekrocit tretinu sirky trysky
    -   vyska vodorovnych vrstev - obdoba perimetru
    -   kvalita
        -   vyhnout se prejizdeni perimetru - zamezuje vytvareni strunek (pavucin - pri prejizdeni zanechava slabou nitku - da se odstranit horkovzdusnou pistoli hned po tisku)
        -   detekce perimetru premosteni - detekuje most a podle toho upravuje parametry tisku mostu (vetsi chlazeni, aby rychleji most tvrdl)
    -   pozice svu - kdyz nechci aby se zacinalo na stejnem miste
    -   vypln
        -   vzor (mrizka, kubicka, 3d plastev, adaptivni kubicka), hustota
        -   ironing - vyzehleni - zahladeni posledni vrstvy
    -   limec (brim) - umele rozsireni prvni vrstvy pro lepsi prilnavost
    -   obrys (skirt) - obrys okolo objektu - slouzi hlavne k ustaleni trysky a filamentu, nekdy se pouziva k udrzeni teploty a odstineni pruvanu (ale lepsi uzavrena tiskarna)
    -   podpery - automaticke, rucne kresleny, mezni uhel, vzor vyplne apod. (nekdy lepsi podpery nakreslit primo do modelu)
