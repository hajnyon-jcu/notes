# 717 - Pocitacova kriminalita (kybernalita)

-   [zpracovane otazky](./otazky.md)

## Uvod

-   pri testovani systemu mame zpravidla omezenou dobu povoleni
-   pomer cena testera vs cas utocnika (kolik ma na testovani vs na prolomeni systemu)
-   metody utocniku a testru jsou stejne
-   zakony
    -   budeme porusovat, ale na svych systemech pro studijni ucely, jinak samozrejme nelegalni
    -   pro testovani potrebuju pisemny souhlas **vlastnika** (sporne kdo je vlastnik a ci podpis staci)
    -   [Zákon č. 40/2009 Sb. trestní zákoník](https://www.zakonyprolidi.cz/cs/2009-40)
        -   [§ 230 Neoprávněný přístup k počítačovému systému a nosiči informací](https://www.zakonyprolidi.cz/cs/2009-40#f3920237)
        -   [§ 231 Opatření a přechovávání přístupového zařízení a hesla k počítačovému systému a jiných takových dat](https://www.zakonyprolidi.cz/cs/2009-40#f3920258)
        -   [§ 232 Poškození záznamu v počítačovém systému a na nosiči informací a zásah do vybavení počítače z nedbalosti](https://www.zakonyprolidi.cz/cs/2009-40#f3920268)
        -   [§ 182 Porušení tajemství dopravovaných zpráv](https://www.zakonyprolidi.cz/cs/2009-40#f3919713)
    -   Občanský zákoník (40/1964)
    -   Obchodní zákoník (513/1991)
    -   Telekomunikační zákon (513/1991)
    -   Autorský zákon (121/2000)
    -   Zákony na ochranu průmyslového vlastnictví (14/1993)
    -   Zákon o ochraně osobních údajů (101/2000)
    -   Zákon o regulaci reklamy (40/1995)
    -   Zákon o elektronickém podpisu (227/2000)
    -   Zákon o svobodném přístupu k informacím (106/1999)
    -   Zákon o informačních systémech veřejné správy (365/2000)
-   sber informacich
    -   co firma dela, jaky vyuziva sw, infrastrukturu
    -   co muzu ukradnou, co ma hodnotu
    -   jak na ne muzu nejlepe zautocit
    -   obhlizeni okoli - pro fyzickou infiltraci (obhlidka vchodu, turniketu, pouzivanych karet atd)
    -   informace o zamestnancich (abych se neprihlasil za nekoho kdo je na dovoleny apod.)

## Metody/vektory utoku

-   [MITRE](https://attack.mitre.org) - organizace, popis vsech vektoru utoku

### Socialni inzenyrstvi

-   fyzicky predmet (rozhazu, dam zadarmo flasky s programem, dostanu se k pc a sam vlozim)
-   emaily s prilohou
    -   phishing - cileny na kohokoliv (hromadne rozesilani)
    -   spearing - cileny na jednotlivce (znam ho a dokazu tak napsat verohodnejsi email, ktery cili na stazeni programu, navstiveni stranky atd)
    -   ochrana: elektronicky podpis
-   falesny telefonni hovor
    -   vydavam se za banku, reditele atd.
    -   obrana - overeni
-   implementace keyloggeru

### Prolamovani autentizace (jmeno/heslo 2FA)

-   jmeno/heslo - utocnik se snazi uhodnou/ziskat login
    -   okoukani (v kavarne), uhodnuti generickeho username (`jmeno.prijmeni@firma.cz` atd, proto by mel byt redirect `info@firma.cz` -> `franta@firma.cz`)
    -   heslo - z uniku, ziskam informace o cloveku a snazim se podle toho uhodnout, slovnikovy utok, brute force atd.
        -   opatreni: omezeni pokusu, silne heslo (neopakujici se na sluzbach), zpomaleni pokusu (dalsi po 30s - vyrazne zpolani brute force), omezeni na IP adresu (ale problem pri pouziti proxy)
-   have i been pwned

### Pristup z internetu

-   zneuziti chyby v sw
-   utok na VPN, email server, web server, ssh
-   IoT pristup (kamery, chytry zarizeni atd - nezaplatovane/nezaheslovane - shodan)
-   exploity - zneuziti chyby (databaze chyb dostupna na internetu)
    -   **buffer overflow** - vyuziju toho, ze napr. do jmena dam delsi string a pokud je to spatne osetreny, tak dokazu zmeni adresu dalsiho volani programu a ten muze zavolat nejaky muj kod -> pod uzivatelem, za ktereho je spusten server (proto se vytvari extra a nepouziva se admin)
-   **DOS/DDOS** - Denial of Service/ Distributed Denial of Service (utok vice pocitacu najednou)
    -   omezeni dostupnosti napr. eshopu (ztrata zakazniku)
    -   vyuziti botnetu (sit pocitacu, ktere jsme ovladli napr. pomoci nejakeho exploitu)
    -   na vsech vrstvach sitovych protokolu

### Odposlech na siti

-   paragraf §182 - Porušení tajemství dopravovaných zpráv
-   viz [opakovani siti](#Opakovani-siti)
-   puvodne komunikace nebyla sifrovana - prichod SSL, HTTPS, sifrovani na strane aplikace
-   falesny AP - podvrhnuta wifi, na ktere odposlouchavam (`CD wifi - free` apod.)
    -   automaticky pripojeni na mobilu - staci mi pouze stejne pojmenovany AP - navic mobil vysila info o sitich, kam byl pripojen
    -   muzu cist zpravy, odchytavat hesla, ale i podvrhovat stahovane programy nebo weby apod

### Dostat opravneni super uzivatele

-   ziskam nejakym predchozim zpusobem
-   vycitani hesel (hashe z disku, otevrena v pameti), klicu
-   zaneseni backdooru (v pripade opraveni chyby, aktualizace apod)
    -   spusteni serveru na ktery se pak pripojim (napadne)
    -   upraveni programu (`ssh` aby me pustil i s neexistujicim uzivatelem, ale zase `ssh` ma jinej CRC atd)
    -   rootkit
        -   napr. upraveni `ls` tak, aby nevypisoval muj zaneseny program
        -   uprava jadra systemu (tak aby `ls` kdyz se pta zase nevypsal muj soubor)
-   prochazeni site, pripadne zkouseni prihlasovani se ziskanymi hesly a jmeny
-   odposlouchavani sitoveho provozu

### Utok na klienta

-   utok na klientsky pocitac (notebook)
-   spjato se soc. inz.
-   nejcastejsi utoky (protoze ostatni veci byvaji dobre zabezpecene)
    -   lidi si nosi pracovni notebooky domu, delaji tam osobni veci atd
    -   dokazu proniknout i do interni site odpojene od internetu
-   casto program, ktery komunikuje do internetu a vykonava prikazy utocnika

### Prenost informaci a dat ziskanych utokem

-   ziskam data, ale potrebuju je napr. z interni site dostat ven - ruzne metody (firmy se brani uniku dat)
    -   pres DNS (DNS tunneling)
        -   davam dotazy na nejakou svoji domenu (`a.x.com` a `b.x.com`) - kdyz dam dotaz na `a`, tak ukladam 1, kdyz na `b` tak 0 a dostavam binarni data

---

-   sw zranitelnosti
    -   zero day - zranitelnost,ktera neni jeste objevena
    -   co s nimi delat pri odhaleni
        -   nahlaseni vyrobci
        -   zneuziti
        -   prodani
-   utility
    -   **Kali Linux**
    -   **metasploit** - nastroj pro utocniky, umoznuje napr. vycitani hesel z pameti a dalsi utility pro utoky
        -   metasploitable - distribuce na zkouseni exploitu (zamerne plny chyb, nesmime zpristupnit do internetu, nastavit sit jako Host Only adapter)
    -   **sqlmap**
    -   **keylogger** - zaznamenavani vstupu (sw nebo hw) a ukladani nebo rovnou odesilani

## Metodiky

-   pri testovani bych si mel vest zaznamy co kdy delam (jaky command a kdy atd abych vedel jestli jsem to byl ja atd.)

### ptes standard

-   http://www.pentest-standard.org/index.php/Main_Page
-   navody a postupy
    -   pre-engament interactions
    -   sber informaci
    -   modelovani utoku
    -   analyza zranitelnosti
    -   exploitation - zneuziti zranitelnosti
    -   vyuziti exploitu ke sberu dalsich informaci
    -   reporting
-   technicke navody
    -   konkretni systemy a nastroje (jak ziskat usernamy, wifi, DNS atd)

### OSSTMM

-   https://cs.wikipedia.org/wiki/Open_Source_Security_Testing_Methodology_Manual
-   vypocetni metoda pro vypocet zranitelnosti
    -   podle toho se pak da porovnavat, jestli se zabezpeceni zlepsilo/zhorsilo

### OWASP

-   https://owasp.org/
-   zabyva se bezpecnostni na webu
-   testing guide
    -   popsano jak se maji testovat webove aplikace
-   development guide
    -   navod jak vyvijet bezpecne webove aplikace

## Faze utoku

### Sber informaci

-   nutny krok, abych vedel na co vubec utocim, co chci ziskat
-   muzu zjistit jake systemy se pouzivaji
    -   registr smluv, dodavatel sluzby
    -   google nazev organizace - ziskam hostnamy
    -   google `login site:jcu.cz` - ziskam stranky s prihlasovanim apod.
-   `nslookup` - ip adresa
    -   RIPE - organizace pridulujici ip adresy pro evropu, http://ripe.net
        -   dokazu ziskat rozsah ip adres (verejna db ip adress - WHOIS)
            -   pak se zeptat na PTR DNS zaznam (nemusi existovat)
            -   nebo zkousim (slovnikovy utok - napr. program pierce - dela automaticky)
        -   pozor na pridelovani adres provozovateli ISP (testuju pak cely rozsah treba starnetu nebo t-mobilu)
-   **METADATA** - data u media filu (fotky, videa, pdf, word atd)
    -   kodovani, hloubka, fotak, cas, misto atd.
    -   ziskani informace o lokaci, jmeno, login name, ip (tiskarna napr.) apod
    -   z druhe strany - pri uploadu ocistit fotky od metadat (`exiftool`)

### Analyza informaci a modelovani utoku

// TODO: prednaska

-   lateralni pohyb
-   automatizovane nastroje
    -   metasploit
-   ghdb, exploit-db

### Web - ziskavani informaci a konfigurace

-   nejrozsirenejsi sluzba
    -   hodne chyb (rozsirene, hodne vyuzivanych technologii)
    -   otevrene do internetu (drive staticky web, dneska API, aplikacni servery, databaze atd.)

#### Ziskavani informaci

-   OWASP, OWASP live CD, OWASP Testing guide - http://owasp.org (zapamatovat ke zkousce!)
-   **robots.txt**
    -   muze obsahovat uzitecne informace (napr. `Disallow: /admin`)
    -   http://www.robotstxt.org/
-   **vyhledavani**
    -   operatory na googlu: `site:`, `inurl:` (prohledava jen url, napr wp-admin atd), `cache:` (google udrzuje cache starsich i smazanych stranek - muzu se dostat na omylem zverejnene veci), `filetype:`
    -   http://archive.org - nacachovane stranky z ruznych let
    -   vyhledavani zajimavych informaci: password, login, index of ...
-   **sitemap.xml**
    -   apod. dokazu zjisti stranky na webu
-   **identifikace serveru**
    -   hlavicky HTTP protokolu (`Server: Apache/Ubuntu`, `X-Powered-By: Wordpress`)
    -   `nc <IP> <PORT>` (zpravidla port 80)
    -   nekorektni dotaz - snazim se najit generickou stranku (napr. 404 od apache, kde byva i server atd.)
    -   lokalni proxy pro testovani webu - pres ni analyzujeme komunikaci (zase muzu odchytavat hlavicky atd.)
    -   poradi hlavicek muze byt pro ruzne servery jine
    -   sitereport - aplikace shromazdujici informace o sluzbach
-   **detekce aplikaci** vice sluzeb na jednom serveru
    1. domeny (napr. virtualhost na apache, `prf.jcu.cz`, `zf.jcu.cz`)
        - DNS zaznamy (A, CNAME, PTR atd. viz vyse)
        - fierce - slovnik, kterej zkousi domeny
        - google - to co ma zaindexovany tak dokazu najit
    2. ruzny adresare (`/prf`, `/zf`)
        - podobny jako predchozi - google, slovnik
    3. porty (:3000 -> prf, :4000 -> zf)
        - scanner portu
    4. query parametry (`index.php?id=prf`, `index.php?id=zf`)
        - slovnik, jinak pokud na to nevede odkaz (goodle), tak na to neprijdu
-   **detekce parametru aplikaci**
-   **analyza chybovych hlaseni**
    -   spatny login, neex. stranky apod.
-   **analyza struktury**
    -   shromazdim odkazy a proklikavam (da se zautomatizovat)
    -   OWASP ZAP, Burp proxy (proklikavam web a proxy server mi z toho tvori strukturu, umi i crawling)
        -   nedostanu stranky za loginem, za formularem, DEEP WEB apod

#### Nedostatky konfigurace webserveru

-   apache, iis, nginx
-   SSL/TLS
    -   SSL uz zastarale, dnes TLS (>=1.2)
    -   kdyz chybi, muze se za me nekdo vydavat (https)
    -   detekce podporovanych slabych sifer
    -   validita certifikatu
    -   kvalita algoritmu, kterym je certifikat podepsan
    -   sifry: RSA, BlowFish, AES, CHACHA - delky klice
    -   hashovaci algoritmy: md5 (zastaraly), SHA256
    -   muzu konfigurovat v ramci serveru
    -   overeni podporovanych protokolu napr. ssllabs.com, `openssl`, `nmap`
-   HSTS
    -   Http Strict Transform Security
    -   prechazeni utoku MIM s downgradem https -> http
    -   protokol rika: od ted uz bude komunikace jen https (ulozeno v cookies)
-   management serveru
    -   zname chyby serveru (aktualizace)
    -   administracni rozhrani (hledani na znamych mistech `/admin`, `?menu=admin` atd.)
    -   aby server nevracel chybovy hlasky, ale generickou chybu (404 apod)
-   management aplikace
    -   odstranit komentare, soubory s napovedou, priklady, manualy atd.
    -   directory traversal - pristoupim na adresu `server.com/../../../../etc/passwd` a zkousim vypsat neco mimo slozky `httpd`, `var/www` atd.
-   soubory
    -   nenechavat tam stare backupy, tmp atd
    -   napovedy...
-   podporovane metody
    -   HTTP `OPTIONS` vraci podporovane metody (`GET`, `PUT` atd)
    -   nektere metody lze zneuzit (TRACE/TRACK - XST - cros site tracing, HEAD - obejiti autentizace)

#### Autentizace

-   metody
    -   GET metoda - `?login=aaa&pass=bbb` viditelny v logach, historii, proxy serverech
    -   POST lepsi
    -   obe by meli byt pouzivane sifrovane
-   login form by nemel napovidat co jsme zadali spatne
-   na strane serveru se generuje hash -> uklada se do cookie a ta se posila s kazdym dalsim requestem, abysme se nemusel s kazdym requestem prihlasovat
    -   cookie format je definovany
-   utoky na autentizacni mechanizmy
    -   testovani kombinaci jmeno/heslo
        -   opatreni: aplikace nesmi napovidat co se zadalo spatne, url, navratove kody (404 a ne 403) atd abysme mu nepomohli
    -   zneuziti obnovy hesla
        -   test, jestli uzivatelsky ucet existuje
    -   implicitni nebo lehce uhadnutelne hesla (routery, televize apod., generovane defaultni hesla)
    -   nechani hesla v repozitari, v kodu, backup slozce atd.
    -   hruba sila, slovnikovy utok
        -   opatreni: omezeni pokusu
-   obejiti autentizacnich mechanizmu
    -   primy pristup k zabezpecene strance (`/admin/adduser`)
    -   modifikace parametru (aplikace kontroluje jen hlavicku/query parametr `authenticated=true`, `admin=1` apod.)
    -   uchodnuti session id (viz dale)
    -   sql injection (viz dale)
-   zapamatovani hesla v prohlizeci
    -   autocomplete na heslo - ne moc dobry (spis password manager)

##### Utok na session id (id relace - cookies)

-   prohlizec ma cookie s hashem ze serveru (po prihlaseni), posila s kazdym dalsim dotazem (nepotrebujeme se znovu prihlasovat)
-   zabezpeceni cookies:
    -   zivotnost - TTL (Time To Live)
        -   vygenerovat po prihlaseni
        -   zrusit na server pri odhlaseni
        -   casove omezena zivotnost
    -   cookie svazana s jednou domenou
    -   sifrovana komunikace (HTTPS)
        -   melo by byt generovano az po prihlaseni (ne napr. pri pristupu na stranku, protoze muze byt http)
    -   bezpecny algoritmus na generovani
    -   bezpecne ulozeni na serveru
    -   atributy: secure, domain, path, expires, cache-control

##### Kontrola vstupu

-   nejbeznejsi nedostatek web aplikaci
-   bezna arch. aplikaci na webu
    -   Internet -> FireWall (WAF - aplikacni FireWall) -> FrontEnd -> BackEnd
    -   komunikace casto http za firewallem (bere se jako bezpecna cast nebo napr. kvuli vykonosti pri sifrovani)
    -   muzu ztratit informace o uzivateli, napr. IP adresu - proto se forwarduje pres hlavicky (X-forwarded-for, apod.)
-   muze vest k utokum:
    -   XSS - Cross Site Scripting
    -   SQL Injection
    -   Interpreter injection
    -   Locale/Unicode utoky
    -   Utoky na file system
    -   Preplneni bufferu
-   "All user's input is evil." - meli bysme vzdycky nejak kontrolovat a spravne escapovat

##### Utoky

-   MIM - Man In the Middle
    -   dokazu ovladnout nejaky prvek mezi uzivatelem a serverem - AP (free wifi, defaultni jmeno na routeru)
    -   pak simuluju chovani serveru/uzivatele
-   XSS - Cross Site Scripting
    -   utocnik utoci na ostatni uzivatele
    -   **stored**
        -   do nejakeho inputu umisti JavaScript `<script>...</script>`, ktery se ulozi na serveru a uzivatel ho pak spusti na svem pocitaci pod svym uctem
        -   ochrana na severu spravnym escapovanim
    -   **reflected**
        -   script utocnik schova napr. do query parametru a posle uzivatelum odkaz, ktery pak script po nacteni vykona
    -   priklady: ziskani session id, cteni clipboardu, prekresleni stranky, poslani dotazu na server, odeslani dat, exploity prohlizece atd.
-   SQL Injection
    -   vlozeni SQL dotazu do inputu, ktery se pak vykona na db
    -   ochrana spravnym escapovanim a pouzivanim nejakym typem prepared statements (parameterizace), omezeni prav uzivatele db na minimum
    -   priklady: cist/editovat/mazat data z db, spoustet systemove prikazy (pod uzivatelem, pod kterym bezi db)
-   OS Commanding
    -   injekce prikazu, ktery se vykonava na systemu (napr. program, ktery dela `ls ${input}` a vypisuje zadanou slozku)
    -   utocnik muze podstrcit napr. `/etc; id` - program vypise `ls` a jeste vystup `id` (info o uzivateli)
    -   ochrana spravnym escapovanim
-   XML Injection, SSI Injection, LDAP Injection atd. (podobny princip)
-   Buffer Overflow
    -   // TODO: dodelat (probirano na hodine kde jsem nebyl)

#### Google hacking

-   https://exploit-db.com
    -   google hacking db

#### Odposlech dat na siti

-   utoky na sitove protokoly
    -   ty vychazeji z toho, ze protokoly mely propojit X pocitacu a ze zacatku neresili bezpecnost ale spise spolehlivost
-   utocnik se typicky snazi dostat pristup nekde po ceste k cili (nas pocitac -> router -> internet .. -> router -> koncova sit a zped)
-   preplneni CAM tabulky
    -   utok na starsi switche
    -   CAM tabulka si drzi informace o datech sbernice <-> MAC adresa
    -   generujeme MAC adresy -> zaplnime tabulku -> switch se zacal chovat jako HUB
    -   dnes uz switche osetrene
-   ovladnuti switche
    -   pokud se utocnik dostane na switch (slabe/implicitni heslo), muze nastavit duplikovani komunikace na svuj port (mirror port)
-   ARP cache poisoning
    -   MIM utok
    -   v cache uz resolvnuty preklady IP <-> MAC (`arp -a`)
    -   utok spociva v tom, ze do cache se utocnik snazi podstrcit svou MAC napr.:
        -   posila falesny ARP requesty a spoleha na to, ze ostatni pocitace si ho ulozi do cache apod.
        -   posloucha ARP requesty a odpovida rychleji nez prijemce
    -   muzeme utocit i na routery
    -   ochrana pomoci kontroly duplicitnich adres, kontrola pri zmene ARP tabulky (detection and prevention), staticke zaznamy, sifrovana komunikace
-   SSL odposlech
    -   nad TCP, treti vrstva - viz opakovani siti dole
    -   muzeme odposlouchavat hlavicky na nizsich vrstvach
    -   sifrovani dat je na aplikacich
    -   dnes uz nepouzivat (ani TLS 1.0 atd)
    -   SSL Strip
        -   Maxie Marlinspike (zakladatel Signal)
        -   MIM kde podvrhnu odkazy z https na http (kdyz server komunikje na `:80` i `:443`)
        -   ochrana: vypnuti `:80`, HSTS hlavicka - enforce https (rika prohlizeci, ze ma komunikovat jen https)
    -   SSL Strip 2 +
        -   nahrazuje odkazy na jinou podobnou domenu (zavisi na tom, ze mame DNS server, kde prekladame upravenou domenu na spravnou), komunikace pak obchazu HSTS hlavicku
        -   obrana: detekce proxy serveru, obrana v prohlizeci (https only, detekce odkazu na jinou domenu)
-   DHCP spoofing
    -   konfigurace falesneho DHCP - podstrcim falesny router
-   DNS spoofing
-   ICMP Redirect
    -   presmerovani komunikace (aby router za me nemusel preposilat komunikaci - napr. pri vice routerech v siti)

## Skodlivy kod

-   cokoliv co nechceme, aby uzivatel spustil (kod podstrceny utocnikem)
-   ruzne kategorie
    -   viry, cervi, chobotnice, kralici
    -   logicke bomby
    -   trojsky kone
    -   backdoory, exploity
    -   stahovace, hoax, adwere, spyware
-   schema pojmenovani
    -   http://www.wildlist.org/wild_desc.htm
    -   http://www.wildlist.org/CurrentList.txt
-   prostredi - definuje prostredky, ktere skodlivy kod pouziva a je jimi limitovan
    -   architektura pocitace, procesor, operacni system (verze, souborovy system), prekladace, jazyky, sitove protokoly, kompilatory atd.
-   metody infekce
    -   viry
        -   siri se jen s interakci uzivatele
            -   je treba nejaka jeho akce: pripojeni ext. zarizen jako USB, fotak, mobil atd
            -   otevreni prilohy mailu apod.
        -   automaticky spusteni na starsich Win (`autorun`) - schovana za ikonu prohlizeni
        -   automaticky prehrani (`autoplay`)
    -   cervi
        -   automaticke sireni
        -   napr. [Morris Worm](https://en.wikipedia.org/wiki/Morris_worm)
        -   ruzne moduly
            -   vyhledavac obeti (scanner, ktery hleda sluzby, pres ktery by se mohl sirit dal)
            -   sireni infekce (typicky nejaky exploit)
            -   rozhrani pro vzdaleny pristup - aby se utocnik dokazal na stroj pripojit (typicky se pripojuje na nejaky server, kam dava vedet, ze ovladl stroj - CCC - Command Control Center)
            -   rozhrani pro aktualizaci (aktivni - vyhledavani aktualizace, pasivni z CCC) - pridava nove chyby, ktere se daji exploitovat, nove metody skryvani pred antiviry atd
            -   funkcni cast - to co utocnik vyuziva na stroji - keylogger, skener, DDoS utok
            -   statistiky
            -   hodiny zivotniho cyklu - jak dlouho ma byt aktivni
        -   interakce
            -   s neznamym virem (souperi o prostredky)
    -   trojsky kun
        -   tvari se jako jiny program/soubor, ktery po spusteni spusti skodlivy kod
    -   soubory
    -   pamet, zavesovani
    -   spoluprace uzivatele
-   ovladnute stroje muzou slouzit jako:
    -   proxy servery
    -   DDoS utoky
    -   vypocetni sila
    -   ziskavani dat
    -   rozesilani spamu
-   botnet
    -   sit ovladnutych stroju
-   obranne strategie skodliveho kodu
    -   vyuzivani nedokumentovanych funkci systemu (nejsou monitorovany antiviry)
    -   kodovani - prehazovani strojovyho kodu
        -   oligomorfni, polymorfni (automaticka modifikace kodu)
    -   ochrana proti debuggerum - aby neslo sledovat zasobnik, stepovat a celkove pozorovat chovani programu (smazani sama sebe, vypinani klavesnice atd)
-   generatory viru
    -   automaticke generovani viru a cervu podle parametru (jaky exploit pouzit, jakou platformu zacilit atd)
-   obrana a detekce
    -   antiviry
        -   spravne nakonfigurovane (monitoring pameti, disku atd)
            -   monitoring pameti - defaultne vypnuty, protoze vypocetne narocny - ale dulezite, protoze vetsina skodliveho kodu funguje jen v pameti
        -
    -   aktualizace - zamezeni vyuzivani exploitu
    -   zabezpeceni
        -   systemu - sandbox (virtualni stroj, docker apod.), bootovani z flasky (pocitac jede z operacni pameti)
        -   site - firewall (spravne nastaveni, blokovani portu, prichozich spojeni apod.), odpojeni od site, HW firewall (muze obsahovat detekcni mechanizmy, skenery, blokovani k podezrelym serverum apod)
    -   sitova detekce
        -   honeypot (wormradar, mwcollect, daji se poustet napr. na Turrisu)
        -   IDS (wireshark, snort) - detekce
        -   IPS - detekce i blokace
    -   https://www.virustotal.com/ - detekce viru v souborech/webech atd.

## Anonymni pristup k internetu

-   pouzivany utocniky aby nebyli detekovani nebo uzivateli k anonymnimu prohlizeni internetu
-   zanechavane stopy v internetu
    -   IP adresa, MAC adresy (lokalni sit)
    -   email adresy
    -   otisk prohlizece (metadata o prohlizeci a systemu)
-   pseudo anonymni sluzby
    -   typicky free proxy servery a VPN
    -   informace schromazduje proxy server nebo VPN, tzn. porad je nekdo ma (logy muze vyzadat stat atd)
-   anonymni sluzby
    -   napr. Tor a dalsi anonymizery sluzby s nizkou latenci (prohlizece) nebo s vysokou latenci (napr. email)
    -   napojovani pres X uzlu a sifrovani hlavicek a adres
    -   nachylne na statisticke utoky, pr.: kdyz budu na Tor siti sam, pripojim se pres ISP -> Tor -> Webserver, tak podle logu od ISP a Webserveru zjistim, ze se nekdo pripojoval z Toru a kdyz jsem tam sam je to jasny - lepsi u sluzeb s vysokou latenci (nevadi, ze email prijde pozdeji)
    -   Tor
        -   client - prohlizec, DNS resolver a dalsi casti, ktere zajistuji komunikaci s Tor siti
        -   pomalejsi - skoky pres X uzlu
    -   Tails - anonomymni system
-   anonymizacni site
    -   virtualni sit v ramci internetu
    -   mame instalovane klienty, kteri provadeji sifrovani
    -   komunikace mezi klienty
    -   metropolitni site (mezi wifi, radiove vysilace) - vlastni sit mimo internet
-   pojmy:
    -   darknet - obecne site, ktere slouzi k anonymni komunikaci, prochazeni internetu, napr. https://freenetproject.org
    -   deepweb - weby, ktere nejdou indexovat vyhledavaci (generovane na zaklade url, netextove dokumenty, zaheslovani)
    -   dark internet - adresni prostor IP, ktery neni dostupny (chybne nastaveni, zakerne zamery)

## WiFi

-   utoky
    -   odposlech
    -   ruseni
    -   falesne AP
    -   ovladnuti routeru
-   WPS - WiFi Protected Setup
    -   pro snadnejsi pripojovani novych zarizeni do siti
    -   konfigurace routeru chranena jen PINem - utok hrubou silou

## Poznamky z cviceni

-   pri debugovani a odchytavani provozu se pouziva lokalni proxy
    -   nastaveni v prohlizeci (network -> use proxy -> 127.0.0.1:8080)
    -   zapnuti BurpSuite - slouzi pro testovani
    -   ma proxy s vlastnim certifikatem, proto muzu pres nej na stranky bez vystrahy

## Opakovani siti

-   url
    -   `<protokol>://<autentizace>@<hostname>:<port>/<adresar>/<soubor>?<parametry>#<kotva>`
    -   autentizace se nepouziva - je videt v logach a ma k ni pristup i napr. proxy server po ceste
-   osi
    1. Linkova
    2. Sitova - IP
    3. Transportni - TCP, UDP
    4. Aplikacni - HTTP, FTP, SSH
-   puvodni protokoly nebyly sifrovane
-   DNS - Domain Name System
    -   nekolik typu zaznamu
        -   `A` - mapuje hostname -> IP
        -   `CNAME` - alias -> hostname
        -   `PTR` - pointer mapuje IP -> hostname
        -   `NS` - domena -> nameserver (kde jsou ulozene napr. A zaznamy)
        -   `MX` - mail exchanger, domena -> postovni servery
    -   root level server
        -   pod nim narodni domeny (.com, .cz, .uk atd)
            -   subdomeny atd.
    -   resolver - ten co se pta (dotaz probulbava az k serveru, na ktery se ptam)
    -   zaznamy cachovane (non-authoritative answer)
        -   na nejaky cas (kdyz zmenim, tak se na me nedostanou protoze IP je nacachovana)
    -   `nslookup`, `dig`
    -   vetsinout hlavni NS a cache (periodicky stahuji konfiguraci o domene -> tzv prenos zony - AXFR)
-   ethernet
    -   CSMACD (Carrier Sense, Multiple Access, Collision Detection)
    -   koaxial - drat a na nem napojeny stanice, nachylne na chyby (jedna chyba oddelala cele spojeni)
    -   kroucena dvoulinka + HUB
        -   HUB - porad jedna sbernice, ktera preposila na vsechny dalsi vystupy
        -   resil castecne problemy koaxialu (jednu linku)
    -   switch, router
        -   switch - vice sbernic - kazdy stroj ma pro komunikaci vlastni sbernici (resi zahlcovani)
        -   smerovani
        -   IP sit
-   ARP protokol (Adress Resolution Protocol)
    -   mapovani IP adresy na MAC (zeptam se jakou MAC ma tahle IP se kterou chci komunikovat)
    -   stary, opet neobsahuje bezpecnostni prvky
    -   nestandardizovana implementace
    -   ARP request
        -   cilova adresa: broadcast (same 1)
        -   odchozi adresa: MAC adresa A
        -   telo: MAC A, MAC B, IP A, IP B, request bit
    -   ARP response
        -   cilova adresa: MAC A
        -   odchozi: MAC B
        -   telo: MAC B, MAC A, IP B, IP A, reply bit
    -   odpovedi se cachuji (aby se nemuselo porad posilat dokola) - ARP Cache
