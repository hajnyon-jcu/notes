# 683 - Pokrocile databazove systemy

## Serverove programovani

-   skalarni funkce
    -   vraci jednu hodnotu
-   procedure - jako funkce, ale nevraci hodnotu
-   tabulkove funkce
    -   funkce nad tabulkou (pr. parametrizovatelny pohled/select)
    -   pouziva se jako tabulka
-   trigger
    -   INSERT, UPDATE, DELETE
    -   inserted a deleted tabulky s prave pridavanymi/odebiranymi radky (hodi se pri hromadnem pridavani)

## ORM - objektove relacni mapovani

-   napr. hibernate (Java?), entity framework (C#), typeorm/sequilize (ts)

## Data warehouse

-   data pro analyticke potreby
-   ![Data warehouse](./datawarehouse.png)
-   hlavni ucel - zdroj pro analytiku
    -   mam X databazi podobneho formatu, ktere ale na sebe nemuzu namapovat (X oddeleni v nemocnici, se stejnymi pacienty, ktere nemuzu propojit pomoci napr. rodneho cisla)
    -   data warehouse mi uklada, cisti a upravuje data a z nej muzu delat vystupy (kolik pacienti prumerne stravi v nemocnici atd.)
-   jakmile se do nej dostanou data, tak uz nikdy nezmiz - casovy snimky businessu -> kdyz ve zdrojovych db dojde ke zmene, ve warehousu zustava stara - mam historii, ikdyz zdrojova db ji nemusi uchovavat
-   ETLR - Extract Transform Load Refresh
    -   vrstva pred db, provadi parovani dat
    -   extract / staging
        -   ziskavani dat (select ve zdrojove db, export do csv/xls, posilani na API, data sama zdrojova db vklada napr do messaging systemu ...)
        -   full upload/incremental
        -   problem - nerozumim strukture zdrojove db (nebo ji muze nekdo upravit)
    -   transform
        -   prevod dat na jednodnotnou podobu (sjednomvtit datove typy)
        -   business key - dohodly format v ramci organizace, podle ktereho se data sjednocuji
    -   load
        -   unifikovany data se nahraji do data warehouse
    -   refresh
-   metadata a monitoring
-   frontend
    -   analysis
    -   query
    -   reports - napr interaktivni seznam dat - mesicni vydelek oddeleni atd
    -   data mining - hlubsi prohledavani vztahu v db
-   warehouse uvnitr
    -   **star schema**
        -   dimenze
        -   problem - potreba vice tabulek (kvuli hledani, pridani dat, atd)
        -   ![Star schema](./star_schema.png)
    -   **snowflake schema**
        -   vzniklo ze star pridavanim tabulek
        -   dimenze se rozpadaji - vice tabulek navazujicich
        -   problem - pridavani atributu (nejde do nekonecna, opakujici se nazvy, atd.)
    -   **datavault**
        -   dimenze na hub
            -   _hub_
                -   obsahuje pouze business key, datum, zdroj odkud prisel a md5
                -   md5 slouzi k porovnavani updatu dat (rika mi, jestli se data zmenila)
                -   neobsahuje zadne informace - ty v satelitu
            -   _satelit_ - odkaz na business key -> hub
                -   obsahuje samotna data
            -   _link_ - propojuje huby mezi sebou (vazby)
        -   podobny jako databaze databazi
        -   ![Datavaul](./datavault.png)
-   prinos v organizaci, ktera ma vice ruznych systemu (ERM, CRM ...) - otazka jestli to prinasi benefity
-   koncovi uzivatele vidi **data marty** - pohledy na data

## Big data

-   charakteruzuje se pomoci V
    -   volume - objem, kolik je dat (typicky nakej exponencialni rust)
    -   velocity - rychlost jakou data prichazeji
    -   variety - ruznorodost (text, obrazky, zvuk, video)
    -   veracity - verohodnost - neuplnost, nejasnost dat (napr. prispevky na soc. siti)
-   data casto

### Data lakes

-   zalozen na jine technologii nez relacni db
-   ELT
    -   extract - ziskani dat
    -   load - netrapi nas jak data vypadaji, ulozime je
    -   transform - kdyz data potrebuju, tak je vycisti, transformuje na duveryhodna data -> **curated data**
-   nad tim muzu budovat ostatni srandy (reporty, data mining, analytika)
-   rozdil je v tom, jak jsou data ulozena
    -   je jich vic, vetsi (streamovana data) - ukladani na file system
    -   potrebuju je rozdistribuovat -> vice nodu
-   tooly
    -   Hadoop (HDFS - hadoopfs)
        -   ulozime vsechno a pak to budeme zpracovavat
    -   hive - vytvari rozhrani podobne rel. db.
-   map-reduce - rozdeleni ukolu na uzly, ktery mi pak vrati vysledek

### NoSQL

-   nosql - historicky ruzne vyznamy, ale vsechny teoreticky spravne
    -   not only sql
    -   no sql
    -   non relational
-   na relacnich db neni vylozene nic spatne, ale maji nevyhody pro konkretni pouziti
    -   pevne dane schema - nemuzu ukladat dynamicka data
    -   nevhodne pro velka data (stovky milionu zaznamu)
    -   rozdeleni na vic serveru (distribuovane systemy)
    -   vysoka dostupnost pri zapisu
    -   oddeleni logicke a fyzicke vrstvy u dotazu s velkym poctem JOIN operaci - ty ktery se pouzivaj spolu, jsou ulozeny u sebe
-   skalovatelnost
    -   horizontalni - vice nodu
    -   vertikalni - lepsi hw

#### Horizontalni skalovatelnost

-   rucne na aplikacni vrstve - db switching
    -   sam zajistuju v aplikaci kam data ukladam/nacitam
    -   pr. rozhazuju data podle roku (na server1 rok 2019, server2 rok 2020 atd)
    -   _nevyhody:_ hodne dat (musim casto pridavat server), overhead
-   db mirroring
    -   ![DB Mirroring](./db_mirroring.png)
    -   data zapisuju do masteru (CUD), ale ctu je z vice slavu (na ktere se mi to replikuje)
    -   pred slavama - load balancer
    -   vyhoda: rychlost a skalovatelnost cteni
    -   _nevyhody:_ overhead na masterovi (neskaluje), hlidani replikace - neaktualnost dat (napr. jeden slave ma nespravna data - problem), selhani masteru, pomala distribuce CUD operaci, porad omezena velikost
    -   varianta - basic cluster database
        -   porad stejne rozdeleny, ale je to na shared storage (virtualizovany)
        -   nedochazi k zasilani aktualizacnich zprav, ale k rizenymu kopirovani bloku pameti (na sdilenym ulozisti) - pomerne rychla distribuce
        -   do jisty miry muzu pridat 2 mastery (failover)
        -   _nevyhody:_ stale muze dojit k neaktualnich hodnot, narocne na sitovy provoz, stale omezena velikost db
-   partitioning - u relacnich db
    -   deleni tabulky bud na sloupce (vertikalne) nebo na radky (horizontalne)
    -   _vertical_
        -   rozdeleni na sloupce
        -   tabulky na ruznych discich/serverech => rozlozeni zateze
        -   malo pouzivane - obvykle potrebujeme cele radky
    -   _horizontal_ (=sharding) - **neni to scaling!**
        -   horizontal partitioning se vetsinou pouziva u relacnich, sharding vetsinou u nosql
        -   rozdeleni na radky
        -   tabulky na ruznych serverech => velmi dobre rozlozeni zateze
        -   failover lze resit zdvojenim serveru
        -   _nevyhody:_
            -   udrzovani primarnich klicu a autoinkrement (pouzivame GUID nebo UID - neco jako hash, nemel by byt stejny)
            -   vazby mezi daty na ruznych serverech
                -   mame globalni cast - shard table a delime pouze tabulky u kterych je to vykonove vyhodne
                -   vazby resime ruzne: napr. mirrorovanim nekterych tabulek, kopirovanim nekterych dat => abych se vyhnul joinum napric servery
        -   pro aplikace se cluster tvari jako jedina databaze (je tam nakej server - management/router server, kterej dotazy rozhazuje na urcitou db)
        -   rozlozeni dat na shardy
            -   proste logiky
                -   pr. prijmeni uzivatel
                -   nemuzeme jednoduse pridat shard (musime upravit logiku => prenest data)
            -   nahodne
                -   data celkem rovnomerne rozproztrena
                -   dotazy na hledani musim posilat na vsechny shardy, protoze nevim kde jsou -> muzeme pridat indexy na management server
                -   muzeme v klidu pridat dalsi shard
            -   matematicky (fuknce)
                -   pr. hash funkce podle ktery urcim na kterej shard
                -   je potreba rychla fce, aby nezatezovalo server
                -   nemuzeme jednoduse pridat shard (musim zmenit hash funkci + prehashovat data v db)
            -   komplexni strategie
                -   kombinace nebo varianty predchozich + dalsi veci, napr geograficka prislusnost
-   kombinace

![](./scalling.png)

### NoSQL

-   casto zalozeno na objektovem pristupu
-   vhodne pro vetsi data -> scaling (ne vsechny)
-   schema free - oproti relacnim databazim
-   shared nothing architecture - typicky neexistujou vazby
-   ACID x BASE (konzistence x vykon)
-   velke rozdily mezi implementaci (specifikace)
    -   grafova, souborove

#### Typicke deleni

-   **key-value**
    -   klic -> hodnota
    -   pristup pouze pomoci klice (indexace)
    -   klic vetsinou hash, nebo nese jeste nejakou informaci (datum)
    -   hodnota obvykle serializovana a komprimovana
    -   problem kdyz chci dalsi index
    -   redis
    -   pouziti: cache
-   **column-oriented**
    -   v podstate dvourozmernych poli (na prvni pohled podobny jako RDB)
    -   ![](./column-oriented.png)
    -   data uchovava po sloupcich (na disku jsou u sebe data z jednoho sloupce) - casto i serazeny
    -   casto u hodnoty ve sloupci ulozena i reference na predchozi a dalsi sloupec toho radku (aby bylo rychly i dostani celyho radku)
    -   hodnoty muzou mit i vice hodnot (napr. produkty objednavky)
    -   sloupce lze vnorovat -> SuperColumn
    -   k datum pristupuju radek + sloupec
    -   radky mohou mit vice sloupcu (nejsou tam null jako v RDB)
    -   google big table, apache cassandra
-   **document-oriented**
    -   rozsireni key-value
    -   ukladani strukturovanych dokumentu - json, xml ...
    -   velka volnost s obsahem - hodnoty
    -   muzeme cokoliv ze struktury dokumentu indexovat
    -   oproti RDB porad chybi vazby
    -   mongodb (bson - binarni json), couchdb, ravendb, dynamodb (amazon)
-   **graph-database**
    -   zalozeny na teorii grafu - vrcholy a hrany (nody a vazby)
    -   existuji vazby - nekdy se i hlidaji (napr pri smazani), maji atributy
    -   neni zadny schema - pridavam nody a vazby
    -   hledani - potrebujeme klic a pak uz jdeme po hranach dal
    -   vrcholy - jsony, key-value - jednoduchy objekty
    -   hrany - reference meze vrcholy + atributy navic
    -   ![](./graph-db.png)

![](./nosql-types.png)

### ACID

-   **atomic** - transakce se provede cela nebo vubec
-   **consistency** - transakce prevadi db z jednoho konzistentniho stavu do dalsiho konz. stavu
-   **isolation**
    -   transakce by se nemely navzajem ovlivnovat - napr. ctu data ktery nemusi mit validni hodnotu
    -   provedeni pomoci zamku, casovych znamek atd (viz [databaze 1](../697-databaze/README.md#acid))
-   **durability** - po potrvzeni transakce jsou data trvale zapsana a muzeme je povazovat za platna
-   NoSQL porusujou hlavne consistency - nemame cizi klice a napr. kdyz mame nakopirovanyho uzivatele u komentaru, po zmene username se mi to tam nepromitne - musim to aktualizovat sam
    -   ale neplati, ze nemohou ACID splnovat, ale spise ohledne dat ne indexace apod. (ty pak odpovidaji spise BASE)

### BASE model

-   vetsinou implementuji nosql, snazi se tim resit hlavne poruseni consistency
-   **Basic Availability** - uprednostnujeme rychlou odpoved pred jeji spravnosti (vetsinou ruzne levely nastaveni, napr: ravendb - normal = kdyz db nema co delat, zacne aktualizovat indexy)
-   **Soft state** - opak konzistentniho stavu - prechodny stav - vetsinou se db vyskytuje v nekonzistentnim stavu - nosql db vetsinou neceka, nez se aktualizuje vsechno spojeny s nakym dotazem (treba update indexu) a rovnou vraci data, ktery ale nejsou "aktualni"
-   **Eventual Consistency** - dotaz ze chci opravdu konzistentni odpoved - tj. pockam na update db (indexy) a az pak vracim data - za cenu delsiho casu

### CAP theorem

-   Brewer's theorem
-   tvrdi, ze vzdy je mozne docilit 2 ze 3 moznosti:
    -   **Consistency** - kazdej read mi vrati nejnovejsi write nebo error
    -   (High) **Availability** - kazdej read mi vrati data a ne error (ale nemusi byt nejnovejsi)
    -   **Partition Tolerance** - moznost udelat distribuovany system (napr. vice node)

::: warning
**zastaraly model**
:::

![](./cap.png)

-   CP - ACID - vetsina relacnich, resime deleni dat (P) - mirroring etc - distribuce
-   AP - BASE - vetsina NoSQL
-   CA - ACID - relacni, ale neresime deleni dat, neni distribuovana

::: tip
**novejsi model**
:::

-   databaze se vyskytuji v celem spektru CAP
-   nemusi splnovat vsechno nebo muze vsechno trochu atd.
-   z tohoto modelu vyplyva, ze pro konkretni problem je nejlepsi konkretni databaze
    -   vybirame podle usecasu, ne podle "nejlepsi" db

![](./cap-new.png)

-   relaci - okolo C
-   column oriented - okolo A
-   key-value - okolo A skoro az k P
-   dokumentove - zhruba uprostred, ale neumi to vsechno nejlip
-   grafove - mezi CA, moc nejdou rozdelit na vice serveru
-   data lake - vhodne pro P, ale protoze to je nad file systemem -> spatny hledani (pomaly), spatna konzistence (hodne nodu)
-   data warehouse - mezi CP - rychle cteni, distribuce
    -   pouziva data z relacnich databazi
    -   ma data v "nenormalnich" formach -> rychlejsi cteni dat
    -   typicky se tam jednou data nahrajou a uz se nemeni - jeden insert, opakovany cteni
    -   lepsi vykon cteni - vetsi dostupnost

![](./cap-new-types.png)

## Q&A

### Rozdil mezi CLUSTER a CLOUD

**cloud** - virtualni sluzba, nezajima me jakej je tam hw

**cluster** - obecne uskupeni serveru (i diskova pole a dalsi specialni konfigurace - napr. v datovem centru)

### Proc neindexovat vsechno

-   aktualizace - kdyz aktualizuju, musim upravit vsechny indexy
-   pamet - zabira to pamet
