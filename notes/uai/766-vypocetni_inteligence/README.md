# 766 - Vypocetni inteligence

## Materialy

-   [Otazky - ukol](https://hajnyon.cz/projects/materials/hajny-ukol-1.pdf)
-   [Otazky - zpracovane](https://hajnyon.cz/projects/materials/vi-otazky.pdf)
-   [Prednasky](https://hajnyon.cz/projects/materials/vi_predn.pdf)

## Uvod

-   umela inteligence - obor informatiky snazici se stavet stroje vykazujici inteligentni chovani
-   srovnava se s lidskym rozumem a jeho schopnostmi
-   Turinguv test
    -   1950
    -   test, kdy za oponou je clovek a stroj a clovek s obouma komunikuje, pokud nepozna rozdil, pak ho povazujeme za inteligentni stroj
-   zalozena na logice, symbolicke pocitani, odvozovani, reseni problemu
    -   mnohdy vypocetne narocne (velke stavove prostory)
    -   vyuziva se prolog, lisp
    -   pr: sachovy stroj, expertni systemy, autonomni rizeni

### Reseni realnych uloh

-   mame problem
    -   vytvorime matematicky popis (linearni, diferencialni rce, relace ...)
    -   z toho vytvorime program
    -   reseni = aplikace
    -   pro radu uloh nelze nalezt matematicky popis (rozpoznani obliceje, rozpoznani hlasu, rizeni auta)
-   jina metoda - uceni
    -   uloha -> uceni (model, klasifikator) -> reseni
    -   uceni - ze vstupnich dat a expertniho ohodnoceni se vytvori trenovaci mnozina
        -   to jsou pary `situace -> chovani`
-   pr. rozpoznani rucne psaneho cisla na 32x32px
    -   rozdelim na segmenty 4x4px
    -   zjistim zabarveni jednotlivych segmentu (predzpracovani)
    -   z toho zjistim jakemu tvaru se podoba
-   zevseobecneni (generalizace)
    -   podle naucenych dat dokazu resit podobne situace (krizovatka v autoskole na obrazku -> dokazu je pak projizdet i jinde)

### Uceni

-   **model**
    -   reprezentuje urcitou realitu a je schopen o teto realite vypovidat a davat predpovedi
    -   model chovani osoby, pocasi, let letadla
-   **klasifikator**
    -   zarazuje data do trid na zaklade pozorovani (inputu prirazuje kategorii)
    -   priznak-druh nemoci, vlastnosti-zvire, vlastnosti-rostlina .., obrazek-pismeno, zvuk-slovo
-   **kontroler**
    -   na zaklade sensorickych vstupu generuje akcni (ridici) hodnoty
    -   ridici cast robota, regulator procesu (vyrobniho, chemickeho ..)
-   **agent**
    -   funkcni celek (program, kombinace sw/hw..), ktery se umi rozhodovat, hledat reseni, interagovat s okolim
    -   nyni ve vyzkumu: kooperace spotrebicu v chytre domacnosti, agenti vyhledavajici neobvykle aktivity v sitovych nebo ekonomickych prostredi atd.
-   co znamena ucit
    -   vstup (vstupni hodnoty) a vystup (pozadovane vystupni) - vzor
    -   vstup vstupuje do modelu/kontroleru a vystupuje vystup
    -   podle vystupu upravujeme vahy v modelu az se vystup blizi ke vzoru
    -   po nauceni dame vstupni hodnoty a neronova sit doda vystupni

### Biologie

-   biologicky neuron
    -   telo s vybezky (dendrity), jadro (nucleus) + axon (vybezek) se zakoncenimi
    -   dendrit - vstup (z jinych axonu) - spojeni = synapse
    -   synapse ma pametovou funkci (ovlivnuje uceni neuronu) - ma se za to, ze se zde uklada dlouhodoba pamet
    -   axon - vystup
-   funkce neuronu
    -   pracuji na pulsnim principu
    -   na dendrity prichazeji impulsy - vzruchy - tzv excitace
    -   to zpusobi zvyseni vnitrniho potencialu (napeti uvnitr neuronu)
        -   klidovy stav - -70mV
        -   pri vzruchu - napeti stoupa (silna +40mV, slaba jen lehce)
        -   excitace v radu ms (tj. **mozek pracuje ~200Hz - ale masivne paralelni**)
    -   synapse
        -   axon - uvolneni neurotransmiteru kdyz neuron "pali"
        -   dendrit - neurotransmitery ovlivnuji membrany tela neuronu a tim ovlivnuji navyseni/snizeni vnitrniho potencialu neuronu
        -   vykazuji plasticitu (meni se v case -> uceni nebo adaptace)

![](./neuron.png)

## Spojite umele neurony

-   znacime kruhem
-   vstupy (x) a vystupy (y) - propojenim vznika graf s ohodnocenymi hranami (w)
-   hrany reprezentuji
-   perceptrony - zpracovavaji vstup (soucin vstup a vahy) - od souctu vsech vstupu se odecte prah => dostavame vystup
-   rbf
-   vystupni funkce neuronu
    -   sigmoida, Gaussova krivka
    -   pro jeden vstup -> krivka, vice vstupu -> rovina, prostor apod
    -   2 vystupy - 0 nepatri do kategorie, 1 patri
    -   fce se muze upravit pokud potrebujeme jiny vystup

## Linearni a nelinearni separabailita

-   linearni - mnozina lze rozdelit jedinou primkou
-   nelinearni - vice primek
-   neseparovatelny problem - nelze rozdelit

## Otazky

### 1. Definujte umělou inteligenci.

Umělá inteligence je obor informatiky, který se zabývá tvorbou strojů vykazující známky inteligentního chování.

“Snažíme se naučit stroje používat lidský rozum”.

### 2. Popište Turingův test.

Za oponou je umístěn stroj a člověk. Jiný člověk (před oponou) s oběma komunikuje (verbálně). Pokud nerozezná stroj od člověka v závislosti na odpovědích, pak můžeme mluvit o inteligentním stroji.

### 3. Jaké oblasti zahrnuje výpočetní inteligence ?

-   Nerunové sítě
-   Fuzzy systémy
-   Evoluční výpočty

### 4. Charakterizujte proces učení (cíl, vstupy)

Učení je proces získávání zkušeností a dovedností. Cílem je, aby systém, který učíme, vracel požadované výstupy pro učené vzory z trénovací množiny a byl i obecný, tedy obstál pro data mimo trénovací množinu.

### 5. Co je zevšeobecnění a proč je klíčové.

Zevšeobecnění je schopnost správně reagovat na neučená (ale učeným datům podobná) data. \
 \
Je klíčové, protože nemá smysl naučit systém, který není schopen generalizovat. Pro neučená data bude dávat špatné výsledky.

### 6. Charakterizujte model, klasifikátor, kontrolér agent.

-   Model: Reprezentuje určitou realitu, je o ní schopen vypovídat a dávat předpovědi (model předpověďi počasí, model chování osoby).
-   Klasifikátor: Dokáže klasifikovat data do tříd (kategorií) na základě pozorování (příznaky - druh nemoci, vlastnosti - druh zvířete)
-   Kontrolér: Na základě (sensorických) vstupů generuje řídící výstupy. Může být kompozicí modelu a klasifikátorů. (regulátor výrobního procesu)
-   Agent: Funkční celek (program nebo kombinace SW+HW), který se UMÍ ROZHODOVAT, HLEDAT ŘEŠENÍ, INTERAGOVAT S OKOLÍM A KOOPEROVAT s ostatními agenty.

### 7. Uveďte příklad trénovací množiny.

Snad není potřeba uvádět.

### 8. Nakreslete a popište biologický neuron.

![](./questions/1.png)

### 9. Jakou funcki plní synapse ?

Plní funkci učení, vykazuje plasticitu a mění se v čase - mluvíme o procesu adaptace či učení. (Váhy)

### 10. Jaký je průběh vnitřního potenciálu biologického neuronu ? Uveďte napěťové i časové relace.

![](./questions/2.png)

### 11. [Zrušená] Popište funkci McCullochova-Pittsova neuronu.

### 12. [Zrušená] Jaký je princip hebovského učení ?

### 13. Uveďte vzorec pro výpočet výstupu neuronu typu perceptron.

![](./questions/3.png)

### 14.Uveďte vzorec pro výpočet výstupu neuronu typu RBF.

![](./questions/4.png)

### 15. Neuron má váhy w=(5,−2,3) a práh Θ=−4. Spočtěte výstup neuronu pro sigmoidní výstupní funkci s parametrem γ=1 a vstupy x=(1,1,0.5).

### 16. Spočtěte hodnotu prahu neuronu typu Perceptron s vahami w=(4,−2,2,2) tak, aby výstup neuronu byl na hraniční hodnotě 0.5 pro vstupní vektor x=(1,−1,−1,1).

### 17. RBF neuron má váhy (centroidy) w=(0.1,0.3) a σ=0.3. Spočtěte výstup neuronu pro x=(0.06,2.8).

### 18. Uveďte příklad separovatelné, lineárně separovatelné a neseparovatelné množiny.

### 19. Jaké jsou topologie neuronových sítí, uveďte příklady.

Podle typu šíření signálu:

-   Dopředná: Signál se šíří od vstupu k výstupu - každý neuron počítán jednou. Příklad: Kategorizace dle zadaných parametrů.
-   Rekurentní: Kromě dopředných spojení existují i zpětné vazby (signál se šíří i ke vstupu). Příklad: Předpověď časových řad, strojový překlad, generování textu.

### 20. Co ovlivňuje schopnosti neuronové sítě ?

Topologie:

    Typ:

    	Dopředná

    	Rekurentní

    Hustota propojení:

    	Úplné


    Částečné


    Počet vrstev


    Počet neuronů

Vlastnosti neuronu:

    Typ aktivační funkce

    Metrika (?)

### 21. Jak budete volit počet neuronů v neuronové síti ?

Počet neuronů ve vstupní a výstupní vrstvě je dán aplikací, tedy volíme pouze počty ve skrytých vrstvách:

    Experimentálně, nejsou pravidla.


    Méně je více.


    Začínáme s jednou skrytou vrstvou, více přidáváme, pokud jsme nespokojeni.


    Počet neuronů ve skryté vrstvě by měl odpovídat členitosti dat.

### 22. Nakreslete schéma klasifikátoru s neuronovou sítí. Jak se kódují výstupy ?

### 23. Nakreslete schéma predikce neuronovou sítí.

### 24. Do jaké třídy úloh patří učení neuronové sítě ?

Učení je optimalizační úloha.

Patří do třídy učení s učitelem (kromě SOM - ta se učí bez učitele)

### 25. Co je trénovací, testovací a validační množina ? Uveďte příklad a zásady výběru vzorů do těchto množin.

-   **Trénovací množina:** množina vzorů určená pro učení.
-   **Testovací množina:** množina vzorů určená pro testování sítě po naučení, jaká je přesnost sítě.
-   **Validační množina:** množina vzoru určená pro testování během učení a používá se pro zabránění přeučení.

Výběr do množin by měl proběhnout tak, aby byly vzory rozmanité, zastoupeno co nejvíce možných shluků

### 26. Co je vybavení, iterace a epocha ?

-   **Vybavení **je předložení jednoho vzoru neuronové síti a výpočet výstupu neuronové sítě.
-   **Iterace **je vybavení a provedení jednoho kroku učení
-   **Epocha **je provedení iterace pro všechny vzory z trénovací množiny

### 27. Co je uváznutí v lokálním minimu, přeučení a zevšeobecnění ?

Lokální minimum = Stav kdy se octneme v parazitním minimu na hyperploše chybovosti, jehož hodnota je vyšší než chyba očekávaného globálního minima.

Přeučení: nadměrná doba učení (Více epoch), které způsobí růst chyby pro netrénovaná data.

Zevšeobecnění: viz 5.

### 28. Jak je definována parciální derivace a jak se vypočítá ?

Počítá se jako normální derivace s tím rozdílem, že všechny proměnné, vyjma té podle které derivujeme, se považují za konstanty.

### 29. Spočtěte gradient funkce f(x,y,z)=3xz−6yx+2sin(zy) v bodě (x,y,z)=(3,−4,π8).

### 30. Co je cílem optimalizační úohy ? Uveďte příklady.

Hledáme množinu parametrů x1...xn tak, abychom minimalizovali cenovou funkci (chybu).

Příklad: mějme N různě těžkých předmětů, přičemž každý předmět má svoji cenu (v Kč) a objem. Nalezněte takový výběr předmětů, který se ještě vejde do batohu o daném objemu a zároveň má ze všech přípustných výběrů největší cenu. Cenovou funkcí je zde cena předmětů v batohu vetovaná přípustným objemem batohu.

### 31. Jak je definována chyba učení metodou back propagation ?

### 32. Co udává parciální derivace ∂E/∂w a jak užíváme pro modifikaci vah ?

Udává směr růstu / poklesu chyby podle váhy. Pokud vyjde kladná (chyba roste), tak váhu snížíme. Pokud vyjde záporná (chyba klesá), tak váhu zvýšíme.

### 33. Uveďte a vysvětlete základní vzorec pro výpočet delta na základě gradientu chybové funkce.

### 34. Uveďte delta pravidlo a popište.

viz 33.

### 35. Jak se vypočítá chyba připadající na neuron ve skryté vrstvě ?

### 36. Uveďte jednotlivé kroky algoritmu učení BackPropagation.

### 37. Jak ovlivňují parametry η a α učící proces.

η rychlost učení

určuje o kolik se upraví váha neuronu proto čím větší, tím rychlejší učení

avšak při příliš velké hodnotě je možné, že se síť nenaučí

α setrvačnost

    udává jak moc se má do změny vah započítat předchozí změna

    slouží k prevenci uváznutí v lokálním minimu, avšak pokud příliš velká, tak se síť nemusí naučit vůbec (setrvačností se vždy vyjede z globálního minima)

### 38. Nakreslete typický průběh chybové funkce v průběhu učení.

![](./questions/5.png)

### 39. Jak se řeší problém uváznutí v lokálním minimu ?

Přidáním setrvačnosti. Chování jako reálná kulička, která díky setrvačnosti vyskočí z lokálního minima.

### 40. Co je a jak se řeší problém přeučení neuronové sítě.

Nadměrná doba učení (epoch), která způsobí růst chyby pro netrénovaná data.

Řešení je méně epoch, použití validační množiny.

### 41. Jak je třeba upravit trénovací množinu, pokud je nevyvážená z hlediska počtu vzorů jednotlivých kategorií :

Upravíme tak, aby bylo stejné procentuentní zastoupení vzorů ze všech kategorií.

-   vynecháním obdobných vzorů v rámci kategorie
-   selektivní výběr vzorů

### 42. [DONE] Popište kroky K-means algoritmu.

K-means je algoritmus, který shromažďuje data do shluků, kde hodnota je počet shluků.

1. (Obvykle náhodně) vygenerujeme středy shluků (středy jsou vektory).
2. Přiřadíme vzory (objekty) ke shlukům dle vzdálenosti od středu (nejbližší shluk).
3. Spočítáme nový střed shluku z přidružených dat, tak aby šlo o těžiště vybraných vzorů.
4. Body 2 a 3 opakujeme, dokud se shluky neustálí (tj. dokud se středy posouvají o více, než je zadaná hodnota).

### 43. Pro níže uvedený obrázek proveďte jednu iteraci K-means algoritmu (přiřazení dat do shluků a výpočet nových středů shluků). Uvažujte dva shluky se středy C1=(0.25,0.5) a C2=(0.75,0.5). Pořadí souřadnic je (x1,x2).

### 44. Pro níže uvedený obrázek určete pravděpodobné shluky a určete středy těchto shluků.

### 45. Jak se používá K-means algoritmus pro učení sítí typu RBF ?

K-means nalezne střed shluku, střed RBF se umístí do středu nalezeného pomocí K-means.

### 46. Jak se spočte vítěz (BMU) v neuronové sítí typu SOM ?

Spočte se jako neuron s nejmenší odezvou pro daný vstup.

### 47. Máme dva neurony v SOM síti s vahami w1=(1,2,3,5) a w2=(2,0,1,1). Určete, který z neuronů bude vítězným neuronem pro vstupní vektor x=(4,1,1,4).

### 48. Jak se upraví váhy neuronů pro danou BMU ?

Alfa je zde rychlost učení (proměnná v čase klesá do nuly).

Funkce h(i,b) jak se budou měnit váhy neuronů blízkých vítěznému neuronu. Jinými slovy, jak se budou prototypy reprezentované neurony posouvat směrem k učenému vzoru. Nejvíce bude přitahován vítězný neuron, neméně neuron, který je od něho nejvíce vzdálen.

### 49. Co ovlivňuje funkce h(i,b) ?

Viz 48. aka říká jak moc se sousední nerony k BMU přitáhnou (případně i odtáhnou - mexickej klobouk)

![](./questions/6.png)

### 50. Jaké jsou topologie sousedství v sítích SOM ?

![](./questions/7.png)

### 51. Co popisuje U matice ?

Vizualizuje odezvu neuronové sítě typu SOM.

Barva odděluje shluky. (indikuje průměrnou vzdálenost vah daného neuronu od jeho sousedů)

![](./questions/8.png)

### 52. V čem se liší rekurentní neuronová síť od dopředné (topologicky i funkčně) ?

topologicky:

    dopředná: pouze vazby dopředu

    rekurentní: vazby dopředu a i vazby zpět

funkčně:

    dopředná: každý neuron se počítá jednou

    rekurentní: má vnitřní stav, neuron může být počítán i vícekrát

### 53. Nakreslete Elmanovu neuronovou síť.

![](./questions/9.png)

### 54. Jak se Elmanova neuronová síť učí ?

Rozthrnou se zpětné vazby, po roztřžení můžeme považovat za dopřednou síť a použijeme algoritmus backpropagation

### 55. Jak vypadá neuronová síť TDNN ?

![](./questions/10.png)

### 56. Jak se liší Fuzzy množina od standardní množiny ?

Klasická: prvek do množiny patří nebo nepatří

Fuzzy: prvek do množiny patří s mírou příslušnosti

### 57. Co je funkce příslušnosti ?

Je zobrazení množiny prvků do množiny měr příslušností. Označujeme ji μ(x).

![](./questions/11.png)

### 58. Uveďte funkce příslušnosti pro fuzzy množiny reprezentující různé hladiny kapaliny v nádrži.

![](./questions/12.png)

### 59. Je dána fuzzy množina A s mírou příslušnosti μA(x)=2−(x−20)2100 a fuzzy množinu B s mírou příslušnosti μB(x)=min(1,max(0,0.04(x−40))). Spočtěte míru příslušnosti pro výraz xisAandyisB, kde x=30 a y=60.

### 60. Je dána fuzzy množina B s mírou příslušnosti viz. obrázek. Spočtěte míru příslušnosti pro x=35 a x=65.

### 61. Uveďte příklad a strukturu fuzzy pravidla.

![](./questions/13.png)

### 62. Vysvětlete pojmy fuzzyfikace, inference a defuzzyfikace.

### 63. Jak se spočtě hodnota veličiny pro zadanou fuzzy množinu ?

### 64. Co je to genom, jedinec a populace ?

Genom: kódovaný předpis jak sestavit jedince

Jedinec: jeden zástupce

Populace: Skupina jedinců

![](./questions/14.png)

### 65. Co je selekce ?

Výběr jedinců do další populace

### 66. Jaké operace je možné provést nad genomemy ?

mutaci a křížení

### 67. Popište základní kroky genetického algoritmu.

1. Náhodně vygenerujeme počáteční populaci
2. Vyhodnotíme zdatnost (fitness funkce) u všech jedinců v populaci
3. Nejhorší jedince odstraníme
4. Populaci doplníme o další jedince (mutace, křížení, reprodukce)
5. Pokračujeme bodem 2 dokud není splněno kriterium
6. Vezmeme nejlepšího jedince jako výsledné řešení

### 68. Jaký je rozdíl mezi genotypem a fenotypem ?

Genotyp: Soubor všech genetických informací. V případě sítě popisuje topologii, jak neuronovou síť vytvořit a parametry.

Fenotyp: Soubor znaků chování jedince. Schopnosti sítě.

### 69. Jaké jsou výhody a nevýhody genetického algoritmu ?

-   Prověřuje několik řešení najednou
-   Na počátku je celá populace rozprostřena v prostoru řešení -> velká pravděpodobnost že některý padně do globálního minima
-   Výběr do nové populace se děje na základě hodnotící funkce bez žádných dalších požadavků -> vhodnot pro širokou škálu optimalizačních úloh
-   Mezi genotypem a fenotypem může být matematicky těžko popsatelný vztah
-   Vysoká paměťová a operační náročnost
-   Nutnost vyhodnocení zdatnosti vždy pro všechny jedince

### 70. Proveďte jednobodové a dvoubodové křížení genomu a) a b) a mutaci genomu b).

### a) 1011111011101111

### b) 1000000111000011

### 71. Pro konkrétní úlohu řešenou genetickým algoritmem je definována fitness funkce fitness=11+|n−4|, kde n je počet jedniček v genomu. Určete, který z následujících jedinců má největší šanci na přežití. Jací jedinci jsou očekávaným řešením optimalizační úlohy.

### a) 1011111011101111

### b) 1000000111000011

### c) 1010101010101010

### d) 1000001100000011

### 72. Co je diverzita popilace a proč ji musíme uměle různými metodami udržovat ?

Diverzita populace je různorodost. Pokud bychom ji neměli, tak se populace může soustředit do oblasti lokálního minima a nenalezneme globální.

Metody: nechat přežít i jedince s nižší zdatností.

### 73. Co je diferenciální evoluce ?

### 74. Co je genetické programování ?

Vytváření programu genetickým algoritmem.

### 75. Popište algorimtus PSO ?

### 76. Jak se spočte rychlost jedince ve stavovém prostoru úlohy ? Na jakých složkách závisí ?

### 77. Popište algoritmus ACO.

### 78. Na základě čeho se mravenec v každém kroku rozhoduje ?

### 79. Jako úlohu hraje feromon a jaké má vlasnosti ?

### 80. Částice v algoritmu PSO má souřadnice xt=(11,11). Jeho nejlepší lokální řešení je na souřadnici p=(6,6) a globální nejlepší řešení je g=(1,1). Jaká bude poloha částice v následujícím kroku, když náhodná složka pohybu byla rp=(0.3,0.2), rg=(0.8,0.5) a rychlost částice v předchozím okamžiku byla vt−1=(1.4,2.5). Parametry φp=0.1, φg=0.1 a ωp=0.9. Situaci nakreslete do grafu.

### 81. Popište konvoluční neuronovou síť

### 82. Popište hlubokou neuronovou síť
