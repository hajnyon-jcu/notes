# JCU Notes

Notes from class and tasks.

## Installation

```bash
npm ci
```

## Usage

```bash
npm start # run local dev
npm run build # builds website to dist folder
npm run build:production # builds website for production to dist folder
```
